#!/bin/sh


aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 560670647316.dkr.ecr.eu-central-1.amazonaws.com
docker build --rm -t lm-airflow .
docker tag lm-airflow:latest 560670647316.dkr.ecr.eu-central-1.amazonaws.com/lm/airflow:0.1.0
docker push 560670647316.dkr.ecr.eu-central-1.amazonaws.com/lm/airflow:0.1.0
aws ecs update-service --cluster lm-airflow --service worker --task-definition airflow-worker:6 --force-new-deployment
aws ecs update-service --cluster lm-airflow --service scheduler --task-definition airflow-scheduler:4 --force-new-deployment
aws ecs update-service --cluster lm-airflow --service webserver --task-definition airflow-webserver:8 --force-new-deployment
