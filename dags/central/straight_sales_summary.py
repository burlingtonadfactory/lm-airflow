import os
import datetime
import json
import boto3
import logging
import pandas as pd
import numpy as np
import janitor
from sshtunnel import SSHTunnelForwarder
from sqlalchemy import create_engine
from airflow import DAG
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.hooks.mysql_hook import MySqlHook
from airflow.providers.google.suite.hooks.sheets import GSheetsHook
from airflow.utils.dates import days_ago
from airflow import AirflowException


default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='CENTRAL_TABLES',
    default_args=default_args,
    catchup=False,
    schedule_interval='45 */3 * * *',
    dagrun_timeout=datetime.timedelta(minutes=15),
    tags=['GOOGLESHEET', 'CENTRAL'])

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

def get_mysql_conn():
    hook = PostgresHook(postgres_conn_id='blacklist_db')
    conn = hook.get_sqlalchemy_engine()

    return conn

def get_google_service():
    service = GSheetsHook(gcp_conn_id='google_sheet')
    return service

def read_query_mysql(mysql_query, mysql_host, mysql_port,mysql_user,mysql_pw,mysql_schema):

    private_key_file = open("/tmp/ssh-tunnel-mysql-rds.pem", "w")
    ssh_key_string = os.getenv('SSH_PRIVATE_KEY', None)
    key_file_write = private_key_file.write(ssh_key_string)
    private_key_file.close()
    tunnel = SSHTunnelForwarder(
    ('18.158.235.83'),
    ssh_username="ubuntu",
    ssh_pkey="/tmp/ssh-tunnel-mysql-rds.pem",
    remote_bind_address=(mysql_host, mysql_port))
    tunnel.skip_tunnel_checkup = False
    tunnel.start()
    print("****SSH Tunnel Established****")
    tunnel.check_tunnels()
    print(tunnel.tunnel_is_up, flush=True)
    # Run sample query in the database to validate connection
    engine = create_engine('mysql://'+ mysql_user  +':'+ mysql_pw+'@127.0.0.1:' + str(tunnel.local_bind_port) + '/'+ mysql_schema+'')
    print("Engine Created")
    try:
        df_out=pd.read_sql_query(mysql_query,con=engine)
        return df_out
    finally:
        engine.dispose()
        tunnel.stop()

def get_gsheet_dataframe(gsheet_id, range):
    
    service = get_google_service()
    service.get_conn()
    values = service.get_values(spreadsheet_id=gsheet_id, range_=range)
    df = pd.DataFrame.from_records(values[1:], columns=values[0]).clean_names(strip_underscores=True, remove_special=True, truncate_limit=40)
    df.replace(to_replace=[None,'None', 'n/a', 'nan', ''], value=np.nan, inplace=True)
    print('dataframe columns: ',df.columns.to_list())
    return df

def sync_sheet_to_redshift(gsheet_id, range, schema, table):
    df = get_gsheet_dataframe(gsheet_id, range)

    logging.info("Updating {} records into Redshift {}.{} via s3.....".format(str(len(df)),schema, table))
    sync_df_to_redshift(df=df, schema=schema, table=table)
    # redshift = get_redshift_conn()
    # df.to_sql(table, schema=schema, con=redshift, index=False, if_exists='replace')

def sync_df_to_redshift(df, schema, table):
    path_to_save_csv = 's3://linkmedia-datalake/redshift_upload_tmp/{schema}_{table}.csv'.format(schema=schema,table=table)
    logging.info("Updating {} records into S3 {} .....".format(str(len(df)),path_to_save_csv))
    df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["",""], regex=True, inplace=True)
    df.replace(r'^\s+$', np.nan, regex=True, inplace=True)
    df.replace('nan', np.nan, inplace=True)
    df.to_csv(path_to_save_csv, sep='\t', index=False, encoding='utf-8')
    copy_cmd = """
        DELETE FROM {schema}.{table} WHERE 1=1;
        COPY {schema}.{table}
        from 's3://linkmedia-datalake/redshift_upload_tmp/{schema}_{table}.csv'
        iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
        delimiter '\t'
        region 'eu-central-1'
        CSV
        IGNOREHEADER 1
        emptyasnull
        blanksasnull;
    """.format(schema=schema,table=table)
    engine_redshift = get_redshift_conn()
    with engine_redshift.connect() as con:
        con.execution_options(isolation_level="AUTOCOMMIT").execute(copy_cmd)
        con.close()
    logging.info('{schema}.{table} loaded successfully'.format(schema=schema,table=table))
    logging.info('count of transactions: {}'.format(len(df)))

def get_exit_sales_gsheet_dataframe(gsheet_id, range):
    
    service = get_google_service()
    service.get_conn()
    values = service.get_values(spreadsheet_id=gsheet_id, range_=range)
    df = pd.DataFrame.from_records(values[1:], columns=values[0]).clean_names(strip_underscores=True, remove_special=True, truncate_limit=40)
    df.replace(to_replace=[None,'None', 'n/a', 'nan', ''], value=np.nan, inplace=True)
    df =df[~(df['cake_id'].isna())]
    df.fillna(method='ffill', inplace=True)
    df.drop_duplicates(inplace=True)
    print('dataframe columns: ',df.columns.to_list())
    return df


def sync_exit_sales_to_redshift(gsheet_id, range, schema, table):
    df = get_exit_sales_gsheet_dataframe(gsheet_id, range)

    logging.info("Updating {} records into Redshift {}.{} via s3.....".format(str(len(df)),schema, table))
    sync_df_to_redshift(df=df, schema=schema, table=table)
    # redshift = get_redshift_conn()
    # df.to_sql(table, schema=schema, con=redshift, index=False, if_exists='replace')


def sync_sheet_to_datalake(gsheet_id, range, path, table):
    df = get_gsheet_dataframe(gsheet_id, range)

    path_to_save_csv = path
    logging.info("Updating {} records into S3 {} .....".format(str(len(df)),path, table))
    df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["",""], regex=True, inplace=True)
    df.replace(r'^\s+$', np.nan, regex=True, inplace=True)
    df.replace('nan', np.nan, inplace=True)
    df.to_csv(path_to_save_csv, sep=',', index=False, encoding='utf-8')

def sync_blacklist_to_redshift():

    table= 'blacklisted_customers'
    schema='central'
    qry = """SELECT * FROM `admindb-prod`.blacklistedCustomers"""
    df = read_query_mysql(mysql_query=qry, mysql_host='admin-prod-replica.cbhq1y6krhnt.eu-central-1.rds.amazonaws.com', mysql_port=3306,mysql_user='analytics',mysql_pw=os.getenv('BLACKLIST_DB_PW', None),mysql_schema='admindb-prod')
    #blacklist_db = get_mysql_conn()
    #df = pd.read_sql_query(qry, blacklist_db)

    logging.info("Updating {} records into Redshift {}.{} via S3...".format(str(len(df)), schema, table))
    sync_df_to_redshift(df=df, schema=schema, table=table)
    #redshift = get_redshift_conn()
    #df.to_sql(table, schema=schema, con=redshift, index=False, if_exists='replace')

ss_summary = PythonOperator(
    task_id='central_straight_sales_summary',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1LkhmR3ymTLyOXsx61qno--Zh-dp7aMPebfNzlFVrmuE', 
        'range': 'SS LOGIC!A:E', 
        'schema': 'central', 
        'table': 'straight_sales_summary'
    },
    dag=(dag),
)

acq_fees_by_contract = PythonOperator(
    task_id='central_acq_fees_by_contract',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1OdUI1fJoc6n0BWsZActyq14dGfZUYonQ7oJXETl5TaM', 
        'range': 'FeesbyContract!A:R',
        'schema': 'central', 
        'table': 'acq_fees_by_contract'
    },
    dag=(dag),
)

acq_fees_by_mids = PythonOperator(
    task_id='central_acq_fees_by_mids',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1OdUI1fJoc6n0BWsZActyq14dGfZUYonQ7oJXETl5TaM', 
        'range': 'Fees by MID!B:AJ',
        'schema': 'central',
        'table': 'acq_fees_by_mids'
    },
    dag=(dag),
)


rg_trx_teir_fees = PythonOperator(
    task_id='rg_trx_teir_fees',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1LkhmR3ymTLyOXsx61qno--Zh-dp7aMPebfNzlFVrmuE', 
        'range': 'RG FEES - LOGIC!A:J',
        'schema': 'rocketgate',
        'table': 'teir_trnsaction_fees'
    },
    dag=(dag),
)

vat_rates = PythonOperator(
    task_id='vat_rates',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1LkhmR3ymTLyOXsx61qno--Zh-dp7aMPebfNzlFVrmuE', 
        'range': 'VAT Rates!A:C',
        'schema': 'central',
        'table': 'vat_rates'
    },
    dag=(dag),
)

bin_exclusions = PythonOperator(
    task_id='bin_exclusions',
    python_callable=sync_sheet_to_datalake,
    op_kwargs={
        'gsheet_id': '1AJga8LDaG2Z080pzafxrA1X9-B7-15FsE8PQyfy3TxE', 
        'range': 'BINs status!A:P',
        'path': 's3://linkmedia-datalake/bindb/BIN_Exclusions/BIN Exclusions.xlsx',
        'table': 'vat_rates'
    },
    dag=(dag),
)


sticky_alerts_refunds_datalake = PythonOperator(
    task_id='sticky_alerts_refunds_datalake',
    python_callable=sync_sheet_to_datalake,
    op_kwargs={
        'gsheet_id': '1w1GCcjHfIuQlX6mg6KbUQl8zBs-IHkD-xdtbqMOUsHE',
        'range': '2021!A:E',
        'path': 's3://linkmedia-datalake/chargeback_alerts/sticky_cb_alerts_manual_refunds/sticky_alerts_manual_refunds.csv',
        'table': 'sticky_alerts_manual_refunds'
    },
    dag=(dag),
)


sticky_alerts_refunds_redshift = PythonOperator(
    task_id='sticky_alerts_refunds_redshift',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1w1GCcjHfIuQlX6mg6KbUQl8zBs-IHkD-xdtbqMOUsHE',
        'range': '2021!A:E',
        'schema': 'central',
        'table': 'sticky_alerts_manual_refunds'
    },
    dag=(dag),
)

blacklist_to_redshift = PythonOperator(
    task_id='sync_blacklist_to_redshift',
    python_callable=sync_blacklist_to_redshift,
    dag=(dag),
)


exit_sales_to_redshift = PythonOperator(
    task_id='exit_sales_to_redshift',
    python_callable=sync_exit_sales_to_redshift,
    op_kwargs={
        'gsheet_id': '1QABrwDdb9XH_NRb-VmdclDWJ-lBWfGv0qLD28eN9muI',
        'range': 'TrackingLinks!A:E',
        'schema': 'central',
        'table': 'exit_sales_campaigns'
    },
    dag=(dag),
)

ecom_ss_unit_cost = PythonOperator(
    task_id='ecom_ss_unit_cost',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1HEpPN0yveC85V14Udp65a6ysGJOIW67VUOeK2-xA1_s', 
        'range': 'Unit Cost!A:D', 
        'schema': 'central', 
        'table': 'ecom_ss_unit_cost'
    },
    dag=(dag),
)


ecom_ss_shipping_cost = PythonOperator(
    task_id='ecom_ss_shipping_cost',
    python_callable=sync_sheet_to_redshift,
    op_kwargs={
        'gsheet_id': '1HEpPN0yveC85V14Udp65a6ysGJOIW67VUOeK2-xA1_s', 
        'range': 'Shipping Cost!A:E', 
        'schema': 'central', 
        'table': 'ecom_ss_shipping_cost'
    },
    dag=(dag),
)
