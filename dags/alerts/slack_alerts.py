import os
import ast
import json
import requests
import pandas as pd
from airflow import DAG
#from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from datetime import datetime, timedelta

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

def alert_mid_declines():
    # query="""
    # select mid, count(declined_leads) declines_count  from rocketgate.rg_overview
    # where datediff(minute , order_timestamp, getdate()) < 15
    # and is_cascade != 1
    # group by 1
    # having declines_count > 15
    # order by 2 desc;
    # """
    query ="""
    select jira_mids.mid_name as mid, count(CASE WHEN reason_desc != 'Success' and ttype_name = 'SALE' and billing_type = 'T'
                        THEN rgt.tr_id
                        ELSE NULL END) declines_count  from rocketgate.transactions rgt
    left join
        (
            select distinct concat(concat(d.name, ' : '), mids.acquiringbank) as mid_name,
                split_part(mids.name,' : ',1)                        as company_name,
                mids.jiraid mid_id,
                status,
                mids.acquiringbank                                as bank,
                mids.rgaccountsequence                            as rg_id,
                labels,
                mcc,
                mids.monthlytransactionscap,
                c.country as company_country
            from jira.mids as mids
            left join jira.domains d
                on mids.jiradomainid = d.jiraid
            left join jira.companies c
                on c.jiraid = d.jiracompanyid
            where mids.rgaccountsequence is not null and mids.project not in ('Social','Loans','HOI')
        ) jira_mids
        ON ((jira_mids.rg_id = rgt.merchant_account) and
        (((jira_mids.labels not like '%%STRAIGHTSALES%%' or jira_mids.labels is null) and merch_id = '1599571287')
        OR (jira_mids.labels like '%%STRAIGHTSALES%%' and merch_id = '1601058492')))
        where datediff(minute , tr_date, getdate()) < 15
        and CASE
            WHEN (tr_id_cascaded<>0 and tr_3d_eci is NULL)
            OR (json_extract_path_text(tui_udf02, 'c3d', true) in ('1','0'))
            THEN 1
            ELSE 0 END != 1
        and merch_id in ('1599571287','1601058492')
    -- test transactions
    and cust_id_ext not in ('rgtest', 'GoLiveDec29Viri2')
    and cust_id_ext not like 'RG%%'
    and upper(cust_id_ext) not like '%%3DS%%'
    and upper(cust_id_ext) not like '%%TEST%%'
    and upper(rgt.email) not like '%%EXAMPLE%%'
    and upper(rgt.email) not like '%%ROCKETGATE%%'
    and upper(rgt.email) not like '%%LINKMEDIA%%'
    and rgt.tui_bill_country !='AE'
    group by 1
    having declines_count > 15
    order by 2 desc;
    """

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    #having declines_count > 15
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        decline_mids = ''
        for mid, declines in zip(df.mid.values, df.declines_count.values):
            decline_mids +=  '- {} has {} declines\\n'.format(mid,declines)

        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S019T18VCCB> The following mids in Rocketgate have high declines in the last 15 mins"}},{"type": "section","text": {"type": "mrkdwn","text": "'+decline_mids+'"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)


def alert_rg_memberships_failure():
    query="""select DATEDIFF(minute, max(updated_at), GETDATE()) mins, max(updated_at) last_update from rocketgate.memberships_view;"""

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    #having date_diff > 60
    df = pd.read_sql(query, con=red_conn)
    if int(df.mins[0]) > 60:
        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S01PPCLL18X> Rocketgate production memberships has not been updated for the past hour"}},{"type": "section","text": {"type": "mrkdwn","text": "the last update was at: '+df.last_update[0].strftime('%Y-%m-%d %H:%I:%S')+' GMT"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)

        red_conn.execution_options(isolation_level="AUTOCOMMIT").execute("REFRESH MATERIALIZED VIEW rocketgate.memberships_view;")
        red_conn.close()


def alert_invoice_id_duplicates():
    # query="""
    # select inv_id_ext from
    # (
    # select inv_id_ext, cust_id_ext from rocketgate.transactions
    # where ttype_name in ('SALE','AUTH_ONLY')
    # and billing_type='T'
    # and reason_desc='Success'
    # and datediff(minute , tr_date, getdate()) < 30
    # group by 1,2
    # having count(distinct tr_id)>1
    # )
    # ;
    # """

    query="""
    select inv_id_ext from
    (
    select inv_id_ext, cust_id_ext from rocketgate.transactions
    where ttype_name in ('SALE','AUTH_ONLY')
    and billing_type='T'
    and reason_desc='Success'
    and datediff(minute , tr_date, getdate()) < 30
    group by 1,2
    having count(distinct tr_id)>1
    )
    union
    select inv_id_ext from rocketgate.transactions
    where ttype_name in ('SALE','AUTH_ONLY')
    and billing_type='T'
    and datediff(minute , tr_date, getdate()) < 30
    group by 1
    having count(distinct cust_id_ext)>1
    order by 1 desc
    ;
    """

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        duplicate_invoices = ''
        for inv_id_ext in df.inv_id_ext.values:
            duplicate_invoices +=  '- {} \\n'.format(inv_id_ext)

        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S01Q13GU7NZ> The following invoice_ids in Rocketgate have duplicates in the last 30 mins"}},{"type": "section","text": {"type": "mrkdwn","text": "'+duplicate_invoices+'"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)


def alert_non_converting_bins():
    query="""
    select bank_bin,
    COUNT(DISTINCT CASE WHEN transaction_category='ORDER' AND is_duplicate_subscription=0 AND trial_completed=1 THEN APPROVED_LEADS END) AS APPROVED_LEAD_COUNT
    from rocketgate.rg_overview_table
    LEFT JOIN athenabindb.bin_exclusion
    ON bin_exclusion.BIN = bank_bin
    left join athenabindb.bin_db
    ON BIN_DB.BIN= bank_bin
    where
    upper(BIN_DB.card_type_2) not like '%%PREPAID%%' AND
    action is null
    GROUP BY 1
    HAVING
    APPROVED_LEAD_COUNT>=50 AND
    COUNT(DISTINCT CASE WHEN transaction_category='ORDER' AND is_duplicate_subscription=0 THEN have_first_rebills_leads END) =0
    ORDER BY 2 DESC;
    """

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        non_converting_bins = ''
        for bank_bin, approved_lead_count in zip(df.bank_bin.values, df.approved_lead_count.values):
            non_converting_bins +=  '- {}  with {} approved leads \\n'.format(bank_bin,approved_lead_count)

        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S019T18VCCB> The following BINs have no conversions in Rocketgate"}},{"type": "section","text": {"type": "mrkdwn","text": "'+non_converting_bins+'"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)


def alert_poorly_converting_bins():
    query="""
    SELECT BANK_BIN, APPROVED_LEAD_COUNT, ROUND((CAST(CONVERTED_LEAD_COUNT AS DECIMAL)/CAST(APPROVED_LEAD_COUNT AS DECIMAL))*100.00,2) AS FIRST_REBILL_PERCENT
    FROM (
    select bank_bin,
        COUNT(DISTINCT CASE WHEN transaction_category='ORDER' AND is_duplicate_subscription=0 AND trial_completed=1 THEN APPROVED_LEADS END) AS APPROVED_LEAD_COUNT,
        COUNT(DISTINCT CASE WHEN transaction_category='ORDER' AND is_duplicate_subscription=0 THEN have_first_rebills_leads END) AS CONVERTED_LEAD_COUNT
        from rocketgate.rg_overview_table
        LEFT JOIN athenabindb.bin_exclusion
        ON bin_exclusion.BIN = bank_bin
        left join athenabindb.bin_db
        ON BIN_DB.BIN= bank_bin
        where
        upper(BIN_DB.card_type_2) not like '%%PREPAID%%' AND
        action is null
        GROUP BY 1
        HAVING
        APPROVED_LEAD_COUNT>=100 AND
        CONVERTED_LEAD_COUNT>0)
    WHERE FIRST_REBILL_PERCENT <20.00
    ORDER BY 3 ASC;
    """

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        poorly_converting_bins = ''
        for bank_bin, approved_lead_count,first_rebill_percent in zip(df.bank_bin.values, df.approved_lead_count.values, df.first_rebill_percent.values):
            poorly_converting_bins +=  '- {}  with {} % of {} approved leads \\n'.format(bank_bin,first_rebill_percent,approved_lead_count)

        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S019T18VCCB> The following BINs have poor 1st Rebill Percent in Rocketgate"}},{"type": "section","text": {"type": "mrkdwn","text": "'+poorly_converting_bins+'"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)


def alert_cc_switch():
    query="""
    select distinct rgt1.inv_id_ext as invoice from
    (
    select inv_id_ext, cust_id_ext, bankbin, tr_pay_num_l4
    from rocketgate.transactions
    where billing_type='T' AND reason_desc='Success'
    and ttype_name not in ('CREDIT','VOID')
    and tr_date >='2021-08-12'
    ) rgt1
    inner join
    (
    select inv_id_ext, cust_id_ext, bankbin, tr_pay_num_l4
    from rocketgate.transactions
    where billing_type<>'T'
    and ttype_name not in ('CREDIT','VOID')
    and tr_date >='2021-08-12'
    ) rgt2
    on rgt1.inv_id_ext = rgt2.inv_id_ext and rgt1.cust_id_ext=rgt2.cust_id_ext and (rgt1.tr_pay_num_l4 <> rgt2.tr_pay_num_l4);
    """

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        cc_switched_invoices = ''
        for invoice_id in df.invoice.values:
            cc_switched_invoices +=  '- {} \\n'.format(invoice_id)

        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "<!subteam^S01Q13GU7NZ> The following subscriptions switched their card in Rocketgate"}},{"type": "section","text": {"type": "mrkdwn","text": "'+cc_switched_invoices+'"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)


with DAG(
    'SLACK_ALERTS',
    default_args=default_args,
    schedule_interval='*/10 * * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=2),
    tags=['ALERTS']
) as dag:
    post_daily_forecast = PythonOperator(
        task_id='trigger_mid_declines_alerts',
        python_callable=alert_mid_declines
    )

    # rg_memberships_failure = PythonOperator(
    #     task_id='trigger_alert_rg_memberships_failure',
    #     python_callable=alert_rg_memberships_failure
    # )

    rg_alert_inv_id_duplicates = PythonOperator(
        task_id='trigger_alert_invoice_id_duplicates',
        python_callable=alert_invoice_id_duplicates
    )


with DAG(
    'DAILY_SLACK_ALERTS',
    default_args=default_args,
    schedule_interval='30 7 * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=5),
    tags=['ALERTS']
) as daily_dag:
    rg_alert_non_converting_bins = PythonOperator(
        task_id='trigger_alert_non_converting_bins',
        python_callable=alert_non_converting_bins
    )

    rg_alert_poorly_converting_bins = PythonOperator(
        task_id='trigger_alert_poorly_converting_bins',
        python_callable=alert_poorly_converting_bins
    )

    rg_alert_cc_switch = PythonOperator(
        task_id='trigger_alert_cc_switch',
        python_callable=alert_cc_switch
    )
