import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta

TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')
YESTERDAY = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='bepayments')
    conn = hook.get_sqlalchemy_engine()

    return conn

def export_bepayments():

    sql_text ='''
    Unload ( '
    select id as payment_id , 
    added_time  order_time, 
    updated_time  order_update_time ,
    initial_total_amount  order_intial_amount, 
    parent_id order_parent_id , 
    payment_type order_payment_type , 
    request_account_id order_account_id , 
    request_affiliate_affiliate_id   affiliate_id , 
    request_affiliate_sub_affiliate_id  sub_affiliate_id  , 
    request_cc_data_card_number_first_digits  cc_first_6 , 
    request_cc_data_card_number_last_digits   cc_last_4 , 
    request_cc_data_cardholder_name  cc_holder_name , 
    request_cc_data_expiry_month  cc_expiry_month , 
    request_cc_data_expiry_year   cc_expiry_year , 
    request_currency              order_currency , 
    request_customer_address_city  order_city , 
    request_customer_address_country order_country , 
    request_customer_address_postcode order_postcode , 
    request_customer_address_state    order_state , 
    request_customer_company       customer_company , 
    request_customer_email        customer_email , 
    request_customer_first_name   customer_first_name , 
    request_customer_last_name   customer_last_name , 
    request_customer_phone        customer_phone ,   
    -- request_d3secure_callback_url
    -- request_gateway_account_id
    request_ip_address   order_ip_address , 
    request_merchant_id   order_merchant_id , 
    request_merchant_info_name   order_merchant_info_name  , -- domain
    request_order_reference    order_reference  , 
    payment_gateway , 
    request_payment_method payment_method , 
    test_transaction is_test , 
    request_threatmetrix_session_id threatmetrix_session_id ,  
    case when status = \\'FAILED\\'  Then \\'Decline\\'
    when status = \\'DONE\\' Then \\'Approved\\'
    when status = \\'FULLY_REFUNDED\\' then \\'Refund\\'
    else \\'Unidentified\\' end as order_status , 
    subscription_id   , 
    item_code  sku  , 
    item_description , 
    item_gw_item_id  , 
    item_name ,   -- may be used for Straight Sale 
    item_quantity  , 
    item_refunded_amount order_refunded_amount, 
    item_unit_price  item_unit_price  , 
    case when ( subscription_id is not null and lower(payment_Type)=\\'installment\\' and  
    parent_Id is not null ) Then \\'Rebill\\'
    when  ( subscription_id is null and  parent_Id is null ) Then \\'Lead\\'
    else \\'Not_Classified_yet\\'
    end as IS_LEAD_REBILL ,
    o.error_response_code , 
    o.error_response_description , 
    \\'order\\' as transaction_type , 
    f_convert_currency(request_currency ,initial_total_amount ) as initial_total_amount_euro ,
    f_convert_currency( request_currency,item_refunded_amount ) as refund_total_amount_euro,
    substring(date(added_time) , 1, 4) as year  ,
    substring(date(added_time) , 6, 2) as month   ,
    substring(date(added_time) , 9, 2) as day   
    from payment p
    left join 
    (select payment_id  , error_response_code , error_response_description from operation) o on ( p.id = o.payment_id)
    where 
    p.test_transaction = false  
    and date(p.added_time) > date(\\'2021-01-01\\') 
    ')
    to 's3://bepayments/payments' 
    access_key_id 'AKIAYFCUTEQKIT44RVV4'
    secret_access_key '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'
    maxfilesize 16 mb
    FORMAT AS PARQUET
    PARTITION BY (year, month, day)
    allowoverwrite;
    '''
    
    engine_redshift = get_redshift_conn()
    with engine_redshift.connect() as con:
        con.execute(sql_text)
        count = con.rowcount
        print(count)
        con.close()


dag = DAG(
    dag_id='BE_PAYMENTS',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 3,12,18 * * *',
    dagrun_timeout=timedelta(minutes=60),
    tags=['ROCKETGATE'])

run_export_bepaymnet = PythonOperator(
    task_id='export_bepaymnet',
    python_callable=export_bepayments,
    dag=(dag),
)