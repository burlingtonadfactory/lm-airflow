import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta

# RG_MERCH1 = Variable.get("RG_MERCH1", deserialize_json=True, default_var=None)
RG_MERCHANTS = json.loads(Variable.get("RG_MERCHANTS"))
RG_HISTORICAL_START = Variable.get("RG_HISTORICAL_START")
RG_HISTORICAL_END = Variable.get("RG_HISTORICAL_END")
TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')
YESTERDAY = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
# RG_MERCH1 = Variable.get("RG_MERCH1", default_var=None)

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def test_redshift_conn():
    IP = "analytics-redshift.ckfhveowzenp.eu-central-1.redshift.amazonaws.com"
    PORT=5439
    a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    location = (IP, PORT)
    result_of_check = a_socket.connect_ex(location)
    if result_of_check == 0:
        print("Port is open")
    else:
        print("Port is not open")
    a_socket.close()

current_dag = DAG(
    dag_id='RG_TEST_CONNECTION',
    default_args=default_args,
    catchup=False,
    schedule_interval=None,
    dagrun_timeout=timedelta(minutes=60),
    tags=['AIRFLOW'])

run_test_redshift_conn = PythonOperator(
    task_id='run_test_redshift_conn',
    python_callable=test_redshift_conn,
    dag=(current_dag),
)
