import datetime
import json
import boto3
import logging
from pyathena import connect
import pandas as pd
from jira import JIRA
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.http_hook import HttpHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.providers.google.suite.hooks.sheets import GSheetsHook
from airflow.utils.dates import days_ago
from airflow import AirflowException


default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='GOOGLE_SHEET',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 3,5,7,12 * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['GSHEET'])

def get_google_service():
    service = GSheetsHook(gcp_conn_id='google_sheet')
    return service

def get_gsheet_dataframe(gsheet_id, range):
    
    service = get_google_service()
    service.get_conn()
    values = service.get_values(spreadsheet_id=gsheet_id, range_=range)
    df = pd.DataFrame.from_records(values[1:], columns=values[0])
    return df

def rg_straight_sales_summary(gsheet_id, range, schema, table):
    df = get_gsheet_dataframe(gsheet_id, range)

SAMPLE_SPREADSHEET_ID = '1LkhmR3ymTLyOXsx61qno--Zh-dp7aMPebfNzlFVrmuE'
SAMPLE_RANGE_NAME = 'SS LOGIC!A:E'

test_sheet = PythonOperator(
    task_id='test_sheet',
    python_callable=get_gsheet_dataframe,
    op_kwargs={'gsheet_id': SAMPLE_SPREADSHEET_ID, 'range': SAMPLE_RANGE_NAME},
    dag=(dag),
)