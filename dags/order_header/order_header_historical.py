import datetime
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException
from airflow.models import Variable
import pandas as pd
import logging
# local packages
from linkmedia.omar import order_controller
from linkmedia.order_header import order_header_stage_1_controller as oh_1_c
from linkmedia.order_header import order_header_stage_2_controller as oh_2_c

START_DATE = Variable.get("START_DATE")
DAYS_AHEAD = Variable.get("DAYS_AHEAD")

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='ORDER_HEADER_HISTORICAL',
    default_args=default_args,
    catchup=False,
    schedule_interval='@once',
    dagrun_timeout=datetime.timedelta(minutes=360),
    tags=['ORDER_HEADER'])


def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()
    return conn


def get_order_history(run_date, number_of_days_ahead, file_formats):
    # start date and number of days to go back
    
    start_date = run_date

    # create an instance of order header stage 1 controller
    run_order_header_1 = oh_1_c.order_header()
    # create an instance of order header stage 2 controller
    run_order_header_2 = oh_2_c.order_header()

    # loop for required days and generate order_header stage 1
    try:
        for i in list(range(0, number_of_days_ahead)):
            run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                        pd.DateOffset(i)).strftime('%Y-%m-%d')
            #run_order_header_1.generate_order_header(run_date)
            run_order_header_2.generate_order_header(run_date, file_formats)
            print(str(run_date))
        
    except Exception as e:
        raise AirflowException(e)

def load_order_header_redshift(run_date, number_of_days_ahead):
    # start date and number of days to go back

    start_date = run_date

    # loop for required days and generate order_header stage 1
    try:
        for i in list(range(0, number_of_days_ahead)):
            run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                        pd.DateOffset(i)).strftime('%Y-%m-%d')
            s3_date = run_date.replace('-','/')
            logging.info('Loading data for {}'.format(run_date))
            copy_cmd = """
            CREATE TEMP TABLE order_header_tmp (acquisition_date timestamp, affiliate_id integer, amount_refunded_to_date numeric(20,2), ancestor_id numeric(20,0), billing_city varchar(200), billing_country varchar(10), billing_cycle integer, billing_first_name varchar(200), billing_last_name varchar(200), billing_postcode varchar(7000), billing_state varchar(50), billing_street_address varchar(500), campaign_id integer, c1 numeric (20,0), c2 varchar(500), c3 numeric (20,0), cc_expires integer, cc_first_6 integer, cc_last_4 integer, cc_type varchar(20), chargeback_date timestamp, current_rebill_discount_percent numeric(6,2), customer_id numeric(20,0), customers_telephone varchar(100), decline_reason varchar(500), decline_salvage_discount_percent numeric(6,2), email_address varchar(100), order_gateway_id integer, hold_date timestamp, ip_address varchar(40), is_recurring integer, is_test_cc integer, is_void integer, main_product_id numeric(20,0), main_product_quantity numeric(10,2), on_hold integer, on_hold_by varchar(50), order_confirmed varchar(20), time_stamp timestamp, order_id numeric(20,0), order_status integer, order_total numeric(20,2), parent_id numeric(20,0), rebill_discount_percent numeric(6,2), recurring_date timestamp, refund_amount numeric(20,2), refund_date timestamp, retry_date timestamp, shipping_date timestamp, sub_affiliate varchar(100), transaction_id varchar(100), upsell_product_id numeric(20,0), upsell_product_quantity numeric(10,2), void_amount numeric(20,2), void_date timestamp, is_create_order integer, order_confirmed_date timestamp, system_notes_last_date timestamp, reserve_release_date timestamp, order_date integer, is_test_order integer, gateway_first_live_date timestamp, gateway_descriptor varchar(200), order_currency varchar(5), is_gateway_3ds integer, brand_id integer, bank_id integer, bank_name varchar(50), company_name varchar(50), vat_number varchar(50), vat_date timestamp, order_total_eur numeric(20,2), refund_amount_eur numeric(20,2), main_product_name varchar(100), main_product_category integer, trial_days numeric(20,0), main_product_subscription_group_id integer, is_lead_rebill_donation varchar(20), is_cascade integer, cascade_parent_order_id numeric(20,0), cascade_first_parent_order_id numeric(20,0), cascade_attempt integer, is_cross_sale integer, subscription_id integer, cross_sale_parent varchar(50), cross_sale_parent_order_id varchar(50), migration_parent numeric(20,0), migration_parent_order_id numeric(20,0), is_migration integer, is_straight_sale integer, is_salvage_sale integer, is_trial_over integer, cost_per_lead numeric(20,2), cost_per_lead_currency varchar(5), everflow_date timestamp, everflow_currency varchar(20), everflow_cost_per_lead numeric(20,2), is_cpa_missing integer, cost_per_lead_eur numeric(20,2), hold_date_derived varchar(19), affiliate_name varchar(50), billing_continent_name varchar(10), gateway_reserve_percent numeric(6,2), reserve_taken_amount_eur numeric(20,0), order_cc_key varchar(100), is_reserve_released integer, cust_subscription_key varchar(100), cust_lead_u_key varchar(100), vat_percent numeric(6,2), vat_amount_eur numeric(20,2), has_first_rebill integer, counter integer, gateway_processing_percent numeric(6,2), gateway_transaction_fee numeric(20,2), gateway_charge_back_fee numeric(20,2), refund_fee numeric(20,2));
            COPY order_header_tmp
            from 's3://linkmedia-datalake/order_header_datawarehouse/{s3_date}/'
            iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
            gzip
            delimiter '\t'
            NULL 'nan'
            region 'eu-central-1'
            CSV
            IGNOREHEADER 1
            emptyasnull
            blanksasnull;

            delete from central.order_header where date(time_stamp)=date('{run_date}');

            insert into central.order_header (select * from order_header_tmp);
            """.format(run_date=run_date, s3_date= s3_date)
    
            engine_redshift = get_redshift_conn()
            with engine_redshift.connect() as con:
                con.execute(copy_cmd)
                con.close()
            logging.info('{} data loaded successfully'.format(run_date))
            print(str(run_date))

    except Exception as e:
        raise AirflowException(e)

start = DummyOperator(task_id='start', dag=dag)
end = DummyOperator(task_id='end', dag=dag)

get_order_history = PythonOperator(
    task_id='get_order_history',
    python_callable=get_order_history,
    op_kwargs={'run_date': START_DATE, 'number_of_days_ahead': int(DAYS_AHEAD), 'file_formats' : ['PARQUET','TSV']},
    dag=(dag),
)

load_order_header_redshift = PythonOperator(
    task_id='load_order_header_redshift',
    python_callable=load_order_header_redshift,
    op_kwargs={'run_date': START_DATE, 'number_of_days_ahead': int(DAYS_AHEAD)},
    dag=(dag),
)

start >> get_order_history >> load_order_header_redshift >> end
    