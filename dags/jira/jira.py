import datetime
import json
import boto3
import logging
from pyathena import connect
import pandas as pd
import numpy as np
from jira import JIRA
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.http_hook import HttpHook
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException


default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='JIRA_BACKEND',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 */1 * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['JIRA'])

def remove_none(d):
    for key, value in dict(d).items():
        if value is None:
            del d[key]

def get_jira_client():
    hook = HttpHook(http_conn_id='jira_http')
    conn = hook.get_connection(hook.http_conn_id)
    url = conn.host
    creds = (conn.login, conn.password)
    jira_client = JIRA(url, basic_auth=creds)

    return jira_client

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

def load_to_redshift(df, path_to_save_csv, schema_name, table_name, table_schema):
    #path_to_save_csv = 's3://linkmedia-datalake/jira/csv/mids/mids.tsv'
    df.replace(to_replace=[r"\\t|\\n|\\r", "\t|\n|\r"], value=["",""], regex=True, inplace=True)
    df.replace(r'^\s+$', np.nan, regex=True, inplace=True)
    df.replace('nan', np.nan, inplace=True)
    df.to_csv(path_to_save_csv, sep='\t', index=False, encoding='utf-8')
    copy_cmd = """
        CREATE TEMP TABLE {table_name}_tmp {table_schema};

        COPY {table_name}_tmp
        from '{s3_path}'
        iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
        delimiter '\t'
        region 'eu-central-1'
        CSV
        IGNOREHEADER 1
        emptyasnull
        blanksasnull;

        CREATE TABLE IF NOT EXISTS {schema_name}.{table_name} {table_schema};

        delete from {schema_name}.{table_name} where 1=1;

        insert into {schema_name}.{table_name} (select * from {table_name}_tmp);
    """.format(s3_path=path_to_save_csv, schema_name=schema_name, table_name=table_name, table_schema=table_schema)
    engine_redshift = get_redshift_conn()
    with engine_redshift.connect() as con:
        con.execute(copy_cmd)
        con.close()
    print('saved transaction data into {schema_name}.{table_name}'.format(schema_name=schema_name, table_name=table_name))

def sync_mids_to_backend():
    jira = get_jira_client()
    #Pass only Live MIDs with MonthlyTransactionsCap <300
    jql_str = 'project = UN AND issuetype = MID AND status = Live AND cf[11471] < 300 ORDER BY cf[11362] DESC, summary ASC, cf[11363] DESC, cf[11333] DESC, created DESC'
    start_idx = 0
    block_size = 100
    block_num = 0
    mids = {}
    mids_dw = []
    issue_count = 0
    while True:
        start_idx=block_num*block_size
        issues = jira.search_issues(jql_str, start_idx, block_size, json_result=True)
        block_num += 1
        if len(issues['issues']) == 0:
            break

        for issue in issues['issues']:
            domainId = None
            has3ds = False
            acquiringBank = None
            billingDescriptorPart2 = None

            if issue['fields']['issuelinks']:
                for li in issue['fields']['issuelinks']:
                    if li['type']['outward'].lower()=='domain for mid':
                        domainId = li['outwardIssue']['key']

            if issue['fields'].get('customfield_11437', None):
                if issue['fields']['customfield_11437'].get('value', None):
                    if issue['fields']['customfield_11437']['value']!='Non 3DS':
                        has3ds = True

            if issue['fields'].get('customfield_11367', None):
                acquiringBank = issue['fields']['customfield_11367'].get('value', None)

            if issue['fields'].get('customfield_11466', None):
                billingDescriptorPart2 = issue['fields']['customfield_11466'].get('value', None)

            if issue['fields'].get('customfield_11351', None):
                project = issue['fields']['customfield_11351'].get('value', None)

            mid = {
                    "jiraId":issue["key"],
                    "name": issue['fields']["summary"],
                    "status": issue['fields']['status']['name'],
                    "labels": ','.join(issue['fields']['labels']) if issue['fields']['labels'] else '',
                    "billingDescriptorPart1": issue['fields']["customfield_11370"],
                    "billingDescriptorPart2": billingDescriptorPart2,
                    "acquiringBank": acquiringBank,
                    "currencies": [v['value'] for v in issue['fields']['customfield_11364']] if issue['fields']['customfield_11364'] else [],
                    "excludedGeos": [v['value'] for v in issue['fields']['customfield_11498']] if issue['fields']['customfield_11498'] else [],
                    "verticals": [{"jiraId":v['id'], "name":v['value']} for v in issue['fields']['customfield_11518']] if issue['fields']['customfield_11518'] else [],
                    "mcc": int(issue['fields']["customfield_11431"]) if issue['fields']["customfield_11431"] else None,
                    "has3ds": has3ds,
                    "contract_id": issue['fields']['customfield_11464'],
                    "rgAccountSequence": issue['fields']["customfield_11467"],
                    "emailAddress": issue['fields']['customfield_11336'],
                    "freshdeskId": issue['fields']['customfield_11404'],
                    "monthlyTransactionsCap": issue['fields']["customfield_11471"],
                    "project": project,
                    "jiraDomainId": domainId}

            remove_none(mid)
            mids[issue['key']] = mid
        issue_count = len(mids.keys())

    logging.info("Dumping {} mids records into {} .....".format(issue_count,'s3://linkmedia-datalake/rg-jira/jiraMids.json'))
    s3 = boto3.resource('s3')
    s3object = s3.Object('linkmedia-datalake', 'rg-jira/jiraMids.json')

    s3object.put(
        Body=(bytes(json.dumps({"mids":mids}).encode('UTF-8'))), ContentType='application/json'
    )


def sync_mids_to_dw():
    jira = get_jira_client()
    jql_str = 'project = UN AND issuetype = MID ORDER BY cf[11362] DESC, summary ASC, cf[11363] DESC, cf[11333] DESC, created DESC'
    start_idx = 0
    block_size = 100
    block_num = 0
    mids = {}
    mids_dw = []
    issue_count = 0
    while True:
        start_idx=block_num*block_size
        issues = jira.search_issues(jql_str, start_idx, block_size, json_result=True)
        block_num += 1
        if len(issues['issues']) == 0:
            break

        for issue in issues['issues']:
            domainId = None
            has3ds = False
            acquiringBank = None
            billingDescriptorPart2 = None

            if issue['fields']['issuelinks']:
                for li in issue['fields']['issuelinks']:
                    if li['type']['outward'].lower()=='domain for mid':
                        domainId = li['outwardIssue']['key']

            if issue['fields'].get('customfield_11437', None):
                if issue['fields']['customfield_11437'].get('value', None):
                    if issue['fields']['customfield_11437']['value']!='Non 3DS':
                        has3ds = True

            if issue['fields'].get('customfield_11367', None):
                acquiringBank = issue['fields']['customfield_11367'].get('value', None)

            if issue['fields'].get('customfield_11466', None):
                billingDescriptorPart2 = issue['fields']['customfield_11466'].get('value', None)

            if issue['fields'].get('customfield_11351', None):
                project = issue['fields']['customfield_11351'].get('value', None)

            mids_dw.append({
                    "jiraId":issue["key"],
                    "name": issue['fields']["summary"],
                    "status": issue['fields']['status']['name'],
                    "labels": ','.join(issue['fields']['labels']) if issue['fields']['labels'] else '',
                    "billingDescriptorPart1": issue['fields']["customfield_11370"],
                    "billingDescriptorPart2": billingDescriptorPart2,
                    "acquiringBank": acquiringBank,
                    "currencies": ','.join([v['value'] for v in issue['fields']['customfield_11364']]) if issue['fields']['customfield_11364'] else '',
                    "excludedGeos": ','.join([v['value'] for v in issue['fields']['customfield_11498']]) if issue['fields']['customfield_11498'] else '',
                    "vertical": ','.join([v['value'] for v in issue['fields']['customfield_11518']]) if issue['fields']['customfield_11518'] else '',
                    "mcc": int(issue['fields']["customfield_11431"]) if issue['fields']["customfield_11431"] else None,
                    "has3ds": has3ds,
                    "contract_id": issue['fields']['customfield_11464'],
                    "rgAccountSequence": issue['fields']["customfield_11467"],
                    "emailAddress": issue['fields']['customfield_11336'],
                    "freshdeskId": issue['fields']['customfield_11404'],
                    "monthlyTransactionsCap": issue['fields']["customfield_11471"],
                    "project": project,
                    "jiraDomainId": domainId,
                    "midFirstLiveDate": issue['fields']['customfield_11363']})

    logging.info("Updating mids records into Redshift {} .....".format('jira.mids'))
    mids_df = pd.DataFrame(mids_dw)
    #mids_df.to_sql('mids', schema='jira', con=redshift, index=False, if_exists='replace')
    table_schema = '("jiraid" character varying, "name" character varying, "status" character varying, "labels" character varying, "billingdescriptorpart1" character varying, "billingdescriptorpart2" character varying, "acquiringbank" character varying, "currencies" character varying, "excludedGeos" character varying, "vertical" character varying, "mcc" double precision, "has3ds" boolean, contract_id character varying,"rgaccountsequence" double precision, "emailaddress" character varying, "freshdeskid" character varying, "monthlytransactionscap" double precision, "project" character varying, "jiradomainid" character varying, "midfirstlivedate" character varying);'
    s3_path = 's3://linkmedia-datalake/jira/csv/mids/mids.tsv'
    load_to_redshift(mids_df, s3_path, 'jira', 'mids', table_schema)

def sync_companies():
    jira = get_jira_client()
    jql_str = 'project = UN AND issuetype = Company ORDER BY cf[11362] DESC, summary ASC, cf[11363] DESC, cf[11333] DESC, created DESC'
    start_idx = 0
    block_size = 100
    block_num = 0
    companies = {}
    companies_dw = []
    issue_count = 0
    while True:
        start_idx=block_num*block_size
        issues = jira.search_issues(jql_str, start_idx, block_size, json_result=True)
        block_num += 1
        if len(issues['issues']) == 0:
            break

        juri = None
        for issue in issues['issues']:
            if issue['fields']['customfield_11357']:
                juri = issue['fields']['customfield_11357']['value']
            company = {
                    "jiraId":issue["key"],
                    "name": issue['fields']["summary"],
                    "address": issue['fields']["customfield_11340"],
                    "address2": issue['fields']["customfield_11414"],
                    "zip": issue['fields']["customfield_11415"],
                    "city": issue['fields']["customfield_11416"],
                    "country": issue['fields']["customfield_11357"]["value"] if issue['fields']["customfield_11357"] else None,
                    "registrationNumber": issue['fields']["customfield_11352"],
                    "jurisdiction": juri,
                    "vat_number": issue['fields']['customfield_11354'],
                    "tax_id": issue['fields']['customfield_11350'],
                    "phoneNumber": issue['fields']["customfield_11335"],
                    "emailAddress": issue['fields']['customfield_11336'],
                    "freshdeskId": issue['fields']['customfield_11404'],
                    "threatmetrixClientId": issue['fields']["customfield_11422"],
                    "threatmetrixOrganizationId": issue['fields']["customfield_11000"]}
            companies_dw.append(company)
            remove_none(company)
            companies[issue['key']] = company
        issue_count = len(companies.keys())

    logging.info("Dumping {} JIRA Company records into {} .....".format(issue_count,'s3://linkmedia-datalake/rg-jira/jiraCompanies.json'))
    s3 = boto3.resource('s3')
    s3object = s3.Object('linkmedia-datalake', 'rg-jira/jiraCompanies.json')

    logging.info("Updating {} mids records into Redshift {} .....".format(issue_count,'jira.companies'))
    companies_df = pd.DataFrame(companies_dw)
    #companies_df.to_sql('companies', schema='jira', con=redshift, index=False, if_exists='replace')
    table_schema = '("jiraid" character varying,"name" character varying,"address" character varying,"address2" character varying,"zip" character varying,"city" character varying,"country" character varying,"registrationnumber" character varying,"jurisdiction" character varying,"tax_id" character varying,"phonenumber" character varying,"emailaddress" character varying,"threatmetrixclientid" character varying,"vat_number" character varying);'
    s3_path = 's3://linkmedia-datalake/jira/csv/companies/companies.tsv'
    load_to_redshift(companies_df, s3_path, 'jira', 'companies', table_schema)

    s3object.put(
        Body=(bytes(json.dumps({"companies":companies}).encode('UTF-8'))), ContentType='application/json'
    )


def sync_domains():
    jira = get_jira_client()
    jql_str = 'project = "LD" ORDER BY created DESC'
    start_idx = 0
    block_size = 100
    block_num = 0
    domains = {}
    domains_dw = []
    issue_count = 0
    while True:
        start_idx=block_num*block_size
        issues = jira.search_issues(jql_str, start_idx, block_size, json_result=True)
        block_num += 1
        if len(issues['issues']) == 0:
            break

        for issue in issues['issues']:
            companyId = None
            if issue['fields']['issuelinks']:
                for li in issue['fields']['issuelinks']:
                    if li['type']['name'].lower()=='company domain':
                        companyId=li['outwardIssue']['key']

            d = {
                    "jiraId":issue["key"],
                    "name": issue['fields']["summary"],
                    "phoneNumber": issue['fields']["customfield_11335"],
                    "emailAddress": issue['fields']['customfield_11336'],
                    "freshdeskId": issue['fields']['customfield_11404'],
                    "websiteCategory": issue['fields']["customfield_11304"]['value'] if issue['fields']["customfield_11304"] else None,
                    "websiteCategoryId": issue['fields']["customfield_11304"]['id'] if issue['fields']["customfield_11304"] else None,
                    "jiraCompanyId": companyId}
            domains_dw.append(d)
            remove_none(d)
            domains[issue['key']] = d
        issue_count = len(domains.keys())

    logging.info("Dumping {} JIRA Company records into {} .....".format(issue_count,'s3://linkmedia-datalake/rg-jira/jiraDomains.json'))
    s3 = boto3.resource('s3')
    s3object = s3.Object('linkmedia-datalake', 'rg-jira/jiraDomains.json')

    s3object.put(
        Body=(bytes(json.dumps({"domains":domains}).encode('UTF-8'))), ContentType='application/json'
    )

    logging.info("Updating {} domains records into Redshift {} .....".format(issue_count,'jira.domains'))
    domains_df = pd.DataFrame(domains_dw)
    # domains_df.to_sql('domains', schema='jira', con=redshift, index=False, if_exists='replace')
    table_schema = '("jiraid" character varying, "name" character varying, "phonenumber" character varying, "emailaddress" character varying, "freshdeskid" character varying, "websitecategory" character varying, "websitecategoryid" character varying, "jiracompanyid" character varying);'
    s3_path = 's3://linkmedia-datalake/jira/csv/domains/domains.tsv'
    load_to_redshift(domains_df, s3_path, 'jira', 'domains', table_schema)

def sync_rocketgate_merchants():
    cursor = connect(
                 s3_staging_dir='s3://linkmedia-datalake/athenatemp/',
                 region_name='eu-central-1').cursor()

    query="select merchant_id, account_sequence, currency as currencies, processor, bank, account_descriptor, last_updated_date, account_name, account_status, merchant, account_notes from AwsDataCatalog.rocketgate.merchantaccounts"

    records = cursor.execute(query).fetchall()
    rgmids = {}
    accounts = {}

    for row in records:
        rgmids[row[0]] = {"accounts":{}}
    for row in records:
        rgmids[row[0]]["accounts"][row[1]]={

            "merchantId": int(row[0]),
            "currencies": row[2].split(','),
            "processor": row[3],
            "bank": row[4],
            "accountDescriptor": row[5],
            "lastUpdatedDate": row[6],
            "accountName": row[7],
            "accountStatus": row[8],
            "accountSequence": int(row[1]),
            "merchant": row[9],
            "accountNotes": row[10]
        }

    s3 = boto3.resource('s3')
    s3object = s3.Object('linkmedia-datalake', 'rg-jira/rocketGateMids.json')

    s3object.put(
        Body=(bytes(json.dumps({"merchants":rgmids}).encode('UTF-8'))), ContentType='application/json'
    )


start = DummyOperator(task_id='start', dag=dag)
end = DummyOperator(task_id='end', dag=dag)

sync_mids_to_backend = PythonOperator(
    task_id='sync_mids_to_backend',
    python_callable=sync_mids_to_backend,
    dag=(dag),
)

sync_mids_to_dw = PythonOperator(
    task_id='sync_mids_to_dw',
    python_callable=sync_mids_to_dw,
    dag=(dag),
)


sync_companies = PythonOperator(
    task_id='sync_companies',
    python_callable=sync_companies,
    dag=(dag),
)

sync_domains = PythonOperator(
    task_id='sync_domains',
    python_callable=sync_domains,
    dag=(dag),
)

sync_rocketgate_merchants = PythonOperator(
    task_id='sync_rocketgate_merchants',
    python_callable=sync_rocketgate_merchants,
    dag=(dag),
)

start >> [sync_mids_to_backend, sync_mids_to_dw, sync_domains, sync_companies, sync_rocketgate_merchants] >> end
