import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta

CURRENCIES = Variable.get("CURRENCIES")
FIXER_API_KEY = Variable.get("FIXER_API_KEY")
START_DATE = Variable.get("START_DATE")
END_DATE = Variable.get("END_DATE")
# TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')

# CURRENCIES = ''
# FIXER_API_KEY = ''
TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')


default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 3,
    'retry_delay': timedelta(minutes=5),
}

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

def generate_currency_base(rundate, base_currency):

    logging.info('processing currency exchange rates for {base_currency} base currency on {rundate}'.format(base_currency=base_currency,rundate=rundate))

    currency_json_path = 'http://data.fixer.io/api/'+rundate+'?access_key='+FIXER_API_KEY+'&base='+base_currency+'&symbols='+CURRENCIES+'&format=1'

    xrdf = pd.read_json(currency_json_path)
    xrdf.reset_index(inplace=True)
    xrdf=xrdf.rename(columns={'index': 'currency'})
    xrdf['date'] = pd.to_datetime(xrdf['date'], format='%Y-%m-%d')
    redshift = get_redshift_conn()
    with redshift.connect() as con:
        con.execute("delete from public.currency_rates where date(date)=date('{}')".format(rundate))
        con.close()
    xrdf.to_sql('currency_rates', schema='public', con=redshift, index=False, if_exists='append')
    logging.info('base currency {} exchange rate for {} processed successfully'.format(base_currency, rundate))

def generate_currency_base_hist(start_date, end_date):

    start = datetime.strptime(start_date, '%Y-%m-%d')
    end = datetime.strptime(end_date, '%Y-%m-%d')

    for single_date in daterange(start, end):
        rundate = single_date.strftime("%Y-%m-%d")
        generate_currency_base(rundate, 'EUR')

current_dag = DAG(
    dag_id='CURRENCY_RATES',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 2 * * *',
    dagrun_timeout=timedelta(minutes=60),
    tags=['CURRENCY'])

run_eur_generate_exchange_rates = PythonOperator(
    task_id='eur_generate_exchange_rates',
    python_callable=generate_currency_base,
    op_kwargs={'rundate': TODAY, 'base_currency': 'EUR'},
    dag=(current_dag),
)

hist_dag = DAG(
    dag_id='CURRENCY_RATES_HIST',
    default_args=default_args,
    catchup=False,
    schedule_interval='@once',
    dagrun_timeout=timedelta(minutes=60),
    tags=['CURRENCY'])

run_hist_generate_exchange_rates = PythonOperator(
    task_id='hist_generate_exchange_rates',
    python_callable=generate_currency_base_hist,
    op_kwargs={'start_date': START_DATE, 'end_date': END_DATE},
    dag=(hist_dag)
)