import datetime
import logging
from airflow import DAG
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException
import pandas as pd
import logging

#local packages
from linkmedia.lib import read_store as rs

default_args = {
    'owner': 'Naseef',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag_current = DAG(
    dag_id='STICKY_CB_ALERTS',
    default_args=default_args,
    catchup=False,
    schedule_interval='15,45 * * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['CHARGEBACK_ALERTS'])

dag_historical = DAG(
    dag_id='STICKY_CB_ALERTS_HISTORICAL',
    default_args=default_args,
    catchup=False,
    schedule_interval=None,
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['CHARGEBACK_ALERTS'])


def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()
    return conn


def get_sticky_cb_alerts(run_date, number_of_days_ahead):
    # start date and number of days to go back

    start_date = run_date

    # loop for required days and process sticky_cb_alerts
    try:
        for i in list(range(0, number_of_days_ahead)):
            run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') + pd.DateOffset(i)).strftime('%Y-%m-%d')
            load_bucket = 'linkmedia-datalake'
            load_prefix = 'chargeback_alerts/sticky_cb_alerts_raw/'
            save_bucket = 'linkmedia-datalake'
            save_prefix = 'chargeback_alerts/sticky_cb_alerts_processed/'
            save_bucket_path = 's3://' + save_bucket + '/' + save_prefix + run_date.replace('-','/') + '/'

            sticky_cb_alert_schema = {
            'Unnamed: 0' :str,
            'Order ID' :str,
            'Provider Account ID' :str,
            'Alert Type Name' :str,
            'Alert Date In Date' :str,
            'Card Issuer' :str,
            'Cc First 6' :str,
            'Cc Last 4' :str,
            'Transaction Date In Date' :str,
            'Transaction Amount' :str,
            'Currency' :str,
            'Chargeback Amount' :str,
            'Arn' :str,
            'Provider Alert ID' :str,
            'Provider Source' :str,
            'Outcome' :str,
            'Three D Secure Status' :str,
            'Merchant Descriptor' :str,
            'Mid' :str,
            'Merch Category Code' :str,
            'Date In Time' : str
            }
            sticky_cb_alert_fields = list(sticky_cb_alert_schema.keys())
            sticky_cb_alert_fields_keep = list(x for x in sticky_cb_alert_schema.keys() if x not in ['Date In Time','Unnamed: 0'])
            #cb_alerts_df = rs.read_all_csv_from_S3_to_pd(load_bucket, load_prefix, sticky_cb_alert_schema, sticky_cb_alert_fields)
            cb_alerts_df = rs.read_sticky_cbalerts_csv_from_S3_to_pd(load_bucket, load_prefix, sticky_cb_alert_schema, sticky_cb_alert_fields, run_date)
            cb_alerts_df.drop('Unnamed: 0',axis =1,inplace=True)
            cb_alerts_df = cb_alerts_df[cb_alerts_df['Alert Date In Date'] == run_date]
            cb_alerts_df = cb_alerts_df.drop_duplicates(subset=sticky_cb_alert_fields_keep, keep ='first')
            cb_alerts_df.reset_index(drop=True,inplace=True)
            if not cb_alerts_df.empty:
                rs.save_to_parquet(cb_alerts_df, save_bucket_path, 'sticky_cb_alerts_' + run_date.replace('-','_'))
            print(str(run_date))

    except Exception as e:
        raise AirflowException(e)


start_date_current = (datetime.date.today()+pd.DateOffset(-3)).strftime('%Y-%m-%d')
number_of_days_ahead = 4

start_date_history = Variable.get("START_DATE")
number_of_days_ahead_history = int(Variable.get("DAYS_AHEAD"))



get_sticky_cb_alerts_current = PythonOperator(
    task_id='get_sticky_cb_alerts',
    python_callable=get_sticky_cb_alerts,
    op_kwargs={'run_date': start_date_current, 'number_of_days_ahead': number_of_days_ahead},
    dag=(dag_current),
)


get_sticky_cb_alerts_history = PythonOperator(
    task_id='get_sticky_cb_alerts',
    python_callable=get_sticky_cb_alerts,
    op_kwargs={'run_date': start_date_history, 'number_of_days_ahead': number_of_days_ahead_history},
    dag=(dag_historical),
)
