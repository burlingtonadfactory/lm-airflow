import os
import ast
import json
import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta


# RG_MERCH1 = Variable.get("RG_MERCH1", deserialize_json=True, default_var=None)
RG_MERCHANTS = json.loads(Variable.get("RG_MERCHANTS"))
RG_HISTORICAL_START = Variable.get("RG_HISTORICAL_START")
RG_HISTORICAL_END = Variable.get("RG_HISTORICAL_END")
TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')
YESTERDAY = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
# RG_MERCH1 = Variable.get("RG_MERCH1", default_var=None)

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag =DAG(
    'RG_DUPLICATE_SUBSCRIPTIONS',
    default_args=default_args,
    schedule_interval='30 5 * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=10),
    tags=['REDSHIFT','ALERTS']
    )

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn


def redshift_execute(sql):

    red_conn = get_redshift_conn()
    with red_conn.connect() as con:
        con.execution_options(isolation_level="AUTOCOMMIT").execute(sql)
        con.close()


def alert_rg_duplicate_subscriptions():
    query="""select distinct subscription_to_cancel, cancel_reason from rocketgate.duplicate_subscriptions_history where alerted_date=date(getdate()) order by 2;"""

    engine_redshift = get_redshift_conn()
    red_conn = engine_redshift.connect()
    #having date_diff > 60
    df = pd.read_sql(query, con=red_conn)
    if len(df) > 0:
        total_count = 260
        sr =0
        er=25
        subset_list =[]
        while er<total_count:
            subset_list.append((sr,er))
            sr=er
            er+=25
        if sr < total_count:
            subset_list.append((sr,total_count))
        message_blocks =''
        alert_block ='{"type": "section","text": {"type": "mrkdwn","text": " *RG DUPLICATE SUBSCRIPTIONS* \\n<!subteam^S019T18VCCB> <!subteam^S02490CFC4E> The following duplicate rocketgate subscriptions were found. Please verify and cancel:"}}'
        message_blocks += alert_block
        for (x,y) in subset_list :
            data_block =',{"type": "section","text": {"type": "mrkdwn","text": "'+ ''.join('{} | {}\\n'.format(subscription_to_cancel, cancel_reason) for subscription_to_cancel, cancel_reason in zip(df.iloc[x:y].subscription_to_cancel.values, df.iloc[x:y].cancel_reason.values))+'\\n"}}'
            message_blocks += data_block
        message ='{"username": "Analytics alert","blocks": [' + message_blocks + ']}'
        slack_msg=ast.literal_eval(message)
        print(slack_msg)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)
    else :
        message ='{"username": "Analytics alert","blocks": [{"type": "section","text": {"type": "mrkdwn","text": "*RG DUPLICATE SUBSCRIPTIONS* \\n<!subteam^S019T18VCCB> <!subteam^S02490CFC4E> No duplicate rocketgate subscriptions were found today. :smile:"}}]}'
        slack_msg=ast.literal_eval(message)

        # requests.post(web_hook,data=json.dumps(slack_msg))
        SimpleHttpOperator(
            task_id='notify_slack_channel',
            http_conn_id='linkmedia_slack',
            endpoint='',
            data=json.dumps(slack_msg),
            headers={"Content-Type": "application/json"},
        ).execute(context=None)



duplicate_subscriptions_table = PythonOperator(
    task_id='update_duplicate_subscriptions_table',
    python_callable=redshift_execute,
    op_kwargs={
        'sql': """insert into rocketgate.duplicate_subscriptions_history
        select date(getdate()) AS alerted_date,inv_id_ext AS subscription_to_cancel,cancel_reason from rocketgate.duplicate_subscriptions_to_cancel;"""
        },
    dag=(dag),
    )


duplicate_subscriptions_alert = PythonOperator(
        task_id='alert_rg_duplicate_subscriptions',
        python_callable=alert_rg_duplicate_subscriptions,
        dag=(dag),
    )

duplicate_subscriptions_table >> duplicate_subscriptions_alert