import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta

# RG_MERCH1 = Variable.get("RG_MERCH1", deserialize_json=True, default_var=None)
RG_MERCHANTS = json.loads(Variable.get("RG_MERCHANTS"))
RG_HISTORICAL_START = Variable.get("RG_HISTORICAL_START")
RG_HISTORICAL_END = Variable.get("RG_HISTORICAL_END")
TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')
YESTERDAY = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
# RG_MERCH1 = Variable.get("RG_MERCH1", default_var=None)

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn


def redshift_execute(sql):

    red_conn = get_redshift_conn()
    with red_conn.connect() as con:
        con.execution_options(isolation_level="AUTOCOMMIT").execute(sql)
        con.close()



with DAG(
    'REDSHIFT_HOURLY_UPDATES',
    default_args=default_args,
    schedule_interval='0 * * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=10),
    tags=['REDSHIFT']
) as dag:
    sticky_alerts_view = PythonOperator(
        task_id='update_sticky_alerts_view_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS central.sticky_cb_alerts_view_table;
            CREATE TABLE central.sticky_cb_alerts_view_table as select * from central.sticky_cb_alerts_view;"""
        }
    )
    affiliate_info = PythonOperator(
        task_id='update_affiliate_info',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS central.aff_info;
            CREATE TABLE central.aff_info as
            (
            SELECT CAST("source_affiliate.source_affiliate_id" AS VARCHAR) as "matched_affiliate_id",'Cake' as "matched_affiliate_platform", max("source_affiliate.source_affiliate_name") as "matched_affiliate_name"
            from
                 cake.conversions cc
                     inner join
                (SELECT CAST("source_affiliate.source_affiliate_id" AS VARCHAR) as "affiliate_id", max(partition_0||partition_1||partition_2) as "latest_conversion_date" from cake.conversions group by 1) cc_latest
                    ON  CAST("source_affiliate.source_affiliate_id" AS VARCHAR) = cc_latest."affiliate_id"
                        AND
                    cc.partition_0||cc.partition_1||cc.partition_2 =cc_latest."latest_conversion_date"
            group by 1,2
            UNION
            SELECT CAST("relationship_affiliate_network_affiliate_id" AS VARCHAR) as "matched_affiliate_id",'Everflow' as "matched_affiliate_platform", max("relationship_affiliate_name") as "matched_affiliate_name"
            from
                 everflow.conversions ec
                    inner join
                 (SELECT CAST("relationship_affiliate_network_affiliate_id" AS VARCHAR) as "affiliate_id", max(year||month||day) as "latest_conversion_date" from everflow.conversions group by 1 ) ec_latest
                    ON  CAST("relationship_affiliate_network_affiliate_id" AS VARCHAR) = ec_latest."affiliate_id"
                        AND
                    ec.year||ec.month||ec.day =ec_latest."latest_conversion_date"
            group by 1,2
            );"""
        }
    )
    cascades_view = PythonOperator(
        task_id='update_cascades_view_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.cascades_view_table;
            CREATE TABLE rocketgate.cascades_view_table as select * from rocketgate.cascades_view;"""
        }
    )
    rebill_reattempts_view = PythonOperator(
        task_id='update_rebill_reattempts_view_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.rebill_reattempts_view_table;
            CREATE TABLE rocketgate.rebill_reattempts_view_table as select * from rocketgate.rebill_reattempts_view;"""
        }
    )
    subscriptions_view = PythonOperator(
        task_id='update_subscriptions_view_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.subscriptions_view_table;
            CREATE TABLE rocketgate.subscriptions_view_table as select * from rocketgate.subscriptions_view;"""
        }
    )
    affiliate_cost_per_lead = PythonOperator(
        task_id='update_affiliate_cost_per_lead_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.affiliate_cost_per_lead_table;
            CREATE TABLE rocketgate.affiliate_cost_per_lead_table as select * from rocketgate.affiliate_cost_per_lead;"""
        }
    )
    txn_billing_cycle = PythonOperator(
        task_id='update_txn_billing_cycle_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.txn_billing_cycle_table;
            CREATE TABLE rocketgate.txn_billing_cycle_table as select * from rocketgate.txn_billing_cycle;"""
        }
    )
    bin_db = PythonOperator(
        task_id='update_bin_db_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS bindb.bin_db;
            CREATE TABLE bindb.bin_db as select * from athenabindb.bin_db;"""
        }
    )
    bin_exclusion = PythonOperator(
        task_id='update_bin_exclusion_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS bindb.bin_exclusion;
            CREATE TABLE bindb.bin_exclusion as select * from athenabindb.bin_exclusion;"""
        }
    )


with DAG(
    'REDSHIFT_DAILY_UPDATES',
    default_args=default_args,
    schedule_interval='45 0,6,12,18 * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=60),
    tags=['REDSHIFT']
) as daily_dag:
    update_tc40 = PythonOperator(
        task_id='update_tc40_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """INSERT INTO tc40.fraud_order_id SELECT * FROM tc40.tc40_order_id WHERE tc40_date > (SELECT max(tc40_date) FROM tc40.fraud_order_id) and order_id is not null;"""
        }
    )
    update_rg_overview_table = PythonOperator(
        task_id='update_rg_overview_table',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': """DROP TABLE IF EXISTS rocketgate.rg_overview_table;
            CREATE TABLE rocketgate.rg_overview_table AS SELECT * FROM rocketgate.rg_overview;"""
        }
    )
