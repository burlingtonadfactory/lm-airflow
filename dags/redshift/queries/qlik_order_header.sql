
WITH refunds as (
    SELECT
            order_id,
            date(refund_date) as refund_date,
            gateway_processing_percent,
            gateway_reserve_percent,
            refund_fee,
            gateway_chargeback_fee
    FROM datawarehouse.refunds_with_bank_dedcutions
    where partition_0='2021' and partition_1= '05'
), tc40 as (
    SELECT -- tc40
    order_id,
    tc40_date
FROM tc40.fraud_order_id
), cb as (
    select
        cbd.order_id,
        concat(cast(cbd.order_id as varchar),cbd.transaction_type) unique_check,
        cbd.bank_name,
        cbd.cb_date,
        cbd.transaction_type,
        cbd.billing_continent_name as billing_continent,
        cbd.order_currency as currency,
        bd.cb_fee_bank as gateway_charge_back_fee
    FROM chargebacks.view_chargebacks_data_current cbd
    LEFT JOIN athenachargebacks.bank_deduction bd
    ON bd.bank_name = cbd.bank_name and bd.billing_continent = cbd.billing_continent_name  and bd.currency = cbd.order_currency
), reserve_release as (
    SELECT
        order_id,
        reserve_release_date
    FROM datawarehouse.reserve_release
    where is_reserve_released=1
), cancelled as (
SELECT  -- cancelled
   order_id,
   hold_date
FROM datawarehouse.cancellations c
    where partition_0 = '2021' and partition_1= '05'
), void as (
SELECT  -- void
   order_id,
   void_date
FROM datawarehouse.void v
    where partition_0='2021' and partition_1= '05'
), cake as (
SELECT
    order_id,
    transaction_type,
    event_conversion_date,
    billing_country,
    'N/A' as  order_gateway_id,
    CASE WHEN len(category) = 0 THEN '999' ELSE category END as main_product_category,
    affiliate_id,
    subaffiliate as sub_affiliate,
   CAST(campaign_id as INTEGER) as campaign_id,
    cost_per_lead,
    affiliate_name
FROM cake.order_header_conversions
)
SELECT COALESCE(oref.transaction_type, tc40.transaction_type,
                cb.transaction_type, rr.transaction_type,
                cancelled.transaction_type, v.transaction_type,
                ckc.transaction_type, 'order')                            as transaction_type,
        COALESCE(oref.date, tc40.date, cb.date, rr.date, cancelled.date,
                v.date, ckc.date,
                date(oh.time_stamp))                                      as date,
        oh.cc_first_6 || oh.cc_last_4 || oh.cc_expires                     as cc_first_6_last_4_expires, --1
        oh.order_gateway_id                                                as order_gateway_id_source,
        CASE
            WHEN ckc.order_id is not null
                THEN CAST(ckc.order_gateway_id as varchar)
            ELSE CAST(oh.order_gateway_id as varchar) END                  as order_gateway_id,          --1 -- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id), order_gateway_id), order_gateway_id) as order_gateway_id,
        -- vat_date,   -- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_vat_date', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),vat_date), vat_date), vat_date) as vat_date,
        date(oh.acquisition_date)                                          as acquisition_date,          --1
        CASE
            WHEN ckc.order_id is not null
                THEN CAST(ckc.affiliate_id as varchar)
            ELSE CAST(oh.affiliate_id as varchar) END                      as affiliate_id,              --1
        ckc.affiliate_name,
        COALESCE(oh.campaign_id, ckc.campaign_id) as campaign_id,
        -- amount_refunded_to_date,
        oh.ancestor_id,                                                                                  --1
        oh.billing_city,                                                                                 --1
        COALESCE(oh.billing_country, ckc.billing_country),                                               --1
        oh.billing_cycle,                                                                                --1
        oh.billing_first_name,                                                                           --1
        oh.billing_last_name,                                                                            --1
        oh.billing_postcode,                                                                             --1
        oh.billing_state,                                                                                --1
        oh.billing_street_address,                                                                       --1
        oh.cc_expires,                                                                                   --1
        oh.cc_first_6,                                                                                   --1
        oh.cc_last_4,
        oh.cc_type                                                         as order_cc_type,
        -- chargeback_date,
        oh.current_rebill_discount_percent,                                                              --1
        oh.customer_id,                                                                                  --1
        oh.customers_telephone,                                                                          --1
        oh.decline_reason,                                                                               --1
        oh.decline_salvage_discount_percent,                                                             --1
        oh.email_address,                                                                                --1
        -- hold_date,
        oh.ip_address,                                                                                   --1
        -- is_recurring,
        -- is_test_cc,
        -- is_void,
        oh.main_product_id,                                                                              --1
        oh.main_product_quantity,                                                                        --1
        oh.on_hold,                                                                                      --1
        oh.on_hold_by,                                                                                   --1
        oh.order_confirmed,
        -- date(time_stamp) as date, --1
        oh.order_id,                                                                                     --1
        oh.order_status,                                                                                 --1
        oh.order_total,                                                                                  --1
        oh.parent_id,                                                                                    --1
        oh.rebill_discount_percent,                                                                      --1
        oh.recurring_date,                                                                               --1
        oh.refund_amount,                                                                                --1
        -- refund_date,
        -- retry_date,
        -- shipping_date,
        CASE
            WHEN ckc.order_id is not null
                THEN CAST(ckc.sub_affiliate as varchar)
            ELSE CAST(oh.sub_affiliate as varchar) END                     as sub_affiliate,             --1
        oh.transaction_id,                                                                               --1
        oh.upsell_product_id,                                                                            --1
        oh.upsell_product_quantity,                                                                      --1
        oh.void_amount,                                                                                  --1
        -- void_date,
        oh.is_create_order,                                                                              --1
        -- order_confirmed_date,
        -- system_notes_last_date,
        oh.cascade_parent_order_id,                                                                      --1
        oh.is_cascade,                                                                                   --1
        oh.migration_parent_order_id,                                                                    --1
        oh.is_migration,                                                                                 --1
        oh.cross_sale_parent_order_id,                                                                   --1
        -- is_cross_sale,
        -- reserve_release_date,
        date(oh.time_stamp)                                                as order_date,
        oh.is_test_order,
        -- gateway_first_live_date,
        -- gateway_descriptor,
        oh.order_currency,
        -- is_gateway_3ds,
        -- brand_id,
        -- bank_id,
        oh.bank_name,
        oh.company_name,-- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_comp', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),company_name), company_name), company_name) as company_name,
        oh.vat_number,-- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_vat_num', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),vat_number), vat_number), vat_number) as vat_number,
        round(oh.order_total_eur, 2)                                       as order_total_eur,
        round(oh.refund_amount_eur, 2)                                     as refund_amount_eur,
        -- main_product_name,
        CASE
            WHEN date(oh.acquisition_date) >= '2020-07-26'
                THEN CASE
                        WHEN oh.main_product_category in (4, 5, 6, 11, 12, 13, 14, 15, 16)
                            THEN '9999'
                        ELSE CAST(oh.main_product_category as varchar) END
            ELSE COALESCE(CAST(ckc.main_product_category as varchar), CAST(oh.main_product_category as varchar))
            END                                                            as main_product_category,
        oh.trial_days,
        oh.is_lead_rebill_donation,
        -- is_trial_over,
        COALESCE(CAST(oh.cost_per_lead AS BIGINT),CAST(ckc.cost_per_lead AS BIGINT)) cost_per_lead,
        oh.cost_per_lead_currency,
        oh.is_cpa_missing,
        round(oh.cost_per_lead_eur, 2)                                     as cost_per_lead_eur,
        -- hold_date_derived,
        -- affiliate_name,
        oh.billing_continent_name,
        COALESCE(oref.gateway_processing_percent,
                oh.gateway_processing_percent)                            as gateway_processing_percent,
        round(order_total_eur * (oh.gateway_processing_percent / 100), 3)  as gateway_processing_fee_eur,
        COALESCE(oref.gateway_reserve_percent, oh.gateway_reserve_percent) as gateway_reserve_percent,
        COALESCE(oref.refund_fee, oh.gateway_transaction_fee)              as gateway_transaction_fee,
        CASE
            WHEN cb.date is not null AND cb.date >= '2020-04-01' AND
                COALESCE(cb.gateway_charge_back_fee, 0) <> 0
                THEN cb.gateway_charge_back_fee
            ELSE COALESCE(oref.gateway_chargeback_fee, oh.gateway_charge_back_fee)
            END                                                            as gateway_chargeback_fee,
        CASE
            WHEN cb.date is not null THEN
                CASE
                    WHEN cb.date >= '2021-03-01' and
                        cb.billing_continent = 'EU' AND cb.bank_name in
                                                        ('EMP - B&S',
                                                            'EMP - BOR',
                                                            'EMP - KORTA',
                                                            'EMP - PAYNETICS',
                                                            'EMP - SIX',
                                                            'EMP - TE',
                                                            'EMP - WDL',
                                                            'NETS',
                                                            'CONCARDIS')
                        THEN CASE
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'VISA'
                                    THEN 12
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'MAST'
                                    THEN 15 END
                    WHEN cb.date >= '2021-01-01' and
                        cb.billing_continent = 'EU' AND cb.bank_name in
                                                        ('EMP - B&S',
                                                            'EMP - BOR',
                                                            'EMP - KORTA',
                                                            'EMP - PAYNETICS',
                                                            'EMP - SIX',
                                                            'EMP - TE',
                                                            'EMP - WDL')
                        THEN CASE
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'VISA'
                                    THEN 12
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'MAST'
                                    THEN 15 END
                    WHEN cb.date >= '2020-04-01' and
                        cb.billing_continent = 'EU' and
                        cb.bank_name like '%EMP%'
                        THEN CASE
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'VISA'
                                    THEN 12
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'MAST'
                                    THEN 15 END
                    WHEN cb.billing_continent = 'EU'
                        THEN CASE
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'VISA'
                                    THEN 12
                                WHEN upper(substring(oh.cc_type, 1, 4)) = 'MAST'
                                    THEN 15 END
                    ELSE 0
                    END
            ELSE NULL END                                                  as gateway_charge_back_cc_fee_eur,
        round(oh.reserve_taken_amount_eur, 2)                              as reserve_taken_amount_eur,
        oh.order_cc_key,
        -- is_reserve_released,
        oh.cust_subscription_key,
        oh.cust_lead_u_key,
        oh.vat_percent,
        round(oh.vat_amount_eur, 2)                                        as vat_amount_eur,
        -- has_first_rebill,
        1                                                                  as counter,
        -- CASE WHEN upper(bank_name) in ('DUMMY' ,'EMP - BOR','BILLPRO - CHINA','EPRO') THEN 0 ELSE 1 end as Exclude_MID,
        oh.is_straight_sale,
        case
            when oh.is_straight_sale = 1 THEN split_part(oh.c2, '&sku=', 2)
            ELSE '' end                                                    as straight_sale_product,
        oh.is_salvage_sale,
        oh.refund_fee,
        -- date(date_trunc('month',date(time_stamp)) as time_stamp_month,
        CASE WHEN oref.order_id = tc40.order_id THEN 1 ELSE 0 END             is_refund,
        CASE WHEN tc40.order_id = cb.order_id THEN 1 ELSE 0 END            as has_tc40_on_cb,
        oh.cascade_attempt
FROM central.order_header oh
            left outer join (select 'refund' as transaction_type, date(refund_date) as date, *
                            from refunds
                            UNION
                            SELECT NULL as transaction_type, null as date, *
                            from refunds) as oref
                            ON oref.order_id = oh.order_id
            left outer join (select 'tc40' as transaction_type, date(tc40_date) as date, order_id
                            from tc40
                            UNION
                            SELECT NULL as transaction_type, null as date, order_id
                            from tc40) as tc40
                            ON oref.order_id = oh.order_id
            left outer join (-- chargebacks & chargeback_reversal
    SELECT order_id,
            CASE
                WHEN transaction_type = 'CB' THEN 'chargeback'
                ELSE 'chargeback_reversal' END as transaction_type,
            date(cb_date)                      as date,
            billing_continent,
            bank_name,
            gateway_charge_back_fee
    FROM cb
    UNION
    SELECT order_id,
            NULL as transaction_type,
            NULL as date,
            billing_continent,
            bank_name,
            NULL as gateway_charge_back_fee
    FROM cb
) cb ON cb.order_id = oh.order_id
            left outer join (select 'reserve_release'          as transaction_type,
                                    date(reserve_release_date) as date,
                                    order_id
                            from reserve_release
                            UNION
                            SELECT NULL as transaction_type, null as date, order_id
                            from reserve_release) as rr
                            ON rr.order_id = oh.order_id
            left outer join (select 'cancelled'     as transaction_type,
                                    date(hold_date) as date,
                                    order_id
                            from cancelled
                            UNION
                            SELECT NULL as transaction_type, null as date, order_id
                            from cancelled) as cancelled
                            ON cancelled.order_id = oh.order_id
            left outer join (select 'void' as transaction_type, date(void_date) as date, order_id
                            from void
                            UNION
                            SELECT NULL as transaction_type, null as date, order_id
                            from void) as v
                            ON v.order_id = oh.order_id
            left outer join (
    select transaction_type,
            date(event_conversion_date) as date,
            order_id,
            billing_country,
            order_gateway_id,
            main_product_category,
            affiliate_id,
            sub_affiliate,
            campaign_id,
            cost_per_lead,
            affiliate_name
    FROM cake
    UNION
    SELECT NULL as transaction_type,
            null as date,
            order_id,
            billing_country,
            order_gateway_id,
            main_product_category,
            affiliate_id,
            sub_affiliate,
            campaign_id,
            cost_per_lead,
            affiliate_name
    FROM cake) as ckc
                            ON v.order_id = oh.order_id
where oh.is_test_order != 1
    and oh.is_test_order != 1
    and date(time_stamp) >= '2021-01-01'
    --and oh.partition_0 = '2021'