create or replace view rocketgate.txn_billing_cycle(tr_id, billing_cycle) as
	SELECT tr_id,
       COALESCE((lead(seq_no IGNORE NULLS)
                 OVER (
                     PARTITION BY inv_id_ext, email
                     ORDER BY tr_date DESC)) + 1, 0) AS billing_cycle
    FROM (SELECT inv_id_ext,
                 email,
                 tr_id,
                 tr_date,
                 reason_desc,
                 (row_number()
                  OVER (
                      PARTITION BY inv_id_ext, email
                      ORDER BY tr_date)) - 1 AS seq_no
          FROM rocketgate.transactions
          WHERE (ttype_name = 'SALE' OR ttype_name = 'AUTH_ONLY' OR ttype_name = 'TICKET')
            AND reason_desc = 'Success'
            AND merch_id = '1599571287'
            AND billing_type in ('X-T','T','C','R','I')
          UNION
          SELECT inv_id_ext,
                 email,
                 tr_id,
                 tr_date,
                 reason_desc,
                 NULL AS seq_no
          FROM rocketgate.transactions
          WHERE NOT((ttype_name = 'SALE' OR ttype_name = 'AUTH_ONLY' OR ttype_name = 'TICKET')
            AND reason_desc = 'Success'
            AND merch_id = '1599571287'
            AND billing_type in ('X-T','T','C','R','I')))
    ORDER BY inv_id_ext, email, tr_date;
