create or replace view rocketgate.subscriptions_view(rebill_amount, currency, initial_amount, status, rebill_cancel_by, rebill_start_date, rebill_date, rebill_cancel_request_date, rebill_end_date, rebill_last_updated_date, merchant_invoice_id, merchant_customer_id, customer_username, customer_password, affiliate, merchant_id, merchant_name, merchant_site_id, merchant_site_name, merchant_product_id, sticky_mid, last_trans_status, rebill_frequency, initial_frequency, card_name, pay_num_l4, merch_id_referrer, site_name_referrer, merch_name_referrer, cust_email, cust_fname, cust_lname) as
	SELECT max(rgs.rebill_amount) as rebill_amount,
                max(rgs.currency)::text as currency,
                max(rgs.initial_amount) as initial_amount,
                min(rgs.status)::text as status,
                max(rgs.rebill_cancel_by)::text as rebill_cancel_by,
                max(rgs.rebill_start_date) as rebill_start_date,
                max(rgs.rebill_date) as rebill_date,
                max(rgs.rebill_cancel_request_date) as rebill_cancel_request_date,
                max(rgs.rebill_end_date) as rebill_end_date,
                max(rgs.rebill_last_updated_date) as rebill_last_updated_date,
                rgs.merchant_invoice_id,
                rgs.merchant_customer_id,
                max(rgs.customer_username)::text as customer_username,
                max(rgs.customer_password)::text as customer_password,
                max(rgs.affiliate)::text as affiliate,
                max(rgs.merchant_id)::text as merchant_id,
                max(rgs.merchant_name)::text as merchant_name,
                max(rgs.merchant_site_id)::text as merchant_site_id,
                max(rgs.merchant_site_name)::text as merchant_site_name,
                max(rgs.merchant_product_id)::text as merchant_product_id,
                max(rgs.sticky_mid)::text as sticky_mid,
                max(rgs.last_trans_status)::text as last_trans_status,
                max(rgs.rebill_frequency)::text as rebill_frequency,
                max(rgs.initial_frequency)::text as initial_frequency,
                max(rgs.card_name)::text as card_name,
                max(rgs.pay_num_l4)::text as pay_num_l4,
                max(rgs.merch_id_referrer)::text as merch_id_referrer,
                max(rgs.site_name_referrer)::text as site_name_referrer,
                max(rgs.merch_name_referrer)::text as merch_name_referrer,
                max(rgs.cust_email)::text as cust_email,
                max(rgs.cust_fname)::text as cust_fname,
                max(rgs.cust_lname)::text as cust_lname
FROM rocketgate.subscriptions rgs
         JOIN (SELECT subscriptions.merchant_invoice_id,subscriptions.merchant_customer_id,
                      "max"(COALESCE(subscriptions.rebill_cancel_request_date, subscriptions.rebill_last_updated_date,
                                     subscriptions.rebill_start_date)) AS rebill_last_updated_date_latest
               FROM rocketgate.subscriptions
               GROUP BY subscriptions.merchant_invoice_id,subscriptions.merchant_customer_id) rgs_latest
              ON rgs.merchant_invoice_id::text = rgs_latest.merchant_invoice_id::text AND
							   rgs.merchant_customer_id::text = rgs_latest.merchant_customer_id::text AND
                 COALESCE(rgs.rebill_cancel_request_date::timestamp with time zone,
                          rgs.rebill_last_updated_date::timestamp with time zone,
                          rgs.rebill_start_date::character varying::timestamp with time zone) =
                 rgs_latest.rebill_last_updated_date_latest::timestamp with time zone
		GROUP BY 11,12;
