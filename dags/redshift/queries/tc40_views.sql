-- ft_checkout
CREATE OR REPLACE VIEW tc40.ft_checkout AS
SELECT
        'LIMELIGHT' as platform,
        'CHECKOUT' as tc40_bank,
        CASE WHEN (fraud_report_date LIKE '%/%')  THEN TO_DATE(split_part(fraud_report_date, ' ', 1), '%m/%d/%Y')
            WHEN (date_formatted LIKE '%/%')  THEN COALESCE(TO_DATE(split_part(date_formatted, ' ', 1), '%m/%d/%Y'), TO_DATE(split_part(date_formatted, ' ', 1), '%d/%m/%Y'))
            WHEN (dummy1 LIKE '%/%')  THEN COALESCE(TO_DATE(split_part(dummy1, ' ', 1), '%m/%d/%Y'), TO_DATE(split_part(dummy1, ' ', 1), '%d/%m/%Y'))
            WHEN (dummy2 LIKE '%/%')  THEN COALESCE(TO_DATE(split_part(dummy2, ' ', 1), '%m/%d/%Y'), TO_DATE(split_part(dummy2, ' ', 1), '%d/%m/%Y'))
            ELSE NULL END as tc40_date,
        trim(track_id) as order_id
from athenatc40.checkout
WHERE fraud_report_date <> '' AND fraud_report_date <> 'fraudreportdate'
UNION ALL
SELECT
        platform,
        tc40_bank,
        date(tc40_date) as tc40_date,
        CASE WHEN order_id='' THEN NULL else order_id end as order_id
from tc40.checkout_historical
WITH NO SCHEMA BINDING;

-- ft_concardis
CREATE OR REPLACE VIEW tc40.ft_concardis AS
SELECT
    DISTINCT 'LIMELIGHT' platform,
    'CONCARDIS' tc40_bank,
    CASE WHEN "pv"."fraud report date" like '%.%' THEN COALESCE(TO_DATE(split_part(trim(pv."fraud report date"), ' ', 1), 'DD.MM.YYYY'), TO_DATE(split_part(trim(pv."fraud report date"), ' ', 1), 'MM.DD.YYYY')) ELSE NULL END AS tc40_date,
    CAST(ll.order_id as varchar) order_id
FROM athenatc40.concardis pv
left join datawarehouse.order_header ll
ON TO_DATE(split_part(ll.time_stamp, ' ',1), 'YYYY-MM-DD') || split_part(upper(ll.gateway_descriptor), '.COM', 1) || ll.cc_first_6 || ll.cc_last_4 =
   COALESCE(TO_DATE(split_part(trim(pv.umsatzdatum),' ', 1), 'DD.MM.YYYY'), TO_DATE(split_part(trim(pv.umsatzdatum),' ', 1), 'MM.DD.YYYY')) || trim(split_part(upper(pv.merchantreference), '.COM', 1)) || substring(pv.cardnumber, 1, 6) || substring(pv.cardnumber,length(pv.cardnumber) - 3, 4)
WHERE upper(ll.bank_name) LIKE '%CONCARDIS%' and pv.umsatzdatum<>'Umsatzdatum'
WITH NO SCHEMA BINDING;

-- ft_emp

CREATE OR REPLACE VIEW tc40.ft_emp AS
WITH emp_temp AS (
    SELECT
        'LIMELIGHT' "System",
        'EMP' "TC40_BANK",
        COALESCE(TO_DATE("import date", 'MM/DD/YYYY'), TO_DATE("import date", 'DD/MM/YYYY')) tc40_date,
        "Transaction ID" transaction_id
    FROM athenatc40.emp_previous_periods
    UNION ALL
    SELECT
        'LIMELIGHT' "System",
        'EMP' "TC40_BANK",
        DATE("import date") tc40_date,
        "Transaction ID" transaction_id
    FROM athenatc40.emp
    UNION ALL
    SELECT
        'LIMELIGHT' "System",
        'EMP' "TC40_BANK",
        DATE("import date") tc_40_date,
       "Transaction ID" transaction_id
   FROM athenatc40.emp_mar2019_aug2019
)
SELECT "system", tc40_bank, tc40_date, CAST(oh.order_id As varchar) as order_id
FROM emp_temp emp left join datawarehouse.order_header oh ON emp.transaction_id = oh.transaction_id
WITH NO SCHEMA BINDING;

-- ft_nets

CREATE OR REPLACE VIEW tc40.ft_nets AS
SELECT
		'LIMELIGHT' as "system", 'NETS' as tc40_bank,
		CASE WHEN fraud_report_date like '%/%' and length(split_part(split_part(fraud_report_date, ' ',1), '/', 1)) <= 2 THEN to_date(split_part(fraud_report_date, ' ', 1), 'MM/DD/YY', false)
		WHEN fraud_report_date like '%/%' and length(split_part(split_part(fraud_report_date, ' ',1), '/', 1)) > 2 THEN to_date(split_part(fraud_report_date, ' ', 1), 'YYYY/MM/DD', false)
		WHEN fraud_report_date like '%-%' THEN to_date(split_part(fraud_report_date, ' ', 1), 'YYYY-MM-DD', false) ELSE NULL END tc40_date,
		CAST(oh.order_id as varchar) as order_id
from tc40.dim_net
left join datawarehouse.order_header oh
ON CASE WHEN trx_date like '%/%' and length(split_part(split_part(trx_date, ' ',1), '/', 1)) <= 2 THEN to_date(split_part(trx_date, ' ', 1), 'MM/DD/YY', false)
		WHEN trx_date like '%/%' and length(split_part(split_part(trx_date, ' ',1), '/', 1)) > 2 THEN to_date(split_part(trx_date, ' ', 1), 'YYYY/MM/DD', false)
		WHEN trx_date like '%-%' THEN to_date(split_part(trx_date, ' ', 1), 'YYYY-MM-DD', false) ELSE NULL END || CASE WHEN upper(merchant) like '%.COM%' THEN trim(split_part(upper(merchant), '.COM', 1))
			 WHEN upper(merchant) like '% COM%' THEN trim(split_part(upper(merchant), ' ', 1))
		ELSE NULL END || SUBSTRING(trim(pan), 1, 6) || SUBSTRING(trim(pan), length(trim(pan)) - 3, 4) = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '')|| oh.cc_first_6 || oh.cc_last_4
		WITH NO SCHEMA BINDING;


-- ft_paynetics


CREATE OR REPLACE VIEW tc40.ft_paynetics AS
SELECT
    'LIMELIGHT' "System",
    'EMP - PAYNETICS' "TC40_BANK",
    DATE(split_part(report_date, ' ', 1)) tc40_date,
    CAST(b.order_id as varchar) order_id
FROM athenatc40.dim_paynetics a
LEFT JOIN datawarehouse.order_header b
ON a.merchant_transaction_id = b.transaction_id
WITH NO SCHEMA BINDING;


-- ft_paysafe

CREATE OR REPLACE VIEW tc40.ft_paysafe AS
SELECT "system", tc40_bank, TC_40_DATE tc40_date, CAST(oh.order_id as varchar) order_id
FROM
(SELECT
    'LIMELIGHT' "System", 'PAYSAFE' "TC40_BANK", CAST("Optimal_Payments_ID" AS varchar) transaction_id, COALESCE(TO_DATE(date, 'MM/DD/YYYY'), TO_DATE(date, 'DD/MM/YYYY')) "TC_40_DATE"
FROM athenatc40.dim_paysafe_new_112019
WHERE length(trim(CAST("Optimal_Payments_ID" AS varchar))) > 0
union
SELECT
    'LIMELIGHT' "System", 'PAYSAFE' "TC40_BANK", CAST("Optimal_Payments_ID" AS varchar) transaction_id, COALESCE(TO_DATE(date, 'MM/DD/YYYY'), TO_DATE(date, 'DD/MM/YYYY')) "TC_40_DATE"
FROM athenatc40.dim_paysafe_new_112019
WHERE length(trim(CAST("Optimal_Payments_ID" AS varchar))) > 0) a
left join datawarehouse.order_header oh
ON a.transaction_id = oh.transaction_id
WITH NO SCHEMA BINDING;


-- ft_payvision

CREATE OR REPLACE VIEW tc40.ft_payvision AS
WITH payvision AS (
    SELECT
        'LIMELIGHT' "System", 'PAYVISION' tc40_bank , COALESCE(TO_DATE(replace(trim(split_part(trim("CreateDate"), ' ', 1)), '"', ''), 'DD/MM/YYYY'), TO_DATE(replace(trim(split_part(trim("CreateDate"), ' ', 1)), '"', ''), 'MM/DD/YYYY')) "TC_40_Date", oh.order_id order_id
    FROM athenatc40.payvision_exports_transaction pv
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(pv.customcode as varchar) = CAST(oh.transaction_id as varchar)
    WHERE length(pv.customcode) > 1
    UNION
    SELECT
        'LIMELIGHT' "System", 'PAYVISION' "TC40_BANK", COALESCE(TO_DATE(trim(substring("FraudReportDate", 2, 9)), 'MM/DD/YYYY'), TO_DATE(trim(substring("FraudReportDate", 2, 9)), 'DD/MM/YYYY')) "TC_40_Date", oh.order_id order_id
    FROM athenatc40.payvision_exports_cc pv
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(COALESCE(TO_DATE(trim(substring("CreateDate", 2, 9)), 'MM/DD/YYYY'), TO_DATE(trim(substring("CreateDate", 2, 9)), 'DD/MM/YYYY')) AS varchar) || replace(split_part(upper(pv.dyndescname), '.COM', 1), '"', '') || replace(pv.firstsixdigits, '"', '') || replace(pv.lastfourdigits, '"', '')
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    WHERE oh.bank_name LIKE '%PAYVISION%'
    UNION
    SELECT
        'LIMELIGHT' "System", 'PAYVISION' "TC40_BANK", COALESCE(TO_DATE(replace(trim(split_part(trim(pv.reporteddate), ' ', 1)), '"', ''), 'MM/DD/YYYY'), TO_DATE(replace(trim(split_part(trim(pv.reporteddate), ' ', 1)), '"', ''), 'DD/MM/YYYY')) "TC_40_Date", oh.order_id order_id
    FROM athenatc40.payvision_sftp pv
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(COALESCE(TO_DATE(replace(split_part(trim(pv.transactiondate), ' ', 1), '"', ''), 'MM/DD/YYYY'), TO_DATE(replace(split_part(trim(pv.transactiondate), ' ', 1), '"', ''), 'DD/MM/YYYY')) AS varchar) || replace(split_part(upper(pv.descriptor), '.COM', 1), '"', '') || replace(pv.firstsixdigits, '"', '') || replace(pv.lastfourdigits, '"', '')
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    WHERE upper(oh.bank_name) LIKE '%PAYVISION%'
)
SELECT "system", tc40_bank, TC_40_Date as tc40_date, CAST(order_id as varchar) order_id FROM payvision
WITH NO SCHEMA BINDING;


-- ft_vp

CREATE OR REPLACE VIEW tc40.ft_vp AS
WITH vp AS (
    SELECT
        'LIMELIGHT' "System", 'VP' "TC40_BANK", COALESCE(TO_DATE(split_part(trim(vp_st.run_date), ' ', 1), 'MM/DD/YY'), TO_DATE(split_part(trim(vp_st.run_date), ' ', 1), 'DD/MM/YY')) "TC_40_Date", oh.order_id order_id
    FROM athenatc40.vp_stfs_xls vp_st
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(COALESCE(TO_DATE(split_part(trim(vp_st.purchase_date), ' ', 1), 'MM/DD/YY'), TO_DATE(split_part(trim(vp_st.purchase_date), ' ', 1), 'DD/MM/YY')) as varchar) || split_part(upper(trim(merchant_name)), '.COM', 1) || SUBSTRING(vp_st.account_number, 1, 6) || SUBSTRING(vp_st.account_number, ("length"(vp_st.account_number) - 3), 4)
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    WHERE vp_st.run_date <> '' AND oh.bank_name LIKE '%VP%' and oh.order_status in (2, 6, 8) and vp_st.purchase_date like '%/%'
    UNION
    SELECT
        'LIMELIGHT' "System", 'VP' "TC40_BANK", COALESCE(TO_DATE(split_part(trim(vp_tv.posted_dt), ' ', 1), 'MM/DD/YY'), TO_DATE(split_part(trim(vp_tv.posted_dt), ' ', 1), 'DD/MM/YY')) "TC_40_Date", oh.order_id order_id
    FROM athenatc40.vp_truevo_limelight vp_tv
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(COALESCE(TO_DATE(split_part(trim(vp_tv.trans_date), ' ', 1), 'MM/DD/YY'), TO_DATE(split_part(trim(vp_tv.trans_date), ' ', 1), 'DD/MM/YY')) as varchar) || split_part(upper(trim(merchant)), '.COM', 1) || substring(vp_tv.account_number, 1, 6) || substring(vp_tv.account_number, (length(vp_tv.account_number) - 3), 4)
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    UNION
    SELECT
        'LIMELIGHT' "System", 'VP' "TC40_BANK", TO_DATE(vp_email."date reported fraud", 'YYYYMMDD') "TC_40_Date", oh.order_id
    FROM athenatc40.vp_truevo_email_export vp_email
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(TO_DATE(vp_email."original transaction date", 'YYYYMMDD') as varchar) || split_part(upper(trim("billing descriptor line 1")), '.COM', 1) || CAST(vp_email.bin as varchar) || CAST(vp_email."last 4" as varchar)
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    UNION
    SELECT
        'LIMELIGHT' "System", 'VP' "TC40_BANK", TO_DATE(vp_vi.date_received, 'DD-Mon-YY') "TC_40_Date", oh.order_id
    FROM athenatc40.vp_truevo_vi vp_vi
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(to_date(vp_vi.txn_date, 'DD-Mon-YY') as varchar) || split_part(upper(trim(merchant)), '.COM', 1) || SUBSTRING(vp_vi.card_number, 1, 6) || SUBSTRING(vp_vi.card_number, length(vp_vi.card_number) - 3, 4)
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    WHERE oh.bank_name LIKE '%VP%'
    UNION
    SELECT
        'LIMELIGHT' "System", 'VP' "TC40_BANK", COALESCE(TO_DATE(split_part(trim(vp_st.run_date), ' ', 1), 'MM-DD-YY'), TO_DATE(split_part(trim(vp_st.run_date), ' ', 1), 'DD-MM-YY')) "TC_40_Date", oh.order_id
    FROM athenatc40.vp_stfs_onetime vp_st
    LEFT JOIN datawarehouse.order_header oh
    ON CAST(COALESCE(TO_DATE(split_part(trim(vp_st.purchase_date), ' ', 1), 'MM/DD/YY'), TO_DATE(split_part(trim(vp_st.purchase_date), ' ', 1), 'DD/MM/YY')) as varchar) || split_part(upper(trim(merchant_name)), '.COM', 1) || SUBSTRING(vp_st.account_number, 1, 6) || SUBSTRING(vp_st.account_number, ("length"(vp_st.account_number) - 3), 4)
        = CAST(DATE(oh.time_stamp) as varchar) || replace(upper(split_part(oh.gateway_descriptor, '.COM', 1)), 'Â€Ž', '') || oh.cc_first_6 || oh.cc_last_4
    WHERE oh.bank_name LIKE '%VP%'
)
SELECT "system", tc40_bank, "TC_40_Date" as tc40_date, CAST(order_id as varchar) as order_id from vp
WITH NO SCHEMA BINDING;