
-- ########################## Dims - Mundu ###########################
-- subscriptions:   
select
    subscription_disabled,
    product_shuffle_group_name,
    order_product_id as main_product_id,
    product_name as main_product_name,
    subscription_country,
    active as subscription_group_active,
    subscription_group_name ,
    subscription_group,
    dim_g.subscription_bank_name
from mundu.dim_subscriptions_v dim_s
left join (select distinct id as subscription_gateway, bank_name as subscription_bank_name from mundu.dim_gateways_v) dim_g
on dim_s.subscription_gateway = dim_g.subscription_gateway;

-- gateways:
SELECT
    qlik_gateway_id as order_gateway_id,
    descriptor as gateway_descriptor,
    currency as gateway_currency,
    active as is_gateway_active,
    1 as is_gateway_live,
    CASE WHEN bank_name in ('DUMMY' ,'EMP - BOR','BILLPRO - CHINA','EPRO') THEN 1 ELSE 0 END as exclude_mid,
    CASE WHEN (descriptor <> '') IS TRUE THEN upper(descriptor || '-' || bank_name) ELSE NULL END as mid_name,
    name as gateway_name,
    bank as bank_id,
    bank_name,
    brand,
	is_3ds as is_gateway_3ds,
    first_time_live_date as gateway_first_live_date,
    mcc
FROM mundu.dim_gateways_v;


-- campaigns
SELECT
    dim_c.limelight_id as campaign_id,
    dim_c.name as campaign_name,
    dim_d.domain as domain,
    dim_d.campaign_domain as campaign_domain
FROM athenamundu.campaigns dim_c
Left Join
(
    SELECT
        hostname_id,
        domain as domain,
        campaign_domain
    FROM mundu.dim_domain_v
) dim_d ON dim_d.hostname_id = dim_c.hostname;

-- company_country
SELECT
    company_name,
    company_country
FROM mundu.dim_company_vat_v;

-- ########################## Dims - old gateways ###########################


-- ########################## Dims - Misc ###########################
-- category:
 Load * Inline [
main_product_category, category_desc
2    , 'Entertainment (Bundle)'
7    , 'Entertainment (Single/Music)'
8    , 'Entertainment (Single/Games)'
9    , 'Entertainment (Shopper)'
3    , 'Donation'
4    , 'Lifestyle (Shopper)'
5    , 'Lifestyle (Trainer)'
6    , 'Lifestyle (Skincare)'
11    , 'Lifestyle Shop (Perfumes)'
12    , 'Lifestyle Shop (Cosmetics)'
13    , 'Lifestyle Shop (Skincare)'
14    , 'Lifestyle Shop (Hair)'
15    , 'Lifestyle Shop (Sports/Fitness)'
16    , 'Lifestyle Shop (General)'
17    , 'Entertainment (Shopper/Games)'
18    , 'Entertainment (Shopper/Audio)'
19    , 'Entertainment (Shopper/Tablet)'
20    , 'Entertainment (Shopper/Laptop)'
21    , 'Entertainment (Shopper/TV)'
10    , 'Private Billing'
9999  , 'Nutra Entertainment'
];

-- business_unit:
 Load * Inline [
main_product_category, business_unit
2    ,'Entertainment'
7    ,'Entertainment'
8    ,'Entertainment'
9    ,'Entertainment'
3    ,'Donation'
4    ,'Lifestyle'
5    ,'Lifestyle'
6    ,'Lifestyle'
11    ,'Lifestyle'
12    ,'Lifestyle'
13    ,'Lifestyle'
14    ,'Lifestyle'
15    ,'Lifestyle'
16    ,'Lifestyle'
17    ,'Entertainment'
18    ,'Entertainment'
19    ,'Entertainment'
20    ,'Entertainment'
21    ,'Entertainment'
10    ,'Private Billing'
9999  , 'Entertainment'
];


-- is_salvage_sale_desc:
Load * Inline [
is_salvage_sale, is_salvage_sale_desc
0    ,'No'
1    ,'Cancellation Salvage'
2    ,'BB Salvage'
];

-- is_cross_sale_desc:
Load * Inline [
is_cross_sale, is_cross_sale_desc
0    ,'No'
1    ,'Cross Sale'
2    ,'Exit Sale'
];

-- blocked_bins:
SELECT
	COALESCE(bindb.cc_first_6,binex.bin) as cc_first_6,
    binex.bin as cc_blocked_bin,
    binex.bin || binex.business_unit as cc_blocked_bin_unit,
    binex.country as cc_blocked_bin_country,
    binex.business_unit as cc_blocked_business_unit,
    CASE WHEN binex.bin is null THEN bindb.cc_blocked_bin_flag 
            ELSE CASE WHEN (binex.action <> '') IS NOT TRUE THEN NULL ELSE binex.action END 
        END as cc_blocked_bin_flag,
    binex.mundu_list as cc_blocked_bin_mundu_list,
    to_date(binex.action_date, 'DD-Mon-YY', false) as cc_blocked_bin_action_date,
    binex.reason as cc_blocked_bin_action_reason,
    binex.old_action as cc_old_blocked_bin_flag,
    to_date(binex.old_action_date, 'DD-Mon-YY', false) as cc_old_blocked_bin_action_date,
    binex.old_reason as cc_old_blocked_bin_action_reason,
    binex.monitored as cc_blocked_bin_monitored
FROM athenabindb.bin_exclusion binex
right join
     (
         SELECT bin               as cc_first_6,
                'Ok'              as cc_blocked_bin_flag,
                'No Action taken' as cc_blocked_business_unit
         FROM athenabindb.bin_db
     ) bindb ON binex.bin = bindb.cc_first_6


-- dim_bins:
SELECT 
	bin as cc_first_6,
    card_type as cc_type,
    CASE WHEN upper(card_type_2) LIKE '%PREPAID%' THEN 1 ELSE 0 END as cc_is_prepaid,
    debit_credit as cc_debit_credit,
    bank_name as cc_bank_name,
    country_code as cc_country_code
FROM athenabindb.bin_db

-- ########################## Subscription cap ###########################
-- subscription_cap_status:
SELECT
    subscription_group_name,
    monthly_cap as subscription_monthly_cap,
    monthly_cap_status as subscription_monthly_cap_status
FROM central.subscription_group_cap_view limit 10;



-- ########################## Orders ###########################
-- orders:
WITH orders as
(SELECT
  	cc_first_6 || cc_last_4 || cc_expires as cc_first_6_last_4_expires, --1
    order_gateway_id as order_gateway_id_source,
    order_gateway_id, --1 -- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id), order_gateway_id), order_gateway_id) as order_gateway_id,
    -- vat_date,   -- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_vat_date', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),vat_date), vat_date), vat_date) as vat_date,   
    date(acquisition_date) as acquisition_date, --1
    affiliate_id, --1
    -- amount_refunded_to_date,
    ancestor_id, --1
    billing_city, --1
    billing_country, --1
    billing_cycle, --1
    billing_first_name, --1
    billing_last_name, --1
    billing_postcode, --1
    billing_state, --1
    billing_street_address, --1
    campaign_id, --1
    cc_expires, --1
    cc_first_6, --1
    cc_last_4,
    -- cc_type,
    -- chargeback_date,
    current_rebill_discount_percent, --1
    customer_id, --1
    customers_telephone, --1
    decline_reason, --1
    decline_salvage_discount_percent, --1
    email_address, --1
    -- hold_date,
    ip_address, --1
    -- is_recurring,
    -- is_test_cc,
    -- is_void,
    main_product_id, --1
    main_product_quantity, --1
    on_hold, --1
    on_hold_by, --1
    order_confirmed,
    -- date(time_stamp) as date, --1
    order_id, --1
    order_status, --1
    order_total, --1
    parent_id, --1
    rebill_discount_percent, --1
    recurring_date, --1
    refund_amount, --1
    -- refund_date,
    -- retry_date,
    -- shipping_date,
    sub_affiliate, --1
    transaction_id, --1
    upsell_product_id, --1
    upsell_product_quantity, --1
    void_amount, --1
    -- void_date,
    is_create_order, --1
    -- order_confirmed_date,
    -- system_notes_last_date,
    cascade_parent_order_id, --1
    is_cascade, --1
    migration_parent_order_id, --1
    is_migration, --1
    cross_sale_parent_order_id, --1
    -- is_cross_sale,
    -- reserve_release_date,
    date(time_stamp) as order_date,
    is_test_order,
    -- gateway_first_live_date,
    -- gateway_descriptor,
    order_currency,
    -- is_gateway_3ds,
    -- brand_id,
    -- bank_id,
    bank_name,
    company_name,-- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_comp', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),company_name), company_name), company_name) as company_name,
    vat_number,-- if(bank_id=20, if(ApplyMap('map_vp_stfs_old_new_gtw_date', order_gateway_id,time_stamp)<time_stamp, ApplyMap('map_vp_stfs_old_new_gtw_vat_num', ApplyMap('map_vp_stfs_old_new_gtw', order_gateway_id),vat_number), vat_number), vat_number) as vat_number,
    round(order_total_eur,2) as order_total_eur,
    round(refund_amount_eur,2) as refund_amount_eur,
    -- main_product_name,
    CASE WHEN date(acquisition_date) >= '2020-07-26' 
            THEN CASE WHEN main_product_category in (4,5,6,11,12,13,14,15,16) THEN 9999 ELSE main_product_category END
            ELSE main_product_category 
        END as main_product_category,
    trial_days,
    is_lead_rebill_donation,
    -- is_trial_over,
    cost_per_lead,
    cost_per_lead_currency,
    is_cpa_missing,
    round(cost_per_lead_eur,2) as cost_per_lead_eur,
    -- hold_date_derived,
    -- affiliate_name,
    billing_continent_name,
    gateway_processing_percent,
    round( order_total_eur*(gateway_processing_percent/100), 3) as gateway_processing_fee_eur,
    gateway_reserve_percent,
    gateway_transaction_fee,
    gateway_charge_back_fee,
    round(reserve_taken_amount_eur,2) as reserve_taken_amount_eur,
    order_cc_key,
    -- is_reserve_released,
    cust_subscription_key,
    cust_lead_u_key,
    vat_percent,
    round(vat_amount_eur,2) as vat_amount_eur,
    -- has_first_rebill,
    counter,
    -- CASE WHEN upper(bank_name) in ('DUMMY' ,'EMP - BOR','BILLPRO - CHINA','EPRO') THEN 0 ELSE 1 end as Exclude_MID,
    is_straight_sale,
    case when is_straight_sale=1 THEN split_part(c2, '&sku=',2) ELSE '' end as straight_sale_product,
    is_salvage_sale,
    -- refund_fee,
    -- date(date_trunc('month',date(time_stamp)) as time_stamp_month,
    cascade_attempt
FROM datawarehouse.order_header
where is_test_order!=1 and is_test_order!=1)
SELECT
    'order' as transaction_type,
    order_date as date,
    *
FROM orders
UNION
SELECT
    'cancelled' as transaction_type,
    oc.hold_date as date,
    orders.*
FROM orders orders
INNER JOIN datawarehouse.cancellations oc on orders.order_id = oc.order_id
UNION
SELECT
    'void' as transaction_type,
    oc.void_date as date,
    orders.*
FROM orders orders
INNER JOIN datawarehouse.void ov on orders.order_id = ov.order_id
SELECT
    'refund' as transaction_type,
    oref.refund_date as date,
    oref.gateway_processing_percent as gateway_processing_percent,
    oref.gateway_reserve_percent as gateway_reserve_percent,
    oref.refund_fee as gateway_transaction_fee,
    oref.gateway_chargeback_fee as gateway_charge_back_fee
    orders.gateway_processing_fee_eur,
    orders.cc_first_6_last_4_expires as cc_first_6_last_4_expires,orders.order_gateway_id_source as order_gateway_id_source,orders.order_gateway_id as order_gateway_id,orders.acquisition_date as acquisition_date,orders.affiliate_id as affiliate_id,orders.ancestor_id as ancestor_id,orders.billing_city as billing_city,orders.billing_country as billing_country,orders.billing_cycle as billing_cycle,orders.billing_first_name as billing_first_name,orders.billing_last_name as billing_last_name,orders.billing_postcode as billing_postcode,
    orders.billing_state as billing_state,orders.billing_street_address as billing_street_address,orders.campaign_id as campaign_id,orders.cc_expires as cc_expires,orders.cc_first_6 as cc_first_6,orders.cc_last_4 as cc_last_4,orders.current_rebill_discount_percent as current_rebill_discount_percent,orders.customer_id as customer_id,orders.customers_telephone as customers_telephone,orders.decline_reason as decline_reason,orders.decline_salvage_discount_percent as decline_salvage_discount_percent,orders.email_address as email_address,orders.ip_address as ip_address,orders.main_product_id as main_product_id,orders.main_product_quantity as main_product_quantity,orders.on_hold as on_hold,orders.on_hold_by as on_hold_by,orders.order_confirmed as order_confirmed,orders.order_id as order_id,orders.order_status as order_status,orders.order_total as order_total,orders.parent_id as parent_id,orders.rebill_discount_percent as rebill_discount_percent,orders.recurring_date as recurring_date,
    orders.refund_amount as refund_amount,orders.sub_affiliate as sub_affiliate,orders.transaction_id as transaction_id,orders.upsell_product_id as upsell_product_id,orders.upsell_product_quantity as upsell_product_quantity,orders.void_amount as void_amount,orders.is_create_order as is_create_order,orders.cascade_parent_order_id as cascade_parent_order_id,orders.is_cascade as is_cascade,orders.migration_parent_order_id as migration_parent_order_id,orders.is_migration as is_migration,orders.cross_sale_parent_order_id as cross_sale_parent_order_id,orders.order_date as order_date,orders.is_test_order as is_test_order,orders.order_currency as order_currency,orders.bank_name as bank_name,orders.company_name as company_name,orders.vat_number as vat_number,orders.order_total_eur as order_total_eur,orders.refund_amount_eur as refund_amount_eur,orders.main_product_category as main_product_category,orders.trial_days as trial_days,orders.is_lead_rebill_donation as is_lead_rebill_donation,
    orders.cost_per_lead as cost_per_lead,orders.cost_per_lead_currency as cost_per_lead_currency,orders.is_cpa_missing as is_cpa_missing,orders.cost_per_lead_eur as cost_per_lead_eur,orders.billing_continent_name as billing_continent_name,orders.reserve_taken_amount_eur as reserve_taken_amount_eur,orders.order_cc_key as order_cc_key,orders.cust_subscription_key as cust_subscription_key,orders.cust_lead_u_key as cust_lead_u_key,orders.vat_percent as vat_percent,orders.vat_amount_eur as vat_amount_eur,orders.counter as counter,orders.is_straight_sale as is_straight_sale,orders.straight_sale_product as straight_sale_product,orders.is_salvage_sale as is_salvage_sale,orders.cascade_attempt as cascade_attempt
FROM orders orders
INNER JOIN datawarehouse.refunds_with_bank_dedcutions oref on orders.order_id = oref.order_id