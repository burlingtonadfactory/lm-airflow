create or replace view rocketgate.cascades_view(cascade_tr_id, cascaded_from_tr_id, cascaded_due_to) as
	SELECT casc1.txn_id                   AS cascade_tr_id,
       casc2.txn_id                   AS cascaded_from_tr_id,
       casc2.bank_reponse_description AS cascaded_due_to
FROM (SELECT transactions.inv_id_ext            AS invoice_id,
             transactions.tr_id                 AS txn_id,
             CASE
                 WHEN (transactions.tr_id_cascaded IS NOT NULL AND transactions.tr_id_cascaded::text <> 0::text AND
                      transactions.tr_3d_eci IS NULL) OR
                      json_extract_path_text(transactions.tui_udf02::text, 'c3d'::character varying::text, true) =
                      '1'::character varying::text OR
                      json_extract_path_text(transactions.tui_udf02::text, 'c3d'::character varying::text, true) =
                      '0'::character varying::text THEN 1
                 ELSE 0
                 END                            AS is_cascade,
             CASE
                 WHEN transactions.reason_desc::text = 'Success'::character varying::text THEN NULL::character varying
                 ELSE COALESCE(
                         CASE
                             WHEN transactions.bank_response::text = '0'::character varying::text OR
                                  transactions.bank_response::text = '10000'::character varying::text
                                 THEN transactions.reason_desc
                             ELSE COALESCE(transactions.bank_response_desc, transactions.reason_desc)
                             END)
                 END                            AS bank_reponse_description,
             pg_catalog.row_number()
             OVER (
                 PARTITION BY transactions.inv_id_ext
                 ORDER BY transactions.tr_date) AS txn_seq_no
      FROM rocketgate.transactions) casc1
         JOIN (SELECT transactions.inv_id_ext            AS invoice_id,
                      transactions.tr_id                 AS txn_id,
                      CASE
                          WHEN (transactions.tr_id_cascaded IS NOT NULL AND
                               transactions.tr_id_cascaded::text <> 0::text AND transactions.tr_3d_eci IS NULL) OR
                               json_extract_path_text(transactions.tui_udf02::text, 'c3d'::character varying::text,
                                                      true) = '1'::character varying::text OR
                               json_extract_path_text(transactions.tui_udf02::text, 'c3d'::character varying::text,
                                                      true) = '0'::character varying::text THEN 1
                          ELSE 0
                          END                            AS is_cascade,
                      CASE
                          WHEN transactions.reason_desc::text = 'Success'::character varying::text
                              THEN NULL::character varying
                          ELSE COALESCE(
                                  CASE
                                      WHEN transactions.bank_response::text = '0'::character varying::text OR
                                           transactions.bank_response::text = '10000'::character varying::text
                                          THEN transactions.reason_desc
                                      ELSE COALESCE(transactions.bank_response_desc, transactions.reason_desc)
                                      END)
                          END                            AS bank_reponse_description,
                      pg_catalog.row_number()
                      OVER (
                          PARTITION BY transactions.inv_id_ext
                          ORDER BY transactions.tr_date) AS txn_seq_no
               FROM rocketgate.transactions) casc2
              ON casc1.invoice_id::text = casc2.invoice_id::text AND casc1.txn_seq_no = (casc2.txn_seq_no + 1)
WHERE casc1.is_cascade = 1;
