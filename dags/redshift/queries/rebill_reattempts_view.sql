create or replace view rocketgate.rebill_reattempts_view as
  select rg_reattempts_1.txn_id as reattempt_txn_id,rg_reattempts_1.reason_desc as reattempt_reason_desc from
  (
      SELECT rgt.inv_id_ext            AS invoice_id,
          rgt.tr_id                 AS txn_id,
          rgt.resp_id,
          rgt.reason_desc,
               row_number()
               OVER (
                   PARTITION BY rgt.inv_id_ext
                   ORDER BY rgt.tr_date) AS txn_seq_no
        FROM rocketgate.transactions rgt
      inner join (select distinct inv_id_ext from rocketgate.transactions where resp_id='113') inv_reattempts
      on rgt.inv_id_ext =inv_reattempts.inv_id_ext
      where billing_type in ('C','R') and ttype_name in ('SALE','TICKET')
  ) rg_reattempts_1
  inner join
  (
      SELECT rgt.inv_id_ext            AS invoice_id,
          rgt.tr_id                 AS txn_id,
          rgt.resp_id,
          rgt.reason_desc,
               row_number()
               OVER (
                   PARTITION BY rgt.inv_id_ext
                   ORDER BY rgt.tr_date) AS txn_seq_no
        FROM rocketgate.transactions rgt
      inner join (select distinct inv_id_ext from rocketgate.transactions where resp_id='113') inv_reattempts
      on rgt.inv_id_ext =inv_reattempts.inv_id_ext
      where billing_type in ('C','R') and ttype_name in ('SALE','TICKET')
  ) rg_reattempts_2
  on rg_reattempts_1.invoice_id = rg_reattempts_2.invoice_id and rg_reattempts_1.txn_seq_no = (rg_reattempts_2.txn_seq_no + 1)
  where rg_reattempts_2.resp_id='113'
WITH NO SCHEMA BINDING;
