CREATE OR REPLACE VIEW rocketgate.rg_overview AS
select distinct rgt.tr_id                                                                     as     order_id,
				rgt.product_id                                                                as 	 product_id,
				CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T' and is_cross_sale = 1
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    cross_sale_leads,
                CASE
                    WHEN cb.category is NOT NULL
						THEN date(cb.report_date)
                    WHEN rg_disputes.category is NOT NULL
						THEN date(rg_disputes.report_date)
                    else date(tr_date) END                                                    as     order_date,
                date(tr_date)                                                                 as     tr_date,
                date(lead_tr_date)                                                            as     acquisition_date,
				CASE
					WHEN ttype_name in ('CREDIT') and resp_id = '0' THEN date(tr_date)
					ELSE refund_orig_date END 										  		  as     refund_date,
				CASE
					WHEN cb.category in ('CHARGEBACK') THEN date(cb.report_date)
					ELSE NULL END															  as 	 chargeback_date,
                CASE
                    WHEN cb.category is NOT NULL
						THEN cb.report_date
                    WHEN rg_disputes.category is NOT NULL THEN rg_disputes.report_date
                    else tr_date END                                                          as     order_timestamp,
                COALESCE(cb.category, rg_disputes.category, 'ORDER')             as     transaction_category,
                rgt.inv_id_ext                                                                as     invoice_id,
                CASE WHEN paynetics_invoice_id IS NOT NULL THEN 1 ELSE 0 END                  as     paynetics_errored_invoice, --To be removed after Oct2021
                bill_cyc.billing_cycle                                                        as     billing_cycle,
                rgt.email,
                rgt.cust_id_ext                                                               as     customer_id,
                rgt.fname                                                                     as     first_name,
                rgt.lname                                                                     as     last_name,
                rgt.tui_ip                                                                    as     ip_address,
                leads_rgt.lead_tui_ip                                                         as     lead_ip_address,
                CASE
                    WHEN leads_rgt.lead_tui_ip LIKE '%:%' THEN 'IPv6'
                    WHEN leads_rgt.lead_tui_ip LIKE '%.%' THEN 'IPv4'
                    ELSE NULL
                    END                                                                       as     ip_version,
                rgt.tui_posting_ip                                                            as     posting_ip_address,
                ttype_name                                                                    as     transaction_type,
                rgt.macct_name                                                                as     merchant_account_name,
                -- B: Adding is_cross_sale field : is_cross_sale_table
                case when is_cross_sale_table.inv_id_ext is not null then 1 else 0 end        as      is_cross_sale,
                -- B: End adding is_cross_sale field
                CASE
                    WHEN leads_rgt.lead_cv is not null AND leads_rgt.lead_cv != ''
                        THEN
                        CASE
                            WHEN leads_rgt.lead_cv = 'SWEEPSTAKE' THEN 'SWEEPS'
                            WHEN leads_rgt.lead_cv = 'FREE-TRIAL' THEN 'FREE TRIAL'
                            ELSE leads_rgt.lead_cv
                            END
                    WHEN bigbuy.bigbuy_transaction_id is not null then 'BIGBUY'
                    WHEN (site_id = '1' and date(lead_tr_date) < '2021-01-31') or
                         efc.relationship_advertiser_name = 'Straight Sales' or
                         (billing_type = 'S' and lead_tui_udf02 is NULL) or (merch_id = '1601058492')
                        THEN 'STRAIGHT SALES'
                    WHEN leads_rgt.lead_mo = '3895' THEN 'FREE TRIAL'
                    WHEN leads_rgt.lead_mo in ('3935', '3936') THEN 'ECOM'
                    WHEN (site_id = '3' and date(lead_tr_date) < '2021-01-25') or
                         cc.vertical_name = 'Prize Offers' or
                         efc.relationship_advertiser_name like 'Prize Offers%' or
                         (billing_type in ('T', 'C', 'R', 'I') and lead_tui_udf02 is NULL)
                        THEN 'SWEEPS'
                    WHEN efc.relationship_advertiser_name is not null THEN efc.relationship_advertiser_name
                    WHEN cc.vertical_name is not null THEN cc.vertical_name

                    ELSE 'OTHER' END                                                          as     vertical,
                rgt.merch_id                                                                  as     rg_vertical_id,
                CASE
                    WHEN billing_type = 'T' THEN 'LEADS'
                    WHEN billing_type = 'S' THEN 'SALE'
                    WHEN billing_type = 'C' THEN '1st REBILL'
                    WHEN billing_type in ('C', 'R') THEN 'REBILL'
                    ELSE 'OTHER' END                                                          as     billing_type,
                bindb.debit_credit															  as	 bank_card_category,
                card_name,
                tui_bill_country                                                              as     billing_country,
                rgt.descriptor                                                                as     gateway_descriptor,
                jira_mids.mid_name                                                            as     mid,
                jira_mids.mid_jira_id                                                         as     mid_jira_id,
                jira_mids.domain_name                                                         as     domain_name,
                jira_mids.company_name                                                        as     company_name,
                jira_mids.company_country                                                     as     company_country,
                jira_mids.bank                                                                as     jira_bank,
                CASE COALESCE(CASE
                                  WHEN rgt.macct_name LIKE 'VIRI%' THEN split_part(rgt.macct_name, '-', 2)
                                  ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                    END)
                    WHEN 'CON' THEN 'CONCARDIS'
                    WHEN 'WLD' THEN 'WORLDLINE'
                    WHEN 'WL' THEN 'WORLDLINE'
					WHEN 'WL2' THEN 'WORLDLINE'
                    WHEN 'WOR' THEN 'WORLDLINE'
                    WHEN 'PAYSAFE' THEN 'PAYSAFE'
                    WHEN 'CON3DS' THEN 'CONCARDIS'
                    WHEN 'KOR' THEN 'KORTA'
                    WHEN 'ECP' THEN 'ECP'
                    WHEN 'BS' THEN 'B&S'
                    WHEN 'NET' THEN 'NETS'
                    WHEN 'BSP' THEN 'B&S'
                    WHEN 'NETS' THEN 'NETS'
                    WHEN 'CHK' THEN 'CHECKOUT'
                    WHEN 'NET3DS' THEN 'NETS'
                    WHEN 'PSUSD' THEN 'PAYSAFE'
                    WHEN 'PSEUR' THEN 'PAYSAFE'
                    WHEN 'STFS' THEN 'SECURE TRADING FINANCIAL SERVICES (STFS)'
                    WHEN 'PT' THEN 'PEOPLE\'S TRUST'
                    WHEN 'PNT' THEN 'PAYNETICS'
                    WHEN 'PYNT' THEN 'PAYNT'
                    ELSE COALESCE(CASE
                                      WHEN rgt.macct_name LIKE 'VIRI%' THEN split_part(rgt.macct_name, '-', 2)
                                      ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                        END)
                    END                                                                       as     bank,
                rgt.bankbin                                                                   as     bank_bin,
                rgt.bankbin                                                                   as     cc_first_6,
                rgt.tr_pay_num_l4                                                             as     cc_last_4,
                lpad(rgt.expiremonth, 2, '0') || rgt.expireyear                               as     cc_expire_date,
                rgt.bankbin || rgt.tr_pay_num_l4 || lpad(rgt.expiremonth, 2, '0') ||
                rgt.expireyear                                                                as     cc_first_6_last_4_expires,
                rgt.tui_udf01                                                                 as     cc_hash,
                bindb.issuing_bank                                                            as     issuing_bank,
                bindb.card_type_2                                                             as     card_type_2,
                rgt.curr_code_settled                                                         as     currency_settled,
				tr_retrievalno															  	  as 	 tr_retrievalno,
                jira_mids.status                                                              as     mid_status,
                jira_mids.monthlytransactionscap                                              as     mid_monthly_leads_cap,
                jira_mids.mid_first_live_date                                                 as     mid_first_live_date,
                jira_mids.labels                                                              as     jira_labels,
                regexp_substr(jira_mids.labels, 'Siteid.')                                    as     jira_label_site_id,
                CASE
                    WHEN upper(jira_mids.labels) like '%AUTOREFUNDFRAUD%' THEN 1
                    ELSE 0 END                                                                as     jira_label_autorefundfraud,
                CASE
                    WHEN upper(jira_mids.labels) like '%VERIFI%' THEN 'Verifi'
                    WHEN upper(jira_mids.labels) like '%ETHOCA%' THEN 'Ethoca'
                    ELSE NULL END                                                             as     jira_label_verifi_ethoca,
                jira_mids.mcc                                                                 as     jira_mcc,
                reason_desc                                                                   as     transaction_response,
                CASE
                    WHEN json_extract_path_text(rgt.tui_udf02, 'apc', true) !=''
                        THEN
                            CASE WHEN json_extract_path_text(rgt.tui_udf02, 'apc', true) = '1'
                                THEN    'Legal/Admin Fees'
                            ELSE NULL END
                    ELSE NULL END                                                             as     add_on,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name <> '3D_LOOKUP' THEN 'Approved'
                    WHEN ttype_name = '3D_LOOKUP' and tr_3d_eci in ('02', '05') THEN 'Approved'
                    ELSE 'Declined' END                                                       as     order_status,
                cb.rdr_outcome                                                                   as     rdr_status,
				cb.rdr_fee																	  as 	rdr_fee,
                CASE
                    WHEN tr_3d_eci in ('01','02','05','06') AND car_paresstatus in ('Y','A')
                                                    THEN '3DS' ELSE 'No3DS' END               as     transaction_3ds,
                CASE
                    WHEN (tr_id_cascaded <> 0 and tr_3d_eci is NULL)
                        OR (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0'))
                        THEN 1
                    ELSE 0 END                                                                as     is_cascade,
                casc_view.cascaded_due_to                                                     as     cascaded_due_to,
                rgt.attempt_no                                                                as     attempt_number,
                CASE
                    WHEN date(rgt.tr_date) >= '2020-12-25' AND rgt.attempt_no > 1
                        THEN 1
                    ELSE 0 END                                                                as     is_stepdown,
                CASE
                    WHEN date(rgt.tr_date) >= '2020-12-25' and date(rgt.tr_date) < '2022-01-26'
                             THEN
                                 CASE
                                 WHEN rgt.attempt_no = 2
                                            THEN 0.03
                                 WHEN  rgt.attempt_no = 3
                                            THEN 0.05
                                 WHEN  rgt.attempt_no = 4
                                            THEN 0.20
                                 WHEN  rgt.attempt_no = 5
                                            THEN 0.70
                                 WHEN  rgt.attempt_no = 6
                                            THEN 0.60
                                 ELSE 0 END
                    WHEN date(rgt.tr_date) >= '2022-01-26' and date(rgt.tr_date) < '2022-07-16'
                             THEN
                                CASE
                                 WHEN rgt.attempt_no = 2
                                            THEN 0.03
                                 WHEN  rgt.attempt_no = 3
                                            THEN 0.05
                                 WHEN  rgt.attempt_no = 4
                                            THEN 0.20
                                 WHEN  rgt.attempt_no = 5
                                            THEN 0.70
                                 WHEN  rgt.attempt_no = 6
                                            THEN 0.10
                                 WHEN  rgt.attempt_no = 7
                                            THEN 0.80
                                 ELSE 0 END
                    WHEN date(rgt.tr_date) >= '2022-07-16'
                             THEN
                                CASE
                                 WHEN rgt.attempt_no = 2
                                            THEN 0.00
                                 WHEN  rgt.attempt_no = 3
                                            THEN 0.03
                                 WHEN  rgt.attempt_no = 4
                                            THEN 0.05
                                 WHEN  rgt.attempt_no = 5
                                            THEN 0.20
                                 WHEN  rgt.attempt_no = 6
                                            THEN 0.70
                                 WHEN  rgt.attempt_no = 7
                                            THEN 0.10
				                 WHEN  rgt.attempt_no = 8
					                        THEN 0.80
                                 ELSE 0 END
                    ELSE 0 END                                                                as     stepdown_discount_percent,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        then rgt.email
                    ELSE NULL END                                                             as     approved_lead_email,
                CASE
                    WHEN ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T' then rgt.email
                    ELSE NULL END                                                             as     lead_email,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        then rgt.inv_id_ext
                    ELSE NULL END                                                             as     approved_lead_invoice,
                CASE
                    WHEN ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T' then rgt.inv_id_ext
                    ELSE NULL END                                                             as     lead_invoice,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        then rgt.email || NVL('-' || leads_rgt.lead_mo, '')
                    ELSE NULL END                                                             as     approved_lead_cust_u_key,
                CASE
                    WHEN ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        then rgt.email || NVL('-' || leads_rgt.lead_mo, '')
                    ELSE NULL END                                                             as     lead_cust_u_key,
                CASE
                    WHEN efc.relationship_offer_network_offer_id is not null THEN 'Everflow'
                    WHEN cc.offer_id is not null THEN 'Cake'
                    ELSE 'DIRECT' end                                                         as     affiliate_platform,
                COALESCE(cpa.affiliate_id, rgt_aff.affiliate_new)                             as     affiliate_id,
                aff_info.matched_affiliate_name                                               as     affiliate_name,
                leads_rgt.lead_ms                                                             as     sub_affiliate_id_1,
                leads_rgt.lead_ms2                                                            as     sub_affiliate_id_2,
                leads_rgt.lead_mo                                                             as     campaign_id,

                CASE
                    WHEN efc.relationship_offer_network_offer_id is not null
                        THEN upper(efc.relationship_offer_name)
                    WHEN cc.offer_id is not null THEN upper(cc.offer_name)
                    ELSE NULL end                                                             as     offer_name,
                ss.step                                                                       as     straight_sales_step,
                ss.type                                                                       as     straight_sales_type,
                ss.product                                                                    as     straight_sales_product,
                ss.units                                                                      as     straight_sales_units,
                ss.sku                                                                        as     straight_sales_sku,
				CASE
					WHEN json_extract_array_element_text(json_extract_path_text(tui_udf02, 'skus', true),0)!=''
						THEN  json_extract_array_element_text(json_extract_path_text(tui_udf02, 'skus', true),0)
					ELSE NULL end															  as	 SKU,
                rgt.bank_response                                                             as     bank_response_code,
                rgt.resp_id                                                                   as     rg_response_code,

                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE') and billing_type = 'S'
                        THEN rgt.tr_id
                    ELSE NULL END															  as	approved_exit_sale_oo,
				CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE') and billing_type = 'S'
                        THEN rgt.tr_id
                    ELSE NULL END 															  as	declined_exit_sale_oo,
				CASE
                    WHEN ttype_name in ('SALE') and billing_type = 'S'
                        THEN rgt.tr_id
                    ELSE NULL END															  as	attempted_exit_sale_oo,
                CASE reason_desc
                    WHEN 'Success' THEN NULL
                    ELSE COALESCE(CASE
                                      WHEN bank_response in ('0', '10000') THEN reason_desc
                                      ELSE COALESCE(bank_response_desc, reason_desc) END) END as     bank_reponse_description,
                CAST(CASE
                         WHEN ttype_name in ('SALE', 'TICKET') THEN tr_amount / cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                               total_volume_attempted_eur,
                CAST(CASE
                         WHEN reason_desc = 'Success'
                             and ttype_name in ('SALE', 'TICKET')
                             THEN tr_amount / cr.rates
                         WHEN vertical = 'BIGBUY'
                             then bigbuy.bbt
                         ELSE NULL END as DECIMAL(10, 2))                                            volume_eur,
                CASE WHEN ttype_name in ('SALE', 'AUTH_ONLY', 'TICKET') THEN rgt.tr_id ELSE NULL END tr_attempted,
                CASE
                    WHEN (reason_desc = 'Success' and ttype_name in ('CREDIT', 'VOID')) or (transaction_category = 'RDR' AND cb.rdr_outcome = 'Accepted')
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    refund_approved,
                CAST(CASE
                         WHEN (reason_desc = 'Success' and ttype_name in ('CREDIT', 'VOID')) or (transaction_category = 'RDR' AND cb.rdr_outcome = 'Accepted')
                             THEN tr_amount / cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                               refund_approved_eur,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') THEN rgt.tr_id
                    ELSE NULL END                                                                    tr_approved,
                CASE
                    WHEN rg_reattempts.reattempt_txn_id is not null
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    rebill_reattempt_attempted,
                CASE
                    WHEN reason_desc = 'Success' and rg_reattempts.reattempt_txn_id is not null
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    rebill_reattempt_approved,
                CASE
                    WHEN reason_desc != 'Success' and rg_reattempts.reattempt_txn_id is not null
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    rebill_reattempt_declined,
                CASE
                    WHEN ttype_name in ('SALE', 'AUTH_ONLY') and billing_type IN ('T', 'X-T')
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    total_leads_attempted,     -- X-T is initial and cross sale and should be included when ttype_name='SALE'
                -- TODO: Paid leads
                -- TODO: Exit Sales are leads which we don't pay for and it's identified by affiliate IDs
                -- TODO: BinBlock Sales are leads which we don't pay for and it's identified by blocked bins
                -- TODO: Cross Sales are leads which we don't pay for and it's identified by Billing_type = X-T
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    approved_leads,
				CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'S'
                        AND vertical!='STRAIGHT SALES'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    initial_only,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    declined_leads,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        and rgs.subscription_status != 'Active'
                        and no_conversions.inv_id_ext is not null
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    cancelled_leads,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        and rgs.subscription_status = 'Active'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    active_leads,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        and ((leads_rgt.lead_tr_date >= '2021-03-08'
                            and leads_rgt.lead_rtc != 0
                            AND COALESCE(cpa.affiliate_id, rgt.affiliate) is not null AND cpa.cost_per_lead_eur > 0.00)
                            OR
                             (leads_rgt.lead_tr_date < '2021-03-08'
                                 AND COALESCE(cpa.affiliate_id, rgt.affiliate) is not null AND
                              cpa.cost_per_lead_eur > 0.00))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    paid_leads,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        and ((leads_rgt.lead_tr_date >= '2021-03-08'
                            and leads_rgt.lead_rtc != 0
                            AND COALESCE(cpa.affiliate_id, rgt.affiliate) is not null)
                            OR
                             (leads_rgt.lead_tr_date < '2021-03-08'
                                 AND COALESCE(cpa.affiliate_id, rgt.affiliate) is not null))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    paid_leads_declined,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and ((leads_rgt.lead_tr_date >= '2021-03-08'
                            and leads_rgt.lead_rtc != 0
                            AND (COALESCE(cpa.affiliate_id, rgt.affiliate) is null or cpa.cost_per_lead_eur = 0.00
                                or cpa.cost_per_lead_eur is null))
                            OR
                                                    (leads_rgt.lead_tr_date < '2021-03-08'
                                                        AND (COALESCE(cpa.affiliate_id, rgt.affiliate) is null or
                                                             cpa.cost_per_lead_eur = 0.00
                                                            or cpa.cost_per_lead_eur is null)))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    unpaid_leads,
                CASE
                    WHEN unpaid_leads is not null and vertical = 'OPEN-GEO' and transaction_category = 'ORDER'
                        then ogi.avg_cpc_per_lead
                        else 0 end                                                                  avg_cpc_per_lead,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and ((leads_rgt.lead_tr_date >= '2021-03-08'
                            and leads_rgt.lead_rtc != 0
                            AND COALESCE(cpa.affiliate_id, rgt.affiliate) is null)
                            OR
                                                    (leads_rgt.lead_tr_date < '2021-03-08'
                                                        AND COALESCE(cpa.affiliate_id, rgt.affiliate) is null))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    unpaid_leads_declined,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and ((leads_rgt.lead_tr_date >= '2021-08-01'
                            and leads_rgt.lead_bb = 1) or
                                                    (leads_rgt.lead_tr_date >= '2021-03-08'
                                                        and leads_rgt.lead_tr_date < '2021-08-01'
                                                        and leads_rgt.lead_rtc = 0))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    bb_leads,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and ((leads_rgt.lead_tr_date >= '2021-08-01'
                            and leads_rgt.lead_bb = 1) or
                                                    (leads_rgt.lead_tr_date >= '2021-03-08'
                                                        and leads_rgt.lead_tr_date < '2021-08-01'
                                                        and leads_rgt.lead_rtc = 0))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    bb_leads_declined,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and (leads_rgt.lead_tr_date >= '2021-08-01'
                            and leads_rgt.lead_ppb = 1)
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    prepaid_leads,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY')
                        and billing_type = 'T' and (leads_rgt.lead_tr_date >= '2021-08-01'
                            and leads_rgt.lead_ppb = 1)
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    prepaid_leads_declined,
                CASE
                    WHEN ((leads_rgt.lead_tr_date >= '2021-08-01'
                        and leads_rgt.lead_bb = 1) or
                          (leads_rgt.lead_tr_date >= '2021-03-08'
                              and leads_rgt.lead_tr_date < '2021-08-01'
                              and leads_rgt.lead_rtc = 0))
                        THEN 'BB'
                    WHEN (leads_rgt.lead_tr_date >= '2021-08-01'
                        and leads_rgt.lead_ppb = 1)
                        THEN 'PREPAID'
                    WHEN (COALESCE(cpa_inv.affiliate_id, rgt.affiliate) is not null)
                        AND ((reason_desc != 'Success' and billing_type = 'T') or (cpa_inv.cost_per_lead_eur > 0.00))
                        THEN 'PAID'
                    WHEN ((COALESCE(cpa_inv.affiliate_id, rgt.affiliate) is null AND reason_desc != 'Success' AND
                           billing_type = 'T')
                        or ((COALESCE(cpa_inv.affiliate_id, rgt.affiliate) is null or cpa_inv.cost_per_lead_eur = 0.00
                            or cpa_inv.cost_per_lead_eur is null)))
                        THEN 'UNPAID'
                    ELSE NULL END                                                                    lead_type,
                coalesce (bin_status.status,
                          case when transaction_category = 'RDR'
                              then 'RDR_new' else null end,
                          bindb_lpa.current_bin_action
                          )                                                                   as     current_bin_action,
                bindb_excl.current_bin_action_date                                            as     current_bin_action_date,
                CAST(CASE
                         WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                             THEN rgt.tr_amount / cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                               approved_leads_amount_eur,
                -- the volume of this is 0 and we usually have 7 days for this to happen
                CASE
                    WHEN (reason_desc = 'Success' and ttype_name = 'AUTH_ONLY' and billing_type = 'X-T')
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    approved_cross_sales,
                CASE
                    WHEN exit_sales.cake_id is not null
                        THEN 1
                    ELSE 0 END                                                                       is_exit_sale,
                CASE
                    WHEN (billing_type in ('R', 'C') OR json_extract_path_text(tui_udf02, 'sc', true) = 1)
                        and ttype_name in ('SALE', 'TICKET') THEN rgt.tr_id
                    ELSE NULL END                                                                    total_rebills_attempted,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and
                         (billing_type = 'R' or billing_type = 'C'
                             OR json_extract_path_text(tui_udf02, 'sc', true) = 1) THEN rgt.tr_id
                    ELSE NULL END                                                                    approved_rebills,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'TICKET') and
                         (billing_type = 'R' or billing_type = 'C'
                             OR json_extract_path_text(tui_udf02, 'sc', true) = 1) THEN rgt.tr_id
                    ELSE NULL END                                                                    declined_rebills,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and
                         (billing_type = 'R' or billing_type = 'C'
                             OR json_extract_path_text(tui_udf02, 'sc', true) = 1) and
                         rgs.subscription_status != 'Active' THEN rgt.tr_id
                    ELSE NULL END                                                                    cancelled_rebills,
                CAST(CASE
                         WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and
                              (billing_type = 'R' or billing_type = 'C'
                                  OR json_extract_path_text(tui_udf02, 'sc', true) = 1)
                             THEN rgt.tr_amount / cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                               approved_rebills_amount_eur,
                -- TODO: map the first rebill  to the initial tx which has a status of T for the customer
                CASE
                    WHEN ttype_name in ('SALE', 'TICKET') and (billing_type = 'C'
                        or (first_rebill_sc.tr_id is not null))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    first_rebill_attempted,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and (billing_type = 'C'
                        or (first_rebill_sc.tr_id is not null))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    first_rebill_approved,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'TICKET') and (billing_type = 'C'
                        or (first_rebill_sc.tr_id is not null))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    first_rebill_declined,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and (billing_type = 'C'
                        or (first_rebill_sc.tr_id is not null))
                        and rgs.subscription_status != 'Active'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    first_rebill_cancelled,
                CAST(CASE
                         WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'TICKET') and (billing_type = 'C'
                             or (first_rebill_sc.tr_id is not null))
                             THEN rgt.tr_amount / cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                               first_rebill_amount_eur,
                CASE
                    WHEN (lfr.tr_lead IS NOT NULL OR first_rebill_free_trial.lead_id IS NOT NULL) then rgt.tr_id
                    ELSE NULL END                                                             as     have_first_rebills_leads,
                rgs.subscription_trial_period                                                 as     trial_period,
                rgs.subscription_status                                                       as     subscription_status,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        AND datediff(hour, tr_date, getdate()) >
                            COALESCE(CASE rgs.subscription_trial_period
                                         WHEN 'Monthly' THEN 30 * 24
                                         ELSE CAST(split_part(rgs.subscription_trial_period, ' ', 1) AS numeric) * 24
                                END)
                        THEN 1
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                        AND datediff(hour, tr_date, getdate()) <=
                            COALESCE(CASE rgs.subscription_trial_period
                                         WHEN 'Monthly' THEN 30 * 24
                                         ELSE CAST(split_part(rgs.subscription_trial_period, ' ', 1) AS numeric) * 24
                                END)
                        THEN 0
                    ELSE NULL END                                                             AS     trial_completed,
                CASE
                    WHEN dup_subs.dup_subscription is NOT NULL and rgs.subscription_status <> 'Active'
                        THEN 1
                    ELSE 0 END                                                                AS     is_duplicate_subscription,
                CASE
                    WHEN car_paresstatus is not null and reason_desc = 'Success' and
                         ttype_name = 'SALE'
                        then rgt.tr_id
                    ELSE NULL END                                                             as     success_tr_3ds,
                CASE
                    WHEN car_paresstatus is null and reason_desc = 'Success' and
                         ttype_name = 'SALE'
                        then rgt.tr_id
                    ELSE NULL END                                                             as     success_tr_non3ds,
                CASE
                    WHEN car_paresstatus is not null and ttype_name = 'SALE' then rgt.tr_id
                    ELSE NULL END                                                             as     all_tr_3ds,
                CASE
                    WHEN car_paresstatus is null and ttype_name = 'SALE' then rgt.tr_id
                    ELSE NULL END                                                             as     all_tr_non3ds,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name = 'SALE' and
                         ((site_id = '1' and date(tr_date) < '2021-01-25') or ss.sku is not null)
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_approved,
                CASE
                    WHEN ttype_name in ('SALE', 'AUTH_ONLY') and ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                                                                  (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        and billing_type = 'T'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    cascade_attempts,
                CASE
                    WHEN reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and
                         ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                          (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        and billing_type = 'T'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    cascade_success,
                CASE
                    WHEN reason_desc != 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and
                         ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                          (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        and billing_type = 'T'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    cascade_declined,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 1 and
                         reason_desc = 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_1_success,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 1 and
                         reason_desc != 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_1_declined,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 1
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_1_attempted,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 1 and
                         reason_desc = 'Success'
                        THEN COALESCE(leads_rgt.lead_mr, rgt.email)
                    ELSE NULL END                                                                    step_1_sessions_success,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 1 and
                         reason_desc != 'Success'
                        THEN COALESCE(leads_rgt.lead_mr, rgt.email)
                    ELSE NULL END                                                                    step_1_sessions_declined,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name in ('SALE', '3D_LOOKUP') and straight_sales_step = 1
                        THEN COALESCE(leads_rgt.lead_mr, rgt.email)
                    ELSE NULL END                                                                    step_1_sessions_attempted,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 2 and
                         reason_desc = 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_2_success,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 2 and
                         reason_desc != 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_2_declined,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and straight_sales_step = 2
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    step_2_attempted,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and reason_desc = 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_success,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and reason_desc != 'Success'
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_declined,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and ttype_name = 'SALE' and
                         ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                          (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_cascade_attempts,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and reason_desc = 'Success' and ttype_name = 'SALE' and
                         ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                          (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_cascade_success,
                CASE
                    WHEN vertical = 'STRAIGHT SALES' and reason_desc != 'Success' and ttype_name = 'SALE' and
                         ((tr_id_cascaded <> 0 and tr_3d_eci is NULL) OR
                          (json_extract_path_text(tui_udf02, 'c3d', true) in ('1', '0')))
                        THEN rgt.tr_id
                    ELSE NULL END                                                                    straight_sales_cascade_declined,
                CASE
                    WHEN vertical = 'BIGBUY'
                         then bigbuy.totalShippingTaxIncl
                    WHEN (vertical = 'ROAS' and reason_desc = 'Success' and ttype_name = 'SALE' ) THEN
                        CASE WHEN date(rgt.tr_date) <= '2022-07-26' THEN
                            CASE WHEN rgt.product_id LIKE '%TWS001%' THEN 4.99
                                 when rgt.product_id LIKE '%TWS002%' THEN 7.12
                                ELSE NULL END
                            ELSE
                                round(CAST( CASE WHEN json_extract_path_text(tui_udf02, 'sh', true)!='' then json_extract_path_text(tui_udf02, 'sh', true)
			                                   else '0.0' END
			                              AS DOUBLE PRECISION)/curr.rates,3)
                            END
                    WHEN (vertical = 'ECOM' and reason_desc = 'Success' and ttype_name = 'SALE' and billing_type = 'T') THEN
                        -- if date before 17/11 load data from central
                        CASE WHEN date(rgt.tr_date) <= '2021-11-17' THEN CAST(etusc.shippingcost AS DOUBLE PRECISION) / curr.rates
                             WHEN date(rgt.tr_date) > '2021-11-17' AND rgt.tr_date <= '2021-12-08 11:10:00' THEN-- after 17/11/2021 shipping cost was passed in 'sc' of udf02
                                    round(CAST( CASE WHEN json_extract_path_text(tui_udf02, 'sc', true)!='' then json_extract_path_text(tui_udf02, 'sc', true)
			                                   else '0.0' END
			                              AS DOUBLE PRECISION)/curr.rates,3)

                             ELSE -- after 08/12/2021 11:10:00 shipping cost was passed in 'sh' of udf02
                                    round(CAST( CASE WHEN json_extract_path_text(tui_udf02, 'sh', true)!='' then json_extract_path_text(tui_udf02, 'sh', true)
			                                   else '0.0' END
			                              AS DOUBLE PRECISION)/curr.rates,3)
                             END
                        -- if not load data from tui_udf02
                    ELSE NULL END                                                                    ecom_shipping_cost,
                CASE
                    WHEN vertical = 'BIGBUY'
                        THEN bigbuy.bbc
                    WHEN (vertical = 'ROAS' and reason_desc = 'Success' and ttype_name = 'SALE' ) THEN
                        CASE WHEN date(rgt.tr_date) <= '2022-07-26' THEN
                            CASE WHEN rgt.product_id LIKE '%TWS001%' THEN 6.1
                                 when rgt.product_id LIKE '%TWS002%' THEN 11.67
                                ELSE NULL END
                            ELSE
                                round(CAST(json_extract_path_text(tui_udf02, 'uc', true) AS DOUBLE PRECISION)/curr.rates,3)
                            END
                    WHEN (vertical = 'ECOM' and reason_desc = 'Success' and ttype_name = 'SALE' and billing_type = 'T') THEN
                      -- if date before 17/11 load data from central
                      CASE WHEN date(rgt.tr_date) <= '2021-11-17' THEN
                        CAST(etusc.productcost AS DOUBLE PRECISION) / curr.rates
                      ELSE -- after 17/11/2021 shipping cost was passed in udf02
                        round(CAST(json_extract_path_text(tui_udf02, 'uc', true) AS DOUBLE PRECISION)/curr.rates,3)
                      END
                    ELSE NULL END                                                                    ecom_wholesale_cost,
                essc.product_id                                                                      ecom_ss_product_name,
                CAST(essc.shipping_cost AS DOUBLE PRECISION) / essc_cr.rates                        ecom_ss_product_shipping_cost,
                CAST(esuc.product_cost AS DOUBLE PRECISION) / essc_cr.rates                          ecom_ss_product_cost,
                CASE
                    WHEN crt.tr_id is not null
                        then 'NON-PAID'
                    ELSE 'PAID'   END                                                     as     cpa_reconciled,
                CASE
                    WHEN vertical = 'OPEN-GEO' and transaction_category = 'ORDER' and reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = 'T'
                       then ogi.avg_cpa_og

                    WHEN vertical = 'ROAS' and transaction_category = 'ORDER' and reason_desc = 'Success' and ttype_name in ('SALE', 'AUTH_ONLY') and billing_type = ('T','S')
                        then roas_cpa.cpa

                    WHEN (vertical!= 'OPEN-GEO' and vertical != 'ROAS') and reason_desc = 'Success' and ttype_name in ('AUTH_ONLY', 'SALE') and billing_type in ('T', 'S') and cpa_reconciled =  'PAID'
                        THEN cpa.cost_per_lead_eur

                    ELSE NULL END                                                             as     cost_per_lead_eur,

                CASE
                    WHEN COALESCE(cb.category, rg_disputes.category) is NULL
                        and rgt.reason_desc = 'Success' and ttype_name in ('SALE','TICKET')
                        THEN ((tr_amount / cr.rates) * (fees.processing_percent_mdr / 100)) + ((tr_amount / cr.rates) * (fees.surcharge_non_eea_transactions / 100))
                    ELSE 0
                    END                                                                       as     mdr_fee,
                CASE
                    WHEN COALESCE(cb.category, rg_disputes.category) is NULL
                        THEN
                         CASE WHEN ttype_name = 'SALE'
				                THEN fees.transaction_fee
			                  WHEN ttype_name ='AUTH_ONLY'
				                THEN fees.authorisation_fee
			                  ELSE 0 END
			        ELSE 0 END 							      as transaction_fee,
                CASE
                    WHEN COALESCE(cb.category, rg_disputes.category) is NULL
                        and ttype_name in ('CREDIT', 'VOID')
                        THEN fees.refund_fee
                    ELSE 0
                    END                                                                       as     refund_fee,
                CASE
                    WHEN cb.category in ('CHARGEBACK', 'CHARGEBACK-2', 'CB-REVERSAL', 'CB-REVERSAL-2')
                        THEN fees.cb_fee_bank
                    ELSE 0 END                                                                as     chargeback_bank_fee,
                CASE
                    WHEN cb.category in ('CHARGEBACK', 'CHARGEBACK-2', 'CB-REVERSAL', 'CB-REVERSAL-2')
                        AND country_codes.continent_code = 'EU'
                        THEN
                        CASE
                            WHEN cb.report_date >= '2021-09-06'
                                THEN
                                CASE
                                    WHEN rgt.card_name = 'Visa' and
                                         (rgt.tr_3d_eci not in ('05', '06') or rgt.tr_3d_eci is NULL)
                                        THEN fees.cb_fee_visa
                                    WHEN rgt.card_name = 'MasterCard'
                                        THEN fees.cb_fee_master
                                    ELSE 0
                                    END
                            ELSE
                                CASE
                                    WHEN rgt.card_name = 'Visa' and
                                         (rgt.tr_3d_eci not in ('05', '06') or rgt.tr_3d_eci is NULL)
                                        THEN fees.cb_fee_visa
                                    WHEN rgt.card_name = 'MasterCard' and
                                         (jira_mids.mcc not in ('5815', '5816') or jira_mids.mcc is NULL)
                                        THEN fees.cb_fee_master
                                    ELSE 0
                                    END
                            END

                    ELSE 0
                    END                                                                       as     chargeback_cc_fee,
                CAST(CASE
                         WHEN rgt.ttype_name in ('SALE', 'CREDIT', 'AUTH_ONLY') AND rgt.reason_desc = 'Success'
                             THEN CAST(success_rate AS DOUBLE PRECISION) / rgt_cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                        as     rgt_success_fee,
                CAST(CASE
                         WHEN rgt.ttype_name in ('SALE', 'AUTH_ONLY') AND rgt.reason_desc != 'Success' AND
                              rgt.respc_id in ('1', '2')
                             THEN CAST(decline_rate AS DOUBLE PRECISION) / rgt_cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                        as     rgt_decline_fee,
                CAST(CASE
                         WHEN rgt.ttype_name in ('3D_LOOKUP') and rgt.resp_id not in ('204')
                             THEN CAST(enrollment_3ds_rate AS DOUBLE PRECISION) / rgt_cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                        as     rgt_enrollment_3ds_fee,
                CAST(CASE
                         WHEN rgt.car_paresstatus is not null
                             THEN CAST(authentication_3ds_rate AS DOUBLE PRECISION) / rgt_cr.rates
                         ELSE 0 END as DECIMAL(10, 2))                                        as     rgt_authentication_3ds_fee,
                CASE
                    WHEN rgt.ttype_name in ('3D_LOOKUP') and rgt.resp_id not in ('204')
                        THEN rgt.tr_id
                    ELSE NULL END                                                             as     enrollment_3ds,
                CASE
                    WHEN rgt.car_paresstatus is not null
                        THEN rgt.tr_id
                    ELSE NULL END                                                             as     authentication_3ds,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.case_source
                    ELSE NULL END                                                             as     rg_dispute,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.alert_handler
                    ELSE NULL END                                                             as     rg_dispute_handler,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.alert_id
                    ELSE NULL END                                                             as     rg_dispute_alert_id,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.is_duplicate_alert
                    ELSE NULL END                                                             as     rg_dispute_is_duplicate_alert,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.original_alert_id
                    ELSE NULL END                                                             as     rg_dispute_original_alert_id,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.is_refunded_alert
                    ELSE NULL END                                                             as     rg_dispute_is_refunded_alert,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.has_cb_on_cb_alert
                    ELSE NULL END                                                             as     rg_dispute_has_cb_on_cb_alert,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.cb_alert_cb_date
                    ELSE NULL END                                                             as     rg_dispute_cb_alert_cb_date,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        THEN rg_disputes.fees_eur
                    ELSE NULL END                                                             as     rg_dispute_fee_eur,
                CASE
                    WHEN rg_disputes.tr_id is not null and COALESCE(cb.category, rg_disputes.category) = 'DISPUTE_ALERT'
                        and rg_disputes.is_duplicate_alert = 0 and rg_disputes.has_cb_on_cb_alert = 0
                        THEN rg_disputes.fees_eur
                    ELSE NULL END                                                             as     rg_dispute_fee_to_pay_eur,
                CAST(CASE
                         WHEN vertical = 'BIGBUY' THEN
                             bigbuy.bbv
                         WHEN vr.country_code is not null and COALESCE(cb.category, rg_disputes.category) is NULL
                             AND reason_desc = 'Success' and ttype_name in ('SALE')
                             THEN ((tr_amount / cr.rates) / (1 + CAST(vr.vat_percent AS DOUBLE PRECISION))) *
                                  CAST(vr.vat_percent AS DOUBLE PRECISION)
                         ELSE 0 END as DECIMAL(10, 2))                                        as     order_vat_eur,
                CASE
                    when vertical = 'BIGBUY' THEN
                        bigbuy.totalShippingTaxIncl - bigbuy.totalShippingTaxExcl
                    else null end                                                             as     shipping_vat_eur,
                CAST(CASE
                         WHEN vr.country_code is not null and COALESCE(cb.category, rg_disputes.category) is NULL
                             AND reason_desc = 'Success' and ttype_name in ('CREDIT', 'VOID')
                             THEN ((tr_amount / cr.rates) / (1 + CAST(vr.vat_percent AS DOUBLE PRECISION))) *
                                  CAST(vr.vat_percent AS DOUBLE PRECISION)
                         ELSE 0 END as DECIMAL(10, 2))                                        as     refund_vat_eur,
                CAST(CASE
                         WHEN vr.country_code is not null
                             and cb.category in ('CHARGEBACK', 'CHARGEBACK-2')
                             THEN ((tr_amount / cr.rates) / (1 + CAST(vr.vat_percent AS DOUBLE PRECISION))) *
                                  CAST(vr.vat_percent AS DOUBLE PRECISION)
                         ELSE 0 END as DECIMAL(10, 2))                                        as     cb_vat_eur,
                CAST(CASE
                         WHEN vr.country_code is not null
                             and cb.category in ('CB-REVERSAL', 'CB-REVERSAL-2')
                             THEN ((tr_amount / cr.rates) / (1 + CAST(vr.vat_percent AS DOUBLE PRECISION))) *
                                  CAST(vr.vat_percent AS DOUBLE PRECISION)
                         ELSE 0 END as DECIMAL(10, 2))                                        as     cb_reversal_vat_eur,
                CASE
                    WHEN json_extract_path_text(tui_udf02, 'sc', true) IN ('1','2')
                        and json_extract_path_text(tui_udf02, 'sc', true) != ''
                        THEN json_extract_path_text(tui_udf02, 'sc', true)
                    ELSE '0' END                                                              as     is_salvage_cancellation,
                CASE
                    WHEN ((leads_rgt.lead_tr_date >= '2021-08-01'
                        and leads_rgt.lead_bb = 1) or
                          (leads_rgt.lead_tr_date >= '2021-03-08'
                              and leads_rgt.lead_tr_date < '2021-08-01'
                              and leads_rgt.lead_rtc = 0))
                        THEN 1
                    ELSE 0 END                                                                as     is_block_bin,
                CASE
                    WHEN json_extract_path_text(tui_udf02, 'sc', true) IN ('1','2')
                        and json_extract_path_text(tui_udf02, 'sc', true) != ''
                        THEN 'Cancellation Salvage'
                    WHEN ((leads_rgt.lead_tr_date >= '2021-08-01'
                        and leads_rgt.lead_bb = 1) or
                          (leads_rgt.lead_tr_date >= '2021-03-08'
                              and leads_rgt.lead_tr_date < '2021-08-01'
                              and leads_rgt.lead_rtc = 0))
                        THEN 'BB Salvage'
                    ELSE 'No' END                                                             as     is_salvage_sale,
                CASE WHEN upper(bindb.card_type_2) LIKE '%PREPAID%' THEN 1 ELSE 0 END         as     is_card_prepaid,
                CASE
                    WHEN (leads_rgt.lead_tr_date >= '2021-08-01' AND leads_rgt.lead_ppb = 1) THEN 1
                    WHEN (leads_rgt.lead_tr_date >= '2021-08-01' AND leads_rgt.lead_ppb = 0) THEN 0
                    ELSE NULL END                                                             as     is_prepaid,
                tr_date                                                                       as     last_updated
from rocketgate.transactions rgt

         left join rocketgate.paynetics_errored_invoices pay_inv
                   ON rgt.inv_id_ext = pay_inv.paynetics_invoice_id
         left join bigbuy.bigbuy_orders_table bigbuy
                   ON rgt.tr_id = bigbuy.bigbuy_transaction_id
         left join public.currency_rates cr
                   ON date(cr.date) = date(rgt.tr_date) and cr.currency = rgt.curr_code_settled
         left join
     (
         select two_letter_country_code as country_code,
                continent_code          as continent_code
         from central.country_continent
     ) country_codes
     ON rgt.tui_bill_country = country_codes.country_code
        left join
    (
                    select distinct tr_id
                    from rocketgate.cpa_reconciliation_transactions
    ) crt
        on crt.tr_id = rgt.tr_id
    left join
    (
        select distinct bin,
               status
        from rocketgate.bin_status
        )bin_status
    on bin_status.bin = rgt.bankbin
	left join
	(select inv_id_ext as invoice, cust_id_ext as customer_id, affiliate as affiliate_new
    from rocketgate.transactions rgt_aff_1
    inner join
    (select inv_id_ext as inv_id, cust_id_ext as cust_id, min(tr_date) as earliest_tr_date
      from rocketgate.transactions
      group by 1, 2
     ) rgt_aff_date
    on rgt_aff_1.inv_id_ext=rgt_aff_date.inv_id and rgt_aff_1.cust_id_ext=rgt_aff_date.cust_id and rgt_aff_1.tr_date=rgt_aff_date.earliest_tr_date) rgt_aff
    on rgt.inv_id_ext =rgt_aff.invoice  and rgt.cust_id_ext=rgt_aff.customer_id
		left join
		(select   tr_original_id as refund_orig_transaction, max(date(tr_date)) as refund_orig_date
         from rocketgate.transactions
         where ttype_name ='CREDIT' and resp_id = '0'
         group by 1 ) rgt_refund
	 ON rgt.tr_id = refund_orig_transaction
         left join
     (
         select distinct concat(concat(d.name, ' : '), mids.acquiringbank) as mid_name,
                         mids.jiraid                                       as mid_jira_id,
                         d.name                                            as domain_name,
                         split_part(mids.name, ' : ', 1)                   as company_name,
                         mids.jiraid                                          mid_id,
                         status,
                         mids.acquiringbank                                as bank,
                         mids.rgaccountsequence                            as rg_id,
                         labels,
                         mcc,
                         mids.monthlytransactionscap,
                         mids.midfirstlivedate                             as mid_first_live_date,
                         c.country                                         as company_country
         from jira.mids as mids
                  left join jira.domains d
                            on mids.jiradomainid = d.jiraid
                  left join jira.companies c
                            on c.jiraid = d.jiracompanyid
         where mids.rgaccountsequence is not null
           and mids.project not in ('Social', 'Loans', 'HOI')
     ) jira_mids
     ON ((jira_mids.rg_id = rgt.merchant_account) and
         (((jira_mids.labels not like '%STRAIGHTSALES%' or jira_mids.labels is null) and merch_id = '1599571287')
             OR ((jira_mids.labels like '%STRAIGHTSALES%' or jira_mids.labels like '%RG-Direct%') and merch_id = '1601058492')))
         left join rocketgate.leads_first_rebill lfr
                   ON lfr.tr_lead = rgt.tr_id
         left join
     (
         select inv_id_ext                                          as lead_inv_id_ext,
                cust_id_ext                                         as lead_cust_id_ext,
                min(tr_date)                                        as lead_tr_date,
                min(tui_udf02)                                      as lead_tui_udf02,
                min(tui_ip)                                         as lead_tui_ip,
                json_extract_path_text(min(tui_udf02), 'mr', true)  as lead_mr,
                json_extract_path_text(min(tui_udf02), 'mo', true)  as lead_mo,
                json_extract_path_text(min(tui_udf02), 'ms', true)  as lead_ms,
                json_extract_path_text(min(tui_udf02), 'ms2', true) as lead_ms2,
                json_extract_path_text(min(tui_udf02), 'rtc', true) as lead_rtc,
                json_extract_path_text(min(tui_udf02), 'cv', true)  as lead_cv,
                json_extract_path_text(min(tui_udf02), 'bb', true)  as lead_bb,
                json_extract_path_text(min(tui_udf02), 'ppb', true) as lead_ppb
         from rocketgate.transactions
         WHERE billing_type in ('T', 'S', 'X-T')
         group by 1, 2
     ) leads_rgt
     on rgt.inv_id_ext = leads_rgt.lead_inv_id_ext and rgt.cust_id_ext = leads_rgt.lead_cust_id_ext
         left join
     (
         select "brand_advertiser.brand_advertiser_name" vertical_name,
                "site_offer.site_offer_id" as            offer_id,
                max("site_offer.site_offer_name")        offer_name
         from cake.conversions
         where event_conversion_date >= '/Date(1601496000)/'
         group by 1, 2
     ) cc
     on leads_rgt.lead_mo = cc.offer_id
         left join
     (
         SELECT relationship_advertiser_name,
                relationship_offer_network_offer_id,
                max(UPPER(relationship_offer_name)) relationship_offer_name
         from athenaeverflow.conversions
         where "year" || '-' || "month" >= '2020-10'
         group by 1, 2
     ) efc
     ON leads_rgt.lead_mo =
        efc.relationship_offer_network_offer_id

         left join central.straight_sales_summary ss
                   on ss.sku = rgt.product_id
         left join central.ecom_ss_shipping_cost essc
                   on essc.product_id = rgt.product_id
         left join central.ecom_ss_unit_cost esuc
                   on esuc.product_id = rgt.product_id

         left join public.currency_rates essc_cr
                   ON date(essc_cr.date) = date(rgt.tr_date) and essc_cr.currency = essc.currency
         left join public.currency_rates esuc_cr
                   ON date(esuc_cr.date) = date(rgt.tr_date) and esuc_cr.currency = esuc.currency

    -- B: start: add the data of ecom trial shiping cost and unit cost, add the currency rates table for this
         left join central.ecom_trial_unit_and_shipping_cost etusc
                   on leads_rgt.lead_mo = etusc.campaignid and date(rgt.tr_date) >= date(etusc.start_date) and
                      date(rgt.tr_date) < date(etusc.end_date)
         left join public.currency_rates curr
                   ON date(curr.date) = date(rgt.tr_date) and curr.currency = coalesce (etusc.currency,json_extract_path_text(tui_udf02, 'suc', True))

    -- B: end
    -- B: start: Adding the is_cross_sale table
    left join
    (select
        inv_id_ext
        , json_extract_path_text(tui_udf02, 'xs', True) as lead_xs
        ,CASE
            WHEN trim(lead_xs) !='' THEN 1 END as is_cross_sale
        from rocketgate.transactions where is_cross_sale=1
    ) is_cross_sale_table on rgt.inv_id_ext =is_cross_sale_table.inv_id_ext
    -- B: End
    left join
     (
         SELECT DISTINCT merchant_invoice_id,
                         merchant_customer_id,
                         status                     as subscription_status,
                         rebill_cancel_request_date as susbscription_cancel_req_date,
                         initial_frequency          as subscription_trial_period
         from rocketgate.subscriptions_view_table
     ) rgs
     ON rgs.merchant_invoice_id = rgt.inv_id_ext and rgs.merchant_customer_id = rgt.cust_id_ext
         left join
     (
         SELECT bin,
                bank_name as issuing_bank,
				debit_credit,
                card_type_2
         from athenabindb.bin_db
         group by 1, 2, 3,4
     ) bindb
     ON rgt.bankbin = bindb.bin
         left join
     (
         SELECT bin                       as actioned_bin,
                listagg(action, ',')      as current_bin_action,
                listagg(action_date, ',') as current_bin_action_date
         from bindb.bin_exclusion
         group by 1
     ) bindb_excl

     ON rgt.bankbin = bindb_excl.actioned_bin
		left join
	(
		select	bin_number as actioned_bin ,
		listagg(distinct bin_rule_category_id,',') as current_bin_action
		from central.bins_info
		where bin_rule_plan = 'Default plan'
		group by 1

	) bindb_lpa

	ON rgt.bankbin = bindb_lpa.actioned_bin


         left join
     (
         select distinct inv_id_ext
         from rocketgate.transactions
         where billing_type = 'T'
         minus
         select distinct inv_id_ext
         from rocketgate.transactions
         where billing_type = 'C'
     ) no_conversions on rgt.inv_id_ext = no_conversions.inv_id_ext
         left join
     (
         select distinct subscription_to_cancel as dup_subscription
         from rocketgate.duplicate_subscriptions_history
     ) dup_subs on rgt.inv_id_ext = dup_subs.dup_subscription
         left join rocketgate.cascades_view_table casc_view
                   ON casc_view.cascade_tr_id = rgt.tr_id
         left join rocketgate.txn_billing_cycle_table bill_cyc
                   ON rgt.tr_id = bill_cyc.tr_id
         left join rocketgate.rebill_reattempts_view_table rg_reattempts
                   on rgt.tr_id = rg_reattempts.reattempt_txn_id
         left join (select cake_id from central.exit_sales_campaigns) exit_sales
                   on leads_rgt.lead_mo = exit_sales.cake_id
    -- left join -- not needed anymore for back response code cause it's now generated by RG trx API
    -- (
    --     select
    --         response_code,
    --         processor,
    --         min(response_text) bank_resp_desc
    --     from central.processor_response_codes_master
    --     group by 1,2
    -- ) prcm
    -- ON prcm.response_code = rgt.bank_response and prcm.processor = rgt.macct_processor
         left join
     ( select
		rc.source as source,
		rc.tr_operation as category,
		rc.tr_id as tr_id,
		rc.report_date as report_date,
		rc.rdr_cost as rdr_fee,
		rc.outcome as rdr_outcome
		from rocketgate.rdr_chargebacks_view rc
		union
		select
		null,
		null,
		rc1.tr_id as tr_id,
		null,
		null,
		null
		from rocketgate.rdr_chargebacks_view rc1
	) cb on rgt.tr_id = cb.tr_id
         left join
     (
         select distinct cpa.tr_id, cpa.affiliate_id, cpa.affiliate_name, cpa.cost_per_lead / cr.rates cost_per_lead_eur
         from rocketgate.affiliate_cost_per_lead cpa
                  left join public.currency_rates cr
                            ON date(cr.date) = date(cpa.date) and cr.currency = cpa.cost_per_lead_currency
     ) cpa ON cpa.tr_id = rgt.tr_id
         left join
     (
         select distinct cpa.inv_id_ext  as           cpa_inv_id_ext,
                         cpa.cust_id_ext as           cpa_cust_id_ext,
                         cpa.affiliate_id,
                         cpa.affiliate_name,
                         cpa.cost_per_lead / cr.rates cost_per_lead_eur
         from rocketgate.affiliate_cost_per_lead cpa
                  left join public.currency_rates cr
                            ON date(cr.date) = date(cpa.date) and cr.currency = cpa.cost_per_lead_currency
     ) cpa_inv ON cpa_inv.cpa_inv_id_ext = rgt.inv_id_ext and cpa_inv.cpa_cust_id_ext = rgt.cust_id_ext
         left join
     central.aff_info
     on COALESCE(cpa.affiliate_id, rgt_aff.affiliate_new) = aff_info.matched_affiliate_id
         and ((efc.relationship_offer_network_offer_id is not null and aff_info.matched_affiliate_platform = 'Everflow')
             or
              (efc.relationship_offer_network_offer_id is null and aff_info.matched_affiliate_platform = 'Cake'))
         left join
     (
         select jm.acquiringbank                                               bank,
                jm.jiraid      as                                              mid_id,
                jm.contract_id as                                              contract_code,
                TO_DATE(contfee.start_date, 'YYYY-MM-DD', False)               start_date,
                DATE(nullif(contfee.end_date, ''))                             end_date,
                CAST(contfee.processing_percent_mdr as DECIMAL(10, 2))         processing_percent_mdr,
                CAST(contfee.transaction_fee as DECIMAL(10, 2))                transaction_fee,
                CAST(contfee.cb_fee_bank as DECIMAL(10, 2))                    cb_fee_bank,
                CAST(contfee.cb_fee_master as DECIMAL(10, 2))                  cb_fee_master,
                CAST(contfee.cb_fee_visa as DECIMAL(10, 2))                    cb_fee_visa,
                CAST(contfee.authorisation_fee as DECIMAL(10, 2))              authorisation_fee,
                CAST(contfee.surcharge_non_eea_transactions as DECIMAL(10, 2)) surcharge_non_eea_transactions,
                CAST(contfee.refund_fee as DECIMAL(10, 2))                     refund_fee
         from jira.mids jm
                  left join central.acq_fees_by_contract contfee on jm.contract_id = contfee.contract_code
         where contfee.start_date is not null
     ) fees on fees.mid_id = jira_mids.mid_id and
               date(rgt.tr_date) between fees.start_date and COALESCE(fees.end_date, '2200-01-01')
         left join
     (
         select *
         from (
                  select to_char(tr_date, 'YYYYMM') txn_month, merch_id txn_vertical, count(distinct tr_id) txn_count
                  from rocketgate.transactions
                  where ttype_name not in ('3D_LOOKUP', 'AUTH_ONLY')
                  group by 1, 2
              ) rgt_txn_counts
                  left join rocketgate.teir_trnsaction_fees
                            on rgt_txn_counts.txn_count between CAST(REPLACE(MIN_TRANSACTIONS, ',', '') AS numeric) and CAST(REPLACE(MAX_TRANSACTIONS, ',', '') AS numeric)
     ) rgt_fees on to_char(rgt.tr_date, 'YYYYMM') = rgt_fees.txn_month and rgt.merch_id = rgt_fees.txn_vertical
         left join public.currency_rates rgt_cr ON date(rgt_cr.date) = date(rgt.tr_date) and rgt_cr.currency = 'USD'
         left join central.vat_rates vr ON vr.country_code = rgt.tui_bill_country
         left join
     (
         select split_part(trx_list, ',', 2) tr_id, split_part(trx_list, ',', 1) lead_id
         from (
                  select inv_id_ext,
                         listagg(tr_id, ',') WITHIN GROUP (ORDER BY tr_date)                   trx_list,
                         listagg(billing_type) WITHIN GROUP (ORDER BY tr_date)                 trx_type,
                         CHARINDEX('I', listagg(billing_type) WITHIN GROUP (ORDER BY tr_date)) chidx
                  from rocketgate.transactions
                  where reason_desc = 'Success'
                    and ttype_name = 'SALE'
                    and inv_id_ext in (select inv_id_ext
                                       from rocketgate.transactions
                                       where json_extract_path_text(tui_udf02, 'sc', true) = 1
                                       group by 1)
                  group by 1
                  having listagg(billing_type) like '%I%'
              )
         where chidx = 2
     ) first_rebill_sc
     ON first_rebill_sc.tr_id = rgt.tr_id
         left join
     (
         select split_part(trx_list, ',', 2) rebill_id, split_part(trx_list, ',', 1) lead_id
         from (
                  select inv_id_ext,
                         listagg(tr_id, ',') WITHIN GROUP (ORDER BY tr_date)                   trx_list,
                         listagg(billing_type) WITHIN GROUP (ORDER BY tr_date)                 trx_type,
                         CHARINDEX('C', listagg(billing_type) WITHIN GROUP (ORDER BY tr_date)) chidx
                  from rocketgate.transactions
                  where reason_desc = 'Success'
                    and inv_id_ext in (select inv_id_ext
                                       from rocketgate.transactions
                                       where json_extract_path_text(tui_udf02, 'mo', true) in ('3895', '4284')
                                       group by 1)
                  group by 1
                  having listagg(billing_type) like '%C%'
              )
         where chidx = 2
     ) first_rebill_free_trial
     ON first_rebill_free_trial.lead_id = rgt.tr_id
         left join
     (
         select 'DISPUTE_ALERT'                                       as category,
                tr_id,
                dispute_date                                          as report_date,
                'RG'                                                  as alert_handler,
                case_source,
                case_id                                               as alert_id,
                0                                                     as is_duplicate_alert,
                alert_id                                              as original_alert_id,
                CASE WHEN alert_refunded_id is NULL THEN 0 ELSE 1 END as is_refunded_alert,
                CASE WHEN alert_cb_id is NULL THEN 0 ELSE 1 END       as has_cb_on_cb_alert,
                alert_cb_date                                         as cb_alert_cb_date,
                CASE
                    WHEN date(dispute_date) >= '2021-04-01' THEN
                        CASE
                            WHEN case_source = 'Ethoca' and alert_month_count < 5000 THEN 25 / cr.rates
                            WHEN case_source = 'Ethoca' and alert_month_count >= 5000 THEN 22.5 / cr.rates
                            WHEN case_source = 'Verifi' and alert_month_count < 1000 THEN 30 / cr.rates
                            WHEN case_source = 'Verifi' and alert_month_count >= 1000 THEN 25 / cr.rates
                            END
                    ELSE 35 / cr.rates
                    END                                               as fees_eur
         from rocketgate.disputes d
                  left join
              (select to_char(date(dispute_date), 'YYYY-MM') as alert_month,
                      case_source                            as alert_month_case_source,
                      count(distinct tr_id)                  as alert_month_count
               from rocketgate.disputes
               group by 1, 2) rgd_month_count
              on to_char(date(d.dispute_date), 'YYYY-MM') = rgd_month_count.alert_month and
                 d.case_source = rgd_month_count.alert_month_case_source
                  left join public.currency_rates cr
                            ON date(cr.date) = date(d.dispute_date) and cr.currency = 'USD'
                  left join (select tr_id                             as alert_cb_id,
                                    max(CASE
                                            WHEN operation = 'CHARGEBACK' THEN tr_chargebackdate
                                            ELSE chargeback_date END) as alert_cb_date
                             from rocketgate.chargebacks
                             where operation in ('CHARGEBACK', 'CHARGEBACK-2', 'CB-REVERSAL', 'CB-REVERSAL-2')
                             group by 1) alert_cbs ON tr_id = alert_cbs.alert_cb_id
                  left join (select distinct tr_original_id as alert_refunded_id
                             from rocketgate.transactions
                             where ttype_name IN ('VOID', 'CREDIT')
                               and reason_desc = 'Success') alert_refunds ON tr_id = alert_refunds.alert_refunded_id
         UNION
         select NULL as category,
                tr_id,
                NULL as report_date,
                NULL as alert_handler,
                NULL as case_source,
                NULL as alert_id,
                NULL as is_duplicate_alert,
                NULL as original_alert_id,
                NULL as is_refunded_alert,
                NULL as has_cb_on_cb_alert,
                NULL as cb_alert_cb_date,
                0    as fees_eur
         from rocketgate.disputes
         UNION
         select 'DISPUTE_ALERT'                                       as category,
                matched_order_id                                      as tr_id,
                date(alert_date_in_date)                              as report_date,
                'Sticky'                                              as alert_handler,
                INITCAP(alert_provider)                               as case_source,
                provider_alert_id                                     as alert_id,
                is_duplicate_alert,
                original_provider_alert_id                            as original_alert_id,
                CASE WHEN alert_refunded_id is NULL THEN 0 ELSE 1 END as is_refunded_alert,
                CASE WHEN alert_cb_id is NULL THEN 0 ELSE 1 END       as has_cb_on_cb_alert,
                alert_cb_date                                         as cb_alert_cb_date,
                alert_fees_in_eur                                     AS fees_eur
         from central.sticky_cb_alerts_view_table
                  left join (select tr_id                             as alert_cb_id,
                                    max(CASE
                                            WHEN operation = 'CHARGEBACK' THEN tr_chargebackdate
                                            ELSE chargeback_date END) as alert_cb_date
                             from rocketgate.chargebacks
                             where operation in ('CHARGEBACK', 'CHARGEBACK-2', 'CB-REVERSAL', 'CB-REVERSAL-2')
                             group by 1) alert_cbs ON matched_order_id = alert_cbs.alert_cb_id
                  left join (select distinct tr_original_id as alert_refunded_id
                             from rocketgate.transactions
                             where ttype_name IN ('VOID', 'CREDIT')
                               and reason_desc = 'Success') alert_refunds
                            ON matched_order_id = alert_refunds.alert_refunded_id
         WHERE matched_by in ('Probable RG Match', 'Manual RG Match')
         UNION
         select NULL             as category,
                matched_order_id as tr_id,
                NULL             as report_date,
                NULL             as alert_handler,
                NULL             as case_source,
                NULL             as alert_id,
                NULL             as is_duplicate_alert,
                NULL             as original_alert_id,
                NULL             as is_refunded_alert,
                NULL             as has_cb_on_cb_alert,
                NULL             as cb_alert_cb_date,
                0                as fees_eur
         from central.sticky_cb_alerts_view_table
         WHERE matched_by in ('Probable RG Match', 'Manual RG Match')
        UNION
                  select 'DISPUTE_ALERT'                                       as category,
                         order_id                                              as tr_id,
                         prevention_timestamp                                  as report_date,
                         'MIDIGATOR'                                           as alert_handler,
                         prevention_type                                       as case_source,
                         prevention_case_number                                as alert_id,
                         0                                                     as is_duplicate_alert,
                         prevention_case_number                                as original_alert_id,
                         CASE WHEN alert_refunded_id is NULL THEN 0 ELSE 1 END as is_refunded_alert,
                         CASE WHEN alert_cb_id is NULL THEN 0 ELSE 1 END       as has_cb_on_cb_alert,
                         alert_cb_date                                         as cb_alert_cb_date,
                         17 / cr.rates                                         AS fees_eur
                  from rocketgate.midigator_alerts mr
                           left join (select distinct tr_original_id as alert_refunded_id
                                      from rocketgate.transactions
                                      where ttype_name IN ('VOID', 'CREDIT')
                                        and reason_desc = 'Success') alert_refunds
                                     ON mr.order_id = alert_refunds.alert_refunded_id
                           left join (select tr_id                             as alert_cb_id,
                                             max(CASE
                                                     WHEN operation = 'CHARGEBACK' THEN tr_chargebackdate
                                                     ELSE chargeback_date END) as alert_cb_date
                                      from rocketgate.chargebacks
                                      where operation in ('CHARGEBACK', 'CHARGEBACK-2', 'CB-REVERSAL', 'CB-REVERSAL-2')
                                      group by 1) alert_cbs ON mr.order_id = alert_cbs.alert_cb_id
                           left join public.currency_rates cr
                                     ON date(cr.date) = date(mr.prevention_timestamp) and cr.currency = 'USD'

                  UNION
                  select NULL     as category,
                         order_id as tr_id,
                         NULL     as report_date,
                         NULL     as alert_handler,
                         NULL     as case_source,
                         NULL     as alert_id,
                         NULL     as is_duplicate_alert,
                         NULL     as original_alert_id,
                         NULL     as is_refunded_alert,
                         NULL     as has_cb_on_cb_alert,
                         NULL     as cb_alert_cb_date,
                         0        as fees_eur
                  from rocketgate.midigator_alerts
     ) rg_disputes ON rg_disputes.tr_id = rgt.tr_id
    left join (
			select invoice_date,
				   invoice_amount/unpaid_leads as avg_cpc_per_lead,
			       invoice_amount,
			       invoice_amount/total_leads as avg_cpa_og
			from rocketgate.opengeo_invoices_cpc
		 ) ogi
				   ON ogi.invoice_date = to_char(tr_date::date,'YYYY-MM')
    left join (
         select tr_date as roas_invoice_date,
                ad_spend,
                leads_count,
                cpa
         from rocketgate.roas_cost_per_lead
    ) roas_cpa
        on roas_cpa.roas_invoice_date = date(tr_date)
where merch_id in ('1599571287', '1601058492')
-- test transactions
  and cust_id_ext not in ('rgtest', 'GoLiveDec29Viri2')
  and cust_id_ext not like 'RG%'
  and upper(cust_id_ext) not like '%3DS%'
  and upper(cust_id_ext) not like '%TEST%'
  and upper(rgt.email) not like '%EXAMPLE%'
  and upper(rgt.email) not like '%ROCKETGATE%'
  and upper(rgt.email) not like '%LINKMEDIA%'
  and billing_country != 'AE'
    -- group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
-- ) group by 1 order by 1 desc
WITH NO SCHEMA BINDING;
