create or replace view rocketgate.affiliate_cost_per_lead(tr_id, inv_id_ext, cust_id_ext, date, affiliate_id, affiliate_name, cost_per_lead_currency, cost_per_lead, platform) as
   SELECT rgt.tr_id,
          rgt.inv_id_ext,
          rgt.cust_id_ext,
       rgt.tr_date                                                 AS date,
       COALESCE(cake.affiliate_id, rgt.affiliate)                  AS affiliate_id,
       cake.affiliate_name                                         AS affiliate_name,
       CASE
           WHEN COALESCE(cake.affiliate_id, rgt.affiliate)::text = '30'::character varying::text OR
                COALESCE(cake.affiliate_id, rgt.affiliate)::text = '78'::character varying::text
               THEN 'EUR'::character varying
           ELSE 'USD'::character varying
           END                                                     AS cost_per_lead_currency,
       cake.cost_per_lead::numeric::numeric(18, 0)::numeric(10, 2) AS cost_per_lead,
       'CAKE'                                                      AS platform
FROM rocketgate.transactions rgt
         JOIN (SELECT date('1970-01-01 00:00:00'::timestamp without time zone +
                           (regexp_substr(conversions.event_conversion_date::text,
                                          '\\d+'::character varying::text)::bigint / 1000)::double precision *
                           '00:00:01'::interval)                         AS event_date,
                      conversions.transaction_id                         AS order_id,
                      conversions."paid.amount"                          AS cost_per_lead,
                      conversions."source_affiliate.source_affiliate_id" AS affiliate_id,
                      conversions."source_affiliate.source_affiliate_name" AS affiliate_name
               FROM cake.conversions INNER JOIN
               (select transaction_id, max(last_updated) as latest_updated from cake.conversions group by 1) cc_latest
               ON conversions.transaction_id = cc_latest.transaction_id and conversions.last_updated =cc_latest.latest_updated
               WHERE conversions.transaction_id IS NOT NULL) cake ON rgt.tr_id::text = cake.order_id::text
UNION
SELECT rgt.tr_id,
       rgt.inv_id_ext,
       rgt.cust_id_ext,
       rgt.tr_date                                                 AS date,
       COALESCE(ef.affiliate_id::character varying, rgt.affiliate) AS affiliate_id,
       ef.affiliate_name                                           AS affiliate_name,
       ef.currency                                                 AS cost_per_lead_currency,
       ef.cost_per_lead,
       'EVERFLOW'                                                  AS platform
FROM rocketgate.transactions rgt
         JOIN (SELECT DISTINCT conversions.adv1                                        AS order_id,
                               date((((conversions."year"::text || '-'::character varying::text) ||
                                      conversions."month"::text) || '-'::character varying::text) ||
                                    conversions."day"::text)                           AS everflow_date,
                               conversions.relationship_affiliate_network_affiliate_id AS affiliate_id,
                               conversions.relationship_affiliate_name                 AS affiliate_name,
                               conversions.currency_id                                 AS currency,
                               conversions.payout                                      AS cost_per_lead
               FROM everflow.conversions
               WHERE conversions.adv1 IS NOT NULL
                 AND conversions.adv1::text <> ''::character varying::text) ef ON ef.order_id::text = rgt.tr_id::text
where tr_id not in (select cc.transaction_id from cake.conversions cc )
WITH NO SCHEMA BINDING;
