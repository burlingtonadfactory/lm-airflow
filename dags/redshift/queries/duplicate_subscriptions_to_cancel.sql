CREATE OR REPLACE VIEW rocketgate.duplicate_subscriptions_to_cancel AS
WITH email_active_subscriptions as (
select distinct cust_email,merchant_invoice_id, rebill_start_date as start_date from (select * from rocketgate.subscriptions_view where merchant_invoice_id not in (select merchant_invoice_id from rocketgate.subscriptions group by 1 having count(distinct cust_email)>1 )) rgs
inner join (select distinct inv_id_ext from rocketgate.transactions where merch_id='1599571287') rgt on rgt.inv_id_ext = rgs.merchant_invoice_id
where rgs.status='Active'
),
cc_active_subscriptions as (
select distinct tui_udf01 as cc_hash, bankbin as cc_first_6, tr_pay_num_l4 as cc_last_4, inv_id_ext, rebill_start_date as start_date from rocketgate.transactions rgt
inner join rocketgate.subscriptions_view  rgs on rgt.inv_id_ext = rgs.merchant_invoice_id and rgs.pay_num_l4 = rgt.tr_pay_num_l4 and rgt.card_name= rgs.card_name
where rgs.status='Active'
and rgt.merch_id='1599571287'
and rgt.billing_type='T'
and rgt.reason_desc='Success'
),
customer_active_subscriptions as
(select distinct cust_email,merchant_invoice_id, fname || ' ' || lname as cust_name, cc_number, rebill_start_date as start_date from (select * from rocketgate.subscriptions_view where merchant_invoice_id not in (select merchant_invoice_id from rocketgate.subscriptions group by 1 having count(distinct cust_email)>1 )) rgs
inner join (select distinct inv_id_ext, fname,lname,bankbin ||'******' || tr_pay_num_l4 as cc_number from rocketgate.transactions where merch_id='1599571287' and reason_desc='Success') rgt on rgt.inv_id_ext = rgs.merchant_invoice_id
where rgs.status='Active')
select merchant_invoice_id as inv_id_ext,cancel_reason from (
                  select merchant_invoice_id,
                         'More than Two Subs on Email : ' || cust_email as cancel_reason,
                         row_number() over ( partition by cust_email order by start_date)    as subscription_rank
                  from email_active_subscriptions
                  where cust_email in
                        (
                            select cust_email
                            from email_active_subscriptions
                            group by 1
                            having count(distinct merchant_invoice_id) > 1
                        )
              )
where subscription_rank > 2
UNION
select inv_id_ext,cancel_reason from (
                  select inv_id_ext,
                         'More than Two Subs on CC : ' || cc_hash || ' - [' || cc_first_6 || '******' || cc_last_4 || ']' as cancel_reason,
                         row_number() over ( partition by cc_hash || ' - [' || cc_first_6 || '******' || cc_last_4 || ']'  order by start_date)    as subscription_rank
                  from cc_active_subscriptions
                  where cc_hash || ' - [' || cc_first_6 || '******' || cc_last_4 || ']'  in
                        (
                            select cc_hash || ' - [' || cc_first_6 || '******' || cc_last_4 || ']'
                            from cc_active_subscriptions
                            group by 1
                            having count(distinct inv_id_ext) > 1
                        )
              )
where subscription_rank > 2
UNION
select merchant_invoice_id as inv_id_ext,cancel_reason from (
                  select merchant_invoice_id,
                         'Suspicious Transactions : ' || cust_email as cancel_reason,
                         rank() over ( partition by cust_email order by start_date)    as subscription_rank
                  from customer_active_subscriptions
                  where cust_email in
                        (
                            select cust_email
                            from customer_active_subscriptions
                            group by 1
                            having count(distinct cust_name) > 1
                            and count(distinct cc_number) > 1
                        )
              )
where subscription_rank > 1;
