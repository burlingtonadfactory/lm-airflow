CREATE OR REPLACE VIEW central.sticky_cb_alerts_view AS
WITH sticky_match as (SELECT CASE
                             WHEN "oh"."order_id" IS NULL THEN 'No Match'
                             ELSE 'Matched By Sticky' END AS       "matched_by"
                           , "scba"."order_id"
                           , (CASE "scba"."provider_account_id"
                                  WHEN '2' THEN 'VERIFI'
                                  WHEN '3' THEN 'ETHOCA'
                                  ELSE 'OTHER' END)                "alert_provider"
                           , "scba"."alert_type_name"
                           , "scba"."alert_date_in_date"
                           , "scba"."card_issuer"
                           , "scba"."cc_first_6"
                           , "scba"."cc_last_4"
                           , "scba"."transaction_date_in_date"
                           , "scba"."transaction_amount"
                           , "scba"."currency"
                           , "scba"."chargeback_amount"
                           , "scba"."arn"
                           , "scba"."provider_alert_id"
                           , "scba"."provider_source"
                           , "scba"."outcome"
                           , "scba"."three_d_secure_status"
                           , "scba"."merchant_descriptor"
                           , "scba"."mid"
                           , "scba"."merch_category_code"
                           , cast("oh"."order_id" as varchar)      "matched_order_id"
                           , cast("oh"."affiliate_id" as varchar)  "affiliate_id"
                           , cast("oh"."sub_affiliate" as varchar) "sub_affiliate"
                           , "oh"."bank_name"
                           , "oh"."gateway_descriptor"
                      FROM (athenacbalerts.sticky_cb_alerts_processed scba
                               LEFT JOIN (
                          SELECT *
                          FROM central.order_header
                          WHERE (("order_status" IN (2, 6, 8)) AND ("order_date" >= '20200101'))
                      ) oh ON ("scba"."order_id" = CAST("oh"."order_id" AS varchar)))
                      WHERE ("scba"."order_id" <> '0')),
     manual_ll as (SELECT 'Manual LL Match'                   "matched_by"
                          , "scba"."order_id"
                          , (CASE "scba"."provider_account_id"
                                 WHEN '2' THEN 'VERIFI'
                                 WHEN '3' THEN 'ETHOCA'
                                 ELSE 'OTHER' END)                "alert_provider"
                          , "scba"."alert_type_name"
                          , "scba"."alert_date_in_date"
                          , "scba"."card_issuer"
                          , "scba"."cc_first_6"
                          , "scba"."cc_last_4"
                          , "scba"."transaction_date_in_date"
                          , "scba"."transaction_amount"
                          , "scba"."currency"
                          , "scba"."chargeback_amount"
                          , "scba"."arn"
                          , "scba"."provider_alert_id"
                          , "scba"."provider_source"
                          , "scba"."outcome"
                          , "scba"."three_d_secure_status"
                          , "scba"."merchant_descriptor"
                          , "scba"."mid"
                          , "scba"."merch_category_code"
                          , cast("oh"."order_id" as varchar)      "matched_order_id"
                          , cast("oh"."affiliate_id" as varchar)  "affiliate_id"
                          , cast("oh"."sub_affiliate" as varchar) "sub_affiliate"
                          , "oh"."bank_name"
                          , "oh"."gateway_descriptor"
                     FROM (athenacbalerts.sticky_cb_alerts_processed scba
                              INNER JOIN central.sticky_alerts_manual_refunds samr
                         ON samr.alert_id = scba.provider_alert_id
                              INNER JOIN (
                         SELECT *
                         FROM central.order_header
                         WHERE (("order_status" IN (2, 6, 8)) AND ("order_date" >= '20200101'))
                     ) oh ON ("samr"."order_id" = CAST("oh"."order_id" AS varchar)))
                     WHERE ("scba"."order_id" = '0')),
     manual_rg as (SELECT 'Manual RG Match'    "matched_by"
                          , "scba"."order_id"
                          , (CASE "scba"."provider_account_id"
                                 WHEN '2' THEN 'VERIFI'
                                 WHEN '3' THEN 'ETHOCA'
                                 ELSE 'OTHER' END) "alert_provider"
                          , "scba"."alert_type_name"
                          , "scba"."alert_date_in_date"
                          , "scba"."card_issuer"
                          , "scba"."cc_first_6"
                          , "scba"."cc_last_4"
                          , "scba"."transaction_date_in_date"
                          , "scba"."transaction_amount"
                          , "scba"."currency"
                          , "scba"."chargeback_amount"
                          , "scba"."arn"
                          , "scba"."provider_alert_id"
                          , "scba"."provider_source"
                          , "scba"."outcome"
                          , "scba"."three_d_secure_status"
                          , "scba"."merchant_descriptor"
                          , "scba"."mid"
                          , "scba"."merch_category_code"
                          , "rgo"."tr_id"          "matched_order_id"
                          , "rgo"."affiliate_id"
                          , "rgo"."sub_affiliate_id_1"
                          , "rgo"."bank"
                          , "rgo"."gateway_descriptor"
                     FROM (
                           athenacbalerts.sticky_cb_alerts_processed scba
                              INNER JOIN central.sticky_alerts_manual_refunds samr
                         ON samr.alert_id = scba.provider_alert_id
                              INNER JOIN (
                         SELECT rgt.tr_id,
                                rgt.tr_date,
                                COALESCE(rgt.affiliate, cpa.affiliate_id)                    as affiliate_id,
                                json_extract_path_text(leads_rgt.lead_tui_udf02, 'ms', true) as sub_affiliate_id_1,
                                CASE COALESCE(CASE
                                                  WHEN rgt.macct_name LIKE 'VIRI%'
                                                      THEN split_part(rgt.macct_name, '-', 2)
                                                  ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                    END)
                                    WHEN 'CON' THEN 'CONCARDIS'
                                    WHEN 'WLD' THEN 'WORLDLINE'
                                    WHEN 'WL' THEN 'WORLDLINE'
                                    WHEN 'WOR' THEN 'WORLDLINE'
                                    WHEN 'PAYSAFE' THEN 'PAYSAFE'
                                    WHEN 'CON3DS' THEN 'CONCARDIS'
                                    WHEN 'KOR' THEN 'KORTA'
                                    WHEN 'ECP' THEN 'ECP'
                                    WHEN 'BS' THEN 'B&S'
                                    WHEN 'NET' THEN 'NETS'
                                    WHEN 'BSP' THEN 'B&S'
                                    WHEN 'NETS' THEN 'NETS'
                                    WHEN 'CHK' THEN 'CHECKOUT'
                                    WHEN 'NET3DS' THEN 'NETS'
                                    WHEN 'PSUSD' THEN 'PAYSAFE'
                                    WHEN 'PSEUR' THEN 'PAYSAFE'
                                    WHEN 'STFS' THEN 'SECURE TRADING FINANCIAL SERVICES (STFS)'
                                    WHEN 'PT' THEN 'PEOPLE\'S TRUST'
                                    WHEN 'PNT' THEN 'PAYNETICS'
                                    ELSE COALESCE(CASE
                                                      WHEN rgt.macct_name LIKE 'VIRI%'
                                                          THEN split_part(rgt.macct_name, '-', 2)
                                                      ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                        END)
                                    END                                                      as bank,
                                rgt.descriptor                                                  "gateway_descriptor",
                                rgt.bankbin,
                                rgt.tr_pay_num_l4
                         FROM rocketgate.transactions rgt
                                  left join
                              (
                                  select inv_id_ext     as lead_inv_id_ext,
                                         min(tr_date)   as lead_tr_date,
                                         min(tui_udf02) as lead_tui_udf02
                                  from rocketgate.transactions
                                  WHERE billing_type in ('T', 'S', 'X-T')
                                  group by 1
                              ) leads_rgt
                              on rgt.inv_id_ext = leads_rgt.lead_inv_id_ext
                                  left join
                              (
                                  select distinct cpa.tr_id, cpa.affiliate_id
                                  from rocketgate.affiliate_cost_per_lead cpa
                              ) cpa ON cpa.tr_id = rgt.tr_id
                         WHERE (rgt."reason_desc" = 'Success' and rgt.ttype_name IN ('SALE','TICKET'))
                     ) rgo ON ("samr"."order_id" = "rgo"."tr_id" ))
                     WHERE ("scba"."order_id" = '0')),
     probable_ll as (SELECT 'Probable LL Match'                   "matched_by"
                          , "scba"."order_id"
                          , (CASE "scba"."provider_account_id"
                                 WHEN '2' THEN 'VERIFI'
                                 WHEN '3' THEN 'ETHOCA'
                                 ELSE 'OTHER' END)                "alert_provider"
                          , "scba"."alert_type_name"
                          , "scba"."alert_date_in_date"
                          , "scba"."card_issuer"
                          , "scba"."cc_first_6"
                          , "scba"."cc_last_4"
                          , "scba"."transaction_date_in_date"
                          , "scba"."transaction_amount"
                          , "scba"."currency"
                          , "scba"."chargeback_amount"
                          , "scba"."arn"
                          , "scba"."provider_alert_id"
                          , "scba"."provider_source"
                          , "scba"."outcome"
                          , "scba"."three_d_secure_status"
                          , "scba"."merchant_descriptor"
                          , "scba"."mid"
                          , "scba"."merch_category_code"
                          , cast("oh"."order_id" as varchar)      "matched_order_id"
                          , cast("oh"."affiliate_id" as varchar)  "affiliate_id"
                          , cast("oh"."sub_affiliate" as varchar) "sub_affiliate"
                          , "oh"."bank_name"
                          , "oh"."gateway_descriptor"
                     FROM (athenacbalerts.sticky_cb_alerts_processed scba
                              INNER JOIN (
                         SELECT *
                         FROM central.order_header
                         WHERE (("order_status" IN (2, 6, 8)) AND ("order_date" >= '20200101'))
                     ) oh ON (((CAST("scba"."cc_first_6" AS varchar) = "oh"."cc_first_6") AND
                               (CAST("scba"."cc_last_4" AS varchar) = LPAD("oh"."cc_last_4", 4, '0000'))) AND
                              ("replace"("scba"."transaction_date_in_date", '-', '') =
                               CAST("oh"."order_date" AS varchar)) AND
                              (upper(trim(split_part("oh"."gateway_descriptor", '+', 1))) =
                               upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                  )))
                     WHERE ("scba"."order_id" = '0')),
     probable_ll_plus as (SELECT 'Probable LL Match'                   "matched_by"
                         , "scba"."order_id"
                         , (CASE "scba"."provider_account_id"
                                WHEN '2' THEN 'VERIFI'
                                WHEN '3' THEN 'ETHOCA'
                                ELSE 'OTHER' END)                "alert_provider"
                         , "scba"."alert_type_name"
                         , "scba"."alert_date_in_date"
                         , "scba"."card_issuer"
                         , "scba"."cc_first_6"
                         , "scba"."cc_last_4"
                         , "scba"."transaction_date_in_date"
                         , "scba"."transaction_amount"
                         , "scba"."currency"
                         , "scba"."chargeback_amount"
                         , "scba"."arn"
                         , "scba"."provider_alert_id"
                         , "scba"."provider_source"
                         , "scba"."outcome"
                         , "scba"."three_d_secure_status"
                         , "scba"."merchant_descriptor"
                         , "scba"."mid"
                         , "scba"."merch_category_code"
                         , cast("oh"."order_id" as varchar)      "matched_order_id"
                         , cast("oh"."affiliate_id" as varchar)  "affiliate_id"
                         , cast("oh"."sub_affiliate" as varchar) "sub_affiliate"
                         , "oh"."bank_name"
                         , "oh"."gateway_descriptor"
                    FROM (athenacbalerts.sticky_cb_alerts_processed scba
                             INNER JOIN (
                        SELECT *
                        FROM central.order_header
                        WHERE (("order_status" IN (2, 6, 8)) AND ("order_date" >= '20200101'))
                    ) oh ON (((CAST("scba"."cc_first_6" AS varchar) = "oh"."cc_first_6") AND
                              (CAST("scba"."cc_last_4" AS varchar) = LPAD("oh"."cc_last_4", 4, '0000'))) AND
                             (to_char(DATEADD('day',1,date("scba"."transaction_date_in_date")),'YYYYMMDD') =
                              CAST("oh"."order_date" AS varchar)) AND
                             (upper(trim(split_part("oh"."gateway_descriptor", '+', 1))) =
                              upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                 )))
                    WHERE ("scba"."order_id" = '0')),
     probable_ll_minus as (SELECT 'Probable LL Match'                   "matched_by"
                         , "scba"."order_id"
                         , (CASE "scba"."provider_account_id"
                                WHEN '2' THEN 'VERIFI'
                                WHEN '3' THEN 'ETHOCA'
                                ELSE 'OTHER' END)                "alert_provider"
                         , "scba"."alert_type_name"
                         , "scba"."alert_date_in_date"
                         , "scba"."card_issuer"
                         , "scba"."cc_first_6"
                         , "scba"."cc_last_4"
                         , "scba"."transaction_date_in_date"
                         , "scba"."transaction_amount"
                         , "scba"."currency"
                         , "scba"."chargeback_amount"
                         , "scba"."arn"
                         , "scba"."provider_alert_id"
                         , "scba"."provider_source"
                         , "scba"."outcome"
                         , "scba"."three_d_secure_status"
                         , "scba"."merchant_descriptor"
                         , "scba"."mid"
                         , "scba"."merch_category_code"
                         , cast("oh"."order_id" as varchar)      "matched_order_id"
                         , cast("oh"."affiliate_id" as varchar)  "affiliate_id"
                         , cast("oh"."sub_affiliate" as varchar) "sub_affiliate"
                         , "oh"."bank_name"
                         , "oh"."gateway_descriptor"
                    FROM (athenacbalerts.sticky_cb_alerts_processed scba
                             INNER JOIN (
                        SELECT *
                        FROM central.order_header
                        WHERE (("order_status" IN (2, 6, 8)) AND ("order_date" >= '20200101'))
                    ) oh ON (((CAST("scba"."cc_first_6" AS varchar) = "oh"."cc_first_6") AND
                              (CAST("scba"."cc_last_4" AS varchar) = LPAD("oh"."cc_last_4", 4, '0000'))) AND
                             (to_char(DATEADD('day',-1,date("scba"."transaction_date_in_date")),'YYYYMMDD') =
                              CAST("oh"."order_date" AS varchar)) AND
                             (upper(trim(split_part("oh"."gateway_descriptor", '+', 1))) =
                              upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                 )))
                    WHERE ("scba"."order_id" = '0')),
     probable_rg as (SELECT 'Probable RG Match'    "matched_by"
                          , "scba"."order_id"
                          , (CASE "scba"."provider_account_id"
                                 WHEN '2' THEN 'VERIFI'
                                 WHEN '3' THEN 'ETHOCA'
                                 ELSE 'OTHER' END) "alert_provider"
                          , "scba"."alert_type_name"
                          , "scba"."alert_date_in_date"
                          , "scba"."card_issuer"
                          , "scba"."cc_first_6"
                          , "scba"."cc_last_4"
                          , "scba"."transaction_date_in_date"
                          , "scba"."transaction_amount"
                          , "scba"."currency"
                          , "scba"."chargeback_amount"
                          , "scba"."arn"
                          , "scba"."provider_alert_id"
                          , "scba"."provider_source"
                          , "scba"."outcome"
                          , "scba"."three_d_secure_status"
                          , "scba"."merchant_descriptor"
                          , "scba"."mid"
                          , "scba"."merch_category_code"
                          , "rgo"."tr_id"          "matched_order_id"
                          , "rgo"."affiliate_id"
                          , "rgo"."sub_affiliate_id_1"
                          , "rgo"."bank"
                          , "rgo"."gateway_descriptor"
                     FROM (
                           athenacbalerts.sticky_cb_alerts_processed scba
                              INNER JOIN (
                         SELECT rgt.tr_id,
                                rgt.tr_date,
                                COALESCE(rgt.affiliate, cpa.affiliate_id)                    as affiliate_id,
                                json_extract_path_text(leads_rgt.lead_tui_udf02, 'ms', true) as sub_affiliate_id_1,
                                CASE COALESCE(CASE
                                                  WHEN rgt.macct_name LIKE 'VIRI%'
                                                      THEN split_part(rgt.macct_name, '-', 2)
                                                  ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                    END)
                                    WHEN 'CON' THEN 'CONCARDIS'
                                    WHEN 'WLD' THEN 'WORLDLINE'
                                    WHEN 'WL' THEN 'WORLDLINE'
                                    WHEN 'WOR' THEN 'WORLDLINE'
                                    WHEN 'PAYSAFE' THEN 'PAYSAFE'
                                    WHEN 'CON3DS' THEN 'CONCARDIS'
                                    WHEN 'KOR' THEN 'KORTA'
                                    WHEN 'ECP' THEN 'ECP'
                                    WHEN 'BS' THEN 'B&S'
                                    WHEN 'NET' THEN 'NETS'
                                    WHEN 'BSP' THEN 'B&S'
                                    WHEN 'NETS' THEN 'NETS'
                                    WHEN 'CHK' THEN 'CHECKOUT'
                                    WHEN 'NET3DS' THEN 'NETS'
                                    WHEN 'PSUSD' THEN 'PAYSAFE'
                                    WHEN 'PSEUR' THEN 'PAYSAFE'
                                    WHEN 'STFS' THEN 'SECURE TRADING FINANCIAL SERVICES (STFS)'
                                    WHEN 'PT' THEN 'PEOPLE\'S TRUST'
                                    WHEN 'PNT' THEN 'PAYNETICS'
                                    ELSE COALESCE(CASE
                                                      WHEN rgt.macct_name LIKE 'VIRI%'
                                                          THEN split_part(rgt.macct_name, '-', 2)
                                                      ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                        END)
                                    END                                                      as bank,
                                rgt.descriptor                                                  "gateway_descriptor",
                                rgt.bankbin,
                                rgt.tr_pay_num_l4
                         FROM rocketgate.transactions rgt
                                  left join
                              (
                                  select inv_id_ext     as lead_inv_id_ext,
                                         min(tr_date)   as lead_tr_date,
                                         min(tui_udf02) as lead_tui_udf02
                                  from rocketgate.transactions
                                  WHERE billing_type in ('T', 'S', 'X-T')
                                  group by 1
                              ) leads_rgt
                              on rgt.inv_id_ext = leads_rgt.lead_inv_id_ext
                                  left join
                              (
                                  select distinct cpa.tr_id, cpa.affiliate_id
                                  from rocketgate.affiliate_cost_per_lead cpa
                              ) cpa ON cpa.tr_id = rgt.tr_id
                         WHERE (rgt."reason_desc" = 'Success' and rgt.ttype_name IN ('SALE','TICKET'))
                     ) rgo ON (((CAST("scba"."cc_first_6" AS varchar) = "rgo"."bankbin") AND
                                (CAST("scba"."cc_last_4" AS varchar) = "rgo"."tr_pay_num_l4")) AND
                               (date("scba"."transaction_date_in_date") = date("rgo"."tr_date")) AND
                               (upper(trim(split_part("rgo"."gateway_descriptor", '+', 1))) =
                                upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                   )
                         ))
                     WHERE ("scba"."order_id" = '0')),
     probable_rg_plus as (SELECT 'Probable RG Match'    "matched_by"
                         , "scba"."order_id"
                         , (CASE "scba"."provider_account_id"
                                WHEN '2' THEN 'VERIFI'
                                WHEN '3' THEN 'ETHOCA'
                                ELSE 'OTHER' END) "alert_provider"
                         , "scba"."alert_type_name"
                         , "scba"."alert_date_in_date"
                         , "scba"."card_issuer"
                         , "scba"."cc_first_6"
                         , "scba"."cc_last_4"
                         , "scba"."transaction_date_in_date"
                         , "scba"."transaction_amount"
                         , "scba"."currency"
                         , "scba"."chargeback_amount"
                         , "scba"."arn"
                         , "scba"."provider_alert_id"
                         , "scba"."provider_source"
                         , "scba"."outcome"
                         , "scba"."three_d_secure_status"
                         , "scba"."merchant_descriptor"
                         , "scba"."mid"
                         , "scba"."merch_category_code"
                         , "rgo"."tr_id"          "matched_order_id"
                         , "rgo"."affiliate_id"
                         , "rgo"."sub_affiliate_id_1"
                         , "rgo"."bank"
                         , "rgo"."gateway_descriptor"
                    FROM (
                          athenacbalerts.sticky_cb_alerts_processed scba
                             INNER JOIN (
                        SELECT rgt.tr_id,
                               rgt.tr_date,
                               COALESCE(rgt.affiliate, cpa.affiliate_id)                    as affiliate_id,
                               json_extract_path_text(leads_rgt.lead_tui_udf02, 'ms', true) as sub_affiliate_id_1,
                               CASE COALESCE(CASE
                                                 WHEN rgt.macct_name LIKE 'VIRI%'
                                                     THEN split_part(rgt.macct_name, '-', 2)
                                                 ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                   END)
                                   WHEN 'CON' THEN 'CONCARDIS'
                                   WHEN 'WLD' THEN 'WORLDLINE'
                                   WHEN 'WL' THEN 'WORLDLINE'
                                   WHEN 'WOR' THEN 'WORLDLINE'
                                   WHEN 'PAYSAFE' THEN 'PAYSAFE'
                                   WHEN 'CON3DS' THEN 'CONCARDIS'
                                   WHEN 'KOR' THEN 'KORTA'
                                   WHEN 'ECP' THEN 'ECP'
                                   WHEN 'BS' THEN 'B&S'
                                   WHEN 'NET' THEN 'NETS'
                                   WHEN 'BSP' THEN 'B&S'
                                   WHEN 'NETS' THEN 'NETS'
                                   WHEN 'CHK' THEN 'CHECKOUT'
                                   WHEN 'NET3DS' THEN 'NETS'
                                   WHEN 'PSUSD' THEN 'PAYSAFE'
                                   WHEN 'PSEUR' THEN 'PAYSAFE'
                                   WHEN 'STFS' THEN 'SECURE TRADING FINANCIAL SERVICES (STFS)'
                                   WHEN 'PT' THEN 'PEOPLE\'S TRUST'
                                   WHEN 'PNT' THEN 'PAYNETICS'
                                   ELSE COALESCE(CASE
                                                     WHEN rgt.macct_name LIKE 'VIRI%'
                                                         THEN split_part(rgt.macct_name, '-', 2)
                                                     ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                       END)
                                   END                                                      as bank,
                               rgt.descriptor                                                  "gateway_descriptor",
                               rgt.bankbin,
                               rgt.tr_pay_num_l4
                        FROM rocketgate.transactions rgt
                                 left join
                             (
                                 select inv_id_ext     as lead_inv_id_ext,
                                        min(tr_date)   as lead_tr_date,
                                        min(tui_udf02) as lead_tui_udf02
                                 from rocketgate.transactions
                                 WHERE billing_type in ('T', 'S', 'X-T')
                                 group by 1
                             ) leads_rgt
                             on rgt.inv_id_ext = leads_rgt.lead_inv_id_ext
                                 left join
                             (
                                 select distinct cpa.tr_id, cpa.affiliate_id
                                 from rocketgate.affiliate_cost_per_lead cpa
                             ) cpa ON cpa.tr_id = rgt.tr_id
                        WHERE (rgt."reason_desc" = 'Success' and rgt.ttype_name IN ('SALE','TICKET'))
                    ) rgo ON (((CAST("scba"."cc_first_6" AS varchar) = "rgo"."bankbin") AND
                               (CAST("scba"."cc_last_4" AS varchar) = "rgo"."tr_pay_num_l4")) AND
                              (DATEADD('day',1,date("scba"."transaction_date_in_date")) = date("rgo"."tr_date")) AND
                              (upper(trim(split_part("rgo"."gateway_descriptor", '+', 1))) =
                               upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                  )
                        ))
                    WHERE ("scba"."order_id" = '0')),
     probable_rg_minus as (SELECT 'Probable RG Match'    "matched_by"
                         , "scba"."order_id"
                         , (CASE "scba"."provider_account_id"
                                WHEN '2' THEN 'VERIFI'
                                WHEN '3' THEN 'ETHOCA'
                                ELSE 'OTHER' END) "alert_provider"
                         , "scba"."alert_type_name"
                         , "scba"."alert_date_in_date"
                         , "scba"."card_issuer"
                         , "scba"."cc_first_6"
                         , "scba"."cc_last_4"
                         , "scba"."transaction_date_in_date"
                         , "scba"."transaction_amount"
                         , "scba"."currency"
                         , "scba"."chargeback_amount"
                         , "scba"."arn"
                         , "scba"."provider_alert_id"
                         , "scba"."provider_source"
                         , "scba"."outcome"
                         , "scba"."three_d_secure_status"
                         , "scba"."merchant_descriptor"
                         , "scba"."mid"
                         , "scba"."merch_category_code"
                         , "rgo"."tr_id"          "matched_order_id"
                         , "rgo"."affiliate_id"
                         , "rgo"."sub_affiliate_id_1"
                         , "rgo"."bank"
                         , "rgo"."gateway_descriptor"
                    FROM (
                          athenacbalerts.sticky_cb_alerts_processed scba
                             INNER JOIN (
                        SELECT rgt.tr_id,
                               rgt.tr_date,
                               COALESCE(rgt.affiliate, cpa.affiliate_id)                    as affiliate_id,
                               json_extract_path_text(leads_rgt.lead_tui_udf02, 'ms', true) as sub_affiliate_id_1,
                               CASE COALESCE(CASE
                                                 WHEN rgt.macct_name LIKE 'VIRI%'
                                                     THEN split_part(rgt.macct_name, '-', 2)
                                                 ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                   END)
                                   WHEN 'CON' THEN 'CONCARDIS'
                                   WHEN 'WLD' THEN 'WORLDLINE'
                                   WHEN 'WL' THEN 'WORLDLINE'
                                   WHEN 'WOR' THEN 'WORLDLINE'
                                   WHEN 'PAYSAFE' THEN 'PAYSAFE'
                                   WHEN 'CON3DS' THEN 'CONCARDIS'
                                   WHEN 'KOR' THEN 'KORTA'
                                   WHEN 'ECP' THEN 'ECP'
                                   WHEN 'BS' THEN 'B&S'
                                   WHEN 'NET' THEN 'NETS'
                                   WHEN 'BSP' THEN 'B&S'
                                   WHEN 'NETS' THEN 'NETS'
                                   WHEN 'CHK' THEN 'CHECKOUT'
                                   WHEN 'NET3DS' THEN 'NETS'
                                   WHEN 'PSUSD' THEN 'PAYSAFE'
                                   WHEN 'PSEUR' THEN 'PAYSAFE'
                                   WHEN 'STFS' THEN 'SECURE TRADING FINANCIAL SERVICES (STFS)'
                                   WHEN 'PT' THEN 'PEOPLE\'S TRUST'
                                   WHEN 'PNT' THEN 'PAYNETICS'
                                   ELSE COALESCE(CASE
                                                     WHEN rgt.macct_name LIKE 'VIRI%'
                                                         THEN split_part(rgt.macct_name, '-', 2)
                                                     ELSE reverse(split_part(reverse(rgt.macct_name), '-', 1))
                                       END)
                                   END                                                      as bank,
                               rgt.descriptor                                                  "gateway_descriptor",
                               rgt.bankbin,
                               rgt.tr_pay_num_l4
                        FROM rocketgate.transactions rgt
                                 left join
                             (
                                 select inv_id_ext     as lead_inv_id_ext,
                                        min(tr_date)   as lead_tr_date,
                                        min(tui_udf02) as lead_tui_udf02
                                 from rocketgate.transactions
                                 WHERE billing_type in ('T', 'S', 'X-T')
                                 group by 1
                             ) leads_rgt
                             on rgt.inv_id_ext = leads_rgt.lead_inv_id_ext
                                 left join
                             (
                                 select distinct cpa.tr_id, cpa.affiliate_id
                                 from rocketgate.affiliate_cost_per_lead cpa
                             ) cpa ON cpa.tr_id = rgt.tr_id
                        WHERE (rgt."reason_desc" = 'Success' and rgt.ttype_name IN ('SALE','TICKET'))
                    ) rgo ON (((CAST("scba"."cc_first_6" AS varchar) = "rgo"."bankbin") AND
                               (CAST("scba"."cc_last_4" AS varchar) = "rgo"."tr_pay_num_l4")) AND
                              (DATEADD('day',-1,date("scba"."transaction_date_in_date")) = date("rgo"."tr_date")) AND
                              (upper(trim(split_part("rgo"."gateway_descriptor", '+', 1))) =
                               upper(trim(split_part("scba"."merchant_descriptor", '+', 1)))
                                  )
                        ))
                    WHERE ("scba"."order_id" = '0')),
     no_match as (SELECT 'No Match'             "matched_by"
                       , "scba"."order_id"
                       , (CASE "scba"."provider_account_id"
                              WHEN '2' THEN 'VERIFI'
                              WHEN '3' THEN 'ETHOCA'
                              ELSE 'OTHER' END) "alert_provider"
                       , "scba"."alert_type_name"
                       , "scba"."alert_date_in_date"
                       , "scba"."card_issuer"
                       , "scba"."cc_first_6"
                       , "scba"."cc_last_4"
                       , "scba"."transaction_date_in_date"
                       , "scba"."transaction_amount"
                       , "scba"."currency"
                       , "scba"."chargeback_amount"
                       , "scba"."arn"
                       , "scba"."provider_alert_id"
                       , "scba"."provider_source"
                       , "scba"."outcome"
                       , "scba"."three_d_secure_status"
                       , "scba"."merchant_descriptor"
                       , "scba"."mid"
                       , "scba"."merch_category_code"
                       , null                   "matched_order_id"
                       , null                   "affiliate_id"
                       , null                   "sub_affiliate"
                       , null                   "bank_name"
                       , null                   "gateway_descriptor"
                  FROM athenacbalerts.sticky_cb_alerts_processed scba
                  WHERE ("scba"."order_id" = '0'))
select *,
      CASE "row_number"() OVER (PARTITION BY matched_order_id ORDER BY alert_date_in_date ASC)
        WHEN 1 THEN 0
        ELSE 1 END as is_duplicate_alert,
      "first_value"(provider_alert_id) over (PARTITION BY matched_order_id ORDER BY alert_date_in_date ASC ROWS UNBOUNDED PRECEDING)
         as original_provider_alert_id
from (
         select *
         from sticky_match
         union
         (select *
         from manual_ll
         where provider_alert_id not in (select provider_alert_id from sticky_match)
         )
         union
         (select *
         from manual_rg
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll))
         union
         (select *
         from probable_ll
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg))
         union
         (select *
         from probable_ll_plus
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll))
         union
         (select *
         from probable_ll_minus
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll_plus))
         union
         (select *
         from probable_rg
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll_plus)
            and provider_alert_id not in (select provider_alert_id from probable_ll_minus))
         union
         (select *
         from probable_rg_plus
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll_plus)
            and provider_alert_id not in (select provider_alert_id from probable_ll_minus)
            and provider_alert_id not in (select provider_alert_id from probable_rg))
         union
         (select *
         from probable_rg_minus
         where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll_plus)
            and provider_alert_id not in (select provider_alert_id from probable_ll_minus)
            and provider_alert_id not in (select provider_alert_id from probable_rg)
            and provider_alert_id not in (select provider_alert_id from probable_rg_plus))
         union
         (select *
          from no_match
          where provider_alert_id not in (select provider_alert_id from sticky_match)
            and provider_alert_id not in (select provider_alert_id from manual_ll)
            and provider_alert_id not in (select provider_alert_id from manual_rg)
            and provider_alert_id not in (select provider_alert_id from probable_ll)
            and provider_alert_id not in (select provider_alert_id from probable_ll_plus)
            and provider_alert_id not in (select provider_alert_id from probable_ll_minus)
            and provider_alert_id not in (select provider_alert_id from probable_rg)
            and provider_alert_id not in (select provider_alert_id from probable_rg_plus)
            and provider_alert_id not in (select provider_alert_id from probable_rg_minus))
     ) scbav
         LEFT JOIN (
    SELECT "alert_month"
         , (CASE "alert_provider" WHEN '2' THEN 'VERIFI' WHEN '3' THEN 'ETHOCA' ELSE 'OTHER' END) "alert_provider_name"
         , (CAST("rate" AS DOUBLE PRECISION) / "rates")                                           "alert_fees_in_eur"
    FROM (((
        SELECT "substring"("alert_date_in_date", 1, 7) "alert_month"
             , "provider_account_id"                   "alert_provider"
             , "count"(DISTINCT "provider_alert_id")   "alert_count"
             , "max"("alert_date_in_date")             "max_alert_date"
        FROM athenacbalerts.sticky_cb_alerts_processed
        GROUP BY 1, 2
    ) sticky_cb_alert_counts
        LEFT JOIN athenacbalerts.sticky_cb_alerts_fees_tier ON (
                ("sticky_cb_alert_counts"."alert_count" BETWEEN CAST("min_txn" AS bigint) AND CAST("max_txn" AS bigint)) AND
                ("sticky_cb_alert_counts"."alert_provider" = "sticky_cb_alerts_fees_tier"."provider_id")))
             LEFT JOIN public.currency_rates
                       ON (("sticky_cb_alerts_fees_tier"."currency" = "currency_rates"."currency") AND
                           ("date"("sticky_cb_alert_counts"."max_alert_date") = "date"("currency_rates"."date"))))
) sticky_cb_alert_fees ON (("substring"("scbav"."alert_date_in_date", 1, 7) = "sticky_cb_alert_fees"."alert_month") AND
                           ("scbav"."alert_provider" = "sticky_cb_alert_fees"."alert_provider_name"))
WITH NO SCHEMA BINDING;
