create or replace view rocketgate.leads_first_rebill(customer_id, invoice_id, tr_lead, first_rebill) as
SELECT transactions.cust_id_ext AS customer_id,
       transactions.inv_id_ext AS invoice_id,
       "max"(
               CASE
                   WHEN transactions.billing_type = 'T' THEN transactions.tr_id
                   ELSE NULL
                   END)   AS tr_lead,
       "max"(
               CASE
                   WHEN transactions.billing_type = 'C' THEN transactions.tr_id
                   ELSE NULL
                   END)   AS first_rebill
FROM rocketgate.transactions
WHERE transactions.reason_desc = 'Success'
  AND transactions.ttype_name = 'SALE'
  AND (transactions.billing_type = 'T' OR transactions.billing_type = 'C')
GROUP BY transactions.cust_id_ext, transactions.inv_id_ext
HAVING "max"(
               CASE
                   WHEN transactions.billing_type = 'C' THEN transactions.tr_id
                   ELSE NULL
                   END) IS NOT NULL
WITH NO SCHEMA BINDING;
