import json
import requests
import boto3
import logging
import pandas as pd
import numpy as np
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.models import Variable
from airflow import AirflowException
from datetime import datetime, date, timedelta

# RG_MERCH1 = Variable.get("RG_MERCH1", deserialize_json=True, default_var=None)
RG_MERCHANTS = json.loads(Variable.get("RG_MERCHANTS"))
RG_HISTORICAL_START = Variable.get("RG_HISTORICAL_START")
RG_HISTORICAL_END = Variable.get("RG_HISTORICAL_END")
TODAY = datetime.strftime(datetime.now(), '%Y-%m-%d')
YESTERDAY = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
# RG_MERCH1 = Variable.get("RG_MERCH1", default_var=None)

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn


def redshift_execute(sql):

    red_conn = get_redshift_conn()
    with red_conn.connect() as con:
        con.execution_options(isolation_level="AUTOCOMMIT").execute(sql)
        con.close()


with DAG(
    'REDSHIFT_5MINS_UPDATES',
    default_args=default_args,
    schedule_interval='*/5 * * * *',
    catchup=False,
    dagrun_timeout=timedelta(minutes=2),
    tags=['REDSHIFT']
) as dag:
    rg_membership_view = PythonOperator(
        task_id='update_rg_membership_view',
        python_callable=redshift_execute,
        op_kwargs={
            'sql': 'REFRESH MATERIALIZED VIEW rocketgate.memberships_view'
        }

    )
