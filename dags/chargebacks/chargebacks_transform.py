import datetime
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException
import pandas as pd
import logging
# local packages
from linkmedia.chargebacks_transform import chargebacks_csv_controller as cbcc
from linkmedia.chargebacks_transform import chargebacks_pq_controller as cbpc

default_args = {
    'owner': 'Naseef',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 3,
    'retry_delay': datetime.timedelta(minutes=2),
}

dag = DAG(
    dag_id='CHARGEBACKS_TRANSFORM',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 */2 * * *',
    dagrun_timeout=datetime.timedelta(minutes=90),
    tags=['CHARGEBACKS_TRANSFORM'])


# def get_chargeback_data():

#     try:
#         chargebacks_csv = cbcc.chargebacks_process_csv()
#         chargebacks_pq = cbpc.chargebacks_process_pq()

#         #get the data

#         # chargebacks_csv.get_chargebacks_csv()
#         # chargebacks_pq.get_chargebacks_pq()
#     except Exception as e:
#         raise AirflowException(e)


def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()
    return conn


def load_chargeback_data_redshift():

    # loop and load chargeback data to redshift
    try:
        logging.info('Loading data')
        copy_cmd = """
        CREATE TEMP TABLE chargeback_order_cc_tmp (order_cc_key VARCHAR(100), transaction_type VARCHAR(20), bank VARCHAR(50), cb_date VARCHAR(10), has_tran_id VARCHAR(1), order_cc_type VARCHAR(50), order_cc_key_cc_6 VARCHAR(6), order_cc_key_cc_4 VARCHAR(4), order_cc_key_order_date VARCHAR(8), order_cc_key_gtw_desc VARCHAR(50), order_cc_key_order_amount VARCHAR(8));
        COPY chargeback_order_cc_tmp
        from 's3://linkmedia-datalake/chargebacks/all_chargebacks_datawarehouse/order_cc/'
        iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
        gzip
        delimiter '\t'
        NULL 'nan'
        region 'eu-central-1'
        CSV
        IGNOREHEADER 1
        emptyasnull
        blanksasnull;

        delete from central.chargebacks_all_chargebacks where partition_0 ='order_cc';
        insert into central.chargebacks_all_chargebacks (select order_cc_key, transaction_type, bank, cb_date, has_tran_id, order_cc_type, order_cc_key_cc_6, order_cc_key_cc_4, order_cc_key_order_date, order_cc_key_gtw_desc, order_cc_key_order_amount, NULL AS order_id, NULL AS order_date, NULL AS transaction_id, 'order_cc' AS partition_0 from chargeback_order_cc_tmp);



        CREATE TEMP TABLE chargeback_order_id_tmp (order_id VARCHAR(50), transaction_type VARCHAR(20), bank VARCHAR(50), cb_date VARCHAR(10), has_tran_id VARCHAR(1), order_date VARCHAR(10));
        COPY chargeback_order_id_tmp
        from 's3://linkmedia-datalake/chargebacks/all_chargebacks_datawarehouse/order_id/'
        iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
        gzip
        delimiter '\t'
        NULL 'nan'
        region 'eu-central-1'
        CSV
        IGNOREHEADER 1
        emptyasnull
        blanksasnull;

        delete from central.chargebacks_all_chargebacks where partition_0 ='order_id';
        insert into central.chargebacks_all_chargebacks (select NULL AS order_cc_key, transaction_type, bank, cb_date, has_tran_id, NULL AS order_cc_type, NULL AS order_cc_key_cc_6, NULL AS order_cc_key_cc_4, NULL AS order_cc_key_order_date, NULL AS order_cc_key_gtw_desc, NULL AS order_cc_key_order_amount, order_id, order_date, NULL AS transaction_id, 'order_id' AS partition_0 from chargeback_order_id_tmp);



        CREATE TEMP TABLE chargeback_transaction_id_tmp (transaction_id VARCHAR(50), transaction_type VARCHAR(20), bank VARCHAR(50), cb_date VARCHAR(10), has_tran_id VARCHAR(1));
        COPY chargeback_transaction_id_tmp
        from 's3://linkmedia-datalake/chargebacks/all_chargebacks_datawarehouse/transaction_id/'
        iam_role 'arn:aws:iam::560670647316:role/redshiftSpectrum'
        gzip
        delimiter '\t'
        NULL 'nan'
        region 'eu-central-1'
        CSV
        IGNOREHEADER 1
        emptyasnull
        blanksasnull;

        delete from central.chargebacks_all_chargebacks where partition_0 ='transaction_id';
        insert into central.chargebacks_all_chargebacks (select NULL AS order_cc_key, transaction_type, bank, cb_date, has_tran_id, NULL AS order_cc_type, NULL AS order_cc_key_cc_6, NULL AS order_cc_key_cc_4, NULL AS order_cc_key_order_date, NULL AS order_cc_key_gtw_desc, NULL AS order_cc_key_order_amount, NULL AS order_id, NULL AS order_date, transaction_id, 'transaction_id' AS partition_0 from chargeback_transaction_id_tmp);
        """
        engine_redshift = get_redshift_conn()
        with engine_redshift.connect() as con:
            con.execute(copy_cmd)
            con.close()
        logging.info('Data loaded successfully')

    except Exception as e:
        raise AirflowException(e)

cbs_csv = cbcc.chargebacks_process_csv()
cbs_pq = cbpc.chargebacks_process_pq()

start = DummyOperator(task_id='start', dag=dag)
end = DummyOperator(task_id='end', dag=dag)

# get_chargeback_data = PythonOperator(
#     task_id='get_chargeback_data',
#     python_callable=get_chargeback_data,
#     trigger_rule='all_done',
#     dag=(dag),
# )

combine_cbs_pq = PythonOperator(
    task_id='combine_all_chargebacks',
    python_callable=cbs_pq.combine_all_cbs,
    trigger_rule='all_done',
    dag=(dag),
)

checkout_csv = PythonOperator(
    task_id='get_checkout_csv',
    python_callable=cbs_csv.get_checkout_cbs_csv,
    dag=(dag),
)

checkout_pq = PythonOperator(
    task_id='get_checkout_pq',
    python_callable=cbs_pq.get_checkout_cbs_pq,
    dag=(dag),
)

start >> checkout_csv >> checkout_pq >> combine_cbs_pq

paysafe_csv = PythonOperator(
    task_id='get_paysafe_csv',
    python_callable=cbs_csv.get_paysafe_cbs_csv,
    dag=(dag),
)

paysafe_pq = PythonOperator(
    task_id='get_paysafe_pq',
    python_callable=cbs_pq.get_paysafe_cbs_pq,
    dag=(dag),
)

start >> paysafe_csv >> paysafe_pq >> combine_cbs_pq

emp_csv = PythonOperator(
    task_id='get_emp_csv',
    python_callable=cbs_csv.get_emp_cbs_csv,
    dag=(dag),
)

emp_pq = PythonOperator(
    task_id='get_emp_pq',
    python_callable=cbs_pq.get_emp_cbs_pq,
    dag=(dag),
)

start >> emp_csv >> emp_pq >> combine_cbs_pq

epro_csv = PythonOperator(
    task_id='get_epro_csv',
    python_callable=cbs_csv.get_epro_cbs_csv,
    dag=(dag),
)

epro_pq = PythonOperator(
    task_id='get_epro_pq',
    python_callable=cbs_pq.get_epro_cbs_pq,
    dag=(dag),
)

start >> epro_csv >> epro_pq >> combine_cbs_pq

billpro_csv = PythonOperator(
    task_id='get_billpro_csv',
    python_callable=cbs_csv.get_billpro_cbs_csv,
    dag=(dag),
)

billpro_pq = PythonOperator(
    task_id='get_billpro_pq',
    python_callable=cbs_pq.get_billpro_cbs_pq,
    dag=(dag),
)

start >> billpro_csv >> billpro_pq >> combine_cbs_pq

payvision_csv = PythonOperator(
    task_id='get_payvision_csv',
    python_callable=cbs_csv.get_payvision_cbs_csv,
    dag=(dag),
)

payvision_pq = PythonOperator(
    task_id='get_payvision_pq',
    python_callable=cbs_pq.get_payvision_cbs_pq,
    dag=(dag),
)

start >> payvision_csv >> payvision_pq >> combine_cbs_pq

vp_csv = PythonOperator(
    task_id='get_vp_csv',
    python_callable=cbs_csv.get_vp_cbs_csv,
    dag=(dag),
)

vp_pq = PythonOperator(
    task_id='get_vp_pq',
    python_callable=cbs_pq.get_vp_cbs_pq,
    dag=(dag),
)

start >> vp_csv >> vp_pq >> combine_cbs_pq

paynetics_csv = PythonOperator(
    task_id='get_paynetics_csv',
    python_callable=cbs_csv.get_paynetics_cbs_csv,
    dag=(dag),
)

paynetics_pq = PythonOperator(
    task_id='get_paynetics_pq',
    python_callable=cbs_pq.get_paynetics_cbs_pq,
    dag=(dag),
)

start >> paynetics_csv >> paynetics_pq >> combine_cbs_pq

concardis_csv = PythonOperator(
    task_id='get_concardis_csv',
    python_callable=cbs_csv.get_concardis_cbs_csv,
    dag=(dag),
)

concardis_pq = PythonOperator(
    task_id='get_concardis_pq',
    python_callable=cbs_pq.get_concardis_cbs_pq,
    dag=(dag),
)

start >> concardis_csv >> concardis_pq >> combine_cbs_pq

nets_csv = PythonOperator(
    task_id='get_nets_csv',
    python_callable=cbs_csv.get_nets_cbs_csv,
    dag=(dag),
)

nets_pq = PythonOperator(
    task_id='get_nets_pq',
    python_callable=cbs_pq.get_nets_cbs_pq,
    dag=(dag),
)

start >> nets_csv >> nets_pq >> combine_cbs_pq



load_chargeback_data_redshift = PythonOperator(
    task_id='load_chargeback_data_redshift',
    python_callable=load_chargeback_data_redshift,
    dag=(dag),
)


combine_cbs_pq >> load_chargeback_data_redshift >> end
