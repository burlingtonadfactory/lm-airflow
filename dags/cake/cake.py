
import os
import glob
import requests
import logging
import datetime
import pandas as pd
from smart_open.s3 import iter_bucket as s3_iter_bucket
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag_current = DAG(
    dag_id='CAKE_CURRENT',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 * * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['TRANSFORM','CAKE'])

dag_yday = DAG(
    dag_id='CAKE_YESTERDAY',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 2,4,8 * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['TRANSFORM','CAKE'])

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

# def get_cake_conversions(run_date):

#     try:
#         date = datetime.datetime.strptime(run_date, '%Y-%m-%d')
#     except Exception as e:
#         raise AirflowException(e)

#     ##Path to load data from
#     load_bucket = 'datalake-imports/{}/{}/CakeImport/'.format(str(date.year), run_date)

#     ##Path to save data
#     dest_file_ireland = 's3://datalake-process/cake/conversions/{}/{}_cake.parquet'.format(run_date.replace('-','/'), run_date)
#     #replicating to frankfurt
#     dest_file_frankfurt = 's3://linkmedia-datalake/cake/conversions/{}/{}_cake.parquet'.format(run_date.replace('-','/'), run_date)
    
#     ## read cake data
#     cake_data = pd.concat([json_normalize(pd.read_json(content)['d']['event_conversions']) for key, content in s3_iter_bucket(
#             bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.json'), workers=1)], sort=False, ignore_index=True)

#     # some events get updates, so keep the last updated value and drop others to remove duplicates
#     cake_data = cake_data.sort_values(by=['last_updated'], ascending=True).drop_duplicates(['transaction_id'], keep = 'last')
#     df = cake_data.astype(str)
#     print("saving history file to {}".format(dest_file_ireland))
#     df.to_parquet(dest_file_ireland, compression='gzip')

#     print("saving history file to {}".format(dest_file_frankfurt))
#     df.to_parquet(dest_file_frankfurt, compression='gzip')
    
    

#     # get affiliate  data
#     affiliates = self.get_cake_affiliates()
#     # save to parquet file
#     rs.save_to_parquet(affiliates, 's3://'+save_bucket+'/cake/affiliates/', 'affiliates')
#     # save to parquet file in frankfurt
#     rs.save_to_parquet(affiliates, 's3://'+save_bucket_ff+'/cake/affiliates/', 'affiliates')
#     #drop data to free up memory
#     affiliates = pd.DataFrame()

# def fetch_cake_affiliates(): 
#     cake_affiliate_schema = cs.cake_affiliate_schema

#     api_url =  'http://mlrmngt.com/api/5/export.asmx/Affiliates'
#     api_request_data = {
#         'api_key': os.getenv('CAKE_API_KEY', None),
#         'affiliate_id':'0',
#         'affiliate_name':'',
#         'account_manager_id':'0',
#         'tag_id':'0',
#         'start_at_row':'1',
#         'row_limit':'100000',
#         'sort_field':'affiliate_id',
#         'sort_descending':'FALSE'
#         }
    
#     response  = requests.post(api_url, json=api_request_data)
#     json_response = response.json()
#     json_response_affiliates = json_response['d']['affiliates']
#     affiliates = pd.read_json(json.dumps(json_response_affiliates), orient='columns').astype(cake_affiliate_schema)
#     affiliates['account_manager_id'] = pd.Series()
#     affiliates['account_manager_name'] = pd.Series()
#     for idx, row in affiliates.iterrows():
#         affiliates['account_manager_id'][idx] = dict(ast.literal_eval(row['account_managers'])[0])['contact_id']
#         affiliates['account_manager_name'][idx] = dict(ast.literal_eval(row['account_managers'])[0])['contact_name']
#     #return affiliates

#     ##Path to save data
#     dest_file_ireland = 's3://datalake-process/cake/affiliates/affiliates.parquet'
#     #replicating to frankfurt
#     dest_file_frankfurt = 's3://linkmedia-datalake/cake/affiliates/affiliates.parquet'

#     print("saving history file to {}".format(dest_file_ireland))
#     affiliates.to_parquet(dest_file_ireland, compression='gzip')

#     print("saving history file to {}".format(dest_file_frankfurt))
#     affiliates.to_parquet(dest_file_frankfurt, compression='gzip')

def update_cake_redshift(run_date):
    year, month, day = run_date.split('-')
    copy_cmd = """
        delete from cake.conversions where partition_0='{year}' and partition_1='{month}' and partition_2='{day}';

        insert into cake.conversions (select * from athenacake.conversions where partition_0='{year}' and partition_1='{month}' and partition_2='{day}');
    """.format(year=year, month=month, day=day)
    engine_redshift = get_redshift_conn()
    with engine_redshift.connect() as con:
        con.execute(copy_cmd)
        con.close()

today = datetime.datetime.today().strftime('%Y-%m-%d')
yesterday = (datetime.datetime.today() - datetime.timedelta(1)).strftime('%Y-%m-%d')

sync_conversions_current = PythonOperator(
    task_id='sync_conversions',
    python_callable=update_cake_redshift,
    op_kwargs={'run_date': today},
    dag=dag_current,
)

sync_conversions_yday = PythonOperator(
    task_id='sync_conversions',
    python_callable=update_cake_redshift,
    op_kwargs={'run_date': yesterday},
    dag=dag_yday,
)

# sync_conversions_yday = PythonOperator(
#     task_id='sync_conversions',
#     python_callable=get_cake_conversions,
#     op_kwargs={'run_date': yesterday},
#     dag=dag_yday,
# )

# sync_affiliates = PythonOperator(
#     task_id='sync_affiliates',
#     python_callable=fetch_cake_affiliates,    
#     dag=dag_yday,
# )


# sync_conversions_yday >> sync_affiliates