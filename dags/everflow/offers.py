
import os
import glob
import requests
import logging
import datetime
import pandas as pd
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='EVERFLOW_OFFERS',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 3,6,9 * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['EVERFLOW'])

temp_dir = '/tmp'
today = datetime.datetime.today().strftime('%Y-%m-%d')
year, month, day = today.split('-')

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def fetch_offers():
    api_url = 'https://api.eflow.team/v1/networks/offers?page_size=500'
    newHeaders = {'Content-type': 'application/json', 'x-eflow-api-key': os.getenv('EVERFLOW_API_KEY', None)}
    response = requests.get(api_url,headers=newHeaders)

    logging.info("Status code: {}".format(response.status_code))
    response_Json = response.json()
    if response.status_code == 200:
        logging.info('lenght of data: {}'.format(len(response_Json['offers'])))
        data_offers=[]
        for cl in response_Json['offers']:
            d2=flatten_json(cl)
            normal = pd.json_normalize(d2)
            data_offers.append((normal)) 

        offers_results = pd.concat(data_offers)
        offers_results.to_parquet('{}/{}-offers.parquet'.format(temp_dir, today),compression='gzip')
    else:
        raise AirflowException(response_Json['Error'])

# def fetch_offers():
#     logging.info('the API key {}'.format(os.getenv('EVERFLOW_API_KEY', None)))

fetch_offers = PythonOperator(
    task_id='fetch_offers',
    python_callable=fetch_offers,
    dag=dag,
)

sync_to_datalake = BashOperator(
    task_id='sync_to_datalake',
    bash_command='mkdir offers && cp {}/{}-offers.parquet offers && aws s3 sync --delete offers/ s3://linkmedia-datalake/test/everflow/offers/'.format(temp_dir, today),
    dag=dag,
)

fetch_offers >> sync_to_datalake

# fetch_conversions = PythonVirtualenvOperator(
#     task_id="fetch_conversions",
#     python_callable=fetch_conversions,
#     requirements=[],
#     system_site_packages=True,
#     dag=(dag),
# )