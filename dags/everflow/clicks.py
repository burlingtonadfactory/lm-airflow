import os
import requests
import logging
import datetime
import pandas as pd
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='EVERFLOW_CLICKS',
    default_args=default_args,
    catchup=False,
    schedule_interval='*/10 * * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['EVERFLOW'])

temp_dir = '/tmp/clicks'
#today = datetime.datetime.today().strftime('%Y-%m-%d')
today='2020-10-25'
year, month, day = today.split('-')

if not os.path.exists(temp_dir):
    os.makedirs(temp_dir)

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def fetch_clicks():
    api_url = 'https://api.eflow.team/v1/networks/reporting/clicks/stream?page_size=10000'
    newHeaders = {'Content-type': 'application/json', 'x-eflow-api-key': os.getenv('EVERFLOW_API_KEY', None)}
    payload='{"from": "'+today+' 00:00:00","to": "'+today+' 23:59:59","timezone_id": 63}'
    response = requests.post(api_url, data=payload,headers=newHeaders)

    logging.info("Status code: {}".format(response.status_code))
    response_Json = response.json()
    if response.status_code == 200:
        logging.info('lenght of data: {}'.format(len(response_Json['clicks'])))
        data_clicks=[]
        for cl in response_Json['clicks']:
            d2=flatten_json(cl)
            normal = pd.json_normalize(d2)
            data_clicks.append((normal)) 

        clicks_results = pd.concat(data_clicks)
        clicks_results.to_parquet('{}/{}-everflow.parquet'.format(temp_dir, today),compression='gzip')
    else:
        raise AirflowException(response_Json['Error'])

fetch_clicks = PythonOperator(
    task_id='fetch_clicks',
    python_callable=fetch_clicks,
    #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
    dag=dag,
)

s3_cmd = 'aws s3 ls s3://linkmedia-datalake/everflow/conversions/year={}/month={}/day={}/'.format(year, month, day)

sync_to_datalake = BashOperator(
    task_id='sync_to_datalake',
    bash_command=s3_cmd,
    dag=dag,
)

fetch_clicks >> sync_to_datalake

# fetch_conversions = PythonVirtualenvOperator(
#     task_id="fetch_conversions",
#     python_callable=fetch_conversions,
#     requirements=[],
#     system_site_packages=True,
#     dag=(dag),
# )