
import os
import glob
import requests
import logging
import datetime
import pandas as pd
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(1),
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=1),
}

dag = DAG(
    dag_id='EVERFLOW_TEXT',
    default_args=default_args,
    catchup=False,
    schedule_interval='@once',
    dagrun_timeout=datetime.timedelta(minutes=15),
    tags=['EVERFLOW'])

temp_dir = '/tmp'
today = datetime.datetime.today().strftime('%Y-%m-%d')
#year, month, day = today.split('-')

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out


def fetch_conversions(start_date, end_date):

    api_url = 'https://api.eflow.team/v1/networks/reporting/conversions?page_size=2000'
    newHeaders = {'Content-type': 'application/json', 'x-eflow-api-key': os.getenv('EVERFLOW_API_KEY', None)}
    daterange = pd.date_range(start_date, end_date)
    for run_date in daterange:
        run_date = run_date.strftime("%Y-%m-%d")
        year, month, day = run_date.split('-')
        payload='{"show_conversions": true,"show_events": true,"from": "'+run_date+' 00:00:00","to": "'+run_date+' 23:59:59","timezone_id": 63}'
        response = requests.post(api_url, data=payload,headers=newHeaders)

        logging.info("Status code: {}".format(response.status_code))
        response_Json = response.json()
        if response.status_code == 200:
            logging.info('lenght of data: {}'.format(len(response_Json['conversions'])))
            data_conversions=[]
            for cl in response_Json['conversions']:
                d2=flatten_json(cl)
                normal = pd.json_normalize(d2)
                data_conversions.append((normal)) 

            if len(data_conversions) > 0:
                conversions_results = pd.concat(data_conversions)
                unq_adv1 = len(conversions_results.adv1.unique())
            else:
                conversions_results = pd.DataFrame()
                unq_adv1 = 0

            #conversions_results.to_parquet('{}/{}-conversions.parquet'.format(temp_dir, run_date),compression='gzip')
            conversions_results.to_parquet('s3://linkmedia-datalake/everflow/conversions/year={}/month={}/day={}/{}-conversions.parquet'.format(year, month, day, run_date))
            logging.info('for {} we have {} distinct adv1 values & {} rows of conversions'.format(run_date, unq_adv1, len(conversions_results)))
            logging.info('saved at location s3://linkmedia-datalake/everflow/conversions/year={}/month={}/day={}/{}-conversions.parquet'.format(year, month, day, run_date))
        else:
            raise AirflowException(response_Json['Error'])

# def fetch_offers():
#     logging.info('the API key {}'.format(os.getenv('EVERFLOW_API_KEY', None)))

fetch_conversions_task = PythonOperator(
    task_id='fetch_conversions',
    python_callable=fetch_conversions,
    op_kwargs={'start_date': '2020-11-07', 'end_date': '2020-11-10'},
    dag=dag,
)
# cmd = """echo {{execution_date}}"""

# sync_to_datalake = BashOperator(
#     task_id='sync_to_datalake',
#     bash_command=cmd,
#     dag=dag,
# )
