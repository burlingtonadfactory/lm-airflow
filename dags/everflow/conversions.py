import os
import glob
import requests
import logging
import datetime
import pandas as pd
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='EVERFLOW_CONVERSIONS',
    default_args=default_args,
    catchup=False,
    schedule_interval='*/10 * * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['EVERFLOW'])

temp_dir = '/airflow-tmp'
today = datetime.datetime.today().strftime('%Y-%m-%d')
#today='2020-10-26'
year, month, day = today.split('-')

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def get_redshift_conn():
    hook = PostgresHook(postgres_conn_id='redshift_dw')
    conn = hook.get_sqlalchemy_engine()

    return conn

def fetch_conversions(run_date):
    api_url = 'https://api.eflow.team/v1/networks/reporting/conversions?page_size=2000'
    newHeaders = {'Content-type': 'application/json', 'x-eflow-api-key': os.getenv('EVERFLOW_API_KEY', None)}
    payload='{"show_conversions": true,"show_events": true,"from": "'+run_date+' 00:00:00","to": "'+run_date+' 23:59:59","timezone_id": 63}'
    response = requests.post(api_url, data=payload,headers=newHeaders)

    logging.info("Status code: {}".format(response.status_code))
    response_Json = response.json()
    if response.status_code == 200:
        logging.info('lenght of data: {}'.format(len(response_Json['conversions'])))
        data_conversions=[]
        for cl in response_Json['conversions']:
            d2=flatten_json(cl)
            normal = json_normalize(d2)
            data_conversions.append((normal)) 

        conversions_results = pd.concat(data_conversions)
        conversions_results.to_parquet('{}/{}-conversions.parquet'.format(temp_dir, run_date),compression='gzip')
        for filepath in glob.glob('{}/*.parquet'.format(temp_dir)):
            print(filepath)
        logging.info('for {} we have {} distinct adv1 values & {} rows of conversions'.format(run_date, len(conversions_results.adv1.unique()), len(conversions_results)))
    else:
        raise AirflowException(response_Json['Error'])

def update_everflow_redshift(run_date):
    copy_cmd = """
        delete from everflow.conversions where everflow_date='{run_date}';

        insert into everflow.conversions (SELECT date(year||'-'||month||'-'||day) as everflow_date, * from athenaeverflow.conversions where date(year||'-'||month||'-'||day)='{run_date}');
    """.format(run_date=run_date)
    engine_redshift = get_redshift_conn()
    with engine_redshift.connect() as con:
        con.execute(copy_cmd)
        con.close()

fetch_conversions = PythonOperator(
    task_id='fetch_conversions',
    python_callable=fetch_conversions,
    op_kwargs={'run_date': today},
    #op_kwargs={'run_date': '2020-10-31'},
    dag=dag,
)

sync_to_datalake = BashOperator(
    task_id='sync_to_datalake',
    bash_command='mkdir conversions && cp {}/{}-conversions.parquet conversions && aws s3 sync --delete conversions/ s3://linkmedia-datalake/everflow/conversions/year={}/month={}/day={}'.format(temp_dir, today, year, month, day),
    dag=dag,
)

fetch_conversions >> sync_to_datalake
    
update_ef_redshift = PythonOperator(
    task_id='update_ef_redshift',
    python_callable=update_everflow_redshift,
    op_kwargs={'run_date': today},
    #op_kwargs={'run_date': '2020-10-31'},
    dag=dag,
)
# fetch_conversions = PythonVirtualenvOperator(
#     task_id="fetch_conversions",
#     python_callable=fetch_conversions,
#     requirements=[],
#     system_site_packages=True,
#     dag=(dag),
# )