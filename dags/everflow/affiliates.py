
import os
import glob
import requests
import logging
import datetime
import pandas as pd
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='EVERFLOW_AFFILIATES',
    default_args=default_args,
    catchup=False,
    schedule_interval='0 3,6,9 * * *',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['EVERFLOW'])

temp_dir = '/tmp'
today = datetime.datetime.today().strftime('%Y-%m-%d')
year, month, day = today.split('-')

# if not os.path.exists(temp_dir):
#     os.makedirs(temp_dir)
#     #os.chdir(temp_dir)
# else:
#     files = glob.glob(temp_dir+'/*')
#     for f in files:
#         os.remove(f)

def flatten_json(y):
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)
    return out

def fetch_affiliates():
    api_url = 'https://api.eflow.team/v1/networks/affiliates?page_size=1000'
    newHeaders = {'Content-type': 'application/json', 'x-eflow-api-key': os.getenv('EVERFLOW_API_KEY', None)}
    response = requests.get(api_url,headers=newHeaders)

    logging.info("Status code: {}".format(response.status_code))
    response_Json = response.json()
    if response.status_code == 200:
        logging.info('lenght of data: {}'.format(len(response_Json['affiliates'])))
        data_affiliates=[]
        for cl in response_Json['affiliates']:
            d2=flatten_json(cl)
            normal = pd.json_normalize(d2)
            data_affiliates.append((normal)) 

        affiliates_results = pd.concat(data_affiliates)
        affiliates_results.to_parquet('{}/{}-affiliates.parquet'.format(temp_dir, today),compression='gzip')
    else:
        raise AirflowException(response_Json['Error'])

fetch_affiliates = PythonOperator(
    task_id='fetch_affiliates',
    python_callable=fetch_affiliates,
    #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
    dag=dag,
)

# #s3_cmd = 'aws s3 sync --delete {} s3://linkmedia-datalake/test/everflow/affiliates/'.format(temp_dir)
# #s3_cmd = 'ls -la {}'.format(temp_dir)
# s3_cmd = ''

# create_somefile = BashOperator(
#     task_id='create_somefile',
#     bash_command='mkdir affiliates && cp {}/{}-affiliates.parquet affiliates && aws s3 sync --delete affiliates/ s3://linkmedia-datalake/test/everflow/affiliates/'.format(temp_dir, today),
#     dag=dag,
# )

sync_to_datalake = BashOperator(
    task_id='sync_to_datalake',
    bash_command='mkdir affiliates && cp {}/{}-affiliates.parquet affiliates && aws s3 sync --delete affiliates/ s3://linkmedia-datalake/test/everflow/affiliates/'.format(temp_dir, today),
    dag=dag,
)

fetch_affiliates >> sync_to_datalake

# fetch_conversions = PythonVirtualenvOperator(
#     task_id="fetch_conversions",
#     python_callable=fetch_conversions,
#     requirements=[],
#     system_site_packages=True,
#     dag=(dag),
# )