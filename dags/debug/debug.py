
import os
import glob
import requests
import logging
import datetime
import pandas as pd
from s3fs import S3FileSystem
import pyarrow as pa
import pyarrow.parquet as pq
from pandas.io.json import json_normalize
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, PythonVirtualenvOperator
from airflow.utils.dates import days_ago
from airflow import AirflowException

default_args = {
    'owner': 'Omar',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['oa@linkmedia.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': datetime.timedelta(minutes=5),
}

dag = DAG(
    dag_id='DEBUG',
    default_args=default_args,
    catchup=False,
    schedule_interval='@once',
    dagrun_timeout=datetime.timedelta(minutes=60),
    tags=['TEST'])


def print_pd_version():
    print(pd.__version__)

def test_pyarrow_pq():
    print('loading json file ....')
    df = pd.read_json('s3://datalake-imports/2021/2021-01-14/rates/eur.json')
    print('convert the df into pyarrow table ....')
    table = pa.Table.from_pandas(df)
    output_file = 's3://datalake-imports/test/debug/pyarrow_pq.parquet'
    s3 = S3FileSystem()
    print('dump parquet file in destination ....')
    pq.write_to_dataset(table=table, 
                        root_path=output_file,
                        filesystem=s3) 

# def test_fast_parquet():
#     print('loading json file ....')
#     df = pd.read_json('s3://datalake-imports/2021/2021-01-14/rates/eur.json')
#     print('dumping parquet file ....')
#     df.to_parquet('s3://datalake-imports/test/debug/fastparquet.parquet', engine='fastparquet')
#     print('done dumping')

def test_save_parquet():
    print('loading json file ....')
    df = pd.read_json('s3://datalake-imports/2021/2021-01-14/rates/eur.json')
    print('dumping parquet file ....')
    df.to_parquet('s3://datalake-imports/test/debug/normaldump.parquet', engine='pyarrow')
    print('done dumping')

print_pd_version = PythonOperator(
    task_id='print_pd_version',
    python_callable=print_pd_version,
    #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
    dag=dag,
)

pyarrow_parquet = PythonOperator(
    task_id='test_pyarrow_pq',
    python_callable=test_pyarrow_pq,
    #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
    dag=dag,
)

# fast_parquet = PythonOperator(
#     task_id='test_fast_parquet',
#     python_callable=test_fast_parquet,
#     #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
#     dag=dag,
# )

pandas_parquet = PythonOperator(
    task_id='test_save_parquet',
    python_callable=test_save_parquet,
    #op_kwargs={'connection_name': 'redshift', 'schema':'aoi', 'table_name':'order_details', 'column':'order_date'},
    dag=dag,
)

get_pandas_version_bash = BashOperator(
    task_id='get_pandas_version_bash',
    bash_command='pip3 freeze | grep sf3s',
    dag=dag,
)

print_pd_version >> [pyarrow_parquet, pandas_parquet] >> get_pandas_version_bash