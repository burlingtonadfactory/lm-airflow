import logging
import pandas as pd
# required to normalize json files
import json
from pandas.io.json import json_normalize
# required to work with parquet files
import pyarrow as pa
import pyarrow.parquet as pq
# s3fs is required to read from s3 buckets
import s3fs
# boto3 is amazon's library to read data from s3
import boto3
# smart_open is a small library which handles s3 objects easily and efficicently
from smart_open.s3 import iter_bucket as s3_iter_bucket
# this import replaces normal open function to help read data thorugh s3 buckets
from smart_open import open
# required to handle io operations when we get file contents as bytes object
import io
from pyathena import connect
from pyathena.pandas.cursor import PandasCursor
import csv

def save_to_parquet(pandas_data_frame, bucket_path, file_name):
    """Saves file_name in parquet format in given bucket path"""
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    fs = s3fs.S3FileSystem()
    # # save to parquet file, flavor = spark is important to make sure Athena reads parquets properly
    pq.write_table(pa.Table.from_pandas(pandas_data_frame),
                   bucket_path+file_name+'.parquet', filesystem=fs, flavor='spark')
    # pandas_data_frame.to_parquet(bucket_path+file_name+'.parquet', engine='pyarrow')

def get_athena_data(table_name, s3_staging_dir, region_name):
    # select data from Athena from provided table name and return a pandas dataframe
    conn = connect(s3_staging_dir, region_name)
    return pd.read_sql("SELECT * FROM "+table_name, conn)

def save_to_tsv(pandas_data_frame, bucket_path, file_name):
    """ write dataframe to tsv file and gzip it """
    # Setting up file system as S3 
    fs = s3fs.S3FileSystem()
    df = pandas_data_frame
    file_path = bucket_path + file_name + '.tsv.gz' 
    # write the pandas dataframe to gzipped tsv file
    df.to_csv(open(file_path,'w'),index = False,sep = "\t")

def read_all_json_from_S3_to_pd(data_source, bucket_to_read, file_prefix, convert_data_schema, convert_date_cols):
    """
    Reads all jsons in a bucket. 
    
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.

    Lime and Cake data is handled differently because Lime data is on the first level, 
    however, cake data is at first child level, due to this this, conditional statements are added.
    """
    if data_source == 'LIME':
        print (' reading lime')
        data_from_json_to_pd = pd.concat([pd.read_json(content, orient='index', dtype=convert_data_schema, convert_dates=convert_date_cols, date_unit='ms')
                                          for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.json'), workers=1)], sort=False, ignore_index=True)
    elif data_source == 'LIME_GATEWAYS':
        print (' reading lime gateways')
        data_from_json_to_pd = pd.concat([pd.read_json(content, orient='column', dtype=convert_data_schema, convert_dates=convert_date_cols, date_unit='ms').transpose()
                                          for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.json'), workers=1)], sort=False, ignore_index=False)
    elif data_source == 'CAKE':
        print (' reading cake')
        data_from_json_to_pd = pd.concat([json_normalize(pd.read_json(content)['d']['event_conversions']) for key, content in s3_iter_bucket(
            bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.json'), workers=1)], sort=False, ignore_index=True)
        #data_from_json_to_pd = data_from_json_to_pd.astype(convert_data_schema)
    return data_from_json_to_pd


def read_all_csv_from_S3_to_pd(bucket_to_read, file_prefix, convert_data_schema, datacols, datasep=','):
    """
    Reads all jsons in a bucket. 
    
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.

    most csv files are utf-8 encoded but there are some files which are saved from windows machines or have windows encoding. 
    To handle these 2 different encodings, two different statements are added to read csv files.
    Also, some files have .Csv which is not captured with .csv, thus that condiiton is also added in lambda function.
    """
    try:
        # try if file can be read with utf-8 unicode encoding
        data_from_csv_to_pd = pd.concat( \
                        [pd.read_csv(io.BytesIO(content), dtype = convert_data_schema, index_col=False, infer_datetime_format=True, sep=datasep) \
                        for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.csv')|key.endswith('.Csv'), workers=1) \
                        ] \
                        , sort=False, ignore_index=True)[datacols]
    except:
        # if not unicode then try to read through windows - western europe encoding
        data_from_csv_to_pd = pd.concat( \
                        [pd.read_csv(io.BytesIO(content), dtype = convert_data_schema, encoding='windows-1252', index_col=False, infer_datetime_format=True, sep=datasep) \
                        for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.csv')|key.endswith('.Csv'), workers=1) \
                        ] \
                        , sort=False, ignore_index=True)[datacols]
    return data_from_csv_to_pd


def read_concardis_nets_cbs_csv_from_S3_to_pd(bucket_to_read, file_prefix, convert_data_schema, datacols, datasep=',',data_quoting='None'):
    """
    Reads all jsons in a bucket.

    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.

    most csv files are utf-8 encoded but there are some files which are saved from windows machines or have windows encoding.
    To handle these 2 different encodings, two different statements are added to read csv files.
    Also, some files have .Csv which is not captured with .csv, thus that condiiton is also added in lambda function.
    """
    try:
        # try if file can be read with utf-8 unicode encoding
        if(data_quoting == 'MINIMAL'):
            data_from_csv_to_pd = pd.concat(
                [pd.read_csv(io.BytesIO(content), dtype=convert_data_schema, index_col=False, infer_datetime_format=True,
                            sep=datasep, quoting=csv.QUOTE_MINIMAL) \
                for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix,
                                                    accept_key=lambda key: key.endswith('.csv') | key.endswith('.Csv'),
                                                    workers=1) \
                ] , sort=False, ignore_index=True)[datacols]
        else:
            data_from_csv_to_pd = pd.concat(
                [pd.read_csv(io.BytesIO(content), dtype=convert_data_schema, index_col=False, infer_datetime_format=True,
                            sep=datasep, quoting=csv.QUOTE_NONE) \
                for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix,
                                                    accept_key=lambda key: key.endswith('.csv') | key.endswith('.Csv'),
                                                    workers=1) \
                ] , sort=False, ignore_index=True)[datacols]


    except:
        # if not unicode then try to read through windows - western europe encoding
        if(data_quoting == 'MINIMAL'):
            data_from_csv_to_pd = pd.concat(
                [pd.read_csv(io.BytesIO(content), dtype=convert_data_schema, encoding='windows-1252', index_col=False,
                            infer_datetime_format=True, sep=datasep, quoting=csv.QUOTE_MINIMAL) \
                for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix,
                                                    accept_key=lambda key: key.endswith('.csv') | key.endswith('.Csv'),
                                                    workers=1) \
                ] , sort=False, ignore_index=True)[datacols]
        else:
            data_from_csv_to_pd = pd.concat(
                [pd.read_csv(io.BytesIO(content), dtype=convert_data_schema, encoding='windows-1252', index_col=False,
                            infer_datetime_format=True, sep=datasep, quoting=csv.QUOTE_NONE) \
                for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix,
                                                    accept_key=lambda key: key.endswith('.csv') | key.endswith('.Csv'),
                                                    workers=1) \
                ] , sort=False, ignore_index=True)[datacols]
    return data_from_csv_to_pd


def read_all_excel_from_S3_to_pd(bucket_to_read, file_prefix, convert_data_schema, datacols):
    """
    Reads all xls or xlsx in a bucket. 
    
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.
    """
    data_from_excel_to_pd = pd.concat( \
                       [pd.read_excel(io.BytesIO(content), dtype = convert_data_schema, engine='openpyxl') \
                       for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.xlsx')|key.endswith('.xls'), workers=1) \
                       ] \
                     , sort=False, ignore_index=True)[datacols]
    return data_from_excel_to_pd


# def read_all_parquet_from_S3_to_pd(bucket_to_read, file_prefix, datacols):
#     """
#     Reads all parquet in a bucket.
#     iterate over given bucket and key prefix and load all the files from that bucket
#     fields and fields which are to be passed as dates.
#     """
#     # Setting up file system as S3 (To be used with PyArrow for Parquet files)
#     fs = s3fs.S3FileSystem()
#
#     data_from_pq_to_pd = pd.concat( \
#                     [pq.read_table(io.BytesIO(content), filesystem=fs, columns=datacols).to_pandas() \
#                     for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: key.endswith('.parquet')) \
#                     ] \
#                     , sort=False, ignore_index=True)
#     return data_from_pq_to_pd


def read_all_parquet_from_S3_to_pd(bucket_to_read, file_prefix, datacols):
    """
    Reads all parquet in a bucket.
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.
    """
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_to_read)
    prefix_objs = bucket.objects.filter(Prefix=file_prefix)
    fs = s3fs.S3FileSystem()

    dfs = []
    for obj in prefix_objs:
        if obj.key.endswith('.parquet'):
            print('Processing parquet file {}'.format(obj.key))
            df = pq.read_table(io.BytesIO(obj.get()['Body'].read()), filesystem=fs, columns=datacols).to_pandas()
            dfs.append(df)

    print('Parquet files read complete')
    data_from_pq_to_pd = pd.concat(dfs, sort=False, ignore_index=True)
    return data_from_pq_to_pd

def read_all_from_S3_to_pd(bucket_to_read, file_prefix, datacols, source_file_extention):
    """
    Reads all parquet in a bucket. 
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.
    """
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_to_read)
    prefix_objs = bucket.objects.filter(Prefix=file_prefix)
    fs = s3fs.S3FileSystem()

    if source_file_extention == 'PARQUET':
        data_from_pq_to_pd = pd.concat( \
                        [pq.read_table(io.BytesIO(obj.get()['Body'].read()), filesystem=fs, columns=datacols).to_pandas() \
                        for obj in prefix_objs if obj.key.endswith('.parquet')], sort=False, ignore_index=True)
    else:
        pass
    
    return data_from_pq_to_pd


def read_sticky_cbalerts_csv_from_S3_to_pd(bucket_to_read, file_prefix, convert_data_schema, datacols, running_date, datasep=','):
    """
    Reads all stickyalerts csv in a bucket.

    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.

    most csv files are utf-8 encoded but there are some files which are saved from windows machines or have windows encoding.
    To handle these 2 different encodings, two different statements are added to read csv files.
    Also, some files have .Csv which is not captured with .csv, thus that condiiton is also added in lambda function.
    """
    try:
        curr_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(0)).strftime('%Y-%m-%d')
        prev_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime('%Y-%m-%d')
        next_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+1)).strftime('%Y-%m-%d')
        next_date_2 = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+2)).strftime('%Y-%m-%d')
        next_date_3 = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+3)).strftime('%Y-%m-%d')
        # try if file can be read with utf-8 unicode encoding
        data_from_csv_to_pd = pd.concat( \
                        [pd.read_csv(io.BytesIO(content), dtype = convert_data_schema, index_col=False, infer_datetime_format=True, sep=datasep) \
                        for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: (key.endswith('.csv')|key.endswith('.Csv'))&((curr_date in key)|(prev_date in key)|(next_date in key)|(next_date_2 in key)|(next_date_3 in key)), workers=1) \
                        ] \
                        , sort=False, ignore_index=True)[datacols]
    except:
        curr_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(0)).strftime('%Y-%m-%d')
        prev_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime('%Y-%m-%d')
        next_date = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+1)).strftime('%Y-%m-%d')
        next_date_2 = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+2)).strftime('%Y-%m-%d')
        next_date_3 = (pd.to_datetime(running_date, format='%Y-%m-%d') + pd.DateOffset(+3)).strftime('%Y-%m-%d')
        # if not unicode then try to read through windows - western europe encoding
        data_from_csv_to_pd = pd.concat( \
                        [pd.read_csv(io.BytesIO(content), dtype = convert_data_schema, encoding='windows-1252', index_col=False, infer_datetime_format=True, sep=datasep) \
                        for key, content in s3_iter_bucket(bucket_to_read, prefix=file_prefix, accept_key=lambda key: (key.endswith('.csv')|key.endswith('.Csv'))&((curr_date in key)|(prev_date in key)|(next_date in key)|(next_date_2 in key)|(next_date_3 in key)), workers=1) \
                        ] \
                        , sort=False, ignore_index=True)[datacols]
    return data_from_csv_to_pd
