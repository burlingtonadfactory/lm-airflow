
from linkmedia.transform_data.subscription_id_transform import subscription_id_controller as sic
from linkmedia.transform_data.subscription_id_transform import subscription_id_schema as sis
import pandas as pd
import dask.dataframe as dd
from linkmedia.transform_data.order_history_transform import order_history_schema as ohis
from linkmedia.order_header import order_header_schema as ohs

def get_data(run_date, number_of_days_ahead):
    # start date and number of days to go back
    start_date = run_date

    subscription_id= sic.subscription_id()
    
    bucket = 'linkmedia-datalake'
    date_path = run_date.replace('-', '/')
    bucket_path = 's3://'+bucket+'/'
    order_header_history_fields = sis.order_header_fields
    # order_header_history_fields = ohis.order_header_fields
    # end_date = '2016-01-01'
    # months_in_history = (int(run_date[:4]) - int(end_date[:4])-1)*12 + int(run_date[5:7]) + (12 - int(end_date[5:7]))

    order_header_history_file = bucket_path+'order_header_history_test/*'
    order_header_history = dd.read_parquet(order_header_history_file, columns=order_header_history_fields,  engine = 'pyarrow' ).compute()
    
    # loop for required days and generate order_header
    for i in list(range(0, number_of_days_ahead)):
        run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                    pd.DateOffset(i)).strftime('%Y-%m-%d')
        # run subscription_billing_cycle controller
        subscription_id.get_subscription_id(run_date, order_header_history)
    order_header_history = pd.DataFrame()
