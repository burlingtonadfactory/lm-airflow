order_header_fields = {
    'order_id', 
    'ancestor_id',
    'email_address', 
    'time_stamp', 
    'main_product_category', 
    'campaign_id', 
    'order_status', 
    'brand_id', 
    'billing_cycle', 
    'cust_subscription_key', 
    'is_create_order',
    'order_total', # uncommented the following columns
    'parent_id',
    'migration_parent_order_id',
    'cascade_first_parent_order_id'
}