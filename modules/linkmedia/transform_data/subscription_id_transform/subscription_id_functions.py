from linkmedia.lib import read_store as rs
from linkmedia.transform_data.order_history_transform import order_history_schema as ohis
from linkmedia.order_header import order_header_schema as ohs

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
import dask.dataframe as dd
fs = s3fs.S3FileSystem()


def get_parquet(read_file_path, read_schema, read_fields):
    """ Read parquet file/(partitions) and return dataframe with required fields. """	
    # get the fields from schema passed
    # read the parquet and return as a pandas dataframe
    read_schema = {key: value for key, value in read_schema.items() if key in read_fields}
    return pd.read_parquet(read_file_path, columns=read_fields).astype(dtype=read_schema)


def get_subscription(order_header_cmb):
    """ takes input order header and return subscription dataframe"""

    order_header_cmb['cust_lead_u_key'] = order_header_cmb['campaign_id'].astype(str) + '/' + order_header_cmb['email_address']

    # get all the 0 billing cycle orders (non migrations)
    order_header_leads = order_header_cmb[(order_header_cmb.billing_cycle == 0) & (order_header_cmb.campaign_id != 181)].copy()

    order_header_leads['subscription_id'] = order_header_leads['ancestor_id']
    order_header_leads = order_header_leads.sort_values(by=['cust_lead_u_key', 'order_id'], ascending=False)

    for i in range(0, len(order_header_leads.subscription_id)):
        if (order_header_leads.order_status.values[i] == 7):
            if (order_header_leads.cust_lead_u_key.values[i] == order_header_leads.cust_lead_u_key.values[i - 1]) & ((order_header_leads.time_stamp.values[i] - order_header_leads.time_stamp.values[i - 1]) < np.timedelta64(30, 's')):
                order_header_leads['subscription_id'].values[i] = order_header_leads['subscription_id'].values[i - 1]

    order_header_rebills = order_header_cmb[(((order_header_cmb.campaign_id==181) & (order_header_cmb.billing_cycle==0)) | (order_header_cmb.billing_cycle!=0) )].copy()
    order_header_rebills = order_header_rebills.sort_values(by=['cust_subscription_key', 'order_id'])
    order_header_rebills = order_header_rebills.assign(subscription_id=0)

    for i in range(0, len(order_header_rebills.subscription_id)):
        if (order_header_rebills.campaign_id.values[i] == 181) & (order_header_rebills.cust_subscription_key.values[i] == order_header_rebills.cust_subscription_key.values[i - 1]):
            order_header_rebills['subscription_id'].values[i] = order_header_rebills['subscription_id'].values[i - 1]
        else:
            order_header_rebills['subscription_id'].values[i] = order_header_rebills['ancestor_id'].values[i]

    order_header_cmb = pd.concat([order_header_leads, order_header_rebills], sort=False, ignore_index=True)
    return order_header_cmb

# def get_subscription(order_header_cmb):
#     """ takes input order header and return subscription dataframe"""
#
#     # get orders which might have triggered the migration (last order before possible migration)
#     pre_migration_ancestors = get_pre_migration_ancestors(order_header_cmb)
#
#     # get new migrations
#     new_migrations = get_new_migrations(order_header_cmb, pre_migration_ancestors)
#
#     # get rebill migrations
#     rebill_migrations = get_rebill_migrations(order_header_cmb, new_migrations)
#
#     # get final ancestor
#     migration_ancestors = get_final_ancestor(rebill_migrations, new_migrations)
#
#     # join ancestor id for migration orders to order header
#     order_header_cmb_join_fields = ['order_id', 'ancestor_id_pre_migration']
#     order_header_cmb = order_header_cmb.merge(migration_ancestors[order_header_cmb_join_fields], how='left', on='order_id')
#
#     # generate subscription id for all the orders
#     order_header_cmb['subscription_id'] = np.where((pd.notna(order_header_cmb.ancestor_id_pre_migration) &
#                                                     order_header_cmb.ancestor_id_pre_migration != 0), order_header_cmb.ancestor_id_pre_migration, order_header_cmb.ancestor_id)
#
#     # drop not needed fields
#     order_header_cmb = order_header_cmb.drop(columns=['ancestor_id_pre_migration'])
#
#     # convert subscription_id to int datatype
#     order_header_cmb = order_header_cmb.astype(dtype={'subscription_id':int})
#
#     return order_header_cmb


# def get_final_ancestor(rebill_migrations, new_migrations):
#
#     # generate final ancestor id for migrations
#     rebill_migration_ancestor_id_condition_1 = (pd.notna(rebill_migrations.ancestor_id_pre_migration)) & (rebill_migrations.ancestor_id_pre_migration!=0)
#     rebill_migration_ancestor_id_condition_2 = (pd.notna(rebill_migrations.ancestor_id_pre_migration_on_email)) & (rebill_migrations.ancestor_id_pre_migration_on_email!=0)
#
#     rebill_migrations['ancestor_id_pre_migration'] = np.where(rebill_migration_ancestor_id_condition_1, rebill_migrations.ancestor_id_pre_migration, np.where(
#         rebill_migration_ancestor_id_condition_2, rebill_migrations.ancestor_id_pre_migration_on_email, rebill_migrations.ancestor_id))
#
#     new_migration_ancestor_id_condition_1 = (pd.notna(new_migrations.ancestor_id_pre_migration)) & (new_migrations.ancestor_id_pre_migration!=0)
#     new_migration_ancestor_id_condition_2 = (pd.notna(new_migrations.ancestor_id_pre_migration_on_email)) & (new_migrations.ancestor_id_pre_migration_on_email!=0)
#
#     new_migrations['ancestor_id_pre_migration'] = np.where(new_migration_ancestor_id_condition_1, new_migrations.ancestor_id_pre_migration, np.where(
#         new_migration_ancestor_id_condition_2, new_migrations.ancestor_id_pre_migration_on_email, new_migrations.ancestor_id))
#
#     return pd.concat([new_migrations, rebill_migrations], sort=False, ignore_index=True)


# def get_new_migrations(order_header_cmb, pre_migration_ancestors):
#     # new migrations will be assigned ancestor id based on previous order's ancestor id
#     # this is for migrations where gateway brand is same as last non-migration orders
#     new_migration_condition = (order_header_cmb.campaign_id == 181) & (order_header_cmb.billing_cycle == 0)
#     new_migrations = order_header_cmb[new_migration_condition][['email_address', 'brand_id', 'ancestor_id','order_id']]
#     new_migrations = new_migrations.merge(pre_migration_ancestors, how='left', left_on=['email_address', 'brand_id'], right_on=['email_address', 'brand_id'])
#
#
#     # new migrations will be assigned ancestor id based on previous order's ancestor id
#     # this is for migrations where we could not find an ancestor based on brand and email as brand changed on 1st migration attempt
#     # we will assign ancestor of last (non migration) failed order to first migration order
#     # this applies to around 2% of cases as on 27 Aug
#     pre_migration_ancestor_condition = (order_header_cmb.campaign_id != 181) & (order_header_cmb.order_status == 7) & (order_header_cmb.billing_cycle != 0)
#
#     pre_migration_ancestors_on_email = order_header_cmb[pre_migration_ancestor_condition][['email_address', 'order_id']].groupby(['email_address']).max().reset_index()
#
#     pre_migration_ancestors_on_email = pre_migration_ancestors_on_email.rename(columns={'order_id':'last_order_id_pre_migration'})
#
#     order_header_cmb_order_ancestor = order_header_cmb[['order_id', 'ancestor_id']].rename(columns={'order_id':'last_order_id_pre_migration', 'ancestor_id':'ancestor_id_pre_migration_on_email'})
#
#     pre_migration_ancestors_on_email = pre_migration_ancestors_on_email.merge(order_header_cmb_order_ancestor, how='left', left_on=['last_order_id_pre_migration'], right_on=['last_order_id_pre_migration'])
#
#     # assign new migrations which do not have an ancestor id, ancestor id based on last order for that email
#     new_migrations_join_fields = ['email_address', 'ancestor_id_pre_migration_on_email']
#
#     return new_migrations.merge(pre_migration_ancestors_on_email[new_migrations_join_fields], how='left', on=['email_address'])


# def get_rebill_migrations(order_header_cmb, new_migrations):
#
#     # assign all rebill migrations correct ancestor id
#     rebill_migration_condition = (order_header_cmb.campaign_id == 181) & (order_header_cmb.billing_cycle != 0)
#     rebill_migrations = order_header_cmb[rebill_migration_condition][['email_address', 'brand_id', 'ancestor_id','order_id']]
#     rebill_migrations_join_fields = ['ancestor_id','ancestor_id_pre_migration','ancestor_id_pre_migration_on_email']
#
#     return rebill_migrations.merge(new_migrations[rebill_migrations_join_fields], how='left', on=['ancestor_id'])


# def get_pre_migration_ancestors(order_header_cmb):
#
#     pre_migration_ancestor_condition = (order_header_cmb.campaign_id != 181) & (order_header_cmb.order_status == 7) & (order_header_cmb.billing_cycle != 0)
#     pre_migration_ancestors = order_header_cmb[pre_migration_ancestor_condition][['email_address', 'brand_id', 'ancestor_id']].groupby(['email_address', 'brand_id']).max().reset_index()
#
#     return pre_migration_ancestors.rename(columns={'ancestor_id':'ancestor_id_pre_migration'})


def get_subscription_related_data(order_header_cmb):

    # get first rebill price
    order_header_cmb = get_first_rebill_price(order_header_cmb)

    # get subcription billing cycle
    order_header_cmb = get_subscriptipon_billing_cycle(order_header_cmb)

    # get stepdown 
    order_header_cmb = get_stepdown(order_header_cmb)

    return order_header_cmb


def get_first_rebill_price(order_header_cmb):
    # get first rebill price for a subscription
    order_first_rebills = order_header_cmb[(order_header_cmb.billing_cycle.isin([1,2]) & order_header_cmb.campaign_id != 181)][['subscription_id', 'order_total']].groupby('subscription_id').max()
    order_first_rebills = order_first_rebills.rename(columns={'order_total':'order_total_first_rebill'})

    # join first rebill price to order header
    return order_header_cmb.merge(order_first_rebills, how='left', on='subscription_id')


def get_subscriptipon_billing_cycle(order_header_cmb):

    # sort based on order for each subscription
    order_header_cmb = order_header_cmb.sort_values(by=['subscription_id', 'order_id'])

    # create a new series, assign an initial value 0
    subs_bill_cycle = [order_header_cmb.billing_cycle.values[0]]
    
    # run a for loop and for each record calculate correct subscription billing cycle
    # value differ from billing cycle for only migration orders
    for i in range(1, len(order_header_cmb.index)):
        if (order_header_cmb.campaign_id.values[i] == 181) & (order_header_cmb.subscription_id.values[i] == order_header_cmb.subscription_id.values[i-1]):
            if np.isin(order_header_cmb.order_status.values[i-1], [2,6,8]):
                subs_bill_cycle.append(subs_bill_cycle[i-1]+1)
            else:
                subs_bill_cycle.append(subs_bill_cycle[i-1])
        else:
            subs_bill_cycle.append(order_header_cmb.billing_cycle.values[i])

    order_header_cmb['subscription_billing_cycle'] = subs_bill_cycle

    return order_header_cmb


def get_stepdown(input_dataframe):
        
        input_dataframe = input_dataframe.assign(is_stepdown=0)
        input_dataframe['date_stamp'] = input_dataframe['time_stamp'].dt.date
        
        input_dataframe_munducheck = input_dataframe[(input_dataframe.order_status == 7)]

        input_dataframe = input_dataframe.merge(input_dataframe_munducheck, how ='left', on = ['cust_subscription_key','email_address','subscription_id','date_stamp'], suffixes = (None,'_MUNDUCHECK') )

        category_eshopper             = [9, 17, 18, 19, 20, 21]
        category_bundle               = [2]
        mundu_stepdown_price_eshopper = [20.00,20.50,20.98,20.99,22.00,23.00,23.50,23.98,23.99]
        mundu_stepdown_price          = [20.00,21.00,21.50,21.98,21.99,22.00,24.00,24.50,24.98,24.99]
        munudu_stepdown_price_norway  = [299.00,399.00,499.00,599.00]
        munudu_stepdown_price_gb      = [19.99]

        stepdown_condition = (input_dataframe.time_stamp >= pd.to_datetime('20190313', format='%Y%m%d')) & (input_dataframe.order_total < input_dataframe.order_total_first_rebill) & (input_dataframe.billing_cycle!=0)
        
        mundu_stepdown_base_condition = ((pd.notnull(input_dataframe["order_id_MUNDUCHECK"])) & (input_dataframe.order_id != input_dataframe.order_id_MUNDUCHECK) & (input_dataframe.time_stamp_MUNDUCHECK < input_dataframe.time_stamp) & (input_dataframe.order_total_MUNDUCHECK > input_dataframe.order_total) & (input_dataframe.campaign_id != 181))

        mundu_stepdown_country_condition = ( input_dataframe.order_total.isin(munudu_stepdown_price_norway) | input_dataframe.order_total.isin(munudu_stepdown_price_gb) )
        
        mundu_stepdown_condition = mundu_stepdown_base_condition & (((input_dataframe.main_product_category.isin(category_eshopper+category_bundle)) & (input_dataframe.order_total.isin(mundu_stepdown_price_eshopper))) | (
            (~input_dataframe.main_product_category.isin(category_eshopper)) & (input_dataframe.order_total.isin(mundu_stepdown_price))) | (mundu_stepdown_country_condition))

        
        input_dataframe['is_stepdown'] = np.where(stepdown_condition, np.where(mundu_stepdown_condition,'M', 'L'), 0)
        input_dataframe['stepdown_disc_percent'] = np.where(stepdown_condition, 1 - (input_dataframe['order_total']/input_dataframe['order_total_first_rebill']), 0)

        input_dataframe = input_dataframe.drop(input_dataframe.columns[input_dataframe.columns.str.endswith('_MUNDUCHECK')],axis=1)
        input_dataframe = input_dataframe.drop(['date_stamp'],axis=1)
        input_dataframe = input_dataframe.sort_values('is_stepdown', ascending=False).groupby(['order_id'], sort=False).first().reset_index()
        input_dataframe = input_dataframe.sort_values(by=['subscription_id', 'order_id'])

        # set data types for new columns
        input_dataframe = input_dataframe.astype(dtype={'is_stepdown':'category', 'stepdown_disc_percent':float})

        return input_dataframe
        #save to parquet file
        #rs.save_to_parquet(input_dataframe[stepdown_keep_columns], bucket_path+'order_flags/stepdown_orders/'+date_path+'/', run_date+'_stepdown_orders')


def get_all_orders_for_email(bucket_path, run_date, order_header_schema, input_emails):

    order_header_history_fields = ohis.order_header_history_fields
    # end_date = '2016-01-01'
    # months_in_history = (int(run_date[:4]) - int(end_date[:4])-1)*12 + int(run_date[5:7]) + (12 - int(end_date[5:7]))
    
    order_header_history_file = bucket_path+'order_header_history_test/*'

    order_header_history = dd.read_parquet(order_header_history_file, columns=order_header_history_fields,  engine = 'pyarrow' ).compute()
    
        
    # for month in range(1, months_in_history):

    #     run_month = (pd.to_datetime(run_date[:7], format='%Y-%m') - pd.to_timedelta(30, unit='d')).strftime(format='%Y-%m')
    #     month_path = run_month.replace('-', '/')

    #     order_header_history_file = bucket_path+'order_header_history_test/' + month_path+'/' + run_month +'_order_header_history.parquet'

    #     # get order header history file
    #     try:
    #         order_header_history = get_parquet(order_header_history_file, order_header_schema, order_header_history_fields)
    #         order_header_history = order_header_history[order_header_history.is_create_order == 1]
    #     except OSError:
    #         order_header_history = pd.DataFrame()
    #         break
        
    order_header_history = order_header_history.merge(input_emails.email_address.drop_duplicates(), how='inner', on='email_address')
   
    return order_header_history


# # not working for a few orders but these orders are very old and have no link e.g. #1973753
# def fix_ancestor(data):
#     # if ancestor and order id are same but parent_id is not then that is a problem and order is triggered by a previous order
#     # in those cases ancestor will be replaced by ancestor of parent order
#     # load only data where ancestor and order id are same but not parent
#     data = data.astype({'order_id':str, 'parent_id':str, 'ancestor_id':str})
#     data_fix_ancestor = data[
#         (data['ancestor_id'] == data['order_id']) & (data['ancestor_id'] != data['parent_id']) & pd.isna(
#             data['migration_parent_order_id'])][['order_id', 'parent_id']]
#     # merge main data to this data based on parent id to get ancestor of parent order
#     data_fix_ancestor = pd.merge(data_fix_ancestor, data[['order_id', 'ancestor_id']], how='left', left_on='parent_id',
#                                  right_on='order_id')
#     data_fix_ancestor = data_fix_ancestor.rename(
#         columns={'order_id_x': 'order_id_step_0', 'ancestor_id': 'ancestor_id_step_0'})
#     data_fix_ancestor = data_fix_ancestor[['order_id_step_0', 'ancestor_id_step_0']]
#     # merge this data to main data to fix ancestor id based on order_id
#     data = pd.merge(data, data_fix_ancestor, how='left', left_on='order_id', right_on='order_id_step_0')
#     # udpate values of ancestor id where value in new generated field is not null
#     data['ancestor_id'] = np.where(pd.notna(data['ancestor_id_step_0']), data['ancestor_id_step_0'],
#                                    data['ancestor_id'])
#     # keep only required columns
#     return data[['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id', 'ancestor_id',
#                  'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]
#
#
# def fix_migration_ancestor(data):
#     # use migration parent order id to assign ancestors to migration orders
#     # migrations can be in multi levels. We will first get migration parent ids and then on those parent orders if they are migration as well assign their parent id
#     data_step1 = data[['order_id', 'migration_parent_order_id']].rename(
#         columns={'order_id': 'order_id_step1', 'migration_parent_order_id': 'migration_parent_order_id_step1'})
#     # join to main data based on migration parent to order id
#     data_step1 = pd.merge(data, data_step1, how='left', left_on='migration_parent_order_id', right_on='order_id_step1')
#     # where new migration parent is not null, assign new value else keep old value
#     data_step1['migration_parent_order_id_step2'] = np.where(pd.isna(data_step1['migration_parent_order_id_step1']),
#                                                              data_step1['migration_parent_order_id'],
#                                                              data_step1['migration_parent_order_id_step1'])
#
#     # now, we will compare this new value to ancestor id and based on this will access previous to migration orders as welll
#     data_set_2 = data[['order_id', 'ancestor_id']].rename(
#         columns={'order_id': 'order_id_step2', 'ancestor_id': 'ancestor_id_step2'})
#     data_step2 = pd.merge(data_step1, data_set_2, how='left', left_on='migration_parent_order_id_step2',
#                           right_on='order_id_step2')
#
#     # where new value is not null keep it else use old value
#     data_step2['ancestor_id_step2'] = np.where(pd.isna(data_step2['ancestor_id_step2']), data_step2['ancestor_id'],
#                                                data_step2['ancestor_id_step2'])
#     return data_step2[
#         ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id', 'ancestor_id',
#          'ancestor_id_step2', 'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]
#
#
# def fix_cancellation_salvages(data_step2):
#     # some cases in cancellation salvages where mp flag is not set, ancestor id will have value of last approved order rather than ancestor order
#     # e.g. order #22059600 doesn't go back to main order but has recorded parent as ancestor
#     # to fix these cases, we need to join once ancestors to their ancestors based on their order id data, in cases where order_id <> ancestor id for ancestors
#     data_ancestor = data_step2[['order_id', 'ancestor_id']].rename(
#         columns={'order_id': 'order_id_ancestor', 'ancestor_id': 'ancestor_id_ancestor'})
#     data_step3 = pd.merge(data_step2, data_ancestor, how='left', left_on='ancestor_id', right_on='order_id_ancestor')
#     data_step3['ancestor_id_step3'] = np.where(
#         (data_step3['ancestor_id'] == data_step3['ancestor_id_step2']) & pd.notna(data_step3['ancestor_id_ancestor'])
#         , data_step3['ancestor_id_ancestor'], data_step3['ancestor_id_step2'])
#     data_step3 = data_step3.drop(columns=['order_id_ancestor', 'ancestor_id_ancestor'])
#     return data_step3[
#         ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id', 'ancestor_id',
#          'ancestor_id_step2', 'ancestor_id_step3', 'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]
#
#
# def fix_cascade(data_step3):
#     # fix cascade cases
#     # get last order on a cascade flow (for a cascade first parent order)
#     data_last_cascade = data_step3[(pd.notna(data_step3['cascade_first_parent_order_id'])) & (data_step3['cascade_first_parent_order_id']!= '') ][
#         ['cascade_first_parent_order_id', 'order_id', 'email_address']].groupby(
#         ['cascade_first_parent_order_id', 'email_address'] ).agg('max').reset_index()
#     # last cascade order will be the order which will start subscription if that order gets approved, so we use that order as ancestor even on previous orders in cascade flow to be able
#     # to filter them as one subscription
#     data_last_cascade = data_last_cascade.rename(
#         columns={'order_id': 'ancestor_id_step4', 'cascade_first_parent_order_id': 'order_id_step4'})
#     # join this ancestor id to each cascade order
#     data_step4 = pd.merge(data_step3, data_last_cascade, how='left', left_on='cascade_first_parent_order_id',
#                           right_on='order_id_step4')
#     # now assign same ancestor id to the first cascade order, cascade starting order
#     data_last_cascade = data_last_cascade.rename(
#         columns={'order_id_step4': 'order_id_step5', 'ancestor_id_step4': 'ancestor_id_step5'})
#     data_step4 = pd.merge(data_step4, data_last_cascade, how='left', left_on='order_id', right_on='order_id_step5')
#
#     #
#     data_step4['ancestor_id_cascade'] = np.where((pd.isna(data_step4['ancestor_id_step5'])) | (data_step4['ancestor_id_step5']== ''),
#                                                  data_step4['ancestor_id_step4'], data_step4['ancestor_id_step5'])
#     data_step4['subscription_id_with_cascade'] = np.where( (pd.isna(data_step4['ancestor_id_cascade'])) | (data_step4['ancestor_id_cascade']== ''),
#                                                           data_step4['ancestor_id_step3'],
#                                                           data_step4['ancestor_id_cascade'])
#
#     data_step4['order_id'] = data_step4['order_id'].astype('int')
#     data_step4 = data_step4.rename(columns={'subscription_id_with_cascade': 'subscription_id'})
#     data_step4['subscription_id'] = data_step4['subscription_id'].astype('int')
#
#     return data_step4[
#         ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id', 'ancestor_id',
#          'ancestor_id_step2', 'ancestor_id_step3', 'ancestor_id_step4', 'ancestor_id_step5', 'ancestor_id_cascade',
#          'subscription_id', 'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]

def fix_cascade(data):
    data = data.astype({'order_id': str, 'parent_id': str, 'ancestor_id': str})
    # fix cascade cases
    # get last order on a cascade flow (for a cascade first parent order)
    data_last_cascade = \
    data[(pd.notna(data['cascade_first_parent_order_id'])) & (data['cascade_first_parent_order_id'] != '')][
        ['cascade_first_parent_order_id', 'order_id']].groupby(
        ['cascade_first_parent_order_id']).agg('max').reset_index()
    # last cascade order will be the order which will start subscription if that order gets approved, so we use that order as ancestor even on previous orders in cascade flow to be able
    # to filter them as one subscription
    data_last_cascade = data_last_cascade.rename(
        columns={'order_id': 'ancestor_id_cascade', 'cascade_first_parent_order_id': 'order_id_cascade'})
    # join this ancestor id to each cascade order
    data = pd.merge(data, data_last_cascade, how='left', left_on='cascade_first_parent_order_id',
                    right_on='order_id_cascade')
    # now assign same ancestor id to the first cascade order, cascade starting order
    data_last_cascade = data_last_cascade.rename(
        columns={'order_id_cascade': 'order_id_cascade_new', 'ancestor_id_cascade': 'ancestor_id_cascade_new'})
    data = pd.merge(data, data_last_cascade, how='left', left_on='order_id', right_on='order_id_cascade_new')

    #
    data['ancestor_id_cascade'] = np.where(
        (pd.isna(data['ancestor_id_cascade_new'])) | (data['ancestor_id_cascade_new'] == ''),
        data['ancestor_id'], data['ancestor_id_cascade_new'])

    return data[
        ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id',
         'ancestor_id', 'ancestor_id_cascade',
         'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]


# not working for a few orders but these orders are very old and have no link e.g. #1973753
def fix_ancestor(data):
    # if ancestor and order id are same but parent_id is not then that is a problem and order is triggered by a previous order
    # in those cases ancestor will be replaced by ancestor of parent order
    # load only data where ancestor and order id are same but not parent
    data_fix_ancestor = data[
        (data['ancestor_id_cascade'] == data['order_id']) & (
                    data['ancestor_id_cascade'] != data['parent_id']) & pd.isna(
            data['migration_parent_order_id'])][['order_id', 'parent_id']]
    # merge main data to this data based on parent id to get ancestor of parent order
    data_fix_ancestor = pd.merge(data_fix_ancestor, data[['order_id', 'ancestor_id_cascade']], how='left',
                                 left_on='parent_id',
                                 right_on='order_id')
    data_fix_ancestor = data_fix_ancestor.rename(
        columns={'order_id_x': 'order_id_step_0', 'ancestor_id_cascade': 'ancestor_id_cascade_step_0'})
    data_fix_ancestor = data_fix_ancestor[['order_id_step_0', 'ancestor_id_cascade_step_0']]
    # merge this data to main data to fix ancestor id based on order_id
    data = pd.merge(data, data_fix_ancestor, how='left', left_on='order_id', right_on='order_id_step_0')
    # udpate values of ancestor id where value in new generated field is not null
    data['ancestor_id_fix_ancestor'] = np.where(pd.notna(data['ancestor_id_cascade_step_0']),
                                                data['ancestor_id_cascade_step_0'],
                                                data['ancestor_id_cascade'])
    # keep only required columns
    return data[['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id',
                 'ancestor_id', 'ancestor_id_cascade', 'ancestor_id_fix_ancestor',
                 'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]


def fix_migration_ancestor(data):
    # use migration parent order id to assign ancestors to migration orders
    # migrations can be in multi levels. We will first get migration parent ids and then on those parent orders if they are migration as well assign their parent id
    data_step1 = data[['order_id', 'migration_parent_order_id']].rename(
        columns={'order_id': 'order_id_step1', 'migration_parent_order_id': 'migration_parent_order_id_step1'})
    # join to main data based on migration parent to order id
    data_step1 = pd.merge(data, data_step1, how='left', left_on='migration_parent_order_id', right_on='order_id_step1')
    # where new migration parent is not null, assign new value else keep old value
    data_step1['migration_parent_order_id_step2'] = np.where(pd.isna(data_step1['migration_parent_order_id_step1']),
                                                             data_step1['migration_parent_order_id'],
                                                             data_step1['migration_parent_order_id_step1'])

    # now, we will compare this new value to ancestor id and based on this will access previous to migration orders as welll
    data_set_2 = data[['order_id', 'ancestor_id_fix_ancestor']].rename(
        columns={'order_id': 'order_id_step2', 'ancestor_id_fix_ancestor': 'ancestor_id_fix_migration'})
    data_step2 = pd.merge(data_step1, data_set_2, how='left', left_on='migration_parent_order_id_step2',
                          right_on='order_id_step2')

    # where new value is not null keep it else use old value
    data_step2['ancestor_id_fix_migration'] = np.where(pd.isna(data_step2['ancestor_id_fix_migration']),
                                                       data_step2['ancestor_id_fix_ancestor'],
                                                       data_step2['ancestor_id_fix_migration'])
    return data_step2[
        ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id',
         'ancestor_id', 'ancestor_id_cascade', 'ancestor_id_fix_ancestor', 'ancestor_id_fix_migration',
         'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp', 'main_product_category']]


def fix_cancellation_salvages(data):
    # some cases in cancellation salvages where mp flag is not set, ancestor id will have value of last approved order rather than ancestor order
    # e.g. order #22059600 doesn't go back to main order but has recorded parent as ancestor
    # to fix these cases, we need to join once ancestors to their ancestors based on their order id data, in cases where order_id <> ancestor id for ancestors
    data_ancestor = data[['order_id', 'ancestor_id_fix_migration']].rename(
        columns={'order_id': 'order_id_ancestor', 'ancestor_id_fix_migration': 'ancestor_id_ancestor'})
    data_step3 = pd.merge(data, data_ancestor, how='left', left_on='ancestor_id_fix_migration',
                          right_on='order_id_ancestor')
    data_step3['ancestor_id_fix_cancellation'] = np.where(pd.notna(data_step3['ancestor_id_ancestor']),
                                                          data_step3['ancestor_id_ancestor'],
                                                          data_step3['ancestor_id_fix_migration'])
    data_step3 = data_step3.drop(columns=['order_id_ancestor', 'ancestor_id_ancestor'])

    data_step3['subscription_id'] = data_step3['ancestor_id_fix_cancellation']
    data_step3['order_id'] = data_step3['order_id'].astype('int')
    data_step3['subscription_id'] = data_step3['subscription_id'].astype('int')

    return data_step3[
        ['email_address', 'order_id', 'parent_id', 'migration_parent_order_id', 'cascade_first_parent_order_id',
         'ancestor_id', 'ancestor_id_cascade', 'ancestor_id_fix_ancestor', 'ancestor_id_fix_migration',
         'ancestor_id_fix_cancellation',
         'subscription_id', 'billing_cycle', 'campaign_id', 'order_total', 'order_status', 'time_stamp',
         'main_product_category']]
