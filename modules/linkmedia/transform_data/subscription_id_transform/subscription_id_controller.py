from linkmedia.lib import read_store as rs
from linkmedia.transform_data.subscription_id_transform import subscription_id_schema as sis
from linkmedia.order_header import order_header_schema as ohs
from linkmedia.transform_data.subscription_id_transform import subscription_id_functions as sif

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
import dask.dataframe as dd

class subscription_id:     
    def __init__(self):
        self

    def get_subscription_id(self, run_date, order_header_history): 
        
        # Path to save/load data
        bucket = 'linkmedia-datalake'
        import_bucket = 'datalake-imports'
        date_path = run_date.replace('-', '/')
        bucket_path = 's3://'+bucket+'/'

        subscription_master_file_dir = bucket_path+'subscription_master/'+ date_path+'/'

        subscription_master_file_dir_new = bucket_path + 'subscription_master_new/' + date_path + '/'

        order_header_file = bucket_path+'order_header/' + date_path+'/' + run_date +'_order_header.parquet'
        # order_header_file_dir = bucket_path+'order_header/'

        # order_header_history_file = bucket_path+'order_header_history/order_header_history.parquet'

        order_header_schema = ohs.order_header_schema
        order_header_fields = sis.order_header_fields

        # get order header for run_date
        order_header = sif.get_parquet(order_header_file, order_header_schema, order_header_fields)
        order_header = order_header[order_header.is_create_order == 1]

        # get full order header table (experimental (later) :: we will use chunking to keep filtering as loading, also, rather than loading full order_header which could become very heavy overtime, 
        # we can save a staging file which will have last order for each email/ancestor and read that file, it will be a lighter file as will only have one record for each email )
        # order_header_full = sbcf.get_parquet(order_header_file_dir, order_header_schema)
        # order_header_full = sif.get_parquet(order_header_history_file, order_header_schema, order_header_fields)
        # keep only email addresses which are also in order_header
        # order_header_full_filtered = order_header_full.merge(order_header.email_address.drop_duplicates(), how='inner', on='email_address')

        # order_header_full_filtered = sif.get_all_orders_for_email(bucket_path, run_date, order_header_schema, order_header[['email_address']])
        # order_header_full_filtered = order_header_history.merge(input_emails.email_address.drop_duplicates(), how='inner', on='email_address')
        order_header_full_filtered = dd.merge(order_header_history, order_header[['email_address']].drop_duplicates(), how='inner', on='email_address')
        # concatenate all the orders for each email together
        order_header_cmb = pd.concat([ order_header_full_filtered, order_header ], sort=False, ignore_index=True)
        order_header_cmb = order_header_cmb.drop_duplicates(subset='order_id', keep = 'first')
        # release unused dataframes
        order_header_full_filtered = pd.DataFrame()

        order_header_new_subscription = sif.fix_cascade(order_header_cmb)
        order_header_new_subscription = sif.fix_ancestor(order_header_new_subscription)
        order_header_new_subscription = sif.fix_migration_ancestor(order_header_new_subscription)
        order_header_new_subscription = sif.fix_cancellation_salvages(order_header_new_subscription)

        order_header_cmb = sif.get_subscription(order_header_cmb)

        order_header_cmb = sif.get_subscription_related_data(order_header_cmb)

        # keep only orders for run date
        order_header_cmb = dd.merge(order_header_cmb, order_header[['order_id']], how='inner', on='order_id')
        # save file
        rs.save_to_parquet(order_header_cmb, subscription_master_file_dir, run_date +'_subscription_master')

        # calculate other data for new subscription file
        order_header_new_subscription = sif.get_subscription_related_data(order_header_new_subscription)
        # keep only orders for run date
        order_header_new_subscription = dd.merge(order_header_new_subscription, order_header[['order_id']], how='inner', on='order_id')
        # save file
        rs.save_to_parquet(order_header_new_subscription, subscription_master_file_dir_new, run_date + '_subscription_master_new')
        # drop data to free up memory
        order_header_cmb = pd.DataFrame()
        order_header = pd.DataFrame()

        order_header_new_subscription = pd.DataFrame()

