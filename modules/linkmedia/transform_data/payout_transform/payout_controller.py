from linkmedia.lib import read_store as rs
from linkmedia.transform_data.payout_transform import payout_schema as ps
from linkmedia.transform_data.payout_transform import payout_functions as pf

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json

class payout:     
    def __init__(self):
        self

    def get_payout(self, run_date):
        
        # Path to save/load data
        bucket = 'linkmedia-datalake'
        import_bucket = 'datalake-imports'
        date_path = run_date.replace('-', '/')
        bucket_path = 's3://'+bucket+'/'

       
        order_header_file = bucket_path+'order_header/' + date_path+'/' + run_date +'_order_header.parquet'
        order_header_file_dir = bucket_path+'order_header/'
        payout_bucket_path = bucket_path+'order_updates/payout/' + date_path+'/'
        payout_file = run_date +'_payout'
        bank_file = 's3://munduexport-datalake/mundu/bank/bank.parquet'
        bank_payout_date_file = bucket_path+'order_updates/payout_bank_dates/bank_payout_dates.txt'

        payout_fields = ps.payout_fields
        # stepdown_keep_columns = ss.stepdown_keep_columns

        # payout = pd.read_excel(bucket_path+'misc\\Affiliates\\Banks_PayoutCycles.xlsx')
        #
        # ######
        # # r added in regex to suppress pylint warning, r suggests that its a raw string format
        # ######
        # payout['Window'] = payout.Window.str.extract(r'(\d+)')
        #
        # dayOfWeek={'Monday':0, 'Tuesday':1, 'Wednesday':2, 'Thursday':3, 'Friday':4, 'Saturday':5, 'Sunday':6}
        # payout['Cutoff_day_num'] = payout['Cut off day'].map(dayOfWeek)
        # payout['settlement_day_num'] = payout['Settlement Day'].map(dayOfWeek)
        #
        # payout['time_zone_num'] = payout['Time Zone'].str.replace('UTC','').str.replace(' ','')
        # payout['time_zone_sign'] = np.where(~(payout['time_zone_num'].str[0].isin(['+','-'])), '+', payout['time_zone_num'].str[0])
        # payout['time_zone_num'] = payout['time_zone_num'].str.strip('+-').apply(lambda x: np.where(x=='',0,x))
        # payout[['time_zone_num', 'time_zone_sign']]

        # get order header for run_date
        order_header = pf.get_parquet(order_header_file, payout_fields)
        order_header['order_date'] = order_header.time_stamp.dt.date
        # get bank payout dates
        bank_payout_dates = pd.read_csv(bank_payout_date_file, index_col=False, infer_datetime_format=True, delimiter='\t', parse_dates=['date', 'datetime_cutoff', 'payout_date_before_cutoff', 'payout_date_cutoff'])
        bank_payout_dates['date'] = bank_payout_dates['date'].dt.date

        payout = pf.calculate_pay_date(order_header, bank_payout_dates)
        # order_header = order_header.assign(payout_date=0)
        # for i in list(range(0, order_header.payout_date.size)):
        #     order_header['payout_date'][i] = pd.to_datetime(pf.calculate_pay_date(order_header.time_stamp[i].strftime('%Y-%m-%d %H:%M'), order_header.bank_name[i]), format='%Y-%m-%d')

        # save payout file
        pf.save_to_parquet(payout, payout_bucket_path, payout_file)

        # generate payout date for every bank for the run date, this will help with getting payout dates for chargebacks.
        # load bank data
        # bank_data = pf.get_parquet(bank_file, ['name'])
        # bank_data = bank_data.rename(columns={'name': 'bank_name'})
        # bank_data = bank_data.assign(date=run_date, payout_date=run_date)

        # try:
        #     bank_payout_date = pf.get_parquet(bank_payout_date_file, ['date', 'bank_name', 'payout_date'])
        # except:
        #     bank_payout_date = pd.DataFrame(columns=['date', 'bank_name', 'payout_date'])
        #
        # for i in list(range(0, bank_data.bank_name.size)):
        #     bank_data['payout_date'][i] = pf.calculate_pay_date(bank_data.date[i]+' 00:00', bank_data.bank_name[i])
        #
        # bank_data['date'] = pd.to_datetime(bank_data.date, format='%Y-%m-%d')
        # bank_data['payout_date'] = pd.to_datetime(bank_data.payout_date, format='%Y-%m-%d')
        # bank_payout_date = pd.concat([bank_payout_date, bank_data], ignore_index=True, sort=False)

        # # save payout file
        # pf.save_to_parquet(bank_payout_date, bucket_path+'payout_bank_dates/', 'payout_bank_dates')




