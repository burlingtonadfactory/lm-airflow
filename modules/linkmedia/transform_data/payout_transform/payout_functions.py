from linkmedia.lib import read_store as rs

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
fs = s3fs.S3FileSystem()
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import tz
import calendar

def get_parquet(read_file_path, read_schema):
    """ Read parquet file/(partitions) and return dataframe with required fields. """
    # read the parquet and return as a pandas dataframe
    read_fields = [key for key in read_schema]
    return pd.read_parquet(read_file_path, columns=read_fields)

def save_to_parquet(pandas_data_frame, bucket_path, file_name):
    """ Save dataframe to parquet file. """
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    fs = s3fs.S3FileSystem()
    # save to parquet file
    pq.write_table(pa.Table.from_pandas(pandas_data_frame, preserve_index=False),
                   bucket_path + file_name + '.parquet', filesystem=fs, flavor='spark')

def calculate_pay_date(order_data, bank_payout_dates):
    payout = order_data.merge(bank_payout_dates, how='left', left_on=['bank_name', 'order_date'], right_on=['bank', 'date'])
    payout['payout_date'] = pd.NaT
    payout['payout_date'] = np.where(payout.time_stamp < payout.datetime_cutoff, payout.payout_date_before_cutoff, payout.payout_date_cutoff)
    return payout[['order_id','time_stamp','bank_name','payout_date']]
#
# def calculate_pay_date(order_date,bankname):
#     order_date_dateformat = datetime.strptime(order_date, '%Y-%m-%d %H:%M')
#     #print(order_date_dateformat)
#     order_hour=int(str(order_date.split(" ")[1]).split(":")[0])
#     #print(order_hour)
#     order_date_1 = datetime.strptime(order_date.split(" ")[0], '%Y-%m-%d').weekday()
#     #print(order_date_1)
#     order_day = calendar.day_name[order_date_1]
#     #print(order_day)
#     day_dict={"Sunday":1,"Monday":2,"Tuesday":3,"Wednesday":4,"Thursday":5,"Friday":6,"Saturday":7}
#     daystoadd = abs((1-day_dict[order_day]))
#     #print(daystoadd)
#     if (daystoadd ==0):
#         daystoadd=7
#     startofweek=(order_date_dateformat  - timedelta(days=daystoadd))
#     #print(startofweek)
#     weekstartdate = datetime.strptime(str(startofweek).split(" ")[0], '%Y-%m-%d').weekday()
#     #print(weekstartdate)
#     weekstartdate1 = calendar.day_name[weekstartdate]
#     #print(weekstartdate1)
#     payout_date=""
#     if (bankname.lower()=="EMP - ECP".lower()):
#         emp_settleday=day_dict['Friday']
#         emp_cutoffday=day_dict['Wednesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="PAYSAFE".lower()):
#         emp_settleday=day_dict['Wednesday']
#         emp_cutoffday=day_dict['Tuesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="VP - STFS".lower()):
#         emp_settleday=day_dict['Tuesday']
#         emp_cutoffday=day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="EMP - WDL".lower()):
#         emp_settleday=day_dict['Thursday']
#         emp_cutoffday=day_dict['Wednesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="EMP - B&S".lower()):
#         emp_settleday=day_dict['Friday']
#         emp_cutoffday=day_dict['Thursday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="EMP - BOR".lower()):
#         emp_settleday=day_dict['Wednesday']
#         emp_cutoffday=day_dict['Wednesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="EMP - KORTA".lower()):
#         emp_settleday=day_dict['Sunday']
#         emp_cutoffday=day_dict['Wednesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="CHECKOUT".lower()):
#         emp_settleday=day_dict['Friday']
#         emp_cutoffday=day_dict['Sunday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="PAYVISION".lower()):
#         emp_settleday=day_dict['Wednesday']
#         emp_cutoffday=day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="EMP - SIX".lower()):
#         emp_settleday=day_dict['Friday']
#         emp_cutoffday=day_dict['Wednesday']
#         order_date_tmp = order_date_dateformat
#     elif( bankname.lower()=="VP - TRUEVO".lower()):
#         emp_settleday=day_dict['Wednesday']
#         emp_cutoffday=day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     elif (bankname.lower() == "None".lower()):
#         emp_settleday = day_dict['Wednesday']
#         emp_cutoffday = day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     elif (bankname.lower() == "DUMMY".lower()):
#         emp_settleday = day_dict['Wednesday']
#         emp_cutoffday = day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     else:
#         emp_settleday = day_dict['Wednesday']
#         emp_cutoffday = day_dict['Monday']
#         order_date_tmp = order_date_dateformat
#     if( (day_dict[order_day] > emp_cutoffday ) ):
#         #print("day_dict[order_day] > emp_cutoffday")
#         return (fn_order_grt_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat))
#     if( (day_dict[order_day] < emp_cutoffday ) ):
#         #print(day_dict[order_day] < emp_cutoffday)
#         return(fn_order_less_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat))
#     if((day_dict[order_day] == emp_cutoffday ) ):     #print("day_dict[order_day] == emp_cutoffday")
#         if (order_hour < 23):
#             return(fn_order_less_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat))
#         else:
#             return(fn_order_grt_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat))
#
#
#
# def fn_order_grt_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat):
#     final_payout =  order_date_tmp
#     while order_date_tmp.weekday() != 4:
#         #print(order_date_tmp.weekday())
#         order_date_tmp += timedelta(1)
#         final_payout =  order_date_tmp
#     if ((order_date_dateformat.weekday() >= (emp_cutoffday-2) ) and (order_date_dateformat.weekday() <= (emp_settleday-2))):
#     #window
#         final_payout =  final_payout+timedelta(7)
#     #print("final pay out :"+str(final_payout))
#     return final_payout
#
#
# def fn_order_less_cutoff(order_date_tmp,emp_cutoffday,emp_settleday,day_dict,order_day,order_date_dateformat):
#     while order_date_tmp.weekday() != 4:
#         #print(order_date_tmp.weekday())
#         order_date_tmp += timedelta(1)
#         final_payout =  order_date_tmp
#     va = final_payout
#     return final_payout
#
