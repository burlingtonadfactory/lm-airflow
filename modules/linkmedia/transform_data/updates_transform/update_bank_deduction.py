import datetime
import pyathena
import boto3
from pyathena import connect
from sqlalchemy import create_engine
import pandas as pd

#s3 file clean up
def clean_up(foldername):
    s3 = boto3.resource('s3')
    lastfolder = foldername
    if (lastfolder):
            bucket = s3.Bucket('linkmedia-datalake')
            prefix_to_clean = "order_updates/refunds_with_bank_dedcutions/"+lastfolder
            print('deleting prefix'+prefix_to_clean)
            bucket.objects.filter(Prefix=prefix_to_clean).delete()
            partition_0 = lastfolder.split('/')[0].split("=")[1]
            print( 'now insert the data for partition_0'+partition_0)
            partition_1= lastfolder.split('/')[1].split("=")[1]
            print( 'now insert the data for parttition_1'+partition_1)
            insert_new_refunds(partition_0, partition_1)
            return ("data processed ")

def insert_new_refunds(partition_0 , partition_1):
    cursor_insert = connect(
        s3_staging_dir='s3://linkmedia-datalake/athenatemp/',
        region_name='eu-west-1').cursor()


    insert_query = '''
    insert into  datawarehouse.refunds_with_bn_deduction
        with aa as
        (
        select odr.order_id , refunds.refund_date, odr.order_total_eur , odr.bank_name ,odr.order_currency, odr.billing_continent_name  ,
        gateway_processing_percent , gateway_reserve_percent ,
        gateway_transaction_fee , 0 as gateway_chargeback_fee
        from datawarehouse.order_header odr
        join
        (
        SELECT order_id  , date(refund_date) as refund_date
        FROM "datawarehouse"."refunds"
         where  
         partition_0 = '''+"'"+partition_0+"'"+'''
        and partition_1='''+"'"+partition_1+"'"+'''
        ) refunds
        on ( odr.order_id = refunds.order_id)
        ), bb as
        ( select * from datawarehouse.bank_deduction )
        , cc as
        ( select * from aa left join bb on
        ( aa.bank_name = bb.bank_name
        and  aa.order_currency = bb.currency
         and aa.billing_continent_name = bb.billing_continent)
         )
        select
        order_id , refund_date ,
        case
        when date(refund_date) >=date('2020-04-01') then
         processing_percent_mdr  -- take from new table 
        else
         gateway_processing_percent -- else from order_header 
        end
        as  gateway_processing_percent , -- let column name be same 
        case
        when date(refund_date) >=date('2020-04-01') then
         transaction_fee
        else
        gateway_transaction_fee
        end
        as  gateway_transaction_fee ,
        case
        when date(refund_date) >=date('2020-04-01') then
        refund_fee
        else
        refund_fee
         end
        as  refund_fee ,
        case
        
        when date(refund_date) >=date('2020-04-01') then
                 reserve_percent
             else
                gateway_reserve_percent
             end
        as  gateway_reserve_percent ,
        
        case
        
        when date(refund_date) >=date('2020-04-01') then
                 0
             else
                0
             end
        as  gateway_chargeback_fee ,
        
        substr ( cast (refund_date as varchar) , 1, 4) as partition_0,
        substr ( cast (refund_date as varchar) , 6, 2) as partition_1,
        substr ( cast (refund_date as varchar) , 9, 2) as partition_2
        from cc
        where order_id not in ( select order_id from datawarehouse.refunds_with_bn_deduction)
            '''

    cursor_insert.execute(insert_query)
    return ("query executed ")



# clean up query
daily_query_clean_up ='''
with  aaa as 
( select order_id , count(order_id ) from datawarehouse.refunds_with_bn_deduction group by 1
having count(order_id) > 1)

select distinct 'partition_0='||partition_0||
'/partition_1='||partition_1||'/'
 from datawarehouse.refunds_with_bn_deduction
where order_id in ( select order_id from aaa);
'''

cursor = connect(
                 s3_staging_dir='s3://linkmedia-datalake/athenatemp/',
                 region_name='eu-west-1').cursor()

cursor.execute(daily_query_clean_up)





for row in cursor:
    print(str(row[0]))
    clean_up(str(row[0]))

#run for last 6 months by default
today = datetime.date.today()
six_month_before_month = (today.month - 6) % 12
six_month_before_year = today.year + ((today.month - 6) // 12)
if ( six_month_before_year== today.year):
    print('Year will not change')
    for month in ( range(six_month_before_month , today.month+1)):
        year = today.year
        if (month <10):
            var_month = "0"+str(month)
            var_year =  str(year)
        else :
            var_month = str(month)
            var_year = str(year)

        var_to_send = "partition_0="+var_year +'/'+"partition_1="+var_month+'/'
        print(var_to_send)
        clean_up(var_to_send)

        month= month+1
else :
    for month in ( range(six_month_before_month , 13)):
        year = six_month_before_year

        if (month < 10):
            var_month = "0" + str(month)
            var_year =  str(year)
        else:
            var_month =  str(month)
            var_year = str(year)

        var_to_send = "partition_0=" +var_year+'/'+"partition_1="+var_month+'/'

        print(var_to_send)
        clean_up(var_to_send)

        month = month + 1

    for month in (range(1,today.month+1 )):
        year = today.year

        if (month < 10):
            var_month =  "0" + str(month)
            var_year =  str(year)
        else:
            var_month =  str(month)
            var_year = str(year)

        var_to_send = "partition_0="+var_year+'/'+"partition_1="+var_month+'/'

        print(var_to_send)
        clean_up(var_to_send)

        month = month + 1










