from linkmedia.lib import read_store as rs
from linkmedia.order_header import order_header_schema as ohs
from linkmedia.transform_data.updates_transform import updates_functions as uf

import logging
import datetime
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
import pyathena
from pyathena import connect
from smart_open import s3_iter_bucket
from smart_open import open 
from pandas.io.json import json_normalize
from sqlalchemy import create_engine
import json
fs = s3fs.S3FileSystem()
# it should run for every udpate file


class updates_controller:     
    def __init__(self):
        self

    #s3 file clean up
    def clean_up(self, foldername):
        s3 = boto3.resource('s3')
        lastfolder = foldername
        if (lastfolder):
                bucket = s3.Bucket('linkmedia-datalake')
                prefix_to_clean = "order_updates/refunds_with_bank_dedcutions/"+lastfolder
                print('deleting prefix'+prefix_to_clean)
                bucket.objects.filter(Prefix=prefix_to_clean).delete()
                partition_0 = lastfolder.split('/')[0].split("=")[1]
                print( 'now insert the data for partition_0'+partition_0)
                partition_1= lastfolder.split('/')[1].split("=")[1]
                print( 'now insert the data for parttition_1'+partition_1)
                self.insert_new_refunds(partition_0, partition_1)
                return ("data processed ")

    def insert_new_refunds(self, partition_0 , partition_1):
        cursor_insert = connect(
            s3_staging_dir='s3://linkmedia-datalake/athenatemp/',
            region_name='eu-central-1').cursor()


        insert_query = '''
        insert into  datawarehouse.refunds_with_bank_dedcutions
            with aa as
            (
            select odr.order_id , refunds.refund_date, odr.order_total_eur , odr.bank_name ,odr.order_currency, odr.billing_continent_name  ,
            gateway_processing_percent , gateway_reserve_percent ,
            gateway_transaction_fee , 0 as gateway_chargeback_fee
            from datawarehouse.order_header odr
            join
            (
            SELECT order_id  , date(refund_date) as refund_date
            FROM "datawarehouse"."refunds"
            where  
            partition_0 = '''+"'"+partition_0+"'"+'''
            and partition_1='''+"'"+partition_1+"'"+'''
            ) refunds
            on ( odr.order_id = refunds.order_id)
            ), bb as
            ( select * from chargebacks.bank_deduction )
            , cc as
            ( select * from aa left join bb on
            ( aa.bank_name = bb.bank_name
            and  aa.order_currency = bb.currency
            and aa.billing_continent_name = bb.billing_continent)
            )
            select
            order_id , refund_date ,
            case
            when date(refund_date) >=date('2020-04-01') then
            processing_percent_mdr  -- take from new table 
            else
            gateway_processing_percent -- else from order_header 
            end
            as  gateway_processing_percent , -- let column name be same 
            case
            when date(refund_date) >=date('2020-04-01') then
            transaction_fee
            else
            gateway_transaction_fee
            end
            as  gateway_transaction_fee ,
            case
            when date(refund_date) >=date('2020-04-01') then
            refund_fee
            else
            refund_fee
            end
            as  refund_fee ,
            case
            
            when date(refund_date) >=date('2020-04-01') then
                    reserve_percent
                else
                    gateway_reserve_percent
                end
            as  gateway_reserve_percent ,
            
            case
            
            when date(refund_date) >=date('2020-04-01') then
                    0
                else
                    0
                end
            as  gateway_chargeback_fee ,
            
            substr ( cast (refund_date as varchar) , 1, 4) as partition_0,
            substr ( cast (refund_date as varchar) , 6, 2) as partition_1,
            substr ( cast (refund_date as varchar) , 9, 2) as partition_2
            from cc
            where order_id not in ( select order_id from datawarehouse.refunds_with_bank_dedcutions)
                '''

        cursor_insert.execute(insert_query)
        return ("query executed ")

    def get_order_updates(self, run_date):
        
        # Path to save/load data
        bucket = 'linkmedia-datalake'
        date_path = run_date.replace('-', '/')
        bucket_path = 's3://'+bucket+'/'

        s3_staging_dir = 's3://munduexport-datalake/mundu/query_staging_location/'
        region_name = 'eu-central-1'

        bank_file = 's3://munduexport-datalake/mundu/bank/bank.parquet'

        run_date_previous_day = (pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime('%Y-%m-%d')
        run_date_previous_day_path = run_date_previous_day.replace('-', '/')

        #order_header_history_file = bucket_path+'order_header_history/order_header_history.parquet'

        lime_schema = ohs.lime_schema
        gateway_processed_schema = ohs.gateway_processed_schema
        bank_schema = ohs.bank_schema

        # load the required renaming
        lime_fields_rename = ohs.lime_fields_rename
        gateway_processed_fields_rename = ohs.gateway_processed_fields_rename
        bank_fields_rename = ohs.bank_fields_rename

        lime_updates = bucket_path+'limelight/' + date_path+'/' + run_date +'_lime.gzip'
        refunds_previous_day_file = bucket_path+'order_updates/refunds/'+run_date_previous_day_path+'/' + run_date_previous_day + '_refunds.parquet'
        cancellations_previous_day_file = bucket_path + 'order_updates/cancellations/' + run_date_previous_day_path + '/' + run_date_previous_day + '_cancellations.parquet'
        void_previous_day_file = bucket_path + 'order_updates/void/' + run_date_previous_day_path + '/' + run_date_previous_day + '_void.parquet'
        recurring_previous_day_file = bucket_path + 'order_updates/recurring/' + run_date_previous_day_path + '/' + run_date_previous_day + '_recurring.parquet'

        # get limelight updated orders for run date and previous day
        logging.info('get limelight updated orders for run date and previous day')
        lime_updates = uf.get_parquet(lime_updates, lime_schema)
        # rename lime fields to required names
        lime_updates = lime_updates.rename(columns=lime_fields_rename)

        # Read gateway processed parquet
        logging.info('Loading gateway data ...')
        # gateway_data = uf.get_parquet('s3://linkmedia-datalake/temp_from_view/gateways.parquet', gateway_processed_schema)
        gateway_data = rs.get_athena_data('mundu_replica.dim_gateways_v', s3_staging_dir, region_name)
        # gateway_data = rs.get_athena_data('mundu_replica.dim_gateways_from_view', s3_staging_dir, region_name)
        gateway_data = gateway_data[list(gateway_processed_schema.keys())]
        gateway_data = gateway_data.astype(dtype=gateway_processed_schema)
        # rename gateway fields to required names
        gateway_data = gateway_data.rename(columns=gateway_processed_fields_rename)
        logging.info('Loaded gateway data')

        # load bank data
        bank_data = uf.get_parquet(bank_file, bank_schema)
        # rename bank fields to required names
        bank_data = bank_data.rename(columns=bank_fields_rename)
        logging.info('Loaded bank data')

        # merge bank data to gateway data
        logging.info('merge bank data to gateway data ..')
        gateway_data = gateway_data.merge(bank_data, how='left', on='bank_id')
        # merge gateway data to limelight data
        logging.info('merge gateway data to limelight data ..')
        lime_updates = lime_updates.merge(
            gateway_data, how='left', on='order_gateway_id')

        try:
            refunds_previous_day = pd.read_parquet(refunds_previous_day_file)
        except:
            # create an empty data frame
            refunds_previous_day = pd.DataFrame(columns=['order_id', 'refund_date', 'order_gateway_id', 'bank_id'])
        try:
            cancellations_previous_day = pd.read_parquet(cancellations_previous_day_file)
        except:
            # create an empty data frame
            cancellations_previous_day = pd.DataFrame(columns=['order_id', 'hold_date', 'on_hold_by'])
        try:
            void_previous_day = pd.read_parquet(void_previous_day_file)
        except:
            # create an empty data frame
            void_previous_day = pd.DataFrame(columns=['order_id', 'void_date'])
        try:
            recurring_previous_day = pd.read_parquet(recurring_previous_day_file)
        except:
            # create an empty data frame
            recurring_previous_day = pd.DataFrame(columns=['order_id', 'recurring_date'])

       # process all the orders which were refunded
        uf.process_refund_orders(lime_updates, refunds_previous_day, bucket_path+'order_updates/refunds/', run_date)
        logging.info('processed all the orders which were refunded')

        # process all the orders which were cancelled
        uf.process_cancelled_orders(lime_updates, cancellations_previous_day, bucket_path+'order_updates/cancellations/', run_date)
        logging.info('processed all the orders which were cancelled')
        # process all the orders which were cancelled
        uf.process_void_orders(lime_updates, void_previous_day, bucket_path+'order_updates/void/', run_date)
        logging.info('processed all the orders which were voided')

        # process all the orders which were cancelled
        uf.process_next_recurring_orders(lime_updates, recurring_previous_day, bucket_path + 'order_updates/recurring/', run_date)
        logging.info('processed all recurring orders')

        # drop data to free up memory
        lime_updates = pd.DataFrame()
        refunds_previous_day = pd.DataFrame()
        cancellations_previous_day = pd.DataFrame()
        void_previous_day = pd.DataFrame()
        recurring_previous_day = pd.DataFrame()

    def update_bank_deduction(self):
        # clean up query
        daily_query_clean_up ='''
        with  aaa as 
        ( select order_id , count(order_id ) from datawarehouse.refunds_with_bank_dedcutions group by 1
        having count(order_id) > 1)

        select distinct 'partition_0='||partition_0||
        '/partition_1='||partition_1||'/'
        from datawarehouse.refunds_with_bank_dedcutions
        where order_id in ( select order_id from aaa);
        '''

        cursor = connect(
                        s3_staging_dir='s3://linkmedia-datalake/athenatemp/',
                        region_name='eu-central-1').cursor()

        cursor.execute(daily_query_clean_up)





        for row in cursor:
            print(str(row[0]))
            self.clean_up(str(row[0]))

        #run for last 6 months by default
        today = datetime.date.today()
        six_month_before_month = (today.month - 6) % 12
        six_month_before_year = today.year + ((today.month - 6) // 12)
        if ( six_month_before_year== today.year):
            print('Year will not change')
            for month in ( range(six_month_before_month , today.month+1)):
                year = today.year
                if (month <10):
                    var_month = "0"+str(month)
                    var_year =  str(year)
                else :
                    var_month = str(month)
                    var_year = str(year)

                var_to_send = "partition_0="+var_year +'/'+"partition_1="+var_month+'/'
                print(var_to_send)
                self.clean_up(var_to_send)

                month= month+1
        else :
            for month in ( range(six_month_before_month , 13)):
                year = six_month_before_year

                if (month < 10):
                    var_month = "0" + str(month)
                    var_year =  str(year)
                else:
                    var_month =  str(month)
                    var_year = str(year)

                var_to_send = "partition_0=" +var_year+'/'+"partition_1="+var_month+'/'

                print(var_to_send)
                self.clean_up(var_to_send)

                month = month + 1

            for month in (range(1,today.month+1 )):
                year = today.year

                if (month < 10):
                    var_month =  "0" + str(month)
                    var_year =  str(year)
                else:
                    var_month =  str(month)
                    var_year = str(year)

                var_to_send = "partition_0="+var_year+'/'+"partition_1="+var_month+'/'

                print(var_to_send)
                self.clean_up(var_to_send)

                month = month + 1