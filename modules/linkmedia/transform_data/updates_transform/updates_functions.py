from linkmedia.transform_data.updates_transform import updates_schema as us

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open
from datetime import datetime
from pandas.io.json import json_normalize
import json

fs = s3fs.S3FileSystem()


def save_to_parquet(pandas_data_frame, bucket_path, file_name):
    """ Save dataframe to parquet file. """
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    fs = s3fs.S3FileSystem()
    # save to parquet file
    pq.write_table(pa.Table.from_pandas(pandas_data_frame, preserve_index=False),
                   bucket_path + file_name + '.parquet', filesystem=fs, flavor='spark')


def get_parquet(read_file_path, read_schema):
    """ Read parquet file and return dataframe with required fields. """
    read_fields = list(read_schema.keys())
    # read the parquet and return as a pandas dataframe
    return pd.read_parquet(read_file_path, columns=read_fields).astype(dtype=read_schema)


def process_refund_orders(input_dataframe_run_date, input_dataframe_previous_day, save_file_dir, run_date):
    """This function checks if there is any refund for the order"""
    refund_orders = input_dataframe_run_date[['order_id', 'refund_date', 'order_gateway_id', 'bank_id']]
    refund_orders['refund_date'] = pd.to_datetime(
        refund_orders['refund_date'], infer_datetime_format=True, errors='coerce')  # format='%Y-%m-%d %H:%M:%S'

    # refund date should be equal to run date then only it should be considered a new refund otherwise it is an order which was refunded previously and
    # another record came due to some other update. Though, due to time zone differences, there could be a few real updates from previous day in current day's
    # files. These need to be handled separately.


    run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    refund_orders_run_date = refund_orders[refund_orders.refund_date.dt.date == run_date]
    refund_orders_run_date = refund_orders_run_date.sort_values('refund_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date = run_date.strftime(format='%Y-%m-%d')
    run_date_path = run_date.replace('-', '/')
    save_to_parquet(refund_orders_run_date, save_file_dir+run_date_path+'/', run_date + '_refunds')


    run_date_previous_day = pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)
    refund_orders_previous_day = refund_orders[refund_orders.refund_date.dt.date == run_date_previous_day]
    refund_orders_previous_day = pd.concat([refund_orders_previous_day, input_dataframe_previous_day], sort=False, ignore_index=True)
    refund_orders_previous_day = refund_orders_previous_day.sort_values('refund_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date_previous_day = run_date_previous_day.strftime('%Y-%m-%d')
    run_date_previous_day_path = run_date_previous_day.replace('-', '/')
    save_to_parquet(refund_orders_previous_day, save_file_dir+run_date_previous_day_path+'/', run_date_previous_day + '_refunds')
    # empty the dataframe
    refund_orders = pd.DataFrame()
    refund_orders_run_date = pd.DataFrame()
    refund_orders_previous_day = pd.DataFrame()


def process_cancelled_orders(input_dataframe_run_date, input_dataframe_previous_day, save_file_dir, run_date):
    """This function checks if there is any cancellation for the order"""
    cancelled_orders = input_dataframe_run_date[['order_id', 'hold_date', 'on_hold_by']]
    cancelled_orders['hold_date'] = pd.to_datetime(
        cancelled_orders['hold_date'], infer_datetime_format=True, errors='coerce')  # format='%Y-%m-%d %H:%M:%S'


    # [input_dataframe_run_date.hold_date == run_date]
    # run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    # cancelled_orders = cancelled_orders[
    #     (cancelled_orders.hold_date.dt.date == run_date) & (cancelled_orders.on_hold_by != 'Lime Light CRM')]
    # cancelled_orders = cancelled_orders.sort_values('hold_date', ascending=True).drop_duplicates(['order_id'],
    #                                                                                              keep='last')
    # # df.sort_values('systemNotes', ascending=True).drop_duplicates(['order_id'],keep='last')
    # # hold date should be equal to run date then only it should be considered a new cancellation otherwise it is an order which was cancelled previously and
    # # another record came due to some other update.
    # run_date = run_date.strftime(format='%Y-%m-%d')
    # save_to_parquet(cancelled_orders, save_file_dir, run_date + '_cancellations')

    run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    cancelled_orders_run_date = cancelled_orders[(cancelled_orders.hold_date.dt.date == run_date) & (cancelled_orders.on_hold_by != 'Lime Light CRM')]
    cancelled_orders_run_date = cancelled_orders_run_date.sort_values('hold_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date = run_date.strftime(format='%Y-%m-%d')
    run_date_path = run_date.replace('-', '/')
    save_to_parquet(cancelled_orders_run_date, save_file_dir+run_date_path+'/', run_date + '_cancellations')



    run_date_previous_day = pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)
    cancelled_orders_previous_day = cancelled_orders[(cancelled_orders.hold_date.dt.date == run_date_previous_day) & (cancelled_orders.on_hold_by != 'Lime Light CRM')]
    cancelled_orders_previous_day = pd.concat([cancelled_orders_previous_day, input_dataframe_previous_day], sort=False, ignore_index=True)
    cancelled_orders_previous_day = cancelled_orders_previous_day.sort_values('hold_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date_previous_day = run_date_previous_day.strftime('%Y-%m-%d')
    run_date_previous_day_path = run_date_previous_day.replace('-', '/')
    save_to_parquet(cancelled_orders_previous_day, save_file_dir+run_date_previous_day_path+'/', run_date_previous_day + '_cancellations')

    # empty the dataframe
    cancelled_orders = pd.DataFrame()
    cancelled_orders_run_date = pd.DataFrame()
    cancelled_orders_previous_day = pd.DataFrame()


def process_void_orders(input_dataframe_run_date, input_dataframe_previous_day, save_file_dir, run_date):
    """This function checks if there is any void status for the order"""
    void_orders = input_dataframe_run_date[['order_id', 'void_date']]
    void_orders['void_date'] = pd.to_datetime(
        void_orders['void_date'], infer_datetime_format=True, errors='coerce')  # format='%Y-%m-%d %H:%M:%S'

    # run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    # void_orders = void_orders[void_orders.void_date.dt.date == run_date]
    # void_orders = void_orders.sort_values('void_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    # # refund date should be equal to run date then only it should be considered a new refund otherwise it is an order which was refunded previously and
    # # another record came due to some other update.
    # run_date = run_date.strftime(format='%Y-%m-%d')
    # save_to_parquet(void_orders, save_file_dir, run_date + '_void')


    run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    void_orders_run_date = void_orders[void_orders.void_date.dt.date == run_date]
    void_orders_run_date = void_orders_run_date.sort_values('void_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date = run_date.strftime(format='%Y-%m-%d')
    run_date_path = run_date.replace('-', '/')
    save_to_parquet(void_orders_run_date, save_file_dir+run_date_path+'/', run_date + '_void')


    run_date_previous_day = pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)
    void_orders_previous_day = void_orders[void_orders.void_date.dt.date == run_date_previous_day]
    void_orders_previous_day = pd.concat([void_orders_previous_day, input_dataframe_previous_day], sort=False, ignore_index=True)
    void_orders_previous_day = void_orders_previous_day.sort_values('void_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date_previous_day = run_date_previous_day.strftime('%Y-%m-%d')
    run_date_previous_day_path = run_date_previous_day.replace('-', '/')
    save_to_parquet(void_orders_previous_day, save_file_dir+run_date_previous_day_path+'/', run_date_previous_day + '_void')

    # empty the dataframe
    void_orders = pd.DataFrame()
    void_orders_run_date = pd.DataFrame()
    void_orders_previous_day = pd.DataFrame()


def process_next_recurring_orders(input_dataframe_run_date, input_dataframe_previous_day, save_file_dir, run_date):
    """This function checks if there is any new recurring date for the order-subscription"""
    recurring_orders = input_dataframe_run_date[['order_id', 'time_stamp', 'recurring_date']]
    recurring_orders['recurring_date'] = pd.to_datetime(
        recurring_orders['recurring_date'], infer_datetime_format=True, errors='coerce')  # format='%Y-%m-%d %H:%M:%S'
    recurring_orders['time_stamp'] = pd.to_datetime(
        recurring_orders['time_stamp'], infer_datetime_format=True, errors='coerce')

    run_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    # recurring_orders_run_date = recurring_orders[recurring_orders.recurring_date.dt.date == run_date]
    # recurring_orders_run_date = recurring_orders_run_date.sort_values('recurring_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    run_date = run_date.strftime(format='%Y-%m-%d')
    run_date_path = run_date.replace('-', '/')
    # # save as string
    # recurring_orders['recurring_date'] = recurring_orders['recurring_date'].astype(str)
    save_to_parquet(recurring_orders, save_file_dir+run_date_path+'/', run_date + '_recurring')

    # recurring_orders['recurring_date'] = pd.to_datetime(
    #     recurring_orders['recurring_date'], infer_datetime_format=True, errors='coerce')

    # run_date_previous_day = pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)
    # recurring_orders_previous_day = recurring_orders[recurring_orders.recurring_date.dt.date == run_date_previous_day]
    # recurring_orders_previous_day = pd.concat([recurring_orders_previous_day, input_dataframe_previous_day], sort=False, ignore_index=True)
    # # recurring_orders_previous_day = recurring_orders_previous_day.sort_values('recurring_date', ascending=True).drop_duplicates(['order_id'], keep='last')
    # run_date_previous_day = run_date_previous_day.strftime('%Y-%m-%d')
    # run_date_previous_day_path = run_date_previous_day.replace('-', '/')
    # # recurring_orders['recurring_date'] = recurring_orders['recurring_date'].astype(str)
    # save_to_parquet(recurring_orders_previous_day, save_file_dir+run_date_previous_day_path+'/', run_date_previous_day + '_recurring')

    # empty the dataframe
    recurring_orders = pd.DataFrame()
    recurring_orders_run_date = pd.DataFrame()
    recurring_orders_previous_day = pd.DataFrame()