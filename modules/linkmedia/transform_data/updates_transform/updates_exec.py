
from linkmedia.transform_data.updates_transform import updates_controller as uc
import pandas as pd

# reserve release needs to run once every month for checking reserve released in that month
def get_data(run_date, number_of_days_ahead):
    # start date and number of days to go back
    start_date = run_date

    # create an instance of controller
    updates_controller = uc.updates_controller()

    # loop for required days and get trial over orders
    for i in list(range(0, number_of_days_ahead)):
        run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                    pd.DateOffset(i)).strftime('%Y-%m-%d')
        # get trial over orders
        updates_controller.get_order_updates(run_date)


