from linkmedia.lib import read_store as rs
from linkmedia.order_header import order_header_schema as ohs
from linkmedia.transform_data.updates_transform import updates_functions as uf

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
fs = s3fs.S3FileSystem()
# it should run for every udpate file


def get_order_updates(run_date):

    # Path to save/load data
    bucket = 'linkmedia-datalake'
    date_path = run_date.replace('-', '/')
    bucket_path = 's3://'+bucket+'/'

    s3_staging_dir = 's3://munduexport-datalake/mundu/query_staging_location/'
    region_name = 'eu-central-1'

    bank_file = bucket_path + 'mundu/bank/bank.parquet'

    run_date_previous_day = (pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime('%Y-%m-%d')
    run_date_previous_day_path = run_date_previous_day.replace('-', '/')

    #order_header_history_file = bucket_path+'order_header_history/order_header_history.parquet'

    lime_schema = ohs.lime_schema
    gateway_processed_schema = ohs.gateway_processed_schema
    bank_schema = ohs.bank_schema

    # load the required renaming
    lime_fields_rename = ohs.lime_fields_rename
    gateway_processed_fields_rename = ohs.gateway_processed_fields_rename
    bank_fields_rename = ohs.bank_fields_rename

    lime_updates = bucket_path+'limelight/updated/' + date_path+'/' + run_date +'_lime_updated.parquet'
    refunds_previous_day_file = bucket_path+'order_updates/refunds/'+run_date_previous_day_path+'/' + run_date_previous_day + '_refunds.parquet'
    cancellations_previous_day_file = bucket_path + 'order_updates/cancellations/' + run_date_previous_day_path + '/' + run_date_previous_day + '_cancellations.parquet'
    void_previous_day_file = bucket_path + 'order_updates/void/' + run_date_previous_day_path + '/' + run_date_previous_day + '_void.parquet'
    recurring_previous_day_file = bucket_path + 'order_updates/recurring/' + run_date_previous_day_path + '/' + run_date_previous_day + '_recurring.parquet'

    # get limelight updated orders for run date and previous day
    lime_updates = uf.get_parquet(lime_updates, lime_schema)
    # rename lime fields to required names
    lime_updates = lime_updates.rename(columns=lime_fields_rename)

    # Read gateway processed parquet
    # gateway_data = uf.get_parquet('s3://linkmedia-datalake/temp_from_view/gateways.parquet', gateway_processed_schema)
    # gateway_data = rs.get_athena_data('mundu_replica.dim_gateways_v', s3_staging_dir, region_name)
    # # gateway_data = rs.get_athena_data('mundu_replica.dim_gateways_from_view', s3_staging_dir, region_name)
    # gateway_data = gateway_data[list(gateway_processed_schema.keys())]
    # gateway_data = gateway_data.astype(dtype=gateway_processed_schema)
    # # rename gateway fields to required names
    # gateway_data = gateway_data.rename(columns=gateway_processed_fields_rename)
    #
    # # load bank data
    # bank_data = uf.get_parquet(bank_file, bank_schema)
    # # rename bank fields to required names
    # bank_data = bank_data.rename(columns=bank_fields_rename)
    #
    # # merge bank data to gateway data
    # gateway_data = gateway_data.merge(bank_data, how='left', on='bank_id')
    # # merge gateway data to limelight data
    # lime_updates = lime_updates.merge(
    #     gateway_data, how='left', on='order_gateway_id')
    #
    # try:
    #     refunds_previous_day = pq.read_table(refunds_previous_day_file, filesystem=fs).to_pandas()
    # except:
    #     # create an empty data frame
    #     refunds_previous_day = pd.DataFrame(columns=['order_id', 'refund_date', 'order_gateway_id', 'bank_id'])
    # try:
    #     cancellations_previous_day = pq.read_table(cancellations_previous_day_file, filesystem=fs).to_pandas()
    # except:
    #     # create an empty data frame
    #     cancellations_previous_day = pd.DataFrame(columns=['order_id', 'hold_date', 'on_hold_by'])
    # try:
    #     void_previous_day = pq.read_table(void_previous_day_file, filesystem=fs).to_pandas()
    # except:
    #     # create an empty data frame
    #     void_previous_day = pd.DataFrame(columns=['order_id', 'void_date'])
    try:
        recurring_previous_day = pq.read_table(recurring_previous_day_file, filesystem=fs).to_pandas()
    except:
        # create an empty data frame
        recurring_previous_day = pd.DataFrame(columns=['order_id', 'recurring_date'])

   # process all the orders which were refunded
   #  uf.process_refund_orders(lime_updates, refunds_previous_day, bucket_path+'order_updates/refunds/', run_date)
   #
   #  # process all the orders which were cancelled
   #  uf.process_cancelled_orders(lime_updates, cancellations_previous_day, bucket_path+'order_updates/cancellations/', run_date)
   #
   #  # process all the orders which were cancelled
   #  uf.process_void_orders(lime_updates, void_previous_day, bucket_path+'order_updates/void/', run_date)

    # process all the orders which were cancelled
    uf.process_next_recurring_orders(lime_updates, recurring_previous_day, bucket_path + 'order_updates/recurring/', run_date)

    # drop data to free up memory
    lime_updates = pd.DataFrame()
    # refunds_previous_day = pd.DataFrame()
    # cancellations_previous_day = pd.DataFrame()
    # void_previous_day = pd.DataFrame()
    recurring_previous_day = pd.DataFrame()

def get_data(run_date, number_of_days_ahead):
    # start date and number of days to go back
    start_date = run_date

    # loop for required days and get trial over orders
    for i in list(range(0, number_of_days_ahead)):
        run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                    pd.DateOffset(i)).strftime('%Y-%m-%d')
        # get trial over orders
        print(run_date)
        get_order_updates(run_date)
