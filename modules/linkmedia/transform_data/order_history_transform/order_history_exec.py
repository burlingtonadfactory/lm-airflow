
from linkmedia.transform_data.order_history_transform import order_history_controller as ohic
import pandas as pd



def get_data(run_date, number_of_days_ahead):
    # start date and number of days to go back
    start_date = run_date

    order_history = ohic.order_history()

    # loop for required days and generate order_header
    for i in list(range(0, number_of_days_ahead)):
        run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                    pd.DateOffset(i)).strftime('%Y-%m-%d')
        # run subscription_billing_cycle controller
        order_history.get_order_history(run_date)



