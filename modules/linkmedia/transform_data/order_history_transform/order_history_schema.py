import pyarrow as pa

order_header_history_fields = {
    'order_id', 
    'transaction_id',
    'ancestor_id',
    'email_address', 
    'time_stamp', 
    'main_product_category', 
    'campaign_id', 
    'order_status', 
    'brand_id', 
    'billing_cycle', 
    'cust_subscription_key', 
    'is_create_order',
    'order_total',
    'order_cc_key',
    'parent_id',
    'migration_parent_order_id',
    'cascade_first_parent_order_id'
}


order_header_history_pq_schema=[
    ('ancestor_id', pa.int64()),
    ('billing_cycle',  pa.int64()),
    ('campaign_id', pa.int64()),
    ('email_address',  pa.string()),
    ('order_id',  pa.int64()),
    ('order_status',  pa.int64()),
    ('order_total', pa.float64()),
    ('transaction_id', pa.string()),
    ('is_create_order',  pa.int64()),
    ('brand_id',  pa.int64()),
    ('main_product_category',  pa.int64()),
    ('order_cc_key', pa.string()),
    ('cust_subscription_key', pa.string()),
    ('time_stamp', pa.timestamp('ms')),
    ('parent_id',  pa.int64()),
    ('migration_parent_order_id', pa.string()),
    ('cascade_first_parent_order_id', pa.string())
]