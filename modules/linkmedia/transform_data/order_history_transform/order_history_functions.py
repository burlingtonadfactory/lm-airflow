from linkmedia.lib import read_store as rs

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
fs = s3fs.S3FileSystem()


def get_parquet(read_file_path, read_schema, read_fields):
    """ Read parquet file/(partitions) and return dataframe with required fields. """	
    # get the fields from schema passed
    # read the parquet and return as a pandas dataframe
    read_schema = {key: value for key, value in read_schema.items() if key in read_fields}
    return pd.read_parquet(read_file_path, columns=read_fields).astype(dtype=read_schema)


def save_to_parquet(pandas_data_frame, bucket_path, file_name, save_schema):
    """Saves file_name in parquet format in given bucket path"""
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    fs = s3fs.S3FileSystem()
    # save to parquet file, flavor = spark is important to make sure Athena reads parquets properly
    pq.write_table(pa.Table.from_pandas(pandas_data_frame, schema=save_schema),
                   bucket_path+file_name+'.parquet', filesystem=fs, flavor='spark')