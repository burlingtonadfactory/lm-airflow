from linkmedia.lib import read_store as rs
from linkmedia.transform_data.order_history_transform import order_history_schema as ohis
from linkmedia.order_header import order_header_schema as ohs
from linkmedia.transform_data.order_history_transform import order_history_functions as ohif

import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto
import boto3
from smart_open import s3_iter_bucket
from smart_open import open 
from datetime import datetime 
from pandas.io.json import json_normalize
import json
import os

class order_history:     
    def __init__(self):
        self

    def get_order_history(self, run_date):
        
        # Path to save/load data
        bucket = 'linkmedia-datalake'
        #import_bucket = 'datalake-imports'
        date_path = run_date.replace('-', '/')
        bucket_path = 's3://'+bucket+'/'

        run_month = run_date[:7]
        month_path = run_month.replace('-', '/')


        order_header_file = bucket_path+'order_header/' + date_path+'/' + run_date +'_order_header.parquet'
        # order_header_file_dir = bucket_path+'order_header/'

        order_header_history_file = bucket_path+'order_header_history_test/' + run_month +'_order_header_history.parquet'
        
        order_header_history_fields = ohis.order_header_history_fields
        order_header_schema = ohs.order_header_schema
        order_header_schema = {key: value for key, value in order_header_schema.items() if key in order_header_history_fields}

        # get order header for run_date
        order_header = ohif.get_parquet(order_header_file, order_header_schema, order_header_history_fields)

        # get order header history file
        # check if file exists
        order_header_history_file_exists = boto.connect_s3().get_bucket(bucket).get_key('order_header_history_test/' + run_month +'_order_header_history.parquet')
        if order_header_history_file_exists:
            order_header_history = ohif.get_parquet(order_header_history_file, order_header_schema, order_header_history_fields)
        else:
            order_header_history = pd.DataFrame()
    
        # concatenate all orders together
        order_header_cmb = pd.concat([order_header, order_header_history], sort=False, ignore_index=True)
        # order_header_cmb = order_header_cmb[order_header_cmb.is_create_order == 1]

        # empty previous data frames as not needed anymore
        order_header_history = pd.DataFrame()
        order_header = pd.DataFrame()

        order_header_cmb = order_header_cmb.sort_values(by=['order_id', 'time_stamp'], ascending=False)
        order_header_cmb = order_header_cmb.drop_duplicates(subset='order_id', keep = 'first')
        
        # apply order header schema
        order_header_cmb = order_header_cmb.astype(dtype=order_header_schema)

        order_header_history_pq_schema = pa.schema(ohis.order_header_history_pq_schema)
        #save to parquet file
        ohif.save_to_parquet(order_header_cmb, bucket_path+'order_header_history_test/', run_month +'_order_header_history', order_header_history_pq_schema)
        #drop data to free up memory
        order_header_cmb = pd.DataFrame()



        



    
        

    
