from linkmedia.order_header import order_header_schema as ohs
from linkmedia.lib import read_store as rs

import os
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open.s3 import iter_bucket as s3_iter_bucket
import pandavro as pdx
from smart_open import open
from datetime import datetime
from datetime import timedelta
from pandas.io.json import json_normalize
import json
import gzip
import io
from ast import literal_eval
from sshtunnel import SSHTunnelForwarder
from sqlalchemy import create_engine
import psycopg2
import boto3
from pyathena import connect
import logging
from pyathena.pandas.cursor import PandasCursor

fs = s3fs.S3FileSystem()

def split_s3_path(s3_path):
    path_parts=s3_path.replace("s3://","").split("/")
    bucket=path_parts.pop(0)
    key="/".join(path_parts)
    return bucket, key

def save_to_parquet(pandas_data_frame, bucket_path, file_name):
    """ Save dataframe to parquet file. """
    # Setting up file system as S3 (To be used with PyArrow for Parquet files)
    fs = s3fs.S3FileSystem()
    # save to parquet file
    pq.write_table(pa.Table.from_pandas(pandas_data_frame, preserve_index=False),
                   bucket_path + file_name + '.parquet', filesystem=fs, flavor='spark')


def get_parquet(read_file_path, read_schema):
    """ Read parquet file and return dataframe with required fields. """
    # get the fields from schema passed
    read_fields = list(read_schema.keys())
    # read the parquet and return as a pandas dataframe
    #return pq.read_table(read_file_path, filesystem=fs, columns=read_fields).to_pandas().astype(dtype=read_schema)
    s3_resource = boto3.resource('s3')
    bucket, key_path = split_s3_path(read_file_path)
    obj = s3_resource.Object(bucket_name=bucket, key=key_path)
    return pq.read_table(io.BytesIO(obj.get()['Body'].read()), filesystem=fs, columns=read_fields).to_pandas().astype(dtype=read_schema)

def save_to_csv(pandas_data_frame, bucket_path, file_name):
    """ write dataframe to csv file and gzip it """
    df = pandas_data_frame
    file_path = bucket_path + file_name + '.csv.gz' 
    # write the pandas dataframe to gzipped csv file
    df.to_csv(open(file_path,'w'),index = False)


def save_to_tsv(pandas_data_frame, bucket_path, file_name):
    """ write dataframe to tsv file and gzip it """
    df = pandas_data_frame
    file_path = bucket_path + file_name + '.tsv.gz' 
    # write the pandas dataframe to gzipped tsv file
    df.to_csv(open(file_path,'w'),index = False,sep = "\t")


# def save_to_avro(pandas_data_frame, bucket_path, file_name):
#     """ write dataframe to avro file """
#     df = pandas_data_frame
#     schema = pdx.schema_infer(df)
#     logging.info(schema)
#     df = df.replace({np.nan:None})
#     for field in schema.get('fields'):
#         if None in field.get('type'):
#             field['type'].remove(None)
#             field['type'].append('string')
#     logging.info(schema)
#     file_path = bucket_path + file_name + '.avro' 
#     # write the pandas dataframe to avro file
#     with open(file_path,'wb') as s3_path:
#         pdx.to_avro(s3_path, df, schema = schema)


def save_to_avro(pandas_data_frame, bucket_path, file_name):
    """ write dataframe to avro file """
    df = pandas_data_frame
    schema = pdx.schema_infer(df)
    logging.info(schema)
    df = df.replace({np.nan:None})
    for field in schema.get('fields'):
        if None in field.get('type'):
            field['type'].remove(None)
            field['type'].append('string')
    logging.info(schema)
    file_path = bucket_path + file_name + '.avro.gz'
    file_bucket, file_key =  split_s3_path(file_path)
    # write the pandas dataframe to avro file
    gz_buffer = io.BytesIO()
    with gzip.GzipFile(mode='wb', fileobj=gz_buffer) as gz_file:
        pdx.to_avro(gz_file, df, schema = schema)
    s3_resource = boto3.resource('s3')
    s3_object = s3_resource.Object(file_bucket, file_key)
    s3_object.put(Body=gz_buffer.getvalue())


def get_csv(read_file_path, read_schema):
    """ read excel file and return dataframe with required fields """
    # get the fields from schema passed
    read_fields = list(read_schema.keys())
    # read the parquet and return as a pandas dataframe, only the fields required
    return pd.read_csv(read_file_path, delimiter='\t', dtype=read_schema)[read_fields]


def get_excel(read_file_path, read_schema):
    """ read excel file and return dataframe with required fields"""
    # get the fields from schema passed
    read_fields = list(read_schema.keys())
    # read the excel and return as a pandas dataframe, only the fields required
    return pd.read_excel(read_file_path, dtype=read_schema, engine='openpyxl')[read_fields]

def read_query_postgresql(postgresql_query, postgresql_host, postgresql_port,postgresql_user,postgresql_pw,postgresql_schema):

    private_key_file = open("/tmp/ssh-tunnel-postgresql-rds.pem", "w")
    ssh_key_string = os.getenv('SSH_PRIVATE_KEY', None)
    key_file_write = private_key_file.write(ssh_key_string)
    private_key_file.close()
    tunnel = SSHTunnelForwarder(
    ('18.158.235.83'),
    ssh_username="ubuntu",
    ssh_pkey="/tmp/ssh-tunnel-postgresql-rds.pem",
    remote_bind_address=(postgresql_host, postgresql_port))
    tunnel.skip_tunnel_checkup = False
    tunnel.start()
    print("****SSH Tunnel Established****")
    tunnel.check_tunnels()
    print(tunnel.tunnel_is_up, flush=True)
    # Run sample query in the database to validate connection
    engine = create_engine('postgresql://'+ postgresql_user  +':'+ postgresql_pw+'@127.0.0.1:' + str(tunnel.local_bind_port) + '/'+ postgresql_schema+'')
    print("Engine Created")
    try:
        df_out=pd.read_sql_query(postgresql_query,con=engine)
        return df_out
    finally:
        engine.dispose()
        tunnel.stop()

def get_lime_data(bucket, file_prefix, lime_schema):
    """ get limelight created and updated file and concatenate these files
        return as a single pandas dataframe
    """
    # get the fields from schema passed
    read_fields = list(lime_schema.keys())
    # read the parquet and return as a pandas dataframe
    lime_created = read_all_from_S3_to_pd(bucket, file_prefix, read_fields, 'PARQUET')
    
    # lime_created = pq.read_table(lime_created_file, filesystem=fs, columns=read_fields).to_pandas()
    lime_created = lime_created[lime_created.order_id != 'nan']
    lime_created = lime_created.astype(dtype=lime_schema)
    return lime_created
    # return pd.concat(
    #     [get_parquet(lime_created_file, lime_schema),
    #         get_parquet(lime_updated_file, lime_schema)
    #         ], sort=False, ignore_index=True)


def set_date_types(input_dataframe, input_date_columns):
    """
    for each datefield in passed dataframe, parse as date and return dataframe
    if dateformat cannot be converted, don't throw error but return null instead
    for that value
    """
    for datefield in input_date_columns:
        input_dataframe[datefield] = pd.to_datetime(
            input_dataframe[datefield], infer_datetime_format=True, errors='coerce')  # format='%Y-%m-%d %H:%M:%S'
    return input_dataframe


def keep_unique_lime_data(dataframe):
    df = dataframe
    df.systemNotes = df.systemNotes.apply(literal_return)
    df = df.explode('systemNotes')
    df['systemNotes'] = df['systemNotes'].str.split(' - ').str[0]
    df['systemNotes'] = pd.to_datetime(df['systemNotes'], format='%B %d, %Y %H:%M %p')
    df = df.sort_values('systemNotes', ascending=True).drop_duplicates(['order_id'], keep='last')
    df = df.rename(columns={'systemNotes': 'system_notes_last_date'})
    return (df)


def literal_return(val):
    try:
        return literal_eval(val)
    except (ValueError, SyntaxError) as e:
        return val


def get_migration_parent_id(input_dataframe, run_date):
    """ parse C2 field and get value for mp parameter """
    try:
        if (pd.to_datetime(run_date, format='%Y-%m-%d') > pd.to_datetime(20181029, format='%Y%m%d')):
            input_dataframe['migration_parent'] = input_dataframe['c2'][input_dataframe['c2'].str.contains('mp=')].str.rsplit('mp=', n=1, expand=True)[
                1].str.rsplit('&', n=1, expand=True)[0]
    except:
        input_dataframe = input_dataframe.assign(migration_parent='')
    return input_dataframe['migration_parent']


def check_if_migration(input_dataframe):
    """ check if it is a migration order based on campaign id """
    return np.where(input_dataframe['campaign_id'] == '181', 1, 0)


def check_if_straight_sale(input_dataframe):
    """ check if it is a straight sale order based on campaign id """
    straight_sale_LL_campaigns = ['1224','1283','2212','2213','1674','1675','1914','1915','2227','2228']
    return np.where(input_dataframe['campaign_id'].isin(straight_sale_LL_campaigns), 1, 0)
    # return np.where(input_dataframe['campaign_id']=='1674', 1, 0)

def check_if_salvage_sale(input_dataframe, dim_subscriptions_from_view):
    """ check if it is a BIN/Cancel salvage sale order based on subscription group id """
    # get products/subscription processed data  
    subscriptions_schema = {'order_product_id': str, 'subscription_group_id': str, 'subscription_country': str}
    subscriptions = get_parquet(dim_subscriptions_from_view, subscriptions_schema)
    # remove duplicates
    subscriptions = subscriptions.sort_values('order_product_id', ascending=True).drop_duplicates(['order_product_id'], keep='last')
    subscriptions = subscriptions.rename(columns={
        'order_product_id': 'main_product_id'})  
    
    input_dataframe['main_product_id'] = input_dataframe.main_product_id.astype(str)
    # join products/subscriptions data to limelight data
    input_dataframe = input_dataframe.merge(
        subscriptions, how='left', on='main_product_id')
    cancel_salvage_subs_groups = ['3121']
    blocked_bin_salvage_subs_groups = ['3189']
    us_prepaid_bins_subs_group = ['3699']
    return np.where(input_dataframe['subscription_group_id'].isin(cancel_salvage_subs_groups), 1, np.where(
        (input_dataframe['subscription_group_id'].isin(blocked_bin_salvage_subs_groups)) | ( (input_dataframe['subscription_group_id'].isin(us_prepaid_bins_subs_group)) & (input_dataframe['subscription_country'].isin(['US'])) & (input_dataframe['time_stamp'] >= '2021-03-18 12:30:00')), 2, 0))
    # return np.where(input_dataframe['campaign_id']=='1674', 1, 0)

def get_updated_main_product_category(input_dataframe, dim_subscriptions_from_view, dim_gateways_from_view):
    """if it's a straight sale or blocked bin or cancellation salvage then category will be decided based on gateway id"""
    # load subscriptions data
    subscriptions_schema = {'subscription_gateway': str, 'product_category': str}
    subscriptions = get_parquet(dim_subscriptions_from_view, subscriptions_schema)
    subscriptions = subscriptions.sort_values('subscription_gateway', ascending=True).drop_duplicates(['subscription_gateway'], keep='last')
    subscriptions = subscriptions.rename(columns={
        'subscription_gateway': 'gateway_id', 'product_category': 'gateway_category'})
    # load gateway data as subscription gateway is internal gateway id
    gateways_schema = {'id': str, 'qlik_gateway_id': str}
    gateways = get_parquet(dim_gateways_from_view, gateways_schema)
    gateways = gateways.rename(columns={
        'id': 'gateway_id', 'qlik_gateway_id': 'order_gateway_id'})
    # join subscriptions data to gateway data
    gateways = gateways.merge(
        subscriptions, how='left', on='gateway_id')

    # gateways['order_gateway_id'] = gateways.order_gateway_id.astype(int)
    gateways['gateway_category'] = np.where(pd.isna(gateways.gateway_category),0,gateways.gateway_category).astype(int)
    # join gateway category data to limelight data
    input_dataframe = input_dataframe.merge(
        gateways, how='left', on='order_gateway_id')

    # assign gateway category instead of product category for Straight sales, Salvage sales
    return np.where( (input_dataframe.is_straight_sale == 1) | (input_dataframe.is_salvage_sale.isin([1,2])), input_dataframe.gateway_category, input_dataframe.main_product_category) 
    


def get_cross_sale_parent_id(input_dataframe, run_date):
    """ parse C2 field and get value for cs parameter """
    try:
        if pd.to_datetime(run_date, format='%Y-%m-%d') > pd.to_datetime(20181029, format='%Y%m%d'):
            input_dataframe['cross_sale_parent'] = input_dataframe['c2'][input_dataframe['c2'].str.contains('cs=')].str.rsplit('cs=', n=0, expand=True)[
                1].str.rsplit('&', n=1, expand=True)[0]
    except:
        input_dataframe = input_dataframe.assign(cross_sale_parent='')
    return input_dataframe['cross_sale_parent']


def get_cross_sale_cascade(input_dataframe):
    """ check if it is a cross sale and/or a cascade """
    # will mark here cross sale or cascade only for leads as these events initiate only on leads
    input_dataframe.sort_values(by=['email_address', 'order_id'], ascending=True, inplace=True)
    input_dataframe.reset_index(drop=True, inplace=True)

    input_dataframe['subscription_id'].values[0] = input_dataframe['ancestor_id'].values[0]

    for i in range(1, len(input_dataframe.index)):
        if not (input_dataframe.is_lead_rebill_donation.values[i] == 'Lead'):
            input_dataframe['is_cross_sale'].values[i] = 0
            input_dataframe['is_cascade'].values[i] = 0
        else:
            if (input_dataframe.email_address.values[i] == input_dataframe.email_address.values[i - 1]) & (
                    input_dataframe.cc_last_4.values[i] == input_dataframe.cc_last_4.values[i - 1]) & (
                    input_dataframe.campaign_id.values[i] == input_dataframe.campaign_id.values[i - 1]) & (
                    input_dataframe.time_stamp[i] - input_dataframe.time_stamp[i - 1] < timedelta(
                seconds=30)):
                if np.isin(input_dataframe.order_status.values[i - 1], ['2', '6', '8']):
                    input_dataframe['is_cross_sale'].values[i] = 1
                    input_dataframe['is_cascade'].values[i] = 0
                else:
                    input_dataframe['is_cascade'].values[i] = 1
                    if input_dataframe.is_cross_sale.values[i - 1] == 1:
                        input_dataframe['is_cross_sale'].values[i] = 1
                    else:
                        input_dataframe['is_cross_sale'].values[i] = 0
            else:
                input_dataframe['is_cross_sale'].values[i] = 0
                input_dataframe['is_cascade'].values[i] = 0
        
        # assign value for exit sale - upsell
        # cross sale will set as 2 for exit sales
        # exit sale is identified based on limelight campaign ids
        if np.isin(input_dataframe.campaign_id.values[i], ['1664', '1665', '1666', '1667', '1668']):
            input_dataframe['is_cross_sale'].values[i] = 2

        # These are step 2 campaigns for straight sale
        if np.isin(input_dataframe.campaign_id.values[i], ['1283', '2213', '1675', '1915', '2228']):
            input_dataframe['is_cross_sale'].values[i] = 1

        # assign for migration billing cycle 0 orders as there can be cascade attempts
        if ((input_dataframe['billing_cycle'].values[i] == 0) & (input_dataframe['campaign_id'].values[i] == 181)):
            if (input_dataframe.email_address.values[i] == input_dataframe.email_address.values[i - 1]) & (
                    input_dataframe.cc_last_4.values[i] == input_dataframe.cc_last_4.values[i - 1]) & (
                    input_dataframe.time_stamp[i] - input_dataframe.time_stamp[i - 1] < timedelta(
                seconds=30)):
                if np.isin(input_dataframe.order_status.values[i - 1], ['2', '6', '8']):
                    input_dataframe['is_cascade'].values[i] = 0
                else:
                    input_dataframe['is_cascade'].values[i] = 1
            else:
                input_dataframe['is_cascade'].values[i] = 0

        # assign values for cascade attempts, cascade_first_parent_order_id, cascade_parent_order
        if (input_dataframe['is_cascade'].values[i] == 1):
            if (input_dataframe['is_cascade'].values[i - 1] == 0):
                input_dataframe['cascade_attempt'].values[i - 1] = 1
                input_dataframe['cascade_attempt'].values[i] = 2
                input_dataframe['cascade_parent_order_id'].values[i] = input_dataframe['order_id'].values[i - 1]
                input_dataframe['cascade_first_parent_order_id'].values[i] = input_dataframe['order_id'].values[i - 1]
            else:
                input_dataframe['cascade_attempt'].values[i] = input_dataframe['cascade_attempt'].values[i - 1] + 1
                input_dataframe['cascade_parent_order_id'].values[i] = input_dataframe['order_id'].values[i - 1]
                input_dataframe['cascade_first_parent_order_id'].values[i] = \
                input_dataframe['cascade_first_parent_order_id'].values[i - 1]
        else:
            input_dataframe['cascade_attempt'].values[i] = 0

        # set values for subscription_id
        # identify same lead attempt but with different credit card
        same_lead_attempt = (input_dataframe.is_lead_rebill_donation.values[i] == 'Lead') & (
                    input_dataframe['campaign_id'].values[i] == input_dataframe['campaign_id'].values[i - 1]) & (
                                    input_dataframe['sub_affiliate'].values[i] ==
                                    input_dataframe['sub_affiliate'].values[i - 1]) & (
                                        input_dataframe['billing_cycle'].values[i] ==
                                        input_dataframe['billing_cycle'].values[i - 1]) & (
                                    input_dataframe['order_status'].values[i - 1] == 7) & (
                                        input_dataframe['email_address'].values[i] ==
                                        input_dataframe['email_address'].values[i - 1])

        if (input_dataframe['is_cascade'].values[i] == 1):
            input_dataframe['subscription_id'].values[i] = input_dataframe['subscription_id'].values[i - 1]
        elif (same_lead_attempt):
            input_dataframe['subscription_id'].values[i] = input_dataframe['subscription_id'].values[i - 1]
        elif ((input_dataframe['campaign_id'].values[i] == 181) & (input_dataframe['billing_cycle'].values[i] == 0) & (
                input_dataframe['email_address'].values[i] == input_dataframe['email_address'].values[i - 1])):
            input_dataframe['subscription_id'].values[i] = input_dataframe['subscription_id'].values[i - 1]
        else:
            input_dataframe['subscription_id'].values[i] = input_dataframe['ancestor_id'].values[i]

    return input_dataframe


# def check_if_cross_sale(input_dataframe):
#     """ check if it is a cross sale order based on cross sale parent order id """
#     return np.where( (input_dataframe['cross_sale_parent_order_id'] == '') | (pd.isna(input_dataframe['cross_sale_parent_order_id'])), 0, 1)


def get_reserve_release_date(input_dataframe):
    """ get reserve release date by adding 6 months to order date """
    return (input_dataframe['time_stamp'].dt.date + pd.DateOffset(months=6)).values.astype('datetime64[M]')


def check_if_test(input_dataframe):
    """ identify test transactions based on certain values
         emails, billing countries, campaigns, credit cards
    """
    return np.where(
        input_dataframe['email_address'].str.contains(
            '@fent.dk|@epayconsult.com|@burlingtonadfactory.com|@inmeli.com|@myservicepartneruae.com|@test.com')
        | np.isin(input_dataframe['billing_country'],
                  ['AE', 'AX', 'PK', 'BR', 'CR', 'DO', 'AM', 'DZ', 'AO', 'AR', 'AW', 'AZ', 'BD'])
        | np.isin(input_dataframe['campaign_id'], ['349', '414', '187'])
        | np.isin(input_dataframe['cc_first_6'] + input_dataframe['cc_last_4'],
                  ['5521911972','5177344061','4907630022','4891159562','5262055930','4833233346','5265671906','5247323036',
                  '5132099793','4920669503','4581906336','5280211910','4571121234','4439136085','4439131686','4987786527',
                  '5177349292','4572685658','4093073398','5121607447','4571001006']
                ), 1, 0)


def id_int_validation(foo):
    """ convert values to int, if cannot then return None.
        it's useful when you only want to keep numbers, e.g. an empty string, or a string like 'hello'.
    """
    try:
        return int(foo)
    except ValueError:
        return None


def prepare_cake_data(cake_data, cake_fields_rename):
    """ This function takes cake data as input as prepares it for further processing."""
    cake_data = cake_data.rename(columns=cake_fields_rename)
    cake_data['order_id'] = cake_data['order_id'].apply(id_int_validation)
    cake_data = cake_data.dropna(subset=['order_id'], how='any')
    cake_data = cake_data.astype({'order_id': 'int'})
    # cake_data['cake_date'] = pd.to_datetime(cake_data.cake_date.str[6:19], unit='ms').dt.date.astype('str').str.replace('-', '').astype('int')
    # cake_data['cake_currency_exchange_id'] = get_affiliate_cost_currency(cake_data.cake_affiliate_id) + cake_data.cake_date.astype('str')
    cake_data = cake_data[['order_id', 'cost_per_lead', 'cake_affiliate_id']]
    return cake_data


def get_affiliate_id(input_dataframe):
    """Get Affiliate ID """
    return np.where(pd.notna(input_dataframe.cake_affiliate_id), input_dataframe.cake_affiliate_id,
                    input_dataframe.affiliate_id)


def get_affiliate_cost_currency(affiliate_id):
    """ for Javandi (Entertainment and Lifestyle both), CPA is paid in EUR, the value in CAKE is EUR, for others it's paid in USD"""
    return np.where(affiliate_id.isin(['30', '78']), 'EUR', 'USD')


def get_currency_exchange_id(input_data_frame):
    """Return currency exchange ID """
    return input_data_frame['order_currency'] + input_data_frame['order_date'].astype('str')


def get_lime_in_eur(input_dataframe, column):
    """Return value in EUR"""
    return input_dataframe[column] * input_dataframe['in_eur']


def get_hold_date(input_dataframe):
    """Return hold date"""
    return pd.to_datetime(
        np.where(input_dataframe['on_hold_by'] != 'Lime Light CRM', input_dataframe['hold_date'], None))


def get_mdr_emp_wdl(input_dataframe):
    """Return MDR for EMP-WDL"""
    return np.where((input_dataframe['time_stamp'] >= pd.to_datetime('20190401', format='%Y%m%d'))
                    & (input_dataframe.bank_name == 'EMP - WDL') & (input_dataframe.billing_continent_name != 'EU'),
                    input_dataframe.gateway_processing_percent + 0.15, input_dataframe.gateway_processing_percent)


def check_if_lead_rebill_donation(input_dataframe):
    """Check if order is a lead or a rebill or a donation."""
    return np.where(input_dataframe.main_product_category == '3', 'Donation',
                    np.where((input_dataframe.billing_cycle == '0') & (input_dataframe.campaign_id != '181') & (
                            input_dataframe.main_product_is_rebill != '1'), 'Lead',
                             'Rebill'))


def get_reserve_taken_amount(input_dataframe):
    """reserve taken is 10% for all banks except emp-ecp which also add transaction fees and mdr into order amount"""
    return 0.1 * (
            input_dataframe.order_total_eur - np.where(input_dataframe.bank_name == 'EMP - ECP',
                                                       input_dataframe.gateway_transaction_fee +
                                                       (input_dataframe.order_total_eur *
                                                        input_dataframe.gateway_processing_percent / 100), 0
                                                       )
    )


def get_order_cc_key(input_dataframe):
    """Return unique order look up key based on Credit card + Order date + Gateway descriptor + Order amount """
    return input_dataframe.cc_first_6 + '/' + input_dataframe.cc_last_4 + '/' + input_dataframe.order_date.astype('str') \
           + '/' + input_dataframe.gateway_descriptor + '/' + \
           np.floor(input_dataframe.order_total).astype('int').astype('str')


def get_subscription_key(input_dataframe):
    """This needs to be revisited, Returns unique subscription key based on Email Address + Brand ID."""
    return input_dataframe.main_product_subscription_group_id.astype('str') + '/' + input_dataframe.email_address


def get_cust_unique_key(input_dataframe):
    """This needs to be revisited, Returns unique customer key based on Email Address + Campaign."""
    return input_dataframe.campaign_id.astype('str') + '/' + input_dataframe.email_address


def get_vat_amount_eur(input_dataframe):
    """Returns VAT amount in EUR"""
    return np.where(input_dataframe.billing_continent_name != 'EU', 0, np.where(
        (input_dataframe.vat_date != '') & (
                pd.to_datetime(input_dataframe.vat_date, format='%Y-%m-%d') <= pd.to_datetime(
            input_dataframe.order_date, format='%Y%m%d')), input_dataframe.order_total_eur - (
                input_dataframe.order_total_eur / ((input_dataframe.vat_percent + 100) * .01)
        ), 0)
                    )
    # return input_dataframe['vat_amount_eur']
    # return input_dataframe.order_total_eur - (input_dataframe.order_total_eur / ((np.where(pd.to_datetime(input_dataframe.order_date, format='%Y%m%d') < pd.to_datetime(input_dataframe.vat_date, format='%Y-%m-%d'),
    #                                                                                        0, np.where(input_dataframe.billing_continent_name != 'EU', 0, input_dataframe.vat_percent))+100)*.01))


def get_country_cpa():
    # rather than using a normal dictionary, dict class is defined and a missing method is created to handle countries where cpa is not found in dictionary
    # can also use dict.get(key, [value]), which has a default value
    class Dict(dict):
        def __missing__(self, key):
            return 0

    country_cpa = Dict(DK=60, SE=70, NO=70, FI=70, FR=50, NL=30)
    return country_cpa


def get_cost_per_lead(input_dataframe):
    # There are 3 steps to calculate cpa-
    #   1. Data after 20181101, using values from cake
    #   2. Data after 20181101 but not found a matching order in cake, then use step 2 logic to get values
    #   3. Data before 20181101, using cost from affiliate cost file for ENTERTAINMENT and based on country/fixed price for period for LIFESTYLE
    #   4. Data after 20200318, there could be orders which are not passed to cake so they may have real 0 CPA
    # Cost data in afifliate cost file is only available for Entertainment affiliates, for lifestyle country based cost is used.

    # cpa is only applicable for approved leads
    cpa_condition = (input_dataframe.order_status.isin(['2', '6', '8'])) & (
            input_dataframe.is_lead_rebill_donation == 'Lead') 
    cpa_missing_condition = (cpa_condition) & (pd.isna(input_dataframe.cost_per_lead)) & (pd.isna(input_dataframe.everflow_cost_per_lead)) & (
            input_dataframe.is_cross_sale == 0) & (
            pd.to_datetime(input_dataframe.order_date, format='%Y%m%d') >= pd.to_datetime('20181101',
                                                                                          format='%Y%m%d'))
    input_dataframe['is_cpa_missing'] = np.where(cpa_missing_condition, 1, 0)
    cake_everflow_no_event_condition = (cpa_condition) & (pd.isna(input_dataframe.cost_per_lead)) & (pd.isna(input_dataframe.everflow_cost_per_lead)) & (
            pd.to_datetime(input_dataframe.order_date, format='%Y%m%d') >= pd.to_datetime('20200317',
                                                                                          format='%Y%m%d'))
    # Assign cost for entertainment orders
    input_dataframe['cost_per_lead'] = np.where(cake_everflow_no_event_condition,
                                            0,
                                            np.where(cpa_condition,
                                                np.where(pd.to_datetime(input_dataframe.order_date,
                                                                        format='%Y%m%d') >= pd.to_datetime('20181101',
                                                                                                           format='%Y%m%d'),
                                                         np.where(pd.notna(input_dataframe.cost_per_lead),
                                                                  input_dataframe.cost_per_lead,
                                                                  np.where(pd.notna(input_dataframe.everflow_cost_per_lead),
                                                                    input_dataframe.everflow_cost_per_lead,
                                                                    np.where(input_dataframe.is_cross_sale == 1,
                                                                           0,
                                                                           input_dataframe.cost_per_lead_affiliate_file
                                                                           )
                                                                    )
                                                                  ),
                                                         np.where(input_dataframe.is_cross_sale == 1,
                                                                    0,
                                                                    input_dataframe.cost_per_lead_affiliate_file
                                                                )
                                                         ),
                                                0
                                                )
    )

    # Assign cost for lifestyle orders
    # There have been changes in CPA at different periods, these conditions are written to assign CPA correctly based on order date
    # if order_date <=20180228, then a cross sell (step 2) costs 35 and normal lead (step 1) costs 45
    # if 20180228 > order_date <= 20180715, then a cross sell (step 2) costs 0 and normal lead (step 1) costs 60
    # if order_date > 20180715, then a cross sell (step 2) costs 0 and normal lead (step 1) cost varies based on billing country
    input_dataframe['cost_per_lead'] = np.where(cake_everflow_no_event_condition,
                                            0,
                                            np.where(cpa_condition,
                                                np.where(pd.isna(input_dataframe.cost_per_lead),
                                                         np.where(pd.notna(input_dataframe.everflow_cost_per_lead),
                                                             input_dataframe.everflow_cost_per_lead,
                                                             np.where(
                                                                 pd.to_datetime(input_dataframe.order_date,
                                                                                format='%Y%m%d') <= pd.to_datetime(
                                                                     '20180228', format='%Y%m%d'),
                                                                 # IF IT IS STEP 2, 35, else 45
                                                                 np.where(input_dataframe.is_cross_sale == 1, 35, 45),
                                                                 np.where(pd.to_datetime(input_dataframe.order_date,
                                                                                         format='%Y%m%d') <= pd.to_datetime(
                                                                     '20180715', format='%Y%m%d'),
                                                                          # IF IT IS STEP 2, 0, else 60
                                                                          np.where(input_dataframe.is_cross_sale == 1, 0,
                                                                                   60),
                                                                          np.where(input_dataframe.is_cross_sale == 1, 0,
                                                                                   input_dataframe.billing_country.map(
                                                                                       get_country_cpa()))
                                                                          )
                                                             )
                                                         ),
                                                         input_dataframe.cost_per_lead
                                                         ),
                                                0
                                                )
    )

    return input_dataframe


def get_cost_per_lead_from_file(affiliate_cost_file, run_date):
    affiliate_cost = pd.read_excel(affiliate_cost_file, engine='openpyxl')

    affiliate_cost_fields_rename = ohs.affiliate_cost_file_fields_rename
    affiliate_cost_schema = ohs.affiliate_cost_file_schema

    # make a key to identify unique record based on affiliate ID and country
    affiliate_cost['affiliate_cost_key'] = affiliate_cost["Affiliate ID"].astype(str) + '|' + affiliate_cost["Country"]
    # rename Date column to start date
    affiliate_cost = affiliate_cost.rename(columns={'Date': 'start_date'})

    # sort values based on affiliate cost key and start date and reset index
    affiliate_cost = affiliate_cost.sort_values(by=['affiliate_cost_key', 'start_date'], axis=0,
                                                ascending=True).reset_index()

    # create end date field and assign it value for today (we generate start date and end date for cost)
    affiliate_cost['end_date'] = pd.to_datetime(pd.Timestamp.today(), format='%Y%m%d')

    # run a loop through each record and assign correct values for end date
    for i in range(len(affiliate_cost.index)):
        if i == len(affiliate_cost.index) - 1:
            0
        elif affiliate_cost.affiliate_cost_key.values[i] == affiliate_cost.affiliate_cost_key.values[i + 1]:
            affiliate_cost['end_date'].values[i] = pd.to_datetime(affiliate_cost.start_date.values[i + 1],
                                                                  format='%Y%m%d')
        else:
            0
    #
    # affiliate_cost['end_date'] = pd.to_datetime(affiliate_cost['end_date'])

    # calendar_min_date = min(affiliate_cost.start_date)
    # calendar_max_date = max(affiliate_cost.start_date)

    calendar_min_date = pd.to_datetime(run_date, format='%Y-%m-%d')
    calendar_max_date = pd.to_datetime(run_date, format='%Y-%m-%d')

    date_range = pd.date_range(start=calendar_min_date, end=calendar_max_date)
    date_range = pd.DataFrame(date_range)

    # give column a name
    date_range = date_range.rename(columns={0: 'calendar_date'})

    # create a new column in both data frames with same value to help with cross join
    date_range = date_range.assign(key=0)
    affiliate_cost = affiliate_cost.assign(key=0)

    # cross join date range dataframe to affiliate cost
    affiliate_cost = affiliate_cost.merge(date_range, how='outer', on='key')

    # keep only records where calendar date is between start date and end date
    affiliate_cost = affiliate_cost[(affiliate_cost.calendar_date < affiliate_cost.end_date) & (
            affiliate_cost.calendar_date >= affiliate_cost.start_date)]

    # keep only required columns
    affiliate_cost = affiliate_cost[['Affiliate ID', 'Country', 'calendar_date', 'Cost']]

    # change calendar date to numeric format
    affiliate_cost['calendar_date'] = affiliate_cost.calendar_date.dt.date.astype('str').str.replace('-', '').astype(
        'int')

    # rename columns
    affiliate_cost = affiliate_cost.rename(columns=affiliate_cost_fields_rename)

    # set schema
    affiliate_cost = affiliate_cost.astype(dtype=affiliate_cost_schema)

    # drop duplicates if any
    affiliate_cost = affiliate_cost.drop_duplicates()

    return affiliate_cost


def get_cost_per_lead_eur(input_dataframe, input_currency_dataframe):
    """Get CPA in EUR."""
    # this date should be changed from order_date to cake/everflow date
    input_dataframe[
        'cake_everflow_currency_exchange_id'] = input_dataframe.cost_per_lead_currency + input_dataframe.order_date.astype('str')
    input_currency_dataframe_cake = input_currency_dataframe.rename(columns={'in_eur': 'in_eur_cake'})
    input_dataframe = input_dataframe.merge(input_currency_dataframe_cake, how='left',
                                            left_on='cake_everflow_currency_exchange_id', right_on='currency_exchange_id')
    return input_dataframe['cost_per_lead'] * input_dataframe['in_eur_cake']


def map_flag_str_to_int(input_field):
    return np.where(input_field.isin(['y', 'yes']), 1, np.where(input_field.isin(['n', 'no']), 0, input_field))


def set_data_types(input_dataframe, input_data_schema, input_id_fields, input_flag_fields, input_category_fields):
    """
    for each field in passed dataframe, set data types as passed in data schema
    """
    input_fields = list(input_data_schema.keys())

    for field in input_id_fields:
        input_dataframe[field] = np.where(
            (input_dataframe[field] == '') | pd.isna(input_dataframe[field]) | (input_dataframe[field] == 'None') | (
                        input_dataframe[field] == 'nan'), 0,
            input_dataframe[field]).astype(input_data_schema[field])

    for field in input_flag_fields:
        input_dataframe[field] = np.where((input_dataframe[field] == '') | pd.isna(input_dataframe[field]), 0,
                                          input_dataframe[field]).astype(input_data_schema[field])

    for field in input_category_fields:
        input_dataframe[field] = np.where((input_dataframe[field] == '') | pd.isna(input_dataframe[field]), 9999,
                                          input_dataframe[field]).astype(input_data_schema[field])

        ## if empty string, safely convert to float
    input_float_fields = dict()
    for (key, value) in input_data_schema.items():
        # Check if key is even then add pair to new dictionary
        if value == float:
            input_float_fields[key] = value

    for field in input_float_fields:
        input_dataframe[field] = pd.to_numeric(input_dataframe[field])

    for field in input_fields:
        input_dataframe[field] = input_dataframe[field].astype(input_data_schema[field])

    return input_dataframe

def get_bank_deductions(lime_data):
    df = lime_data
    bank_query = '''
    select  bank_name  as bank_name, billing_continent as billing_continent_name , currency  as order_currency , 
    processing_percent_mdr  as gateway_processing_percent, transaction_fee  as gateway_transaction_fee, cb_fee_bank as gateway_charge_back_fee,
    refund_fee as refund_fee
    from  routing_user_ui.bank_deduction
    '''
    df_bank = read_query_postgresql(postgresql_query=bank_query, postgresql_host='routing.cbhq1y6krhnt.eu-central-1.rds.amazonaws.com', postgresql_port=5432,postgresql_user='router',postgresql_pw=os.getenv('ROUTING_DB_PW', None),postgresql_schema='routing_app_data')
    df_merge = pd.merge(df, df_bank, left_on=['bank_name','billing_continent_name','order_currency'], right_on=['bank_name','billing_continent_name','order_currency'] , suffixes=('_left','_right'),how='left')
    df_merge.drop('gateway_processing_percent_left', axis=1, inplace=True)
    df_merge.drop('gateway_transaction_fee_left', axis=1, inplace=True)
    df_merge.drop('gateway_charge_back_fee_left', axis=1, inplace=True)
    df_merge.rename(columns={'gateway_processing_percent_right':'gateway_processing_percent',
                          'gateway_transaction_fee_right':'gateway_transaction_fee',
                          'gateway_charge_back_fee_right':'gateway_charge_back_fee',
                          'refund_fee_right':'refund_fee'
                          }, 
                 inplace=True)
    nan_rows_gpc = df_merge[df_merge['gateway_processing_percent'].isnull()]
    nan_rows_gtf  = df_merge[df_merge['gateway_transaction_fee'].isnull()]
    nan_gcbf = df_merge[df_merge['gateway_charge_back_fee'].isnull()]
    if ( (nan_rows_gpc.shape[0]!=0) or (nan_rows_gtf.shape[0]!=0) or (nan_gcbf.shape[0]!=0)):
        print('bad file found')
        client = boto3.client('sns' , region_name ='eu-west-1')

        for idx, row in nan_rows_gpc.iterrows():
            var_send = str(row['bank_name'])+','+str(row['billing_continent_name'])+','+str(row['order_currency'])
            print('miss match '+var_send)
    
    
    return (df_merge)

def get_everflow_data(run_date):
    run_date = (pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime('%Y-%m-%d')
    s3_staging_dir = 's3://munduexport-datalake/mundu/query_staging_location/'
    region_name = 'eu-central-1'
    cursor = connect(s3_staging_dir, region_name, cursor_class=PandasCursor).cursor()
    everflow_query = '''
            select
              adv1 as order_id,  
              date(year||'-'||month||'-'||day) as everflow_date,
              currency_id as everflow_currency,
              "payout" as everflow_cost_per_lead 
            from "everflow"."conversions"
            where year='{}' and month||day>='{}' '''.format(run_date[:4], run_date[5:7] + run_date[8:10])

    return cursor.execute(everflow_query).as_pandas().astype(ohs.everflow_schema)

def read_all_from_S3_to_pd(bucket_to_read, file_prefix, datacols, source_file_extention):
    """
    Reads all parquet in a bucket. 
    iterate over given bucket and key prefix and load all the files from that bucket
    fields and fields which are to be passed as dates.
    """
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_to_read)
    prefix_objs = bucket.objects.filter(Prefix=file_prefix)
    fs = s3fs.S3FileSystem()

    if source_file_extention == 'PARQUET':
        dfs = []
        for obj in prefix_objs:
            if obj.key.endswith('.parquet'):
                print('hey!, I found parquet file {} and I am processing it ...'.format(obj.key))
                df=pq.read_table(io.BytesIO(obj.get()['Body'].read()), filesystem=fs, columns=datacols).to_pandas()
                print('hey!, {} is processed successfully!'.format(obj.key))
                dfs.append(df)

        data_from_pq_to_pd = pd.concat(dfs, sort=False, ignore_index=True)
        print('woohoo!, data is ready!'.format(obj.key))
        # data_from_pq_to_pd = pd.concat( \
        #                 [pq.read_table(io.BytesIO(obj.get()['Body'].read()), filesystem=fs, columns=datacols).to_pandas() \
        #                 for obj in prefix_objs if obj.key.endswith('.parquet')], sort=False, ignore_index=True)
    else:
        pass
    
    return data_from_pq_to_pd