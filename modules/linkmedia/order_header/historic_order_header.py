#!/usr/bin/env python
# coding: utf-8
import os 
import sys
sys.path.append("/opt/lmrepo/data-transforms/")

from linkmedia.order_header import order_header_stage_1_controller as oh_1_c
from linkmedia.order_header import order_header_stage_2_controller as oh_2_c
import pandas as pd



def get_data(run_date, number_of_days_ahead,file_formats):
    # start date and number of days to go back
    
    start_date = run_date

    # create an instance of order header stage 1 controller
    run_order_header_1 = oh_1_c.order_header()
    # create an instance of order header stage 2 controller
    run_order_header_2 = oh_2_c.order_header()

    # loop for required days and generate order_header stage 1
    for i in list(range(0, number_of_days_ahead)):
        run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
                    pd.DateOffset(i)).strftime('%Y-%m-%d')
        run_order_header_1.generate_order_header(run_date)
        run_order_header_2.generate_order_header(run_date,file_formats)
        print(str(run_date))
get_data('2020-10-28' , 10, ['PARQUET','TSV'])