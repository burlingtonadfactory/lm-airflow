#!/usr/bin/env python
# coding: utf-8

from linkmedia.lib import read_store as rs
from linkmedia.order_header import order_header_functions as ohf
from linkmedia.order_header import order_header_schema as ohs
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open.s3 import iter_bucket as s3_iter_bucket
from smart_open import open
from datetime import datetime
from pandas.io.json import json_normalize
import json
import io
import logging
fs = s3fs.S3FileSystem()

class order_header:     
    def __init__(self):
        self

    # generate order header for the date that is passed
    def generate_order_header(self, run_date):
        logging.info('Starting Stage 1 Order Header Load')
        # Path to save/load data
        bucket = 'datalake-process'
        frankfurt_bucket = 'linkmedia-datalake'
        import_bucket = 'datalake-imports'
        date_path = run_date.replace('-', '/')
        bucket_path = 's3://'+bucket+'/'
        frankfurt_bucket_path = 's3://' + frankfurt_bucket + '/'

        s3_staging_dir = 's3://munduexport-datalake/mundu/query_staging_location/'
        region_name = 'eu-central-1'

        # get paths for all the files needed to be loaded
        lime_created_file = bucket_path + 'limelight/created/' + \
            date_path + '/' + run_date + '_lime_created.parquet'
        lime_updated_file = bucket_path + 'limelight/updated/' + \
            date_path + '/' + run_date + '_lime_updated.parquet'
        # is from Athena
        dim_gateways_from_view = bucket_path + 'mundu/dim_gateways_from_view/dim_gateways_from_view.parquet'
        # is from Athena
        dim_company_vat_from_view = bucket_path+'mundu/dim_company_vat_from_view/dim_company_vat_from_view.parquet'
        bank_file = bucket_path+'mundu/bank/bank.parquet'
        cake_file = bucket_path+'cake/conversions/' + \
            date_path+'/'+run_date+'_cake.parquet'
        currency_file = bucket_path+'currency_exchange/' + \
            date_path+'/'+run_date+'_currency_exchange.parquet'
        affiliate_file = bucket_path+'cake/affiliates/affiliates.parquet'
        country_continent_file = 's3://'+import_bucket + \
            '/misc/qlik/BIN Database/Country-Continent.xlsx'
        gateway_fees_file = bucket_path+'misc/gateway_fees_qlik_export.txt'
        #  is from Athena
        dim_subscriptions_from_view = bucket_path + 'mundu/dim_subscriptions_from_view/dim_subscriptions_from_view.parquet'
        vat_rates_file = bucket_path+'misc/VAT Rates_country.txt'
        # order_header_file_dir = bucket_path+'order_header/' + date_path+'/'
        order_header_stage_1_file_dir = frankfurt_bucket_path + 'order_header_stage_1/' + date_path+'/'
        affiliate_cost_file = 's3://datalake-imports/misc/qlik/Affiliates/Affiliates ID - names.xlsx'

        # get schema for all the tables/files needed
        lime_schema = ohs.lime_schema
        gateway_processed_schema = ohs.gateway_processed_schema
        vat_processed_schema = ohs.vat_processed_schema
        bank_schema = ohs.bank_schema
        cake_schema = ohs.cake_schema
        currency_schema = ohs.currency_schema
        affiliate_schema = ohs.affiliate_schema
        country_continent_schema = ohs.country_continent_schema
        # gateway_fees_schema = ohs.gateway_fees_schema
        subscription_processed_schema = ohs.subscription_processed_schema
        vat_rates_schema = ohs.vat_rates_schema
        order_header_schema = ohs.order_header_schema

        # load the renaming required
        lime_fields_rename = ohs.lime_fields_rename
        cake_fields_rename = ohs.cake_fields_rename
        gateway_processed_fields_rename = ohs.gateway_processed_fields_rename
        # gateway_fees_fields_rename = ohs.gateway_fees_fields_rename
        bank_fields_rename = ohs.bank_fields_rename
        country_continent_fields_rename = ohs.country_continent_fields_rename
        subscription_processed_fields_rename = ohs.subscription_processed_fields_rename
        vat_rates_fields_rename = ohs.vat_rates_fields_rename

        # get the fields which are to be parsed as specific data types
        lime_data_schema_dates = ohs.lime_data_schema_dates
        order_header_schema_dates = ohs.order_header_schema_dates
        order_header_schema_id_fields = ohs.order_header_schema_id_fields
        order_header_schema_flag_fields = ohs.order_header_schema_flag_fields
        order_header_schema_category_fields = ohs.order_header_schema_category_fields

        # Read lime created and updated parquets and set schema
        logging.info('Reading lime created and updated parquets and set schema')
        lime_data = ohf.get_lime_data('datalake-process', 'limelight/created/'+date_path, lime_schema)
        # rename lime fields to required names
        logging.info('Renaming fields')
        lime_data = lime_data.rename(columns=lime_fields_rename)

        # keep only last record
        logging.info('Keeping unique (last) records')
        lime_data = ohf.keep_unique_lime_data(lime_data)

        # Parse the dates
        logging.info('Parsing dates')
        lime_data = ohf.set_date_types(lime_data, lime_data_schema_dates)

        # reserve release date
        logging.info('Calculating Reserve Release date')
        lime_data['reserve_release_date'] = ohf.get_reserve_release_date(lime_data)

        # create order date
        logging.info('Assign Order Date')
        lime_data['order_date'] = lime_data.time_stamp.dt.date.astype('str').str.replace('-', '').astype('int')

        # Processing Lime data, create a column to identify live test transactions (live test are transactions which actually go through gateway and are processed but are done for testing by us)
        logging.info('Check ')
        lime_data['is_test_order'] = ohf.check_if_test(lime_data)
        logging.info('Lime data processed!')

        # Read gateway processed parquet
        logging.info('Reading gateway processed parquet and renaming fields')
        gateway_data = ohf.get_parquet(dim_gateways_from_view, gateway_processed_schema)
        # gateway_data = rs.get_athena_data('mundu_replica.dim_gateways_v', s3_staging_dir, region_name)
        # gateway_data = gateway_data[list(gateway_processed_schema.keys())]
        # gateway_data = gateway_data.astype(dtype=gateway_processed_schema)
        # rename gateway fields to required names
        gateway_data = gateway_data.rename(columns=gateway_processed_fields_rename)

        # load bank data
        logging.info('Reading bank file and renaming fields')
        bank_data = ohf.get_parquet(bank_file, bank_schema)
        # rename bank fields to required names
        bank_data = bank_data.rename(columns=bank_fields_rename)

        # merge bank data to gateway data
        logging.info('Merging bank data on gateway data')
        gateway_data = gateway_data.merge(bank_data, how='left', on='bank_id')

        # load company details and vat details for company
        logging.info('Reading company vat data')
        company_data = ohf.get_parquet(dim_company_vat_from_view, vat_processed_schema)
        # company_data = rs.get_athena_data('mundu_replica.dim_company_vat_v', s3_staging_dir, region_name)
        # company_data = company_data[list(vat_processed_schema.keys())]
        # company_data = company_data.astype(dtype=vat_processed_schema)
        # join company details to gateway table
        logging.info('Merging vat data on gateway data')
        gateway_company = gateway_data.merge(
            company_data, how='left', on='order_gateway_id')

        # merge gateway data to limelight data
        logging.info('Merging gateway+company data on limelight data')
        lime_data = lime_data.merge(
            gateway_company, how='left', on='order_gateway_id')

        # create currency exchange ID
        logging.info('Creating currency exchange ID')
        lime_data['currency_exchange_id'] = ohf.get_currency_exchange_id(lime_data)

        # Read currency exchange parquet
        logging.info('Reading currency exchange ID')
        currency_data = ohf.get_parquet(currency_file, currency_schema)

        # merge currency exchange data to limelight
        logging.info('Merging currency exchange data to limelight')
        lime_data = lime_data.merge(currency_data, how='left', on='currency_exchange_id')

        # Convert Amounts to EUR values
        logging.info('Converting amounts to Euros')
        lime_data['order_total_eur'] = ohf.get_lime_in_eur(lime_data, 'order_total')
        lime_data['refund_amount_eur'] = ohf.get_lime_in_eur(lime_data, 'refund_amount')

        # get products/subscription processed data
        logging.info('Getting subsciptions processed data')
        subscriptions = ohf.get_parquet(dim_subscriptions_from_view, subscription_processed_schema)
        # There are duplicate records as one product could be assigned to multiple shuffle groups
        # in lifestyle, it is assigned to Lifestyle and Straight Sale - KETO
        # we remove these duplicates.
        subscriptions = subscriptions.sort_values('order_product_id', ascending=True).drop_duplicates(['order_product_id'], keep='last')
        # subscriptions = rs.get_athena_data('mundu_replica.dim_subscriptions_v', s3_staging_dir, region_name)
        # subscriptions = subscriptions[list(subscription_processed_schema.keys())]
        # subscriptions = subscriptions.astype(dtype=subscription_processed_schema)
        # rename subscription fields and give them prefixed names to avoid name conflicts
        subscriptions = subscriptions.rename(
            columns=subscription_processed_fields_rename)

        # join products/subscriptions data to limelight data
        logging.info('Merging subscriptions on limelight data')
        lime_data = lime_data.merge(
            subscriptions, how='left', on='main_product_id')

        # get lead/rebill/donation flag
        logging.info('Lead/Rebill/Donation')
        lime_data['is_lead_rebill_donation'] = ohf.check_if_lead_rebill_donation(
            lime_data)


        logging.info('Dropping main_product_is_rebill column')

        lime_data.drop(columns='main_product_is_rebill', inplace=True)

        order_header_stage_1_schema_dates = [
            'acquisition_date',
            'hold_date',
            'time_stamp',
            'recurring_date',
            'refund_date',
            'retry_date',
            'shipping_date',
            'void_date',
            'order_confirmed_date',
            'reserve_release_date',
            'gateway_first_live_date',
            'vat_date'
        ]
        # Parse the dates
        logging.info('Parse dates')
        lime_data = ohf.set_date_types(lime_data, order_header_stage_1_schema_dates)

        # save to parquet file
        logging.info('Saving to Parquet file')
        ohf.save_to_parquet(lime_data, order_header_stage_1_file_dir, run_date+'_order_header_stage_1')
        logging.info('Saved to Parquet file')

        #drop data to free up memory
        lime_data = pd.DataFrame()

