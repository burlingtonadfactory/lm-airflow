#!/usr/bin/env python
# coding: utf-8

from linkmedia.lib import read_store as rs
from linkmedia.order_header import order_header_functions as ohf
from linkmedia.order_header import order_header_schema as ohs
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open.s3 import iter_bucket as s3_iter_bucket
from smart_open import open
from datetime import datetime
from pandas.io.json import json_normalize
import json
import io
import logging


fs = s3fs.S3FileSystem()
from pyathena import connect
from pyathena.pandas.cursor import PandasCursor


class order_header:
    def __init__(self):
        self

    # generate order header for the date that is passed
    def generate_order_header(self, run_date, file_formats=['PARQUET','TSV']):
        logging.info('Starting Stage 2 Order Header Load')
        # Path to save/load data
        bucket = 'datalake-process'
        frankfurt_bucket = 'linkmedia-datalake'
        import_bucket = 'datalake-imports'
        date_path = run_date.replace('-', '/')
        data_start_date = '2016-09-01'
        bucket_path = 's3://' + bucket + '/'
        frankfurt_bucket_path = 's3://' + frankfurt_bucket + '/'

        s3_staging_dir = 's3://munduexport-datalake/mundu/query_staging_location/'
        region_name = 'eu-central-1'

        # get paths for all the files needed to be loaded
        lime_created_file = bucket_path + 'limelight/created/' + \
                            date_path + '/' + run_date + '_lime_created.parquet'
        lime_updated_file = bucket_path + 'limelight/updated/' + \
                            date_path + '/' + run_date + '_lime_updated.parquet'
        # Will be taken from Athena
        # gateway_processed_file = bucket_path + 'mundu/gateways_processed/gateways_processed.parquet'
        # will be taken from Athena
        # vat_processed_file = bucket_path+'mundu/vat_processed/vat_processed.parquet'
        bank_file = bucket_path + 'mundu/bank/bank.parquet'
        cake_file = bucket_path + 'cake/conversions/' + \
                    date_path + '/' + run_date + '_cake.parquet'
        currency_file = bucket_path + 'currency_exchange/' + \
                        date_path + '/' + run_date + '_currency_exchange.parquet'
        affiliate_file = bucket_path + 'cake/affiliates/affiliates.parquet'
        country_continent_file = 's3://' + import_bucket + \
                                 '/misc/qlik/BIN Database/Country-Continent.xlsx'
        # gateway_fees_file = bucket_path+'misc/gateway_fees_qlik_export.txt'
        #  is from Athena
        dim_gateways_from_view = bucket_path + 'mundu/dim_gateways_from_view/dim_gateways_from_view.parquet'
        dim_subscriptions_from_view = bucket_path + 'mundu/dim_subscriptions_from_view/dim_subscriptions_from_view.parquet'
        vat_rates_file = bucket_path + 'misc/VAT Rates_country.txt'
        #order_header_file_dir = bucket_path + 'order_header/' + date_path + '/'
        order_header_file_dir = frankfurt_bucket_path + 'order_header/' + date_path + '/'
        order_header_tsv_file_dir = frankfurt_bucket_path + 'order_header_datawarehouse/' + date_path + '/'
        affiliate_cost_file = 's3://datalake-imports/misc/qlik/Affiliates/Affiliates ID - names.xlsx'

        # get schema for all the tables/files needed
        lime_schema = ohs.lime_schema
        gateway_processed_schema = ohs.gateway_processed_schema
        vat_processed_schema = ohs.vat_processed_schema
        bank_schema = ohs.bank_schema
        cake_schema = ohs.cake_schema
        currency_schema = ohs.currency_schema
        affiliate_schema = ohs.affiliate_schema
        country_continent_schema = ohs.country_continent_schema
        # gateway_fees_schema = ohs.gateway_fees_schema
        gateway_lime_fees_schema = ohs.gateway_lime_fees_schema
        gateway_lime_fees_fields = list(gateway_lime_fees_schema.keys())
        subscription_processed_schema = ohs.subscription_processed_schema
        vat_rates_schema = ohs.vat_rates_schema
        order_header_schema = ohs.order_header_schema

        # load the renaming required
        lime_fields_rename = ohs.lime_fields_rename
        cake_fields_rename = ohs.cake_fields_rename
        gateway_processed_fields_rename = ohs.gateway_processed_fields_rename
        # gateway_fees_fields_rename = ohs.gateway_fees_fields_rename
        gateway_lime_fees_fields_rename = ohs.gateway_lime_fees_fields_rename
        bank_fields_rename = ohs.bank_fields_rename
        country_continent_fields_rename = ohs.country_continent_fields_rename
        subscription_processed_fields_rename = ohs.subscription_processed_fields_rename
        vat_rates_fields_rename = ohs.vat_rates_fields_rename

        # get the fields which are to be parsed as specific data types 
        lime_data_schema_dates = ohs.lime_data_schema_dates
        order_header_schema_dates = ohs.order_header_schema_dates
        order_header_schema_id_fields = ohs.order_header_schema_id_fields
        order_header_schema_flag_fields = ohs.order_header_schema_flag_fields
        order_header_schema_category_fields = ohs.order_header_schema_category_fields

        order_header_stage_1_file = bucket_path + 'order_header_stage_1/' + date_path + '/' + run_date + '_order_header_stage_1.parquet'
        # order header for run date - 1 as a cascade could start at a previous day partition and finish in next day partition (if at midnight)
        run_date_prev_day = (pd.to_datetime(run_date, format='%Y-%m-%d') + pd.DateOffset(-1)).strftime(
            format='%Y-%m-%d')
        run_date_prev_day_path = run_date_prev_day.replace('-', '/')
        order_header_stage_1_file_prev_day = bucket_path + 'order_header_stage_1/' + run_date_prev_day_path + '/' + run_date_prev_day + '_order_header_stage_1.parquet'

        # Read order header stage 1 data files for current and previous day
        logging.info('Reading Order_Header Stage_1 file')
        lime_data = pd.read_parquet(order_header_stage_1_file)
        lime_data = lime_data.assign(is_run_date_data=1)
        if run_date == data_start_date:
            lime_data_prev_day = pd.DataFrame()
            lime_data_prev_day = lime_data_prev_day.assign(email_address='')
        else:
            logging.info('Reading Order_Header Stage_1 file for Previous Day')
            lime_data_prev_day = pd.read_parquet(order_header_stage_1_file_prev_day)
        lime_data_prev_day = lime_data_prev_day.assign(is_run_date_data=0)

        # keep only email addresses from previous day which are also in run date order_header
        logging.info('Filtering prev day file with only emails present in both days')
        lime_data_prev_day_filtered = lime_data_prev_day.merge(lime_data.email_address.drop_duplicates(),
                                                               how='inner', on='email_address')

        # concatenate both the order_header stage 1 data
        logging.info('Concatenating both prev day and curr day files')
        lime_data = pd.concat([lime_data, lime_data_prev_day_filtered], sort=False, ignore_index=True)
        # empty non-required dataframe
        lime_data_prev_day = pd.DataFrame()
        lime_data_prev_day_filtered = pd.DataFrame()

        # Cascade/cross sale orders
        # lime_data = ohf.get_cascade(lime_data, run_date)
        logging.info('Defaulting cascade / cross sale parameters')
        lime_data = lime_data.assign(is_cascade=0)
        lime_data = lime_data.assign(cascade_parent_order_id='')
        lime_data = lime_data.assign(cascade_first_parent_order_id='')
        lime_data = lime_data.assign(cascade_attempt=0)
        lime_data = lime_data.assign(is_cross_sale=0)
        lime_data = lime_data.assign(subscription_id=0)
        
        logging.info('Getting cross sale parent id')
        lime_data = lime_data.assign(cross_sale_parent='')
        lime_data['cross_sale_parent_order_id'] = ohf.get_cross_sale_parent_id(lime_data, run_date)
        # lime_data['is_cross_sale'] = ohf.check_if_cross_sale(lime_data)
        logging.info('Getting cross sale & cascade values')
        lime_data = ohf.get_cross_sale_cascade(lime_data)

        # Migration orders
        logging.info('Getting migration parent id')
        lime_data = lime_data.assign(migration_parent='')
        lime_data['migration_parent_order_id'] = ohf.get_migration_parent_id(lime_data, run_date)
        logging.info('Checking if migration')
        lime_data['is_migration'] = ohf.check_if_migration(lime_data)
        
        ## keep only current day data and drop is_run_date_data columns as not needed anymore
        logging.info('Keeping only curr day data')
        lime_data = lime_data[lime_data.is_run_date_data == 1]
        lime_data.drop(columns='is_run_date_data', inplace=True)
        
        # Straight sale
        logging.info('Checking if straight sale')
        lime_data['is_straight_sale'] = ohf.check_if_straight_sale(lime_data)

        # Salvage sale
        logging.info('Checking if salvage sale')
        lime_data['is_salvage_sale'] = ohf.check_if_salvage_sale(lime_data, dim_subscriptions_from_view)

        # update main_product_category for Straight Sales, Salvage sales
        # these products are by default assigned ENT category but they should get correct category
        # based on where that order is going
        logging.info('Getting main product category')
        lime_data['main_product_category'] = ohf.get_updated_main_product_category(lime_data, dim_subscriptions_from_view, dim_gateways_from_view)

        # Cross sale orders

        # load step 1/2 data
        # it is commented as of now but we need to look into where to insert step1/2 logic
        # step1_2 = pq.read_table(bucket_path+'order_flags/step1_2/'+date_path+'/'+run_date+'_step1_2.parquet', filesystem=fs, columns = ['order_id', 'is_step_1_2']).to_pandas()

        # join step1/2 data to limelight data
        # commented for step1/2
        # lime_data = lime_data.merge(step1_2, how='left', on='order_id')

        # check if trial is over
        # here every new order will be assgined value 0 then through trial_over module, correct value will be assigned
        # as trial will be over after a few days, checking for it on order day will always be false.
        # giving a default value here is helpful for schema consistency as trial_over needs to run only once a day and for current day it won't run.
        lime_data['is_trial_over'] = 0

        # Read cake parquet
        # consider loading today and yesterday's cake file as some orders could possibly be across different day buckets, based on when load runs.
        logging.info('Reading cake data')
        cake_data = ohf.get_parquet(cake_file, cake_schema)

        # prepare cake data for further operations
        logging.info('Preparing cake data')
        cake_data = ohf.prepare_cake_data(cake_data, cake_fields_rename)

        # join cake data to limelight data
        logging.info('Merging cake data on limelight data')
        lime_data = lime_data.merge(cake_data, how='left', on='order_id')

        # get affiliate id and currency in which affiliate is paid
        logging.info('Getting affiliate id and affiliate cost currency')
        lime_data['affiliate_id'] = ohf.get_affiliate_id(lime_data)
        lime_data['cost_per_lead_currency'] = ohf.get_affiliate_cost_currency(lime_data.affiliate_id)

        # get affiliate cost from file, for historical and missing cpa orders
        logging.info('Getting affiliate cost from file')
        affiliate_cost_from_file = ohf.get_cost_per_lead_from_file(affiliate_cost_file, run_date)

        # join affiliate cost from file to limelight orders
        logging.info('Merging affiliate cost on lime data')
        lime_data = lime_data.merge(affiliate_cost_from_file, how='left',
                                    on=['affiliate_id', 'billing_country', 'order_date'])

        # drop duplicates if any
        # check and verify, as this should not be needed to be done, make sure to remove it after verification
        # lime_data = lime_data.drop_duplicates()

        # get everflow data from Athena
        logging.info('Getting everflow data')
        everflow_data = ohf.get_everflow_data(run_date)
        # join everflow data to limelight data
        logging.info('Merging everflow data on limelight')
        lime_data = lime_data.astype({'order_id': str})
        lime_data = lime_data.merge(everflow_data, how='left', on='order_id')
        lime_data = lime_data.astype({'order_id': int})
        # this should be reviewed to see how it affects rebills, does a rebill for cake has cpa data and everflow not? should work consistently across both platforms
        lime_data['cost_per_lead_currency'] = np.where(pd.notna(lime_data.everflow_cost_per_lead), lime_data.everflow_currency, lime_data.cost_per_lead_currency)

        # get cost per lead
        logging.info('Get cost per lead')
        lime_data = ohf.get_cost_per_lead(lime_data)

        # Read currency exchange parquet ## this file has been read in stage 1 already but again needed here, so loaded here.
        logging.info('Read currency file')
        currency_data = ohf.get_parquet(currency_file, currency_schema)

        logging.info('Getting cost per lead in eur')
        lime_data['cost_per_lead_eur'] = ohf.get_cost_per_lead_eur(lime_data, currency_data)

        # drop missing cpa column, cake_affiliate_id as they are utilized and not needed anymore
        logging.info('Dropping unwanted columns')
        lime_data = lime_data.drop(columns=['cost_per_lead_affiliate_file', 'cake_affiliate_id', 'currency_exchange_id',
                                            'cake_everflow_currency_exchange_id', 'in_eur'])

        # calculate hold date based on conditions, 
        # orders which are cancelled
        logging.info('Getting hold date')
        lime_data['hold_date_derived'] = ohf.get_hold_date(lime_data)

        # affiliate table, we get affiliate name from this table
        logging.info('Reading affiliate file')
        affiliates = ohf.get_parquet(affiliate_file, affiliate_schema)

        # merging affiliate names to limelight
        logging.info('Merging affiliates to limelight')
        lime_data = lime_data.merge(affiliates, how='left', on='affiliate_id')

        # country-continent mapping table
        # country continent mapping is needed to identify if extra processing fees will be applicable.
        # EMP - WDL charges 0.15% extra fees for non-european orders.
        logging.info('Getting country continent data')
        country_continent = ohf.get_excel(
            country_continent_file, country_continent_schema)
        country_continent = country_continent.rename(
            columns=country_continent_fields_rename)
        # drop duplicate values if any
        logging.info('Dropping duplicates from country continent file')

        country_continent = country_continent.drop_duplicates(['billing_country'], keep='first')

        # merging continent names to limelight
        logging.info('Merging country continent on lime data')

        lime_data = lime_data.merge(
            country_continent, how='left', on='billing_country')

        # gateway fees table
        # gateway_fees = ohf.get_csv(gateway_fees_file, gateway_fees_schema)
        logging.info('Reading gateway fees file')
        #gateway_lime_fees = rs.read_all_parquet_from_S3_to_pd(bucket, 'limelight/created_gateways/'+date_path, gateway_lime_fees_fields)
        gateway_lime_fees = ohf.read_all_from_S3_to_pd(bucket, 'limelight/created_gateways/'+date_path, gateway_lime_fees_fields, 'PARQUET')
        gateway_lime_fees = gateway_lime_fees.astype(gateway_lime_fees_schema)
        # This condition should be temporary and removed and data for gateway fees should be made sure ot not be duplicate
        # gateway_fees = gateway_fees.drop_duplicates(['gateway_order_id'], keep='first')
        logging.info('Dropping duplicates from gateway fees')
        gateway_lime_fees = gateway_lime_fees.drop_duplicates(['order_id'], keep='first')
        # rename gateway fees fields to required names
        # gateway_fees = gateway_fees.rename(columns=gateway_fees_fields_rename)
        logging.info('Renaming fields')
        gateway_lime_fees = gateway_lime_fees.rename(columns=gateway_lime_fees_fields_rename)

        # merge gateway fees with limelight data
        logging.info('Merging gateway fees on limelight data')
        lime_data = lime_data.merge(gateway_lime_fees, how='left', on='order_id')

        # calculating gateway processing percentage for EMP-WDL
        # EMP-WDL charges an extra percentage if it is a non-european order
        logging.info('Getting gateway processing percent')
        lime_data['gateway_processing_percent'] = ohf.get_mdr_emp_wdl(lime_data)
        
        # for gateway 9, there should not be any transaction fee as order doesn't get processed
        logging.info('Getting gateway transaction fee')
        lime_data['gateway_transaction_fee'] = np.where(lime_data.order_gateway_id=='9',0,lime_data.gateway_transaction_fee)

        # get reserve taken amount in eur
        logging.info('Reserve taken amount in eur')
        lime_data['reserve_taken_amount_eur'] = ohf.get_reserve_taken_amount(
            lime_data)

        # create an order lookup key which is based on credit card and order date details etc
        logging.info('Getting order cc key')
        lime_data['order_cc_key'] = ohf.get_order_cc_key(lime_data)

        # check if reserve is released (reserve release happens after 6 months of order), here assign 0 and will be calculated in reserve release module
        logging.info('Defaulting reserve released to 0. Will be overridden')
        lime_data['is_reserve_released'] = 0

        # subscription key will not work for donations as brand is same but donations do not rebill
        # (using Ancestor ID could work, I will try later)
        # brand never changes on a subscription and key will be same for further billing cycles, even for cascades and migrations
        # customer lead key will not work for cross sales but we only want one approval from incoming lead, however we cannot go for only email as customer might have previous subscriptions/attempts, also we cannot go for brand as cross sale will have different brand
        # and it will reduce lead U %, adding campaign id is safer than these 2 approaches, but customer could have previous approved attempts on same campaign which could give us a hit even if current attempt failed but we are ignoring this as of now as its a rare case
        logging.info('Getting Cust Subscription Key')
        lime_data['cust_subscription_key'] = ohf.get_subscription_key(lime_data)
        logging.info('Getting Cust Unique Key')
        lime_data['cust_lead_u_key'] = ohf.get_cust_unique_key(lime_data)

        # load vat percentange rates from a file maintained in dropbox
        logging.info('Reading vat rates file')
        vat_rates = ohf.get_csv(vat_rates_file, vat_rates_schema)
        vat_rates = vat_rates.rename(columns=vat_rates_fields_rename)

        # join vat rates to limelight data
        logging.info('Getting Cust Subscription Key')
        lime_data = lime_data.merge(vat_rates, how='left', on='billing_country')

        logging.info('Getting Vat Amount in eur')
        lime_data['vat_amount_eur'] = ohf.get_vat_amount_eur(lime_data)

        # First rebill will be checked in daily run script, assigning value 0 here, we can remove it from here later.
        # lime_data['has_first_rebill'] = ohf.check_if_first_rebill(lime_data)
        logging.info('Defaulting has_first_rebill to 0. Will be overridden')
        lime_data['has_first_rebill'] = 0

        # map void flag values to int values
        logging.info('Getting is void flag')
        lime_data['is_void'] = ohf.map_flag_str_to_int(lime_data.is_void)

        # counter field to count records
        logging.info('Assigning counter = 1')
        lime_data = lime_data.assign(counter=1)

        # set data types for order header to be stored
        logging.info('Setting data types for order header to be stored')
        lime_data = ohf.set_data_types(lime_data, order_header_schema, order_header_schema_id_fields,
                                       order_header_schema_flag_fields, order_header_schema_category_fields)

        # vat dates are having issue in conversion using infer datetime, so manually converting it
        # lime_data['vat_date'] = pd.to_datetime(lime_data['vat_date'], format='%Y%m%d', errors='coerce')
        # Parse the dates
        logging.info('Parsing dates')
        lime_data = ohf.set_date_types(lime_data, order_header_schema_dates)
        lime_data = ohf.get_bank_deductions(lime_data)
        for file_format in file_formats:
            if file_format == 'PARQUET':
                # save to parquet file
                logging.info('Saving to parquet file')
                ohf.save_to_parquet(lime_data, order_header_file_dir, run_date + '_order_header')
                logging.info('Saved to Parquet file')
            elif file_format == 'TSV':
                logging.info('Saving to TSV file')
                ohf.save_to_tsv(lime_data, order_header_tsv_file_dir, run_date + '_order_header')
                logging.info('Saved to TSV file')

        # drop data to free up memory
        lime_data = pd.DataFrame()
