lime_data_schema_dates = [
    'acquisition_date',
    'chargeback_date', 
    'hold_date',
    'order_confirmed_date',
    'recurring_date',
    'refund_date',
    'retry_date',
    'shipping_date',
    'void_date',
    'time_stamp',
    'system_notes_last_date'
]

lime_schema = {
    'acquisition_date':str,
    'affiliate' :str,
    'amount_refunded_to_date':float ,
    'ancestor_id' :str,
    'billing_city' :str,
    'billing_country' :str,
    'billing_cycle' :str,
    'billing_first_name' :str,
    'billing_last_name' :str,
    'billing_postcode' :str,
    'billing_state' :str,
    'billing_street_address' :str,
    'campaign_id' :str,
    'c1':str,
    'c2':str,
    'c3':str,
    'cc_expires' :str,
    'cc_first_6' :str,
    'cc_last_4' :str,
    'cc_type' :str,
    'chargeback_date':str, 
    'current_rebill_discount_percent' :str,
    'customer_id' :str,
    'customers_telephone' :str,
    'decline_reason' :str,
    'decline_salvage_discount_percent' :str,
    'email_address' :str,
    'gateway_id' :str,
    'hold_date':str,
    'ip_address' :str,
    'is_recurring' :str,
    'is_test_cc' :str,
    'is_void' :str,
    'main_product_id' :str,
    'main_product_quantity' :str,
    'on_hold' :str,
    'on_hold_by' :str,
    'order_confirmed' :str,
    'time_stamp':str,
    'order_id' :int,
    'order_status' :str,
    'order_total' :float,
    'parent_id' :str,
    'rebill_discount_percent' :str,
    'recurring_date':str,
    'refund_amount':float ,
    'refund_date':str,
    'retry_date':str,
    'shipping_date':str,
    'sid' :str,
    'transaction_id' :str,
    'upsell_product_id' :str,
    'upsell_product_quantity' :str,
    'void_amount':float ,
    'void_date':str,
    'is_create_order':str,
    'order_confirmed_date':str,
    'systemNotes':str
}

lime_fields_rename = {
    'affiliate' : 'affiliate_id',
    'sid' : 'sub_affiliate',
    'gateway_id':'order_gateway_id'
}

gateway_processed_schema = {
    'qlik_gateway_id': str,   # order_gateway_id
    'first_time_live_date':str, 
    'descriptor':str, 
    'currency':str, 
    'is_3ds': str,
    'brand':str, 
    'bank':str
}

gateway_processed_fields_rename = {
    'bank':'bank_id',
    'brand':'brand_id',
    'currency':'order_currency',
    'descriptor':'gateway_descriptor',
    'first_time_live_date':'gateway_first_live_date',
    'is_3ds': 'is_gateway_3ds',
    'qlik_gateway_id': 'order_gateway_id'
}

vat_processed_schema = {
    'order_gateway_id':str,
    'company_name':str, 
    'vat_number':str, 
    'vat_date':str
}

bank_schema = {
    'id':str,
    'name':str
}

bank_fields_rename = {
    'id':'bank_id',
    'name':'bank_name'
}

cake_schema = {
    'transaction_id':str, 
    'paid.amount':float, 
    'event_conversion_date':str,
    'source_affiliate.source_affiliate_id':str
}

cake_fields_rename = {
    'transaction_id': 'order_id', 
    'paid.amount': 'cost_per_lead', 
    'event_conversion_date':'cake_date', 
    'source_affiliate.source_affiliate_id':'cake_affiliate_id'
}

everflow_schema = {
    'order_id':str,
    'everflow_cost_per_lead':float,
    'everflow_date':str,
    'everflow_currency':str
}

currency_schema = {
    'currency_exchange_id':str,
    'in_eur':float
}

affiliate_schema = {
    'affiliate_id':str,
    'affiliate_name':str
}

country_continent_schema = {
    'CountryCode':str,
    'Continent':str
}

country_continent_fields_rename = {
    'CountryCode':'billing_country', 
    'Continent':'billing_continent_name'
}

# gateway_fees_schema = {
#     'gateway_order_id': str,
#     'gateway_processing_percent': float,
#     'gateway_reserve_percent': float,
#     'gateway_transaction_fee': float,
#     'gateway_charge_back_fee': float
# }

gateway_lime_fees_schema = {
    'order_id':int, 
    'chargeback_fee':float, 
    'processing_percent':float, 
    'reserve_percent':float, 
    'transaction_fee':float
}

gateway_lime_fees_fields_rename = {
    'chargeback_fee':       'gateway_charge_back_fee',
    'processing_percent':   'gateway_processing_percent',
    'reserve_percent':      'gateway_reserve_percent',
    'transaction_fee':      'gateway_transaction_fee'
}

# gateway_fees_fields_rename = {
#     'gateway_order_id': 'order_gateway_id'
# }

subscription_processed_schema = {
    'order_product_id':str, 
    'product_name':str, 
    'product_category':str,
    'subscription_initial_days':str,
    'product_is_rebill': str,
    'subscription_group_id':str
}

subscription_processed_fields_rename = { 
    'order_product_id':'main_product_id',
    'product_name':'main_product_name', 
    'product_category':'main_product_category',
    'subscription_initial_days':'trial_days',
    'product_is_rebill':'main_product_is_rebill',
    'subscription_group_id':'main_product_subscription_group_id'
}

vat_rates_schema = {
    'Country Code':str, 
    'E-Services':float
}

vat_rates_fields_rename = {
    'E-Services':'vat_percent', 
    'Country Code':'billing_country'
}

order_header_schema = {
    'affiliate_id':	 int,
    'amount_refunded_to_date':	 float,
    'ancestor_id':	 int,
    'billing_city':	 str,
    'billing_country':	 str,
    'billing_cycle':	 int,
    'billing_first_name':	 str,
    'billing_last_name':	 str,
    'billing_postcode':	 str,
    'billing_state':	 str,
    'billing_street_address':	 str,
    'campaign_id':	 int,
    'c1':	 str,
    'c2':	 str,
    'c3':	 str,
    'cc_expires':	 str,
    'cc_first_6':	 str,
    'cc_last_4':	 str,
    'cc_type':	 str,
    'current_rebill_discount_percent':	 float,
    'customer_id':	 int,  
    'customers_telephone':	 str,
    'decline_reason':	 str,
    'decline_salvage_discount_percent':	 float,
    'email_address':	 str,
    'ip_address':	 str,
    'is_recurring':	 int,
    'is_test_cc':	 int,
    'is_void':	 int,
    'main_product_id':	 int,
    'main_product_quantity':	 float,
    'on_hold':	 int,
    'on_hold_by':	 str,
    'order_confirmed':	 str,
    'order_id':	 int,
    'order_status':	 int,
    'order_total':	 float,
    'parent_id':	 int,
    'rebill_discount_percent':	 float,
    'refund_amount':	 float,
    'sub_affiliate':	 str,
    'transaction_id':	 str,
    'upsell_product_id':	 str,
    'upsell_product_quantity':	 float,
    'void_amount':	 float,
    'is_create_order':	 int,
    'cascade_parent_order_id':	 str,
    'is_cascade':	 int,
    'cascade_attempt': int,
    'cascade_first_parent_order_id': str,
    'migration_parent_order_id':	 str,
    'is_migration':	 int,
    'cross_sale_parent_order_id':	 str,
    'is_cross_sale':	 int,
    'subscription_id':	 int,
    'order_date':	 int,
    'is_test_order':	 int,
    'order_gateway_id':	 int,
    'gateway_descriptor':	 str,
    'order_currency':	 str,
    'is_gateway_3ds':	 int,
    'brand_id':	 int,
    'bank_id':	 int,
    'company_name':	 str,
    'vat_number':	 str,
    'bank_name':	 str,
    'cost_per_lead':	 float,
    'order_total_eur':	 float,
    'refund_amount_eur':	 float,
    'cost_per_lead_eur':	 float,
    'affiliate_name':	 str,
    'billing_continent_name':	 str,
    'gateway_processing_percent':	 float,
    'gateway_reserve_percent':	 float,
    'gateway_transaction_fee':	 float,
    'gateway_charge_back_fee':	 float,
    'main_product_name':	 str,
    'main_product_category':	 int,
    'is_lead_rebill_donation':	 str,
    'is_trial_over':	 int,
    'reserve_taken_amount_eur':	 float,
    'order_cc_key':	 str,
    'is_reserve_released':	 int,
    'cust_subscription_key':	 str,
    'cust_lead_u_key':	 str,
    'vat_percent':	 float,
    'vat_amount_eur':	 float,
    'is_cpa_missing':	 int,
    # added later
    'cost_per_lead_currency':	str,
    'has_first_rebill':	int,
    'trial_days':	float,  #trial days are set to be float as for rebills trial days will be null, setting 0 wouldn't be a good option and int type cannot have null
    'counter':  int ,
    'is_straight_sale': int,
    'is_salvage_sale': int,
    'main_product_subscription_group_id': int
}

order_header_schema_dates = [
    'acquisition_date',
    'chargeback_date',
    'hold_date',
    'time_stamp',
    'recurring_date',
    'refund_date',
    'retry_date',
    'shipping_date',
    'void_date',
    'order_confirmed_date',
    'reserve_release_date',
    'gateway_first_live_date',
    'vat_date',
    'hold_date_derived',
    'system_notes_last_date'
]

order_header_schema_id_fields = [
## These are id fields, where values are null these values will be set to 0 (which will be considered as an id for missing values)
## not setting 999999 as in future, an order could have that kind of ID and it could create an issue with order/ancestor/parent ids 
## 0 should be safe as there wouldn't be any valid id with 0
    'affiliate_id',
    'ancestor_id',
    'campaign_id',
    'customer_id',
    'order_gateway_id',
    'main_product_id',
    'order_id',
    'parent_id',
    'brand_id',
    'bank_id',
    'main_product_category',
    'subscription_id',
    'main_product_subscription_group_id'
]

order_header_schema_flag_fields = [
## These are flag fields, where values are null these values will be set to 0 (it will be considered as if missing values then that flag is false)
    'is_recurring',
    'is_test_cc',
    'is_void',
    'on_hold',
    'is_create_order',
    'is_cascade',
    'cascade_attempt',
    'is_migration',
    'is_cross_sale',
    'is_test_order',
    'is_gateway_3ds',
    'is_trial_over',
    'is_reserve_released',
    'is_cpa_missing',
    'has_first_rebill',
    'counter',
    'is_straight_sale',
    'is_salvage_sale'
]

order_header_schema_category_fields = [
## These are category fields, where values are null these values will be set to 9999 (which will be considered as an id for missing values)
## These cannot be set to 0, as we do have a billing cycle 0 order and could have an order status 0 (a possiblity)
    'billing_cycle',
    'order_status'
]

affiliate_cost_file_schema = {
    'affiliate_id':str, 
    'billing_country':str, 
    'order_date':int, 
    'cost_per_lead_affiliate_file':float
}

affiliate_cost_file_fields_rename = {
   'Affiliate ID':'affiliate_id', 
   'Country':'billing_country', 
   'calendar_date':'order_date', 
   'Cost':'cost_per_lead_affiliate_file'
}