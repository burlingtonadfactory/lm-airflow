import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open.s3 import iter_bucket as s3_iter_bucket
from smart_open import open
from datetime import datetime
from dateutil.relativedelta import relativedelta
from pandas.io.json import json_normalize
import json
import io
fs = s3fs.S3FileSystem()
import csv
import logging

# local packages
from linkmedia.lib import read_store as rs


def get_checkout_chargebacks_v1_csv(load_bucket, load_prefix, save_file_path, checkout_schema_v1):
    # Checkout CB v1 (before sep 2019)
    checkout_fields_v1 = list(checkout_schema_v1.keys())
    print('reading files')
    checkout_v1 = rs.read_all_csv_from_S3_to_pd(load_bucket, load_prefix + 'Checkout/Before_sep_2019/', checkout_schema_v1, checkout_fields_v1)
    print('done reading files')

    save_path = save_file_path + 'checkout/version_1/'
    rs.save_to_parquet(checkout_v1, save_path, 'checkout_cb_v1')
    # print('trying to dump to parquet ....')
    # checkout_v1.to_parquet(save_path+'checkout_cb_v1.parquet', engine='pyarrow')
    return print('dumped checkout v1 csv files to parquet')


def get_checkout_chargebacks_v2_csv(csv_bucket, csv_prefix, save_file_path, start_month, checkout_cb_schema_v2):
    # Checkout CB v2 (after sep 2019)
    checkout_cb_fields_v2 = list(checkout_cb_schema_v2.keys())

    # generate list of months to read from start month to current month
    month_to_read_list = []
    #
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(csv_bucket)
    current_month = int(pd.to_datetime('today').strftime(format='%Y%m'))
    for i in list(range(0, 1000000)):
        month = pd.to_datetime(pd.to_datetime(start_month, format='%Y%m') + relativedelta(months=i)).strftime(
            format='%Y%m')
        if int(month) <= current_month:
            # check if data folder is created for month or not, for new months, sometimes it could take a few days
            # before folder for that month is created
            obj_prefix = csv_prefix + 'Checkout/From_sep_2019/' + month + '/'
            if (len(list(bucket.objects.filter(Prefix=obj_prefix))) > 0):
                month_to_read_list.append(month)
        else:
            break

    # for each month read file and store
    for month_to_read in month_to_read_list:
        print(month_to_read)
        print('reading files')
        checkout_v2 = rs.read_all_csv_from_S3_to_pd(csv_bucket,
                                                 csv_prefix + 'Checkout/From_sep_2019/' + month_to_read + '/',
                                                 checkout_cb_schema_v2,
                                                 checkout_cb_fields_v2)
        print('done reading files')
        save_path = save_file_path + 'checkout/version_2/' + month_to_read + '/'
        rs.save_to_parquet(checkout_v2, save_path, 'checkout_cb_v2')
        # checkout_v2.to_parquet(save_path+'checkout_cb_v2.parquet', engine='pyarrow')

    return print('dumped checkout v2 csv files to parquet')


def get_paysafe_chargebacks_csv(load_bucket, load_prefix, save_file_path, paysafe_cb_schema):
    ## save without month
    paysafe_fields = list(paysafe_cb_schema.keys())
    cb_path = load_prefix+'Paysafe/Chargebacks/Re Exports with ARN #/'
    cb_rev_path = load_prefix+'Paysafe/Reversals/'
    print('reading files')

    ## chargebacks
    paysafe_cb = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, paysafe_cb_schema,
                                            paysafe_fields)
    print('done reading files')

    save_path = save_file_path + 'paysafe/chargebacks/'
    rs.save_to_parquet(paysafe_cb, save_path, 'paysafe_cb')
    # paysafe_cb.to_parquet(save_path+'paysafe_cb.parquet', engine='pyarrow')

    # reversals
    paysafe_cb_rev = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_rev_path, paysafe_cb_schema,
                                            paysafe_fields)
    print('done reading files')

    save_path = save_file_path + 'paysafe/reversals/'
    rs.save_to_parquet(paysafe_cb_rev, save_path, 'paysafe_cb_rev')
    # paysafe_cb_rev.to_parquet(save_path+'paysafe_cb_rev.parquet', engine='pyarrow')
    return print('dumped paysafe csv files to parquet')


def get_emp_chargebacks_csv(load_bucket, load_prefix, save_file_path, emp_cb_schema):
    ## save without month
    emp_fields = list(emp_cb_schema.keys())
    cb_path = load_prefix + 'EMP/Chargebacks/'
    print('reading files')
    emp_cb = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, emp_cb_schema, emp_fields)
    print('done reading files')

    save_path = save_file_path + 'emp/chargebacks/'
    rs.save_to_parquet(emp_cb, save_path, 'emp_cb')
    # emp_cb.to_parquet(save_path+'emp_cb.parquet', engine='pyarrow')
    return print('dumped emp csv files to parquet')


def get_emp_wdl_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_wdl_manual_rev_schema):
    ## save without month
    emp_wdl_fields = list(emp_wdl_manual_rev_schema.keys())
    cb_path = load_prefix + 'EMP/EMP_manual_representments/WDL/'
    print('reading files')
    emp_wdl_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, emp_wdl_manual_rev_schema, emp_wdl_fields)
    print('done reading files')

    save_path = save_file_path + 'emp/manual_reversals/wdl/'
    rs.save_to_parquet(emp_wdl_cb, save_path, 'emp_wdl_cb_manual_rev')
    # emp_wdl_cb.to_parquet(save_path+'emp_wdl_cb_manual_rev.parquet', engine='pyarrow')
    return print('dumped emp wdl manual reversal csv files to parquet')


def get_emp_six_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_six_manual_rev_schema):
    ## save without month
    emp_six_fields = list(emp_six_manual_rev_schema.keys())
    cb_path = load_prefix + 'EMP/EMP_manual_representments/SIX/'
    print('reading files')
    emp_six_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, emp_six_manual_rev_schema, emp_six_fields)
    print('done reading files')

    save_path = save_file_path + 'emp/manual_reversals/six/'
    rs.save_to_parquet(emp_six_cb, save_path, 'emp_six_cb_manual_rev')
    # emp_six_cb.to_parquet(save_path+'emp_six_cb_manual_rev.parquet', engine='pyarrow')
    return print('dumped emp six manual reversal files to parquet')


def get_emp_bns_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_bns_manual_rev_schema):
    ## save without month
    emp_bns_fields = list(emp_bns_manual_rev_schema.keys())
    cb_path = load_prefix + 'EMP/EMP_manual_representments/B&S/'
    print('reading files')
    emp_bns_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, emp_bns_manual_rev_schema, emp_bns_fields)
    print('done reading files')

    save_path = save_file_path + 'emp/manual_reversals/b&s/'
    rs.save_to_parquet(emp_bns_cb, save_path, 'emp_b&s_cb_manual_rev')
    # emp_bns_cb.to_parquet(save_path+'emp_b&s_cb_manual_re.parquet', engine='pyarrow')
    return print('dumped emp b&s manual reversal csv files to parquet')


def get_epro_chargebacks_csv(load_bucket, load_prefix, save_file_path, epro_cb_schema):
    ## save without month
    epro_fields = list(epro_cb_schema.keys())
    cb_path = load_prefix + 'EPRO/'
    print('reading files')
    epro_cb = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, epro_cb_schema, epro_fields)
    print('done reading files')

    save_path = save_file_path + 'epro/'
    rs.save_to_parquet(epro_cb, save_path, 'epro_cb')
    # epro_cb.to_parquet(save_path+'epro_cb.parquet', engine='pyarrow')
    return print('dumped epro csv files to parquet')


def get_billpro_chargebacks_csv(load_bucket, load_prefix, save_file_path, billpro_cb_schema):
    ## save without month
    billpro_fields = list(billpro_cb_schema.keys())
    cb_path = load_prefix + 'Billpro/'
    print('reading files')
    billpro_cb = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, billpro_cb_schema, billpro_fields)
    print('done reading files')

    save_path = save_file_path+'billpro/'
    rs.save_to_parquet(billpro_cb, save_path, 'billpro_cb')
    # billpro_cb.to_parquet(save_path+'billpro_cb.parquet', engine='pyarrow')
    return print('dumped billpro csv files to parquet')


def get_payvision_chargebacks_v1_csv(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v1):
    ## save without month
    payvision_v1_fields = list(payvision_cb_schema_v1.keys())
    cb_path = load_prefix + 'Payvision/Before_apr_2019/'
    print('reading files')
    # epro_cb = read_all_csv_from_S3_to_pd(load_bucket, 'misc/qlik/CHARGEBACKS/EPRO/', epro_cb_schema, epro_fields)
    payvision_cb_v1 = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, payvision_cb_schema_v1, payvision_v1_fields)
    print('done reading files')

    save_path = save_file_path + 'payvision/version_1/'
    rs.save_to_parquet(payvision_cb_v1, save_path, 'payvision_cb_v1')
    # payvision_cb_v1.to_parquet(save_path+'payvision_cb_v1.parquet', engine='pyarrow')
    return print('dumped payvision v1 csv files to parquet')


def get_payvision_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v2):
    ## save without month
    payvision_v2_fields = list(payvision_cb_schema_v2.keys())
    cb_path = load_prefix + 'Payvision/From_apr_2019/'
    print('reading files')
    payvision_cb_v2 = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, payvision_cb_schema_v2, payvision_v2_fields)
    print('done reading files')

    save_path = save_file_path + 'payvision/version_2/'
    rs.save_to_parquet(payvision_cb_v2, save_path, 'payvision_cb_v2')
    # payvision_cb_v2.to_parquet(save_path+'payvision_cb_v2.parquet', engine='pyarrow')
    return print('dumped payvision v2 csv files to parquet')


def get_vp_frick_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_frick_cb_schema):
    ## save without month
    vp_frick_fields = list(vp_frick_cb_schema.keys())
    cb_path = load_prefix + 'VP/VP-FRICK/'
    print('reading files')
    vp_frick_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, vp_frick_cb_schema, vp_frick_fields)
    print('done reading files')

    save_path = save_file_path + 'vp_frick/'
    rs.save_to_parquet(vp_frick_cb, save_path, 'vp_frick_cb')
    # vp_frick_cb.to_parquet(save_path+'vp_frick_cb.parquet', engine='pyarrow')
    return print('dumped vp-frick csv files to parquet')


def get_vp_stfs_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_stfs_cb_schema):
    ## save without month
    vp_stfs_fields = list(vp_stfs_cb_schema.keys())
    cb_path = load_prefix + 'VP/VP-STFS/'
    print('reading files')
    vp_stfs_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, vp_stfs_cb_schema, vp_stfs_fields)
    # payvision_cb = read_all_csv_from_S3_to_pd(load_bucket, 'misc/qlik/CHARGEBACKS/Payvision/From_apr_2019/', payvision_cb_schema_v2, payvision_fields)
    print('done reading files')

    save_path = save_file_path + 'vp_stfs/'
    rs.save_to_parquet(vp_stfs_cb, save_path, 'vp_stfs_cb')
    # vp_stfs_cb.to_parquet(save_path+'vp_stfs_cb.parquet', engine='pyarrow')
    return print('dumped vp-stfs csv files to parquet')


def get_vp_truevo_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_truevo_cb_schema):
    ## save without month
    vp_truevo_fields = list(vp_truevo_cb_schema.keys())
    cb_path = load_prefix + 'VP/VP-TRUEVO/'
    print('reading files')
    vp_truevo_cb = rs.read_all_excel_from_S3_to_pd(load_bucket, cb_path, vp_truevo_cb_schema, vp_truevo_fields)
    print('done reading files')

    save_path = save_file_path + 'vp_truevo/'
    rs.save_to_parquet(vp_truevo_cb, save_path, 'vp_truevo_cb')
    # vp_truevo_cb.to_parquet(save_path+'vp_truevo_cb.parquet', engine='pyarrow')
    return print('dumped vp-truevo csv files to parquet')


def get_paynetics_chargebacks_csv(load_bucket, load_prefix, save_file_path, paynetics_cb_schema):
    ## save without month
    paynetics_fields = list(paynetics_cb_schema.keys())
    cb_path = load_prefix + 'Paynetics/'
    print('reading files')
    paynetics_cb = rs.read_all_csv_from_S3_to_pd(load_bucket, cb_path, paynetics_cb_schema, paynetics_fields)
    print('done reading files')

    save_path = save_file_path + 'paynetics/'
    rs.save_to_parquet(paynetics_cb, save_path, 'paynetics_cb')
    # paynetics_cb.to_parquet(save_path+'paynetics_cb.parquet', engine='pyarrow')
    return print('dumped paynetics csv files to parquet')


def get_concardis_chargebacks_csv(load_bucket, load_prefix, save_file_path, concardis_cb_schema):
    # save without month
    concardis_fields = list(concardis_cb_schema.keys())
    cb_path = load_prefix + 'Concardis/'
    print('reading files')
    concardis_cb = rs.read_concardis_nets_cbs_csv_from_S3_to_pd(load_bucket, cb_path, concardis_cb_schema, concardis_fields, datasep=';')
    print('done reading files')

    save_path = save_file_path + 'concardis/'
    rs.save_to_parquet(concardis_cb, save_path, 'concardis_cb')
    # concardis_cb.to_parquet(save_path+'concardis_cb.parquet', engine='pyarrow')
    return print('dumped concardis csv files to parquet')


def get_nets_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, nets_cb_schema_v2):
    # save without month
    nets_v2_fields = list(nets_cb_schema_v2.keys())
    cb_path = load_prefix + 'NETS/From_Jun_9_2020_New_File_Format/'
    print('reading files')
    nets_cb_v2 = rs.read_concardis_nets_cbs_csv_from_S3_to_pd(load_bucket, cb_path, nets_cb_schema_v2, nets_v2_fields,data_quoting='MINIMAL')
    print('done reading files')

    save_path = save_file_path + 'nets/version_2/'
    rs.save_to_parquet(nets_cb_v2, save_path, 'nets_cb_v2')
    # nets_cb_v2.to_parquet(save_path+'nets_cb_v2.parquet', engine='pyarrow')
    return print('dumped nets v2 csv files to parquet')

