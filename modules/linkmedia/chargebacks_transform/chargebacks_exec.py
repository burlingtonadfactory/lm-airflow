from transform_data.chargebacks_transform import chargebacks_csv_controller as cbcc
from transform_data.chargebacks_transform import chargebacks_pq_controller as cbpc

import pandas as pd

def get_data():
    # start date and number of days to go back
    # start_date = run_date

    chargebacks_csv = cbcc.chargebacks_process_csv()
    chargebacks_pq = cbpc.chargebacks_process_pq()

    chargebacks_csv.get_chargebacks_csv()
    chargebacks_pq.get_chargebacks_pq()

    # bucket = 'datalake-process'
    # date_path = run_date.replace('-', '/')
    # bucket_path = 's3://'+bucket+'/'
    # order_header_history_fields = cbs.order_header_fields

    # order_header_history_file = bucket_path+'order_header_history_test/*'
    # order_header_history = dd.read_parquet(order_header_history_file, columns=order_header_history_fields,  engine = 'pyarrow' ).compute()
    # order_header_history['order_id'] = order_header_history['order_id'].astype(int)
    # order_header_history['order_date'] = pd.to_datetime(order_header_history['time_stamp']).dt.date

    # loop for required days and generate order_header
    # for i in list(range(0, number_of_days_ahead)):
    #     run_date = (pd.to_datetime(start_date, format='%Y-%m-%d') +
    #                 pd.DateOffset(i)).strftime('%Y-%m-%d')
    #     # Run chargeback controller
    #     chargeback.get_chargebacks(run_date, order_header_history)
    
    # order_header_history = pd.DataFrame()
    # gc.collect()
