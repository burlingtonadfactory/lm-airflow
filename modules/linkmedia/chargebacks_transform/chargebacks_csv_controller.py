import pandas as pd
from datetime import datetime
from dateutil.relativedelta import relativedelta
import logging

# local packages
from linkmedia.chargebacks_transform import chargebacks_csv_schema as cbcs
from linkmedia.chargebacks_transform import chargebacks_csv_functions as cbcf

class chargebacks_process_csv:

    def __init__(self):
        logging.info('Initialising buckets and schemas for csv chargeback data')

        self.load_bucket = 'datalake-imports'
        self.load_prefix = 'misc/qlik/CHARGEBACKS/'

        # Path to save CB data
        self.save_bucket = 'linkmedia-datalake'
        self.bucket_path = 's3://' + self.save_bucket + '/'
        self.save_prefix = 'chargebacks/csv_dump_to_pq/'
        self.save_file_path = self.bucket_path + self.save_prefix

        # if run date is less than 5th, also process previous month data else process only new month files
        if int(pd.to_datetime('today').strftime('%Y%m%d')[6:8]) <= 5:
            month_delta = -1
        else:
            month_delta = 0
        #start_month = '201909'
        self.start_month = pd.to_datetime(pd.to_datetime('today') + relativedelta(months=month_delta)).strftime(format='%Y%m')

    def get_chargebacks_csv(self):

        # checkout_cb_schema_v1 = cbcs.checkout_cb_schema_v1
        # checkout_cb_schema_v2 = cbcs.checkout_cb_schema_v2
        # paysafe_cb_schema = cbcs.paysafe_cb_schema
        # emp_cb_schema = cbcs.emp_cb_schema
        # emp_six_manual_rev_schema = cbcs.emp_six_manual_rev_schema
        # emp_wdl_manual_rev_schema = cbcs.emp_wdl_manual_rev_schema
        # emp_bns_manual_rev_schema = cbcs.emp_bns_manual_rev_schema
        # epro_cb_schema = cbcs.epro_cb_schema
        # billpro_cb_schema = cbcs.billpro_cb_schema
        # payvision_cb_schema_v1 = cbcs.payvision_cb_schema_v1
        # payvision_cb_schema_v2 = cbcs.payvision_cb_schema_v2
        # vp_frick_cb_schema = cbcs.vp_frick_cb_schema
        # vp_stfs_cb_schema = cbcs.vp_stfs_cb_schema
        # vp_truevo_cb_schema = cbcs.vp_truevo_cb_schema
        # paynetics_cb_schema = cbcs.paynetics_cb_schema
        # concardis_cb_schema = cbcs.concardis_cb_schema
        # nets_cb_schema_v2 = cbcs.nets_cb_schema_v2

        # logging.info('Getting Checkout Chargebacks v1 csv')
        # cbcf.get_checkout_chargebacks_v1_csv(load_bucket, load_prefix, save_file_path, checkout_cb_schema_v1)

        # logging.info('Getting Checkout Chargebacks v2 csv')
        # cbcf.get_checkout_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, start_month, checkout_cb_schema_v2)

        # logging.info('Getting Paysafe Chargebacks csv')
        # cbcf.get_paysafe_chargebacks_csv(load_bucket, load_prefix, save_file_path, paysafe_cb_schema)

        # logging.info('Getting EMP Chargebacks csv')
        # cbcf.get_emp_chargebacks_csv(load_bucket, load_prefix, save_file_path, emp_cb_schema)

        # logging.info('Getting EMP WDL Manual Reversals Excel')
        # cbcf.get_emp_wdl_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_wdl_manual_rev_schema)

        # logging.info('Getting EMP Six Manual Reversals Excel')
        # cbcf.get_emp_six_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_six_manual_rev_schema)

        # logging.info('Getting EMP BNS Manual Reversals Excel')
        # cbcf.get_emp_bns_manual_reversals_xlsx(load_bucket, load_prefix, save_file_path, emp_bns_manual_rev_schema)

        # logging.info('Getting Epro Chargebacks csv')
        # cbcf.get_epro_chargebacks_csv(load_bucket, load_prefix, save_file_path, epro_cb_schema)

        # logging.info('Getting Billpro Chargebacks csv')
        # cbcf.get_billpro_chargebacks_csv(load_bucket, load_prefix, save_file_path, billpro_cb_schema)

        # logging.info('Getting Payvision Chargebacks v1 csv')
        # cbcf.get_payvision_chargebacks_v1_csv(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v1)

        # Commenting out as incorrectly formatted file was disrupting the flow. No impact as we have terminated the association with Payvision
        # logging.info('Getting Payvision Chargebacks v2 csv')
        # cbcf.get_payvision_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v2)

        # logging.info('Getting VP Frick Chargebacks Excel')
        # cbcf.get_vp_frick_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_frick_cb_schema)

        # logging.info('Getting VP STFS Chargebacks Excel')
        # cbcf.get_vp_stfs_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_stfs_cb_schema)

        # logging.info('Getting VP Truevo Chargebacks Excel')
        # cbcf.get_vp_truevo_chargebacks_xlsx(load_bucket, load_prefix, save_file_path, vp_truevo_cb_schema)

        # logging.info('Getting Paynetics Chargebacks csv')
        # cbcf.get_paynetics_chargebacks_csv(load_bucket, load_prefix, save_file_path, paynetics_cb_schema)

        # logging.info('Getting Concardis Chargebacks csv')
        # cbcf.get_concardis_chargebacks_csv(load_bucket, load_prefix, save_file_path, concardis_cb_schema)

        # logging.info('Getting Nets Chargebacks v2 csv')
        # cbcf.get_nets_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, nets_cb_schema_v2)

        return print('updated csv/excel files dumped to parquet')

    def get_checkout_cbs_csv(self):

        # checkout_cb_schema_v1 = cbcs.checkout_cb_schema_v1
        checkout_cb_schema_v2 = cbcs.checkout_cb_schema_v2

        # logging.info('Getting Checkout Chargebacks v1 csv')
        # cbcf.get_checkout_chargebacks_v1_csv(self.load_bucket, self.load_prefix, self.save_file_path, checkout_cb_schema_v1)

        logging.info('Getting Checkout Chargebacks v2 csv')
        cbcf.get_checkout_chargebacks_v2_csv(self.load_bucket, self.load_prefix, self.save_file_path, self.start_month, checkout_cb_schema_v2)

    def get_paysafe_cbs_csv(self):
        paysafe_cb_schema = cbcs.paysafe_cb_schema

        logging.info('Getting Paysafe Chargebacks csv')
        cbcf.get_paysafe_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, paysafe_cb_schema)

    def get_emp_cbs_csv(self):
        emp_cb_schema = cbcs.emp_cb_schema
        emp_six_manual_rev_schema = cbcs.emp_six_manual_rev_schema
        emp_wdl_manual_rev_schema = cbcs.emp_wdl_manual_rev_schema
        emp_bns_manual_rev_schema = cbcs.emp_bns_manual_rev_schema

        logging.info('Getting EMP Chargebacks csv')
        cbcf.get_emp_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, emp_cb_schema)

        logging.info('Getting EMP WDL Manual Reversals Excel')
        cbcf.get_emp_wdl_manual_reversals_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, emp_wdl_manual_rev_schema)

        logging.info('Getting EMP Six Manual Reversals Excel')
        cbcf.get_emp_six_manual_reversals_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, emp_six_manual_rev_schema)

        logging.info('Getting EMP BNS Manual Reversals Excel')
        cbcf.get_emp_bns_manual_reversals_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, emp_bns_manual_rev_schema)

    def get_epro_cbs_csv(self):
        epro_cb_schema = cbcs.epro_cb_schema

        logging.info('Getting Epro Chargebacks csv')
        cbcf.get_epro_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, epro_cb_schema)

    def get_billpro_cbs_csv(self):

        billpro_cb_schema = cbcs.billpro_cb_schema

        logging.info('Getting Billpro Chargebacks csv')
        cbcf.get_billpro_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, billpro_cb_schema)

    def get_payvision_cbs_csv(self):
        payvision_cb_schema_v1 = cbcs.payvision_cb_schema_v1
        # payvision_cb_schema_v2 = cbcs.payvision_cb_schema_v2

        logging.info('Getting Payvision Chargebacks v1 csv')
        cbcf.get_payvision_chargebacks_v1_csv(self.load_bucket, self.load_prefix, self.save_file_path, payvision_cb_schema_v1)

        # Commenting out as incorrectly formatted file was disrupting the flow. No impact as we have terminated the association with Payvision
        # logging.info('Getting Payvision Chargebacks v2 csv')
        # cbcf.get_payvision_chargebacks_v2_csv(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v2)

    def get_vp_cbs_csv(self):
        vp_frick_cb_schema = cbcs.vp_frick_cb_schema
        vp_stfs_cb_schema = cbcs.vp_stfs_cb_schema
        vp_truevo_cb_schema = cbcs.vp_truevo_cb_schema


        logging.info('Getting VP Frick Chargebacks Excel')
        cbcf.get_vp_frick_chargebacks_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, vp_frick_cb_schema)

        logging.info('Getting VP STFS Chargebacks Excel')
        cbcf.get_vp_stfs_chargebacks_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, vp_stfs_cb_schema)

        logging.info('Getting VP Truevo Chargebacks Excel')
        cbcf.get_vp_truevo_chargebacks_xlsx(self.load_bucket, self.load_prefix, self.save_file_path, vp_truevo_cb_schema)

    def get_paynetics_cbs_csv(self):
        paynetics_cb_schema = cbcs.paynetics_cb_schema

        logging.info('Getting Paynetics Chargebacks csv')
        cbcf.get_paynetics_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, paynetics_cb_schema)

    def get_concardis_cbs_csv(self):
        concardis_cb_schema = cbcs.concardis_cb_schema

        logging.info('Getting Concardis Chargebacks csv')
        cbcf.get_concardis_chargebacks_csv(self.load_bucket, self.load_prefix, self.save_file_path, concardis_cb_schema)

    def get_nets_cbs_csv(self):
        nets_cb_schema_v2 = cbcs.nets_cb_schema_v2

        logging.info('Getting Nets Chargebacks v2 csv')
        cbcf.get_nets_chargebacks_v2_csv(self.load_bucket, self.load_prefix, self.save_file_path, nets_cb_schema_v2)