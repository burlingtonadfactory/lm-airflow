import logging

# local packages
from linkmedia.chargebacks_transform import chargebacks_pq_schema as cbps
from linkmedia.chargebacks_transform import chargebacks_pq_functions as cbpp

class chargebacks_process_pq:

    def __init__(self):

        logging.info('Initialising buckets and schemas for Parquet chargeback data')

        self.load_bucket = 'linkmedia-datalake'
        self.load_prefix = 'chargebacks/csv_dump_to_pq/'

        # Path to save CB data
        self.save_bucket = 'linkmedia-datalake'
        self.bucket_path = 's3://' + self.save_bucket + '/'
        self.save_prefix = 'chargebacks/banks_staging/'
        self.save_file_path = self.bucket_path + self.save_prefix
        self.final_load_prefix = self.save_prefix
        self.final_save_file_path = self.bucket_path + 'chargebacks/all_chargebacks/'
        self.final_save_file_path_tsv = self.bucket_path + 'chargebacks/all_chargebacks_datawarehouse/'

    def get_chargebacks_pq(self):

        logging.info('Initialising buckets and schemas for Parquet chargeback data')

        load_bucket = 'linkmedia-datalake'
        load_prefix = 'chargebacks/csv_dump_to_pq/'

        # Path to save CB data
        save_bucket = 'linkmedia-datalake'
        bucket_path = 's3://' + save_bucket + '/'
        save_prefix = 'chargebacks/banks_staging/'
        save_file_path = bucket_path + save_prefix
        final_load_prefix = save_prefix
        final_save_file_path = bucket_path + 'chargebacks/all_chargebacks/'
        final_save_file_path_tsv = bucket_path + 'chargebacks/all_chargebacks_datawarehouse/'

        checkout_cb_schema_v1 = cbps.checkout_cb_schema_v1
        checkout_cb_schema_v2 = cbps.checkout_cb_schema_v2
        paysafe_cb_schema = cbps.paysafe_cb_schema
        emp_cb_schema = cbps.emp_cb_schema
        emp_six_manual_rev_schema = cbps.emp_six_manual_rev_schema
        emp_wdl_manual_rev_schema = cbps.emp_wdl_manual_rev_schema
        epro_cb_schema = cbps.epro_cb_schema
        billpro_cb_schema = cbps.billpro_cb_schema
        payvision_cb_schema_v1 = cbps.payvision_cb_schema_v1
        payvision_cb_schema_v2 = cbps.payvision_cb_schema_v2
        vp_frick_cb_schema = cbps.vp_frick_cb_schema
        vp_stfs_cb_schema = cbps.vp_stfs_cb_schema
        vp_truevo_cb_schema = cbps.vp_truevo_cb_schema
        paynetics_cb_schema = cbps.paynetics_cb_schema
        concardis_cb_schema = cbps.concardis_cb_schema
        nets_cb_schema_v2 = cbps.nets_cb_schema_v2
        transaction_id_cb_schema = cbps.transaction_id_cb_schema
        transaction_id_cb_schema_emp = cbps.transaction_id_cb_schema_emp
        order_id_cb_schema = cbps.order_id_cb_schema
        order_id_cb_schema_emp_epro = cbps.order_id_cb_schema_emp_epro
        order_cc_cb_schema = cbps.order_cc_cb_schema

        logging.info('Getting Checkout Chargebacks Parquet')
        cbpp.get_checkout_chargebacks_pq(load_bucket, load_prefix, save_file_path, checkout_cb_schema_v1,
                                         checkout_cb_schema_v2)


        logging.info('Getting Paysafe Chargebacks Parquet')
        cbpp.get_paysafe_chargebacks(load_bucket, load_prefix, save_file_path, paysafe_cb_schema)

        logging.info('Getting EMP Chargebacks Parquet')
        cbpp.get_emp_chargebacks(load_bucket, load_prefix, save_file_path, emp_cb_schema, emp_six_manual_rev_schema,
                                 emp_wdl_manual_rev_schema)

        logging.info('Getting Epro Chargebacks Parquet')
        cbpp.get_epro_chargebacks(load_bucket, load_prefix, epro_cb_schema)

        logging.info('Getting Billpro Chargebacks Parquet')
        cbpp.get_billpro_chargebacks(load_bucket, load_prefix, billpro_cb_schema)

        logging.info('Getting Payvision Chargebacks v1 Parquet')
        cbpp.get_payvision_chargebacks_v1(save_file_path, payvision_cb_schema_v1)

        logging.info('Getting Payvision Chargebacks v2 Parquet')
        cbpp.get_payvision_chargebacks_v2(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v2)

        logging.info('Getting VP Frick Chargebacks Parquet')
        cbpp.get_vp_frick_chargebacks(load_bucket, load_prefix, save_file_path, vp_frick_cb_schema)

        logging.info('Getting VP STFS Chargebacks Parquet')
        cbpp.get_vp_stfs_chargebacks(load_bucket, load_prefix, save_file_path, vp_stfs_cb_schema)

        logging.info('Getting VP Truevo Chargebacks Parquet')
        cbpp.get_vp_truevo_chargebacks(load_bucket, load_prefix, save_file_path, vp_truevo_cb_schema)

        logging.info('Getting Paynetics Chargebacks Parquet')
        cbpp.get_paynetics_chargebacks(load_bucket, load_prefix, save_file_path, paynetics_cb_schema)

        logging.info('Getting Concardis Chargebacks Parquet')
        cbpp.get_concardis_chargebacks(load_bucket, load_prefix, save_file_path, concardis_cb_schema)

        logging.info('Getting Nets Chargebacks Parquet')
        cbpp.get_nets_chargebacks(load_bucket, load_prefix, save_file_path, nets_cb_schema_v2)

        logging.info('Merging All Chargebacks')
        cbpp.merge_all_chargebacks(load_bucket, final_load_prefix, final_save_file_path, final_save_file_path_tsv,
                                   transaction_id_cb_schema, transaction_id_cb_schema_emp,
                                   order_id_cb_schema, order_id_cb_schema_emp_epro,
                                   order_cc_cb_schema )
        return 'all chargebacks saved'

    def get_checkout_cbs_pq(self):

        checkout_cb_schema_v1 = cbps.checkout_cb_schema_v1
        checkout_cb_schema_v2 = cbps.checkout_cb_schema_v2

        logging.info('Getting Checkout Chargebacks Parquet')
        cbpp.get_checkout_chargebacks_pq(self.load_bucket, self.load_prefix, self.save_file_path, checkout_cb_schema_v1,
                                         checkout_cb_schema_v2)

    def get_paysafe_cbs_pq(self):

        paysafe_cb_schema = cbps.paysafe_cb_schema

        logging.info('Getting Paysafe Chargebacks Parquet')
        cbpp.get_paysafe_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, paysafe_cb_schema)

    def get_emp_cbs_pq(self):    
        emp_cb_schema = cbps.emp_cb_schema
        emp_six_manual_rev_schema = cbps.emp_six_manual_rev_schema
        emp_wdl_manual_rev_schema = cbps.emp_wdl_manual_rev_schema

        logging.info('Getting EMP Chargebacks Parquet')
        cbpp.get_emp_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, emp_cb_schema, emp_six_manual_rev_schema,
                                 emp_wdl_manual_rev_schema)

    def get_epro_cbs_pq(self):
        epro_cb_schema = cbps.epro_cb_schema

        logging.info('Getting Epro Chargebacks Parquet')
        cbpp.get_epro_chargebacks(self.load_bucket, self.load_prefix, epro_cb_schema)

    def get_billpro_cbs_pq(self):
        billpro_cb_schema = cbps.billpro_cb_schema

        logging.info('Getting Billpro Chargebacks Parquet')
        cbpp.get_billpro_chargebacks(self.load_bucket, self.load_prefix, billpro_cb_schema)

    def get_payvision_cbs_pq(self):
        payvision_cb_schema_v1 = cbps.payvision_cb_schema_v1
        payvision_cb_schema_v2 = cbps.payvision_cb_schema_v2


        logging.info('Getting Payvision Chargebacks v1 Parquet')
        cbpp.get_payvision_chargebacks_v1(self.save_file_path, payvision_cb_schema_v1)

        logging.info('Getting Payvision Chargebacks v2 Parquet')
        cbpp.get_payvision_chargebacks_v2(self.load_bucket, self.load_prefix, self.save_file_path, payvision_cb_schema_v2)

    def get_vp_cbs_pq(self):
        vp_frick_cb_schema = cbps.vp_frick_cb_schema
        vp_stfs_cb_schema = cbps.vp_stfs_cb_schema
        vp_truevo_cb_schema = cbps.vp_truevo_cb_schema

        logging.info('Getting VP Frick Chargebacks Parquet')
        cbpp.get_vp_frick_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, vp_frick_cb_schema)

        logging.info('Getting VP STFS Chargebacks Parquet')
        cbpp.get_vp_stfs_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, vp_stfs_cb_schema)

        logging.info('Getting VP Truevo Chargebacks Parquet')
        cbpp.get_vp_truevo_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, vp_truevo_cb_schema)

    def get_paynetics_cbs_pq(self):
        paynetics_cb_schema = cbps.paynetics_cb_schema

        logging.info('Getting Paynetics Chargebacks Parquet')
        cbpp.get_paynetics_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, paynetics_cb_schema)

    def get_concardis_cbs_pq(self):
        concardis_cb_schema = cbps.concardis_cb_schema

        logging.info('Getting Concardis Chargebacks Parquet')
        cbpp.get_concardis_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, concardis_cb_schema)

    def get_nets_cbs_pq(self):
        nets_cb_schema_v2 = cbps.nets_cb_schema_v2

        logging.info('Getting Nets Chargebacks Parquet')
        cbpp.get_nets_chargebacks(self.load_bucket, self.load_prefix, self.save_file_path, nets_cb_schema_v2)


    def combine_all_cbs(self):
        transaction_id_cb_schema = cbps.transaction_id_cb_schema
        transaction_id_cb_schema_emp = cbps.transaction_id_cb_schema_emp
        order_id_cb_schema = cbps.order_id_cb_schema
        order_id_cb_schema_emp_epro = cbps.order_id_cb_schema_emp_epro
        order_cc_cb_schema = cbps.order_cc_cb_schema


        logging.info('Merging All Chargebacks')
        cbpp.merge_all_chargebacks(self.load_bucket, self.final_load_prefix, self.final_save_file_path, self.final_save_file_path_tsv,
                                   transaction_id_cb_schema, transaction_id_cb_schema_emp,
                                   order_id_cb_schema, order_id_cb_schema_emp_epro,
                                   order_cc_cb_schema )