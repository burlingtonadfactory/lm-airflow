import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import s3fs
import boto3
from smart_open.s3 import iter_bucket as s3_iter_bucket
from smart_open import open
from datetime import datetime
from pandas.io.json import json_normalize
import json
import io

# local packages
from linkmedia.lib import read_store as rs


fs = s3fs.S3FileSystem()

load_bucket = 'linkmedia-datalake'
load_prefix = 'misc/chargebacks/'

##Path to save CB data
save_bucket = 'linkmedia-datalake'
bucket_path = 's3://' + save_bucket + '/'
save_prefix = 'chargebacks/banks_staging/'
save_file_path = bucket_path + save_prefix


def get_checkout_chargebacks_pq(load_bucket, load_prefix, save_file_path, checkout_cb_schema_v1, checkout_cb_schema_v2):
    checkout_cb_fields_v1 = list(checkout_cb_schema_v1.keys())
    checkout_v1 = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'checkout/version_1',
                                                    checkout_cb_fields_v1)
    checkout_cb_fields_v2 = list(checkout_cb_schema_v2.keys())
    checkout_v2 = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'checkout/version_2',
                                                    checkout_cb_fields_v2)
    checkout_v2 = checkout_v2.rename(
        columns={
            'Dispute_ID': 'Chargeback_ID',
            "Dispute_Date_UTC": "Chargeback_Date_UTC",
            'Dispute_Indicator': 'Chargeback_Indicator',
            'Dispute_Amount': 'Chargeback_Amount',
            "Dispute_Code": "Chargeback_Code",
            'Dispute_Date_Europe/London': 'Chargeback_Date_Europe/London'
        })

    checkout = pd.concat([checkout_v1, checkout_v2], ignore_index=True, sort=False)
    checkout['Chargeback_Indicator'] = checkout['Chargeback_Indicator'].str.replace('"', '')

    checkout_v1 = pd.DataFrame()
    checkout_v2 = pd.DataFrame()
    checkout = checkout[np.isin(checkout['Chargeback_Indicator'], ['ADJM', 'AUTO', 'CBRV'])]
    checkout['Chargeback_Date'] = pd.to_datetime(checkout['Chargeback_Date_UTC'], infer_datetime_format=True)

    # Add extra columns required
    checkout = checkout.assign(bank='CHECKOUT', transaction_type='CB', has_tran_id=0)
    checkout['transaction_type'] = np.where(checkout['Chargeback_Indicator'] == 'ADJM', 'CB', 'CB_R')
    # rename columns
    checkout = checkout.rename(columns={"Reference": "order_id", "Chargeback_Date": "cb_date"})

    # AUTO indicator chargeback from Checkout generates only one records, but that auto representment means that there was a
    # chargeback which was represented. To count a chargeback for it, we duplicate each auto record and mark one of it as CB and other as reversal
    checkout_auto = checkout[checkout['Chargeback_Indicator'] == 'AUTO']
    # in previous condition auto is already marked as a CB_R type, here we mark it as CB
    checkout_auto['transaction_type'] = 'CB'
    # merge and drop this checkout_auto dataframe to checkout dataframe
    checkout = pd.concat([checkout, checkout_auto], ignore_index=True, sort=False)
    checkout_auto = pd.DataFrame()

    checkout['order_id'] = checkout['order_id'].str.replace('"', '')
    # converting types
    checkout = checkout[pd.notna(checkout['order_id'])]
    # checkout['transaction_id'] = checkout['transaction_id'].astype('int64')
    checkout['cb_date'] = checkout['cb_date'].dt.date

    # drop duplicate records based on Transaction ID and type if any. (limited to CB/CB_R)
    checkout = checkout.sort_values('cb_date', ascending=True)
    checkout = checkout.drop_duplicates(subset=['order_id', 'transaction_type'], keep='first')

    save_path = save_file_path + 'checkout/'
    rs.save_to_parquet(checkout, save_path, 'checkout_cb')
    return print('processed chargebacks from checkout parquet files')


def get_paysafe_chargebacks(load_bucket, load_prefix, save_file_path, paysafe_cb_schema):
    paysafe_cb_fields = list(paysafe_cb_schema.keys())

    # Paysafe Chargebacks
    paysafe_cb_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'paysafe/chargebacks',
                                                       paysafe_cb_fields)

    paysafe_cb_cmb['Posting_Date'] = pd.to_datetime(paysafe_cb_cmb['Posting_Date'], infer_datetime_format=True)
    # Add extra columns required
    paysafe_cb_cmb = paysafe_cb_cmb.assign(bank='PAYSAFE', transaction_type='CB', has_tran_id=0)
    # rename columns
    paysafe_cb_cmb = paysafe_cb_cmb.rename(
        columns={"Merchant_Transaction_ID": "order_id", "Posting_Date": "cb_date"})
    # converting types
    paysafe_cb_cmb = paysafe_cb_cmb[~pd.isna(paysafe_cb_cmb['order_id'])]
    paysafe_cb_cmb['cb_date'] = paysafe_cb_cmb['cb_date'].dt.date
    # drop duplicate records based on Transaction ID, if any.
    paysafe_cb_cmb = paysafe_cb_cmb.sort_values('cb_date', ascending=True)
    paysafe_cb_cmb = paysafe_cb_cmb.drop_duplicates(subset=['order_id'], keep='first')

    # Paysafe Chargebacks reversal
    paysafe_cb_rev_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'paysafe/reversals',
                                                           paysafe_cb_fields)
    paysafe_cb_rev_cmb['Posting_Date'] = pd.to_datetime(paysafe_cb_rev_cmb['Posting_Date'], infer_datetime_format=True)
    # Add extra columns required
    paysafe_cb_rev_cmb = paysafe_cb_rev_cmb.assign(bank='PAYSAFE', transaction_type='CB_R', has_tran_id=0)
    # rename columns
    paysafe_cb_rev_cmb = paysafe_cb_rev_cmb.rename(
        columns={"Merchant_Transaction_ID": "order_id", "Posting_Date": "cb_date"})
    # converting types
    paysafe_cb_rev_cmb = paysafe_cb_rev_cmb[~pd.isna(paysafe_cb_rev_cmb['order_id'])]
    paysafe_cb_rev_cmb['cb_date'] = paysafe_cb_rev_cmb['cb_date'].dt.date
    # drop duplicate records based on Transaction ID, if any.
    paysafe_cb_rev_cmb = paysafe_cb_rev_cmb.sort_values('cb_date', ascending=True)
    paysafe_cb_rev_cmb = paysafe_cb_rev_cmb.drop_duplicates(subset=['order_id'], keep='first')

    paysafe = pd.concat([paysafe_cb_cmb, paysafe_cb_rev_cmb], ignore_index=True, sort=False)

    save_path = save_file_path + 'paysafe/'
    rs.save_to_parquet(paysafe, save_path, 'paysafe_cb')
    return print('processed chargebacks from paysafe parquet files')


def get_emp_chargebacks(load_bucket, load_prefix, save_file_path, emp_cb_schema, emp_six_manual_rev_schema,
                        emp_wdl_manual_rev_schema):
    # EMP CB
    # EMP Chargebacks are identified based on order ID + order date, because we can have chargebacks from Limelight and ACME both.
    # Both these systems can have same order keys are they are independent to each other. Adding date also helps us to pick correct order
    # from correct system.
    # EMP has multiple CB files so we need to run a loop and load all the CB files
    emp_cb_fields = list(emp_cb_schema.keys())

    # emp chargebacks
    emp_cb_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'emp/chargebacks/', emp_cb_fields)
    emp_cb_cmb['Impact_Date'] = pd.to_datetime(emp_cb_cmb['Impact_Date'], infer_datetime_format=True)
    emp_cb_cmb['Transaction_DateTime'] = pd.to_datetime(emp_cb_cmb['Transaction_DateTime'], infer_datetime_format=True)
    # emp_cb['Transaction DateTime'] = pd.DatetimeIndex(emp_cb['Transaction DateTime']).tz_convert(None)
    # Add extra columns required
    emp_cb_cmb = emp_cb_cmb.assign(bank='EMP')
    # rename columns
    emp_cb_cmb = emp_cb_cmb.rename(
        columns={"Merchant_Ref": "order_id", "Impact_Date": "cb_date", 'Transaction_ID': 'transaction_id'})
    # cleaning data and creating required columns
    emp_cb_cmb = emp_cb_cmb[pd.notna(emp_cb_cmb['order_id']) | pd.notna(emp_cb_cmb['transaction_id'])]
    emp_cb_cmb = emp_cb_cmb[emp_cb_cmb['Type'].isin(['C', 'P', 'R'])]
    emp_cb_cmb['transaction_type'] = np.where(emp_cb_cmb['Type'] == 'C', 'CB', 'CB_R')
    emp_cb_cmb['has_tran_id'] = np.where(pd.isna(emp_cb_cmb['order_id']), 1, 0)
    # emp_cmb['transaction_id'] = emp_cmb['transaction_id'].astype('int64')
    emp_cb_cmb['cb_date'] = emp_cb_cmb['cb_date'].dt.date
    emp_cb_cmb['order_date'] = emp_cb_cmb['Transaction_DateTime'].dt.date

    # drop duplicate records based on Transaction ID, if any. However, in future, if we want to look at chargebacks which got reversed,
    # it might be useful to see if these duplicate records are due to reversal entries etc
    # before dropping, sort on dates to keep first time chargeback date

    # pick only order_id based records which are cb
    emp_cb_order_id = emp_cb_cmb[(emp_cb_cmb['has_tran_id'] == 0)]
    emp_cb_order_id = emp_cb_order_id.sort_values('cb_date', ascending=True)
    # EMP also has some acme chargebacks, which could be same order_id as limelight but we expect to have different
    # transaction time - keeping transaction time to uniquely identify transaction
    emp_cb_order_id = emp_cb_order_id.drop_duplicates(subset=['order_id', 'Transaction_DateTime', 'transaction_type'],
                                                      keep='first')

    # pick only transaction_id based records which are cb
    emp_cb_tnx_id = emp_cb_cmb[(emp_cb_cmb['has_tran_id'] == 1)]
    emp_cb_tnx_id = emp_cb_tnx_id.sort_values('cb_date', ascending=True)
    # EMP also has some acme chargebacks, which could be same order_id as limelight but we expect to have different
    # transaction time - keeping transaction time to uniquely identify transaction
    emp_cb_tnx_id = emp_cb_tnx_id.drop_duplicates(subset=['transaction_id', 'Transaction_DateTime', 'transaction_type'],
                                                  keep='first')

    # concatenate all 2 dataframes into 1 chargeback dataframe
    emp_cb_cmb = pd.concat([emp_cb_order_id, emp_cb_tnx_id], ignore_index=True, sort=False)
    emp_cb_order_id = pd.DataFrame()
    emp_cb_tnx_id = pd.DataFrame()

    # emp chargebacks reversals - six
    emp_six_cb_rev_fields = list(emp_six_manual_rev_schema.keys())
    emp_six_cb_rev_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'emp/manual_reversals/six/',
                                                           emp_six_cb_rev_fields)

    emp_six_cb_rev_cmb['Statement_Period_End_Date_Time_UTC'] = pd.to_datetime(
        emp_six_cb_rev_cmb['Statement_Period_End_Date_Time_UTC'], infer_datetime_format=True)
    # emp_cb['Transaction DateTime'] = pd.DatetimeIndex(emp_cb['Transaction DateTime']).tz_convert(None)
    # drop duplicates based on ARN
    emp_six_cb_rev_cmb = emp_six_cb_rev_cmb.sort_values('ARN', ascending=True)
    emp_six_cb_rev_cmb = emp_six_cb_rev_cmb.drop_duplicates(subset=['ARN'], keep='first')

    emp_six_cb_rev_cmb = emp_cb_cmb.merge(emp_six_cb_rev_cmb, how='inner', left_on='Chargeback_Reference_/_ARN',
                                          right_on='ARN')
    emp_six_cb_rev_cmb = emp_six_cb_rev_cmb.drop(columns=['Chargeback_Reference_/_ARN', 'cb_date', 'transaction_type'])
    # Add extra columns required
    emp_six_cb_rev_cmb = emp_six_cb_rev_cmb.assign(transaction_type='CB_R')
    # rename columns
    emp_six_cb_rev_cmb = emp_six_cb_rev_cmb.rename(columns={"Statement_Period_End_Date_Time_UTC": "cb_date"})
    emp_six_cb_rev_cmb['cb_date'] = emp_six_cb_rev_cmb['cb_date'].dt.date

    # emp chargebacks reversals - wdl
    emp_wdl_cb_rev_fields = list(emp_wdl_manual_rev_schema.keys())
    emp_wdl_cb_rev_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'emp/manual_reversals/wdl/',
                                                           emp_wdl_cb_rev_fields)

    emp_wdl_cb_rev_cmb_filter_1 = emp_wdl_cb_rev_cmb[emp_wdl_cb_rev_cmb['Statement_Period_End_Date'].str[0:1] == '4']
    emp_wdl_cb_rev_cmb_filter_2 = emp_wdl_cb_rev_cmb[emp_wdl_cb_rev_cmb['Statement_Period_End_Date'].str[0:1] != '4']
    emp_wdl_cb_rev_cmb_filter_1['Statement_Period_End_Date'] = pd.TimedeltaIndex(
        emp_wdl_cb_rev_cmb_filter_1['Statement_Period_End_Date'].astype(int), unit='d') + datetime(1899, 12, 30)
    emp_wdl_cb_rev_cmb = pd.concat([emp_wdl_cb_rev_cmb_filter_1, emp_wdl_cb_rev_cmb_filter_2], ignore_index=True,
                                   sort=False)

    emp_wdl_cb_rev_cmb['Statement_Period_End_Date'] = pd.to_datetime(emp_wdl_cb_rev_cmb['Statement_Period_End_Date'],
                                                                     infer_datetime_format=True)
    # emp_cb['Transaction DateTime'] = pd.DatetimeIndex(emp_cb['Transaction DateTime']).tz_convert(None)

    # drop duplicates based on ARN
    emp_wdl_cb_rev_cmb = emp_wdl_cb_rev_cmb.sort_values('ARN', ascending=True)
    emp_wdl_cb_rev_cmb = emp_wdl_cb_rev_cmb.drop_duplicates(subset=['ARN'], keep='first')

    emp_wdl_cb_rev_cmb = emp_cb_cmb.merge(emp_wdl_cb_rev_cmb, how='inner', left_on='Chargeback_Reference_/_ARN',
                                          right_on='ARN')
    emp_wdl_cb_rev_cmb = emp_wdl_cb_rev_cmb.drop(columns=['Chargeback_Reference_/_ARN', 'cb_date', 'transaction_type'])
    # Add extra columns required
    emp_wdl_cb_rev_cmb = emp_wdl_cb_rev_cmb.assign(transaction_type='CB_R')
    # rename columns
    emp_wdl_cb_rev_cmb = emp_wdl_cb_rev_cmb.rename(columns={"Statement_Period_End_Date": "cb_date"})
    emp_wdl_cb_rev_cmb['cb_date'] = emp_wdl_cb_rev_cmb['cb_date'].dt.date

    emp = pd.concat([emp_cb_cmb, emp_six_cb_rev_cmb, emp_wdl_cb_rev_cmb], ignore_index=True, sort=False)

    save_path = save_file_path + 'emp/'
    rs.save_to_parquet(emp, save_path, 'emp_cb')
    return print('processed chargebacks from emp parquet files')


def get_epro_chargebacks(load_bucket, load_prefix, epro_cb_schema):
    # EPRO CB
    # EPRO Chargebacks are identified based on order ID + order date, because we can have chargebacks from Limelight and ACME both.
    # Both these systems can have same order keys are they are independent to each other. Adding date also helps us to pick correct order
    # from correct system.
    # epro has multiple CB files so we need to run a loop and load all the CB files
    epro_cb_fields = list(epro_cb_schema.keys())

    epro_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'epro/', epro_cb_fields)

    epro_cmb['Date_chargebacked'] = pd.to_datetime(epro_cmb['Date_chargebacked'], infer_datetime_format=True)
    epro_cmb['order_date'] = pd.to_datetime(epro_cmb['Date'], infer_datetime_format=True).dt.date

    # Add extra columns required
    epro_cmb = epro_cmb.assign(bank='EPRO', transaction_type='CB', has_tran_id=0)
    # rename columns
    epro_cmb = epro_cmb.rename(columns={"Transaction_ID": "order_id", "Date_chargebacked": "cb_date"})
    # converting types
    epro_cmb = epro_cmb[~pd.isna(epro_cmb['order_id'])]
    epro_cmb['cb_date'] = epro_cmb['cb_date'].dt.date

    # drop duplicate records based on Transaction ID, if any. However, in future, if we want to look at chargebacks which got reversed,
    # it might be useful to see if these duplicate records are due to reversal entries etc
    # before dropping, sort on dates to keep first time chargeback date
    epro_cmb = epro_cmb.sort_values('cb_date', ascending=True)
    epro_cmb = epro_cmb.drop_duplicates(subset=['order_id', 'order_date'], keep='first')

    save_path = save_file_path + 'epro/'
    rs.save_to_parquet(epro_cmb, save_path, 'epro_cmb')
    return print('processed chargebacks from epro parquet files')


def get_billpro_chargebacks(load_bucket, load_prefix, billpro_cb_schema):
    # billpro CB
    billpro_cb_fields = list(billpro_cb_schema.keys())

    billpro = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'billpro/', billpro_cb_fields)
    # billpro = billpro[:10000]
    billpro['Created'] = pd.to_datetime(billpro['Created'], infer_datetime_format=True)
    # Add extra columns required
    billpro = billpro.assign(bank='BILLPRO', transaction_type='CB', has_tran_id=1)
    # rename columns
    billpro = billpro.rename(columns={"TransactionID": "transaction_id", "Created": "cb_date"})
    # converting types
    billpro = billpro[~pd.isna(billpro['transaction_id'])]
    # billpro['transaction_id'] = billpro['transaction_id'].astype('int64')
    billpro['cb_date'] = billpro['cb_date'].dt.date

    # drop duplicate records based on Transaction ID, if any.
    billpro = billpro.sort_values('cb_date', ascending=True)
    billpro = billpro.drop_duplicates(subset=['transaction_id'], keep='first')

    save_path = save_file_path + 'billpro/'
    rs.save_to_parquet(billpro, save_path, 'billpro_cb')
    return print('processed chargebacks from billpro parquet files')


def get_payvision_chargebacks_v1(save_file_path, payvision_cb_schema_v1):
    # payvision CB - 1
    # payvision CB - 1
    payvision_order_fields = list(payvision_cb_schema_v1.keys())
    payvision_order = pd.read_csv(
        's3://datalake-process/chargebacks/do_not_delete_csv/payvision/version_1/payvision_cb_version_1_do_not_delete.csv',
        usecols=payvision_order_fields)

    payvision_order['chargeback_date'] = pd.to_datetime(payvision_order['chargeback_date'], infer_datetime_format=True)
    payvision_order = payvision_order.rename(columns={"chargeback_date": "cb_date", "bank_code": "bank"})
    # Add extra columns required
    payvision_order = payvision_order.assign(bank='PAYVISION', has_tran_id=0)

    payvision_order['transaction_type'] = np.where(payvision_order['transaction_type'].isin(['Chargeback']), 'CB',
                                                   'CB_R')
    # converting types
    payvision_order = payvision_order[pd.notna(payvision_order['order_id'])]
    payvision_order['cb_date'] = payvision_order['cb_date'].dt.date

    # drop duplicate records based on Transaction ID, if any.
    payvision_order = payvision_order.sort_values('cb_date', ascending=True)
    payvision_order = payvision_order.drop_duplicates(subset=['order_id', 'transaction_type'], keep='first')

    save_path = save_file_path + 'payvision/version_1/'
    rs.save_to_parquet(payvision_order, save_path, 'payvision_cb_v1')
    return print('processed chargebacks from payvision v1 parquet files')


def get_payvision_chargebacks_v2(load_bucket, load_prefix, save_file_path, payvision_cb_schema_v2):
    # Payvision CB - 2
    # Read exported file for Apr-Jun, 2019 where we need to use credit card information
    payvision_cc_fields = list(payvision_cb_schema_v2.keys())

    payvision_cc = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'payvision/version_2',
                                                     payvision_cc_fields)

    payvision_cc = payvision_cc[
        np.isin(payvision_cc['MethodType'], ['Repres.', 'First', 'Second', 'Pre-A. Resp.', 'Reversal'])]

    payvision_cc['CreateDate'] = pd.to_datetime(payvision_cc['CreateDate'], infer_datetime_format=True)
    payvision_cc['OriginalTransactionDate'] = pd.to_datetime(payvision_cc['OriginalTransactionDate'],
                                                             infer_datetime_format=True)
    #     payvision_cc['DynDescName'] = payvision_cc['DynDescName'].str.split('|',1).str[0]
    payvision_cc['DynDescName'] = payvision_cc['DynDescName'].str.replace('|', '+')

    payvision_cc = payvision_cc.assign(order_cc_type='cc_order-date_gtw-desc')
    payvision_cc['order_cc_key_cc_6'] = payvision_cc['FirstSixDigits']
    payvision_cc['order_cc_key_cc_4'] = payvision_cc['LastFourDigits']
    payvision_cc['order_cc_key_order_date'] = payvision_cc['OriginalTransactionDate'].dt.date.astype('str').str.replace(
        '-', '')
    payvision_cc['order_cc_key_gtw_desc'] = payvision_cc['DynDescName'].apply(str.upper).apply(
        lambda x: x.split("+")[0]).apply(lambda x: x.split(" ")[0])
    payvision_cc['order_cc_key_order_amount'] = np.floor(payvision_cc['OriginalAmount'].astype('double')).astype(
        'int').abs().astype('str')

    payvision_cc['order_cc_key'] = payvision_cc['order_cc_key_cc_6'] + '/' + payvision_cc['order_cc_key_cc_4'] + '/' + \
                                   payvision_cc['order_cc_key_order_date'] + \
                                   '/' + payvision_cc['order_cc_key_gtw_desc']

    payvision_cc = payvision_cc.drop(
        columns=["FirstSixDigits", "LastFourDigits", 'OriginalTransactionDate', 'DynDescName', 'OriginalAmount'])

    payvision_cc = payvision_cc.rename(columns={"CreateDate": "cb_date"})
    # Add extra columns required
    payvision_cc = payvision_cc.assign(bank='PAYVISION', transaction_type='CB', has_tran_id=0)
    payvision_cc['transaction_type'] = np.where(np.isin(payvision_cc['MethodType'], ['First', 'Second']), 'CB', 'CB_R')
    # converting types
    payvision_cc = payvision_cc[~pd.isna(payvision_cc['order_cc_key'])]
    payvision_cc['cb_date'] = payvision_cc['cb_date'].dt.date

    # drop duplicate records based on Transaction ID, if any.
    payvision_cc = payvision_cc.sort_values('cb_date', ascending=True)
    payvision_cc = payvision_cc.drop_duplicates(subset=['order_cc_key', 'transaction_type'], keep='first')

    save_path = save_file_path + 'payvision/version_2/'
    rs.save_to_parquet(payvision_cc, save_path, 'payvision_cb_v2')
    return print('processed chargebacks from payvision v2 parquet files')


def get_vp_frick_chargebacks(load_bucket, load_prefix, save_file_path, vp_frick_cb_schema):
    # VP-STFS/FRICK CB
    # in VP - STFS and FRICK Chargeback files, we do not have order ID information, so we need to create an order lookup
    # key based on credit card, trnasaction date, gateway descriptor and order amount.

    # VP-FRICK CB
    vp_frick_cb_fields = list(vp_frick_cb_schema.keys())

    vp_frick = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'vp_frick', vp_frick_cb_fields)
    vp_frick = vp_frick[np.isin(vp_frick['Kind'], ['CBK1', 'CBK2', 'REP', 'RRQ'])]

    vp_frick['Posting_Date'] = pd.to_datetime(vp_frick['Posting_Date'], infer_datetime_format=True).dt.date
    vp_frick['Transaction_Date'] = pd.to_datetime(vp_frick['Transaction_Date'], infer_datetime_format=True).dt.date

    vp_frick = vp_frick.assign(order_cc_type='cc_order-date_gtw-desc')
    vp_frick['order_cc_key_cc_6'] = vp_frick['Card_No.'].str[:6]
    vp_frick['order_cc_key_cc_4'] = vp_frick['Card_No.'].str[12:16]
    vp_frick['order_cc_key_order_date'] = vp_frick['Transaction_Date'].astype('str').str.replace('-', '')
    vp_frick['order_cc_key_gtw_desc'] = vp_frick['Merchant_Name'].apply(str.upper).apply(
        lambda x: x.split("+")[0]).apply(lambda x: x.split(" ")[0])
    vp_frick['order_cc_key_order_amount'] = np.absolute(np.floor(vp_frick['Netwk_Sett_Amt'].astype('double'))).astype(
        'int').astype('str')

    vp_frick['order_cc_key'] = vp_frick['order_cc_key_cc_6'] + '/' + vp_frick['order_cc_key_cc_4'] + '/' + vp_frick[
        'order_cc_key_order_date'] + \
                               '/' + vp_frick['order_cc_key_gtw_desc']

    vp_frick = vp_frick.drop(columns=['Transaction_Date', 'Card_No.', 'Netwk_Sett_Amt', 'Merchant_Name'])
    vp_frick = vp_frick.rename(columns={'Posting_Date': 'cb_date'})
    # Add extra columns required
    vp_frick = vp_frick.assign(bank='VP', has_tran_id=0)
    vp_frick['transaction_type'] = np.where(np.isin(vp_frick['Kind'], ['CBK1', 'CBK2']), 'CB', 'CB_R')
    # converting types
    vp_frick = vp_frick[~pd.isna(vp_frick['order_cc_key'])]

    # drop duplicate records based on Transaction ID, if any.
    vp_frick = vp_frick.sort_values('cb_date', ascending=True)
    vp_frick = vp_frick.drop_duplicates(subset=['order_cc_key', 'transaction_type'], keep='first')

    save_path = save_file_path + 'vp_frick/'
    rs.save_to_parquet(vp_frick, save_path, 'vp_frick_cb')
    return print('processed chargebacks from vp-frick parquet files')


def get_vp_stfs_chargebacks(load_bucket, load_prefix, save_file_path, vp_stfs_cb_schema):
    # VP-STFS/FRICK CB
    # in VP - STFS and FRICK Chargeback files, we do not have order ID information, so we need to create an order lookup
    # key based on credit card, trnasaction date, gateway descriptor and order amount.

    # VP-FRICK CB
    vp_stfs_cb_fields = list(vp_stfs_cb_schema.keys())

    vp_stfs = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'vp_stfs', vp_stfs_cb_fields)
    vp_stfs = vp_stfs[np.isin(vp_stfs['Kind'], ['CBK1', 'CBK2', 'REP', 'RRQ'])]

    vp_stfs['Posting_Date'] = pd.to_datetime(vp_stfs['Posting_Date'], infer_datetime_format=True).dt.date
    vp_stfs['Transaction_Date'] = pd.to_datetime(vp_stfs['Transaction_Date'], infer_datetime_format=True).dt.date
    vp_stfs = vp_stfs[pd.notna(vp_stfs['Merchant_Name'])]

    vp_stfs = vp_stfs.assign(order_cc_type='cc_order-date_gtw-desc')
    vp_stfs['order_cc_key_cc_6'] = vp_stfs['Card_No.'].str[:6]
    vp_stfs['order_cc_key_cc_4'] = vp_stfs['Card_No.'].str[12:16]
    vp_stfs['order_cc_key_order_date'] = vp_stfs['Transaction_Date'].astype('str').str.replace('-', '')
    # taking only part before phone number in descriptor
    vp_stfs['order_cc_key_gtw_desc'] = vp_stfs['Merchant_Name'].apply(str.upper).apply(lambda x: x.split("+")[0])
    # there are some gateway descriptors duplicated, below condition will fix it by taking name before .com and adding .com to it
    vp_stfs['order_cc_key_gtw_desc'] = vp_stfs['order_cc_key_gtw_desc'].str.split('.COM').str[0] + '.COM'
    vp_stfs['order_cc_key_order_amount'] = np.absolute(np.floor(vp_stfs['Netwk_Sett_Amt'].astype('double'))).astype(
        'int').astype('str')

    vp_stfs['order_cc_key'] = vp_stfs['order_cc_key_cc_6'] + '/' + vp_stfs['order_cc_key_cc_4'] + '/' + vp_stfs[
        'order_cc_key_order_date'] + \
                              '/' + vp_stfs['order_cc_key_gtw_desc']

    vp_stfs = vp_stfs.drop(columns=['Transaction_Date', 'Card_No.'])
    vp_stfs = vp_stfs.rename(columns={'Posting_Date': 'cb_date'})
    # Add extra columns required
    vp_stfs = vp_stfs.assign(bank='VP', has_tran_id=0)
    vp_stfs['transaction_type'] = np.where(np.isin(vp_stfs['Kind'], ['CBK1', 'CBK2']), 'CB', 'CB_R')
    # converting types
    vp_stfs = vp_stfs[~pd.isna(vp_stfs['order_cc_key'])]

    # drop duplicate records based on Transaction ID, if any.
    vp_stfs = vp_stfs.sort_values('cb_date', ascending=True)
    vp_stfs = vp_stfs.drop_duplicates(subset=['order_cc_key', 'transaction_type'], keep='first')

    save_path = save_file_path + 'vp_stfs/'
    rs.save_to_parquet(vp_stfs, save_path, 'vp_stfs_cb')
    return print('processed chargebacks from vp-stfs parquet files')


def get_vp_truevo_chargebacks(load_bucket, load_prefix, save_file_path, vp_truevo_cb_schema):
    # VP-TRUEVO CB
    # in VP - TRUEVO Chargeback files, we do not have order ID information, so we need to create an order lookup,
    # key based on credit card, trnasaction date and order amount.
    # we need to create for truevo separately from other VP files as we do not have gateway descriptor information in truevo file.
    vp_truevo_cb_fields = list(vp_truevo_cb_schema.keys())
    vp_truevo = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'vp_truevo', vp_truevo_cb_fields)

    vp_truevo['Date_Received'] = pd.to_datetime(vp_truevo['Date_Received'], infer_datetime_format=True).dt.date
    vp_truevo['TXN_Date'] = pd.to_datetime(vp_truevo['TXN_Date'], infer_datetime_format=True).dt.date
    # converting types
    vp_truevo = vp_truevo[~pd.isna(vp_truevo['Card_Number'])]

    vp_truevo = vp_truevo.assign(order_cc_type='cc_order-date')
    vp_truevo['order_cc_key_cc_6'] = vp_truevo['Card_Number'].str[:6]
    vp_truevo['order_cc_key_cc_4'] = vp_truevo['Card_Number'].str[12:16]
    vp_truevo['order_cc_key_order_date'] = vp_truevo['TXN_Date'].astype('str').str.replace('-', '')
    vp_truevo['order_cc_key_order_amount'] = np.floor(vp_truevo['Amount'].astype('double')).astype('int').astype('str')

    vp_truevo['order_cc_key'] = vp_truevo['order_cc_key_cc_6'] + '/' + vp_truevo['order_cc_key_cc_4'] + '/' + vp_truevo[
        'order_cc_key_order_date']

    vp_truevo = vp_truevo.drop(columns=['TXN_Date', 'Card_Number', 'Amount'])
    vp_truevo = vp_truevo.rename(columns={'Date_Received': 'cb_date'})
    # Add extra columns required
    vp_truevo = vp_truevo.assign(bank='VP', transaction_type='CB', has_tran_id=0, order_cc_key_gtw_desc=np.nan)

    # drop duplicate records based on Transaction ID, if any.
    vp_truevo = vp_truevo.sort_values('cb_date', ascending=True)
    vp_truevo = vp_truevo.drop_duplicates(subset=['order_cc_key'], keep='first')

    save_path = save_file_path + 'vp_truevo/'
    rs.save_to_parquet(vp_truevo, save_path, 'vp_truevo_cb')
    return print('processed chargebacks from vp-truevo parquet files')


def get_paynetics_chargebacks(load_bucket, load_prefix, save_file_path, paynetics_cb_schema):
    paynetics_cb_fields = list(paynetics_cb_schema.keys())

    # Paynetics Chargebacks
    paynetics_cb_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'paynetics',
                                                         paynetics_cb_fields)

    paynetics_cb_cmb['Report_Date'] = pd.to_datetime(paynetics_cb_cmb['Report_Date'], infer_datetime_format=True)
    paynetics_cb_cmb['transaction_type'] = np.where(paynetics_cb_cmb['Status'] == 'chargebacked', 'CB', 'CB_R')

    # Add extra columns required
    paynetics_cb_cmb = paynetics_cb_cmb.assign(bank='PAYNETICS', has_tran_id=1)
    # rename columns
    paynetics_cb_cmb = paynetics_cb_cmb.rename(
        columns={"Merchant_Transaction_ID": "transaction_id", "Report_Date": "cb_date"})
    # converting types
    paynetics_cb_cmb = paynetics_cb_cmb[~pd.isna(paynetics_cb_cmb['transaction_id'])]
    paynetics_cb_cmb['cb_date'] = paynetics_cb_cmb['cb_date'].dt.date
    # drop duplicate records based on Transaction ID, if any.
    paynetics_cb_cmb = paynetics_cb_cmb.sort_values('cb_date', ascending=True)
    paynetics_cb_cmb = paynetics_cb_cmb.drop_duplicates(subset=['transaction_id', 'transaction_type'], keep='first')

    save_path = save_file_path + 'paynetics/'
    rs.save_to_parquet(paynetics_cb_cmb, save_path, 'paynetics_cb')
    return print('processed chargebacks from paynetics parquet files')


def return_number(foo):
    """ convert values to int, if cannot then return None.
        it's useful when you only want to keep numbers, e.g. an empty string, or a string like 'hello'.
    """
    try:
        return int(foo)
    except ValueError:
        return 0


def get_concardis_chargebacks(load_bucket, load_prefix, save_file_path, concardis_cb_schema):
    concardis_cb_fields = list(concardis_cb_schema.keys())

    # concardis Chargebacks
    concardis_cb_cmb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'concardis',
                                                         concardis_cb_fields)

    # there are some blank rows, drop them
    concardis_cb_cmb = concardis_cb_cmb[pd.notna(concardis_cb_cmb['Transaktionsbetrag_Original-Transaktionsbetrag'])]
    concardis_cb_cmb = concardis_cb_cmb[concardis_cb_cmb['Transaktionsbetrag_Original-Transaktionsbetrag'] != '']
    concardis_cb_cmb = concardis_cb_cmb[concardis_cb_cmb['Eingangsdatum_der_Reklamation'] != '""']

    concardis_cb_cmb['cb_amount_temp'] = concardis_cb_cmb['Transaktionsbetrag_Original-Transaktionsbetrag'].str[
                                         :-4].str.replace('.', '').str.replace('"', '')
    concardis_cb_cmb['cb_amount_temp'] = concardis_cb_cmb['cb_amount_temp'].apply(return_number)
    concardis_cb_cmb = concardis_cb_cmb[pd.notna(concardis_cb_cmb['Handler_Ref_Number'])]
    concardis_cb_cmb['Transaktionsdatum'] = pd.to_datetime(concardis_cb_cmb['Transaktionsdatum'],
                                                           infer_datetime_format=True).dt.date

    concardis_cb_cmb = concardis_cb_cmb.assign(order_cc_type='cc_order-date_gtw-desc')
    concardis_cb_cmb['order_cc_key_cc_6'] = concardis_cb_cmb['Karteninhabernummer'].str[:6]
    concardis_cb_cmb['order_cc_key_cc_4'] = concardis_cb_cmb['Karteninhabernummer'].str[12:16]
    concardis_cb_cmb['order_cc_key_order_date'] = concardis_cb_cmb['Transaktionsdatum'].astype('str').str.replace('-',
                                                                                                                  '')
    concardis_cb_cmb['order_cc_key_gtw_desc'] = concardis_cb_cmb['Handler_Ref_Number'].apply(str.upper).apply(
        lambda x: x.split("+")[0])
    concardis_cb_cmb['order_cc_key_order_amount'] = concardis_cb_cmb['cb_amount_temp'].astype('str')

    concardis_cb_cmb['order_cc_key'] = concardis_cb_cmb['order_cc_key_cc_6'] + '/' + concardis_cb_cmb[
        'order_cc_key_cc_4'] + '/' + concardis_cb_cmb['order_cc_key_order_date'] + '/' + \
                                       '/' + concardis_cb_cmb['order_cc_key_gtw_desc']

    concardis_cb_cmb['Eingangsdatum_der_Reklamation'] = pd.to_datetime(
        concardis_cb_cmb['Eingangsdatum_der_Reklamation'], infer_datetime_format=True)
    concardis_cb_cmb['transaction_type'] = np.where(
        concardis_cb_cmb['Debit-/Creditindikator_der_Original-Transaktion'] == 'DR', 'CB', 'CB_R')

    # Add extra columns required
    concardis_cb_cmb = concardis_cb_cmb.assign(bank='CONCARDIS', has_tran_id=0)
    # rename columns
    concardis_cb_cmb = concardis_cb_cmb.rename(
        columns={"Eingangsdatum_der_Reklamation": "cb_date", 'Referenznummer_des_Acquirers': 'arn'})
    # converting types
    concardis_cb_cmb['cb_date'] = concardis_cb_cmb['cb_date'].dt.date
    # drop duplicate records based on order_cc_key, if any.
    concardis_cb_cmb = concardis_cb_cmb.sort_values('cb_date', ascending=True)
    concardis_cb_cmb = concardis_cb_cmb.drop_duplicates(subset=['order_cc_key', 'transaction_type'], keep='first')

    save_path = save_file_path + 'concardis/'
    rs.save_to_parquet(concardis_cb_cmb, save_path, 'concardis_cb')
    return print('processed chargebacks from concardis parquet files')


def get_nets_chargebacks(load_bucket, load_prefix, save_file_path, nets_cb_schema_v2):
    nets_cb_fields = list(nets_cb_schema_v2.keys())

    # concardis Chargebacks
    nets_cb = rs.read_all_parquet_from_S3_to_pd(load_bucket, load_prefix + 'nets', nets_cb_fields)

    # there are some blank rows, drop them
    nets_cb = nets_cb[nets_cb['TRX_AMT'] != 'Sep-95']
    nets_cb = nets_cb[pd.notna(nets_cb['TRX_AMT'].astype('double'))]
    nets_cb['TRX_DATE_TIME'] = pd.to_datetime(nets_cb['TRX_DATE_TIME'].str.replace('"', '').str.replace('+', ''),
                                              infer_datetime_format=True)
    nets_cb['BUSINESS_NAME'] = nets_cb['BUSINESS_NAME'].str.replace('"', '').str.replace(' ', '.').str.lower()
    nets_cb['cb_amount_temp'] = np.floor(nets_cb['TRX_AMT'].astype('double')).astype('int').astype('str')

    nets_cb = nets_cb.assign(order_cc_type='cc_order-date_gtw-desc')
    nets_cb['order_cc_key_cc_6'] = nets_cb['CARD_NUM_MASKED'].str[:6]
    nets_cb['order_cc_key_cc_4'] = nets_cb['CARD_NUM_MASKED'].str[12:16]
    nets_cb['order_cc_key_order_date'] = nets_cb['TRX_DATE_TIME'].astype('str').str[:10].str.replace('-', '')
    nets_cb['order_cc_key_gtw_desc'] = nets_cb['BUSINESS_NAME'].apply(str.upper)
    nets_cb['order_cc_key_order_amount'] = nets_cb['cb_amount_temp'].astype('str')

    nets_cb['order_cc_key'] = nets_cb['order_cc_key_cc_6'] + '/' + nets_cb['order_cc_key_cc_4'] + '/' + nets_cb[
        'order_cc_key_order_date'] \
                              + '/' + nets_cb['order_cc_key_gtw_desc']

    nets_cb['FILE_GENERATION_DATE'] = pd.to_datetime(
        nets_cb['FILE_GENERATION_DATE'].str.replace('"', '').str.replace('+', ''),
        infer_datetime_format=True).dt.date

    # Add extra columns required
    nets_cb = nets_cb.assign(bank='NETS', transaction_type='CB', has_tran_id=0)
    # rename columns
    nets_cb = nets_cb.rename(
        columns={"FILE_GENERATION_DATE": "cb_date"})

    # drop duplicate records based on order_cc_key, if any.
    nets_cb = nets_cb.sort_values('cb_date', ascending=True)
    nets_cb = nets_cb.drop_duplicates(subset=['order_cc_key', 'transaction_type'], keep='first')

    save_path = save_file_path + 'nets/'
    rs.save_to_parquet(nets_cb, save_path, 'nets_cb')
    return print('processed chargebacks from nets parquet files')


def read_bank_cb_files(bucket, load_prefix, cb_schema, banks):
    cb_fields = list(cb_schema.keys())
    df = pd.concat([rs.read_all_parquet_from_S3_to_pd(bucket, load_prefix + bank, cb_fields) for bank in banks],
                   sort=False, ignore_index=True)
    df = df.astype(cb_schema)
    return df


def merge_all_chargebacks(bucket, load_prefix, save_file_path, save_file_path_tsv, transaction_id_cb_schema, transaction_id_cb_schema_emp,
                          order_id_cb_schema, order_id_cb_schema_emp_epro, order_cc_cb_schema):
    # emp has both order_id and transaction ids
    cb_transaction_id = read_bank_cb_files(bucket, load_prefix, transaction_id_cb_schema, ['billpro', 'paynetics'])
    cb_transaction_id_emp = read_bank_cb_files(bucket, load_prefix, transaction_id_cb_schema, ['emp'])
    cb_transaction_id_emp = cb_transaction_id_emp[cb_transaction_id_emp['has_tran_id'] == '1']
    cb_transaction_id = pd.concat([cb_transaction_id, cb_transaction_id_emp], sort=False, ignore_index=True)

    cb_order_id = read_bank_cb_files(bucket, load_prefix, order_id_cb_schema,
                                     ['checkout', 'payvision/version_1', 'paysafe'])
    cb_order_id_emp_epro = read_bank_cb_files(bucket, load_prefix, order_id_cb_schema_emp_epro, ['emp', 'epro'])
    cb_order_id_emp_epro = cb_order_id_emp_epro[cb_order_id_emp_epro['has_tran_id'] == '0']
    cb_order_id = pd.concat([cb_order_id, cb_order_id_emp_epro], sort=False, ignore_index=True)

    cb_order_cc = read_bank_cb_files(bucket, load_prefix, order_cc_cb_schema,
                                     ['payvision/version_2', 'vp_frick', 'vp_stfs', 'vp_truevo', 'concardis', 'nets'])

    rs.save_to_parquet(cb_transaction_id, save_file_path+'transaction_id/', 'transaction_id')
    print('processed chargebacks from transaction id parquet files')
    rs.save_to_parquet(cb_order_id, save_file_path + 'order_id/', 'order_id')
    print('processed chargebacks from order_id parquet files')
    rs.save_to_parquet(cb_order_cc, save_file_path + 'order_cc/', 'order_cc')
    print('processed chargebacks from order_cc parquet files')

    rs.save_to_tsv(cb_transaction_id, save_file_path_tsv+'transaction_id/', 'transaction_id')
    print('processed chargebacks from transaction id tsv files')
    rs.save_to_tsv(cb_order_id, save_file_path_tsv + 'order_id/', 'order_id')
    print('processed chargebacks from order_id tsv files')
    rs.save_to_tsv(cb_order_cc, save_file_path_tsv + 'order_cc/', 'order_cc')
    print('processed chargebacks from order_cc tsv files')
    cb_transaction_id = pd.DataFrame()
    cb_transaction_id_emp = pd.DataFrame()
    cb_order_id = pd.DataFrame()
    cb_order_id_emp_epro = pd.DataFrame()
    cb_order_cc = pd.DataFrame()
    return print('success')


