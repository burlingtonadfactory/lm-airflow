checkout_cb_schema_v1 = {
    'Chargeback_ID':str,
    'Chargeback_Date_UTC':str,
    'Account_Name':str,
    'Business_Name':str,
    'Channel_Name':str,
    'Chargeback_Indicator':str,
    'Payment_Method':str,
    'Card_Holder_Name':str,
    'CC_Number':str,
    'Reference':str,
    'Currency':str,
    'Chargeback_Amount':str,
    'Chargeback_Code':str,
    'Customer_Email':str,
    'Payment_Type':str,
    'Payment_ID':str,
    'Payment_Date_UTC':str,
    'Acquiring_Reference_Number':str,
    'CVV_Check':str,
    'AVS_Check':str,
    'ECI_Code':str,
    'Card_Wallet_Type':str,
    'Chargeback_Date_Europe/London':str,
    'Payment_Date_Europe/London':str
}
checkout_cb_schema_v2 = {
    'Dispute_ID':str,
    'Dispute_Date_UTC':str,
    'Account_Name':str,
    'Business_Name':str,
    'Channel_Name':str,
    'Dispute_Indicator':str,
    'Payment_Method':str,
    'Card_Holder_Name':str,
    'CC_Number':str,
    'Reference':str,
    'Currency':str,
    'Dispute_Amount':str,
    'Dispute_Code':str,
    'Customer_Email':str,
    'Payment_Type':str,
    'Payment_ID':str,
    'Payment_Date_UTC':str,
    'Acquiring_Reference_Number':str,
    'CVV_Check':str,
    'AVS_Check':str,
    'ECI_Code':str,
    'Card_Wallet_Type':str,
    'Dispute_Date_Europe/London':str,
    'Payment_Date_Europe/London':str
}

paysafe_cb_schema = {
'Account':str,
'Credit_Card_Number':str,
'Transaction_Date':str,
'Posting_Date':str,
'CB_Posting_Amount':str,
'Original_Transaction_Amount':str,
'Original_Transaction_Currency':str,
'Transaction_ID':str,
'Name':str,
'Reason_Code':str,
'Reason_Description':str,
'ARN':str,
'Control_Number':str,
'Status':str,
'Record_ID':str,
'Merchant_Transaction_ID':str,
'Brand':str,
'Card_Present':str,
'Terminal_S/N':str,
'Card_Entry_Option':str
}

emp_cb_schema = {
    'Type':str,
    'Impact_Date':str,
    'Import_Date':str,
    'Account':str,
    'Account_ID':str,
    'MCC':str,
    'Transaction_ID':str,
    'Transaction_DateTime':str,
    'Merchant_DateTime':str,
    'Acquirer_DateTime':str,
    'Order_ID':str,
    'Trans_Type':str,
    'Authcode':str,
    'Card_Type':str,
    'Card_No':str,
    'Expiry_Date':str,
    'Cardholder_Name':str,
    'Card_Category':str,
    'Card_Sub_Category':str,
    'Card_Issuing_Bank':str,
    'Card_Country':str,
    'Region_Class':str,
    'Classification_Code':str,
    'Amount':str,
    'Curency':str,
    'Country':str,
    'Descriptor':str,
    'Email':str,
    'GTW':str,
    'Related':str,
    'Merchant_Ref':str,
    'Company_Name':str,
    'Website_URL':str,
    'Reason':str,
    'Unnamed:_34':str,
    'Source':str,
    'Chargeback_Reference_/_ARN':str,
    'Case_ID':str,
    'Sequence':str,
    'IP_Address':str
}

emp_six_manual_rev_schema = {
'Unnamed:_0':str,
'Statement_ID':str,
'Merchant_Name':str,
'Bank':str,
'Manual_Adjustment':str,
'ARN':str,
'Chargeback_date':str,
'Case_ID':str,
'Billing_Curr':str,
'Billing_Amount':str,
'Statement_Period_Start_Date_Time_UTC':str,
'Statement_Period_End_Date_Time_UTC':str
}

emp_wdl_manual_rev_schema = {
'ARN':str,
'Case_ID':str,
'Status':str,
'Trans._ID':str,
'Account':str,
'Company':str,
'Chargeback_Date':str,
'Trans._Curr.':str,
'Trans._Amount':str,
'Rate':str,
'Billing_Curr.':str,
'Billing_Amount':str,
'Statement_Period_Start_':str,
'Statement_Period_End_Date':str
}


emp_bns_manual_rev_schema = {
	'Qlik_Order_ID':str,
	'email':str,
	'Company':str,
	'Bank Name':str,
	'Order Date':str,
    'billing_cycle':str,
	'Refund Date':str,
	'ARN':str,
	'Case ID':str,
	'Type':str,
    'Reversal Date':str,
	'transaction_id':str,
	'Chargeback Date':str,
    'Days Chargebacked After refund':str,
	'Refund EUR':str,
	'CB EUR':str,
	'CB Fee EUR':str,
    'CB CC Fee EUR':str
}

epro_cb_schema = {
'Site':str,
'Reference':str,
'Transaction_ID':str,
'Transaction_status':str,
'IP_address':str,
'Amount':str,
'Date_chargebacked':str,
'Client_name':str,
'Client_Unique_ID':str,
'Client_Mail':str,
'Client_Phone':str,
'Client_Address':str,
'Client_Zip':str,
'Client_City':str,
'Client_Country':str,
'Client_Birth_date':str,
'Client_Birth_place':str,
'3DSecure_Transaction':str,
'OneClick_Transaction':str,
'Date':str,
'Amount_chargeback':str,
'Card_enrolled':str,
'Message':str,
'PAN':str,
'Card_country':str,
'Original_amount':str,
'Original_currency':str,
'KYC_Status':str,
'KYC_Request_Date':str,
'KYC_Closing_Date':str,
'Operation_Type':str,
'Date_representment':str,
'ARN':str,
'RC':str,
'Reason_Code':str,
'Scheme':str,
'Issuing_bank':str,
'Credit_card_level':str
}

billpro_cb_schema = {
	'TransactionID':str,
	'AccountID':str,
	'Description':str,
	'First_Name':str,
	'Last_Name':str,
    'Email':str,
	'Amount':str,
	'Created':str,
	'Descriptor':str
}

payvision_cb_schema_v1 = {
    "bank_code":str,
    "transaction_type":str,
    "order_id":str,
    "chargeback_date":str,
    "arn":str
}

payvision_cb_schema_v2 = {
	'CreateDate':str,
	'OriginalTransactionDate':str,
	'MethodType':str,
	'OriginalAmount':str,
    'CustomCode':str,
	'ARN':str,
	'FirstSixDigits':str,
	'LastFourDigits':str,
	'DynDescName':str
}

vp_frick_cb_schema = {
    'Doc._Received':str,
    'Case_ID/VROL':str,
    'Status':str,
    'External_Merchant_No.':str,
    'Client':str,
    'Central_Processing_Date':str,
    'Kind':str,
    'Ntwk':str,
    'Card_No.':str,
    'UTI':str,
    'ARN':str,
    'RC':str,
    'Reason_Code':str,
    'Dispute_Condition':str,
    'Case_Priority':str,
    'Netwk_Sett_Amt':str,
    'Netwk_Sett_Curr':str,
    'Local_Amt':str,
    'Local_Curr':str,
    'Work_By_Date':str,
    'C':str,
    'Due_Date':str,
    'Posting_Date':str,
    'CCN':str,
    'Assigned_To':str,
    'Merchant_Name':str,
    'Capture_Method':str,
    'Visa_Case_Number':str,
    'Last_Merchant_Action':str,
    'User_Merchant_Action':str,
    'Custom_Data':str,
    'Transaction_Date':str,
    'Merch_Tran_Ref.':str,
    'Merchant_Funding_Currency':str,
    'Merch_Funding_Amt_Gr':str,
    'E-Wallet_Type':str
}

vp_stfs_cb_schema = {
    'Doc._Received':str,
    'Case_ID/VROL':str,
    'Status':str,
    'External_Merchant_No.':str,
    'Client':str,
    'Central_Processing_Date':str,
    'Kind':str,
    'Ntwk':str,
    'Card_No.':str,
    'UTI':str,
    'ARN':str,
    'RC':str,
    'Reason_Code':str,
    'Dispute_Condition':str,
    'Case_Priority':str,
    'Netwk_Sett_Amt':str,
    'Netwk_Sett_Curr':str,
    'Local_Amt':str,
    'Local_Curr':str,
    'Work_By_Date':str,
    'C':str,
    'Due_Date':str,
    'Posting_Date':str,
    'CCN':str,
    'Assigned_To':str,
    'Merchant_Name':str,
    'Capture_Method':str,
    'Visa_Case_Number':str,
    'Last_Merchant_Action':str,
    'User_Merchant_Action':str,
    'Custom_Data':str,
    'Transaction_Date':str,
    'Merch_Tran_Ref.':str,
    'Merchant_Funding_Currency':str,
    'Merch_Funding_Amt_Gr':str,
    'E-Wallet_Type':str
}

vp_truevo_cb_schema = {
	'Time_Difference':str,
	'Date_Received':str,
	'TXN_Date':str,
	'Card_Number':str,
	'Amount':str,
    'Currency':str
}

paynetics_cb_schema = {
    'Report_Date':str,
    'Trx_Date':str,
    'Merchant':str,
    'Terminal':str,
    'Report_Type':str,
    'Transition':str,
    'Report_Message':str,
    'Report_Info':str,
    'Status':str,
    'Merchant_Transaction_ID':str,
    'Unique_ID':str,
    'Reference_Transaction_ID':str,
    'Reference_Transaction_Type':str,
    'Currency':str,
    'Amount__in_minor_currency_unit_':str,
    'Card_Holder':str,
    'Card_Brand':str,
    'Card_Number':str,
    'Email':str,
    'Country':str,
    'Remote_IP':str
}

concardis_cb_schema = {
	'Händlernummer':str,
	'Händlername':str,
	'Kurzbezeichnung':str,
	'GP-Nummer':str,
	'Händler_MCC':str,
	'Terminal_ID':str,
	'Vorgangsnummer':str,
	'Karteninhabernummer':str,
	'Karteninhaber-Land__Issuer_Land_':str,
	'Referenznummer_des_Acquirers':str,
	'Authentifizierungscode_Genehmigungsnummer':str,
	'Mastercard_Secure':str,
	'Verified_by_Visa':str,
	'POS':str,
	'Handler_Ref_Number':str,
	'Eingangsdatum_der_Reklamation':str,
	'Reason_Code':str,
	'erster_Lifecycle_Status':str,
	'aktueller_Lifecycle_Status':str,
	'Datum_der_letzten_Aktion':str,
	'Debit-/Creditindikator_der_Original-Transaktion':str,
	'Transaktionsdatum':str,
	'Transaktionsbetrag_Original-Transaktionsbetrag':str,
	'Transaktionsbetrag_Originalwährung':str
}

nets_cb_schema_v2 = {
	'TRX_DATE_TIME':str,
	'BUSINESS_NAME':str,
	'MERCHANT_NUM':str,
    'MERCHANT_TERMINAL_ID':str,
	'MERCHANT_CATEGORY_CODE':str,
	'TRX_AMT':str,
    'TRX_CURR_CODE':str,
	'TRX_AMT_EURO':str,
	'CARD_NUM_MASKED':str,
	'ISSUER_COUNTRY':str,
    'CARD_ORG_CODE':str,
	'CARD_PRODUCT':str,
	'MEMBER_NUM':str,
	'APPROVAL_CODE':str,
    'CARD_DATA_INPUT_MODE':str,
	'ACQ_RETRIEVAL_REF_NUM':str,
	'RETRIEVAL_REF_NUM':str,
    'FILE_GENERATION_DATE':str
}

# transaction id
# billpro, paynetics, some of emp
transaction_id_cb_schema = {
    'transaction_id': str,
    'transaction_type': str,
    'bank': str,
    'cb_date': str,
    'has_tran_id':str
}

transaction_id_cb_schema_emp = {
    'transaction_id': str,
    'transaction_type': str,
    'bank': str,
    'cb_date': str,
    'has_tran_id':str,
    'order_date':str
}

# order ids
# checkout, paysafe, epro, payvision_v1, some of emp
order_id_cb_schema = {
    'order_id': str,
    'transaction_type': str,
    'bank': str,
    'cb_date': str,
    'has_tran_id':str
}

order_id_cb_schema_emp_epro = {
    'order_id': str,
    'transaction_type': str,
    'bank': str,
    'cb_date': str,
    'has_tran_id':str,
    'order_date':str
}

# order cc key
# payvision_v2, vp_frick, vp_stfs, vp_truevo, concardis, nets
order_cc_cb_schema = {
    "order_cc_key":str,
    "transaction_type":str,
    "bank":str,
    "cb_date":str,
    'has_tran_id':str,
    'order_cc_type':str,
    'order_cc_key_cc_6':str,
    'order_cc_key_cc_4':str,
    'order_cc_key_order_date':str,
    'order_cc_key_gtw_desc':str,
    'order_cc_key_order_amount':str
}